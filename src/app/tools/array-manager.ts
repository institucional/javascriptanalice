export class ArrayManager {
    public static ADD = 1;
    public static MODIFY = 2;
    public static REMOVE = 3;
    public static PREPEND = 4;

    constructor() { }

    /**
   * METODO PARA AGREGAR UN NUEVO USUARIO A LA TABLA DE ASOCIADOS:
   * AUTOR: FREDI ROMAN
   */
    public static alterArrayData(array: any[], updatedData: any, option: number = this.ADD) {
        if (array) {
            let index;
            switch (option) {
                case this.PREPEND:
                    array.splice(0, 0, updatedData);
                    break;
                case this.ADD:
                    array.push(updatedData);
                    break;
                case this.MODIFY:
                    index = array.findIndex(data => data.id == updatedData.id);
                    array.splice(index, 1, updatedData);
                    break;
                case this.REMOVE:
                    index = array.findIndex(data => data.id == updatedData.id);
                    array.splice(index, 1);
                    break;
            }
        }
        return array;
    }
}