/**
 * AUTOR: FREDI ROMAN
 * REF: https://codepen.io/Jvsierra/pen/BNbEjW https://gist.github.com/gordonbrander/2230317
 */
export class UUIDManager {
    constructor() { }

    /**
     * METODO PARA GENERAR UN BLOQUE DE 4 CARACTERES RANDOMICOS
     * AUTOR: FREDI ROMAN
     */
    private static block4() {
        return Math.floor((1 + parseFloat("0." + Date.now()) + Math.random()) * 0x10000).toString(16).substring(1);
    }

    /**
     * METODO PARA GENERAR UN UNIVERSAL USER ID
     * AUTOR: FREDI ROMAN
     */
    public static generateUUID() {
        return this.block4() + this.block4() + '-' + this.block4() + '-' + this.block4() + '-' +
            this.block4() + '-' + this.block4() + this.block4() + this.block4();
    }
}