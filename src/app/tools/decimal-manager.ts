export class DecimalManager {
    constructor() { }

    /**
     * METODO PARA REDONDEAR UN NUMERO A 2 DECIMALES:
     * AUTOR: FREDI ROMAN
     * REF: https://www.w3resource.com/javascript-exercises/javascript-math-exercise-14.php
     * @param num 
     * @param dec 
     */
    public static precise_round(num, dec) {
        if (num !== '0.00' && ((typeof num !== 'number') || (typeof dec !== 'number')))
            return false;

        var num_sign = num >= 0 ? 1 : -1;
        return (Math.round((num * Math.pow(10, dec)) + (num_sign * 0.0001)) / Math.pow(10, dec)).toFixed(dec);
    }
}