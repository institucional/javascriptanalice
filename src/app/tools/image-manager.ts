/**
 * AUTOR: FREDI ROMAN
 */
export class ImageManager {

    constructor() { }

    /**
   * METODO PARA OBTENER UNA IMAGEN EN BASE64 A PARTIR DE UN URL
   * AUTOR: FREDI ROMAN
   * REF: https://turbofuture.com/computers/HTML5-Tutorial-How-To-Use-Canvas-toDataURL
   * @param customImg 
   */
    public static getBase64FromImgPath(customImg: string, imgWidth: number, imgHeight: number) {
        let canvas: any = document.getElementById('dynaCanvas');
        let ctx = canvas.getContext('2d');
        let image = new Image();
        image.src = customImg;
        image.width = imgWidth;
        image.height = imgHeight;
        let promise = new Promise((resolve, reject) => {
            image.onload = () => {
                canvas.width = image.width;
                canvas.height = image.height;
                ctx.drawImage(image, 0, 0, image.width, image.height);
                resolve(canvas.toDataURL());
            };
        });
        return promise;
    }
}