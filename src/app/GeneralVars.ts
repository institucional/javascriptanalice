export const
  GV = {
    "state": {
      "active": "21",
      "inactive": "22"
    },
    "editSave": {
      "edit": "E",
      "save": "S"
    },
    "afirmation": {
      "si": "23",
      "no": "24"
    },
    "catalogos": {
      "estados": "31",
      "afirmacion": "33",
      "tipoCatalogo": "59",
      "tipoReporte": "34",
      "periodoFacturacion": "39",
      "facturaImpresa": "40",
      "formasRegistro": "37",
      "tipoFacturación": "38",
      "tipoServicio": "43",
      "operadoras": "50",
      "tiposIdentificacion": "55",
      "calificacion": "44",
      "motivo": "47",
      "tipoempresa": "48",
      "ciudad": "5",
      "sector": "49",
      "estadosCarreras": "62",
      "motivoCancelacion": "63",
      "formasPago": "56",
      "tiposTarjetas": "58",
      "inconvenientes": "64",
      "tiempoEspera": "66",
      "ciudadFast": "67",
      'estadosrechazo': '72'
    },
    "tiposServicios": {
      "voucherManual": "39",
      "voucherElectronico": "40"
    },
    "estadosCarreras": {
      "pendiente": "109",
      "activa": "110",
      "cancelada": "111",
      "finalizada": "112"
    },
    estadosCarrerasRep: {
      "109": "Pendiente",
      "110": "Activa",
      "111": "Cancelada",
      "112": "Finalizada"
    },
    "tiposIdentificacion": {
      "cedula": "CÉDULA",
      "ruc": "RUC",
      "pasaporte": "PASAPORTE"
    },
    "formasPago": {
      "efectivo": "91",
      "voucherElectronico": "92",
      "voucherManual": "93",
      "tarjeta": "94"
    },
    formasPagoCat: {
      "91": "Efectivo",
      "92": "Voucher Electrónico",
      "93": "Voucher Manual",
      "94": "Tarjeta"
    },
    "solicitarTaxi": {
      "tiempoSolicitud": 5000
    },
    "ciudadesFastline": {
      "quito": "150",
      "guayaquil": "151",
      "quevedo": "152"
    },
    ciudadesServicios: {
      "150": "Quito",
      "151": "Guayaquil",
    },
    controlConvenios: {
      190: "Por valor",
      191: "Por número de servicios"
    },
    catClaseContacto: {
      contactoFastline: 163,
      responsableCliente: 164,
    },
    tipoResponsable: 69,
    subCatTipoResposable: {
      repreConvenio: 161,
      respCartera: 162,
      respPresup: 180,
      respPoliticaTaxi: 189
    },
    scTipoResposablePresup: {
      respPresup: 180,
      respPoliticaTaxi: 189
    },
    grupoFastLine: 71,
    autoGenPresupuesto: {
      0: "Para esta unidad en el periodo del convenio",
      1: "Para todo el nivel en el periodo del convenio"
    },
    autoGenPresupValues: {
      autoPresupByUnit: 0,
      autoPresupForAllUnits: 1
    },
    autoGenFeriados: {
      0: "Feriado Nacional",
      1: "Feriado Local"
    },
    autoFeriadosValues: {
      nacional: 0,
      local: 1
    },
    tiposDatoCatalogo: 178,
    tiposTablaUsuario: 103,
    tiposDato: 73,
    catalogTipoControlPresup: 74,
    estadosrechazo: {
      empresa: 168,
      convenio: 169
    },
    estadopreautorizados: {
      autorizar: 195,
      rechazado: 196,
      autorizado: 197,
      cancelado: 198
    },
    origenservicio: {
      and: 203,
      web: 204,
      mov: 205
    },
    tipoEmpresaProvServ: 202,
    urlsfotos: {
      quito: 'http://192.168.41.112/fotos/',
      guayaquil: 'http://192.168.40.112/fotos/'
    }
  };
