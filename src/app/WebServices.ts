import { GV } from './GeneralVars';

export const
  WS = {
    'general': {
      'status': 'api/v1/catalogos/' + GV.catalogos.estados + '/subcatalogos/by-estado?estado=' + GV.state.active,
      'afirmation': 'api/v1/catalogos/' + GV.catalogos.afirmacion + '/subcatalogos/by-estado?estado=' + GV.state.active,
      'cities': 'api/v1/catalogos/' + GV.catalogos.ciudad + '/subcatalogos/by-estado?estado=' + GV.state.active
    },
    'login': {
      'oauth': '/oauth/token',
      'register': '/api/register/email/${ban}',
      'carteraVencida': 'api/register/carteravencida?celular=${celular}',
      'getCorreo': 'api/register/getEmail?celular=${celular}',
      'getPassword': 'api/register/getPassword?correo=${correo}',
      'sendPin': 'api/register/sendpinlogin/${cell}',
      'validatePin': 'api/register/validatepinlogin/${cell}/${pin}/${correo}/${tipoid}/${identificacion}'
    },
    'catalog': {
      'type': 'api/v1/catalogos/' + GV.catalogos.tipoCatalogo + '/subcatalogos/by-estado?estado=' + GV.state.active,
      'fetch': 'api/v1/catalogos?page=${pageNumber}&size=${size}&sort=codCatalogo',
      'update': 'api/v1/catalogos/${idCatalogo}',
      'save': 'api/v1/catalogos'
    },
    'subcatalog': {
      'update': 'api/v1/catalogos/${idCatalogo}/subcatalogos/${idSubcatalogo}',
      'save': 'api/v1/catalogos/${idCatalogo}/subcatalogos',
      'fetch': 'api/v1/catalogos/${idCatalogo}/subcatalogos?page=${pageNumber}&size=${size}&sort=codSubcatalogo'
    },
    'characteristics': {
      'typeReports': 'api/v1/catalogos/' + GV.catalogos.tipoReporte + '/subcatalogos/by-estado?estado=' + GV.state.active,
      'facPeriod': 'api/v1/catalogos/' + GV.catalogos.periodoFacturacion + '/subcatalogos/by-estado?estado=' + GV.state.active,
      'facPrint': 'api/v1/catalogos/' + GV.catalogos.facturaImpresa + '/subcatalogos/by-estado?estado=' + GV.state.active,
      'registerForm': 'api/v1/catalogos/' + GV.catalogos.formasRegistro + '/subcatalogos/by-estado?estado=' + GV.state.active,
      'typeFact': 'api/v1/catalogos/' + GV.catalogos.tipoFacturación + '/subcatalogos/by-estado?estado=' + GV.state.active,
      'fetch': 'api/v1/convenios?page=${pageNumber}&size=${size}&sort=codConvenio',
      'fetchByEnterpriseId': 'api/v1/empresasPage/${enterpriseId}/convenios?page=${pageNumber}&size=${size}&sort=fechaVencimiento',
      'update': 'api/v1/convenios/${idConvenio}',
      'inactive': 'api/v1/convenios/inactive',
      'exist': 'api/v1/convenios/exist',
      'save': 'api/v1/convenios'
    },
    'empresa': {
      'empByState': 'api/v1/empresas/by-estado?estado=' + GV.state.active,
      'qualifications': 'api/v1/catalogos/' + GV.catalogos.calificacion + '/subcatalogos/by-estado?estado=' + GV.state.active,
      'motives': 'api/v1/catalogos/' + GV.catalogos.motivo + '/subcatalogos/by-estado?estado=' + GV.state.active,
      'companyTypes': 'api/v1/catalogos/' + GV.catalogos.tipoempresa + '/subcatalogos/by-estado?estado=' + GV.state.active,
      'sectors': 'api/v1/catalogos/' + GV.catalogos.sector + '/subcatalogos/by-estado?estado=' + GV.state.active,
      'fetch': 'api/v1/empresas?page=${pageNumber}&size=${size}&sort=Id',
      'save': 'api/v1/empresas',
      'update': 'api/v1/empresas/${empresaId}',
      'getEmpresaRuc': 'api/v1/empresas/${ruc}'
    },
    'services': {
      'services': 'api/v1/catalogos/' + GV.catalogos.tipoServicio + '/subcatalogos/by-estado?estado=' + GV.state.active,
      'fetch': 'api/v1/servicios/convenio?idConvenio=${idConvenio}&page=${pageNumber}&size=${size}&sort=Id',
      'update': 'api/v1/servicios/update/${idServicio}',
      'save': 'api/v1/servicios/${idServicio}'
    },
    'employees': {
      'operators': 'api/v1/catalogos/' + GV.catalogos.operadoras + '/subcatalogos/by-estado?estado=' + GV.state.active,
      'identificationTypes': 'api/v1/catalogos/' + GV.catalogos.tiposIdentificacion + '/subcatalogos/by-estado?estado=' + GV.state.active,
      'identTypesLogin': 'api/andrea/login/' + GV.catalogos.tiposIdentificacion + '/subcatalogos/by-estado?estado=' + GV.state.active,
      'fetch': 'api/v1/empleados?page=${pageNumber}&size=${size}&sort=codEmpleado',
      'fetchByEnterpriseId': 'api/v1/empresasPage/${enterpriseId}/empleados?page=${pageNumber}&size=${size}&sort=primerApellido',
      'save': 'api/v1/empleados',
      'update': 'api/v1/empleados/${empleadoId}',
      'getData': 'api/v1/empleados/${celular}'
    },
    'sucursales': {
      'sucByState': 'api/v1/sucursales/by-estado?estado=' + GV.state.active,
      'sucByStateCompany': 'api/v1/sucursales/estadoempresa?estado=' + GV.state.active + '&empresa=${empresa}',
      'fetch': 'api/v1/sucursales?page=${pageNumber}&size=${size}&sort=Id',
      'fetchByEnterpriseId': 'api/v1/empresasPage/${enterpriseId}/sucursales?page=${pageNumber}&size=${size}&sort=nomSucursal',
      'save': 'api/v1/sucursales',
      'update': 'api/v1/sucursales/${sucursalId}',
      'getCodSucursal': 'api/v1/sucursales/${codigosucursal}'
    },
    'centroCostos': {
      'ctroByState': 'api/v1/centrocostos/by-estado?estado=' + GV.state.active,
      'ctroByStateCompany': 'api/v1/centrocostos/estadoempresa?estado=' + GV.state.active + '&empresa=${empresa}',
      'fetch': 'api/v1/centrocostos?page=${pageNumber}&size=${size}&sort=codCentroCosto',
      'fetchByEnterpriseId': 'api/v1/empresasPage/${enterpriseId}/areas?page=${pageNumber}&size=${size}&sort=nombre',
      'save': 'api/v1/centrocostos',
      'update': 'api/v1/centrocostos/${idCentroCosto}',
    },
    'geo': {
      'coordsToStreets': '/api/andrea/servicio/coordenada/${lat}/${long}',
      'validateStreet': '/api/andrea/servicio/validstreet/${street}/${ciudad}',
      'findStreet': '/api/andrea/servicio/findstreet/${street}/${ciudad}',
      'crossStreet': '/api/andrea/servicio/cruce/${street1}/${street2}/${ciudad}',
      'findStreetsCom': '/api/andrea/servicio/findstreetscom/${street}/${ciudad}'
    },
    'usuario': {
      'data': '/api/v1/usuariosid/${id}',
      'get': 'api/v1/usuario/${usuarioId}',
      'update': 'api/v1/usuario',
      confirmPwd: 'api/v1/usuarios/confirmData'
    },
    'formasPago': {
      'formas': 'api/v1/catalogos/' + GV.catalogos.formasPago + '/subcatalogos/by-estado?estado=' + GV.state.active,
      'listaformas': 'api/andrea/servicio/formaspago/${celular}',
      'fetch': 'api/v1/formaspago/${idUser}?page=${pageNumber}&size=${size}&sort=formapago',
      'get': 'api/v1/formaspago/${idUser}',
      'getActive': 'api/v1/formaspagoactivo/${idUser}',
      'getid': 'api/v1/formaspagoId/${id}',
      'save': 'api/v1/formaspago/${idUser}/${cvc}/${email}',
      'tiposTarjetas': 'api/v1/catalogos/' + GV.catalogos.tiposTarjetas + '/subcatalogos/by-estado?estado=' + GV.state.active,
      'update': 'api/v1/formaspago/${formaPagoId}/${idUser}',
      'delete': 'api/v1/deleteformapago/${formaPagoId}/${userId}/${token}'
    },
    'inconvenietes': {
      'inconveniences': 'api/v1/catalogos/' + GV.catalogos.inconvenientes + '/subcatalogos/by-estado?estado=' + GV.state.active,
      'subinconveniente': 'api/v1/catalogos/${codigo}/subinconvenientes/by-estado?estado=' + GV.state.active,
    },
    'solicitarTaxi': {
      'addsw': 'api/andrea/servicio/addsw/${voucher}/${idRechazado}',
      'addswtar': 'api/andrea/servicio/addswtar/${uid}/${voucher}/${idRechazado}',
      'getUnity': 'api/andrea/servicio/${autorizacion}/${ciudad}',
      'tiempoEspera': 'api/v1/catalogos/' + GV.catalogos.tiempoEspera + '/subcatalogos/by-estado?estado=' + GV.state.active,
      'getRuta': 'api/andrea/servicio/ruta/${uid}/${autorizacion}',
      'getCity': 'api/andrea/city/${latitud}/${longitud}',
      'asociados': 'api/v1/usuarios/${userId}/asociados/active',
      'getPin': 'api/andrea/pines/${autorizacion}/0',
      'validate': 'api/andrea/validaciones/${celular}/${formapago}',
      'updtRejected': '/api/andrea/validaciones/${rechazadoId}/${autorizacion}',
      'preautorizar': 'api/andrea/validaciones/preutorizar/${origenserv}'
    },
    'travels': {
      'canceltravel': 'api/andrea/servicio/canceltravel/${autorizacion}',
      'fetchActivas': 'api/v1/viajes/activos?idUser=${idUser}',
      'fetchReservas': 'api/v1/viajes/reservas?idUser=${idUser}',
      'fetchAnteriores': 'api/v1/viajes/anteriores?idUser=${idUser}',
      'unidad': 'api/v1/unidad/${codigo}/${ciudad}',
      'saveCalification': 'api/v1/calificaviaje/${autorizacion}',
      'getCalification': 'api/v1/calificaviaje/${idServicio}',
      'ultimoViaje': 'api/v1/viajes/ultimo?idUser=${idUser}',
      'finViaje': 'api/v1/viajes/emailfv'
    },
    asociados: {
      get: 'api/v1/usuarios/${userId}/asociados',
      post: 'api/v1/usuarios/${userId}/asociados',
      activate_deactivate: 'api/v1/usuarios/${userId}/asociados/${assocUserId}',
      getUsersByFilter: 'api/v1/usuariosfilter/${userId}?filterPattern=${filter}'
    },
    'secuenciaVoucher': {
      'fetch': 'api/v1/secuencias?page=${pageNumber}&size=${size}&sort=valorMinimo',
      'save': 'api/v1/secuencias',
      'get': 'api/v1/secuencias/${secuenciaId}',
      'fetchEmp': 'api/v1/secuenciasemp/${empId}?page=${pageNumber}&size=${size}&sort=id',
    },
    'favoritos': {
      'get': 'api/v1/favoritos/${idUsuario}?page=${pageNumber}&size=${size}&sort=id',
      'save': 'api/v1/favoritos/${idusuario}/${param}',
      'delete': 'api/v1/deletefavorito/${favoritoId}'
    },
    cargos: {
      fetch: 'api/v1/cargos?page=${pageNumber}&size=${size}&sort=nombre',
      fetchByDepaId: 'api/v1/departamentos/${depaId}/cargos?page=${pageNumber}&size=${size}&sort=nombre',
      get: 'api/v1/departamentos/${depaId}/cargos/activos',
      post: 'api/v1/departamentos/${depaId}/cargos',
      put: 'api/v1/departamentos/${depaId}/cargos/${cargoId}?active=${isActive}',
    },
    presupuesto: {
      getActiveConvenio: 'api/v1/empresas/${enterpriseId}/convenios/activo',
      fetch: 'api/v1/convenios/${convenioId}/sucursales/${sucId}/presupuestos?page=${pageNumber}&size=${size}',
      post: 'api/v1/convenios/${convenioId}/presupuestos',
      put: 'api/v1/convenios/${convenioId}/presupuestos/${presupuestoId}?active=${isActive}',
      getGeneralDetail: 'api/v1/detallePresupuestos',
      autoPresupByUnit: 'api/v1/convenios/${convenioId}/presupuestosAuto',
      autoPresupForAllUnits: 'api/v1/convenios/${convenioId}/presupuestosAutoAll',
      presupListBy: 'api/v1/convenios/${convenioId}/sucursales/${sucId}/departamentos/${depaId}/presupuestos'
    },
    responsableContacto: {
      fetchContactoByConv: 'api/v1/convenios/${convId}/contactos',
      fetchResponsableByConv: 'api/v1/convenios/${convId}/responsables',
      presupFetch: 'api/v1/sucursales/${sucId}/convenios/${convId}/responsables?presupId=${presupId}',
      convenioFetch: 'api/v1/convenios/activos',
      postPut: 'api/v1/presupuestos/responsableContactos',
    },
    feriado: {
      fetch: 'api/v1/sucursales/${sucursalId}/feriados?page=${pageNumber}&size=${size}&sort=fechaFeriado',
      post: 'api/v1/sucursales/${sucursalId}/feriados',
      put: 'api/v1/sucursales/${sucursalId}/feriados/${feriadoId}?active=${isActive}',
      del: 'api/v1/sucursales/${sucursalId}/feriados/${feriadoId}',
      autoFeriadosForAllEnter: 'api/v1/sucursales/feriadosAuto'
    },
    usuarioEmpresa: {
      get: 'api/v1/usuarios/${userId}/usuariosEmpresa',
      post: 'api/v1/empresas/${enterpriseId}/usuariosEmpresa?usuarioId=${userId}',
      del: 'api/v1/empresas/${enterpriseId}/usuariosEmpresa/${userEnterId}',
      getUsersByFilter: 'api/v1/usuariosfilter?page=${pageNumber}&size=${size}&filterPattern=${pattern}&activeInactive=${isActive}',
      getEmpresasToAsoc: 'api/v1/usuarios/${userId}/empresasToAsoc?page=${pageNumber}&size=${size}&sort=nombreEmpresa'
    },
    reportes: {
      getRequestedServicesReport: 'api/v1/empresas/reportes/servicios',
      filterEmpoyeeByEnterId: 'api/v1/empresas/${enterpriseId}/sucursales/${sucId}/empleados?filterPattern=${filter}',
      getMovilDetail: 'api/v1/reportes/movilDetail/${numAutorizacion}',
      getReporteConsumo: 'api/v1/empresas/reportes/consumo'
    },
    nivel: {
      get: 'api/v1/empresas/${enterpriseId}/niveles',
      post: 'api/v1/empresas/${enterpriseId}/niveles',
      put: 'api/v1/empresas/${enterpriseId}/niveles/${nivelId}?active=${isActive}',
      delete: 'api/v1/niveles/${nivelId}',
    },
    departamento: {
      fetchByNivelId: 'api/v1/niveles/${nivelId}/departamentos?page=${pageNumber}&size=${size}',
      post: 'api/v1/niveles/${nivelId}/departamentos',
      put: 'api/v1/niveles/${nivelId}/departamentos/${depaId}?active=${isActive}',
      get: 'api/v1/niveles/${nivelId}/departamentos/activos',
      getByParentDepaId: 'api/v1/departamentos/${parentDepaId}/subdepartamentos',
      getDepasNivelsByEmpId: 'api/v1/empresas/${empId}/niveles/departamentos'
    },
    empleadosSucursal: {
      operators: 'api/v1/catalogos/' + GV.catalogos.operadoras + '/subcatalogos/by-estado?estado=' + GV.state.active,
      identificationTypes: 'api/v1/catalogos/' + GV.catalogos.tiposIdentificacion + '/subcatalogos/by-estado?estado=' + GV.state.active,
      postPut: 'api/v1/sucursales/${sucId}/empleadosSuc?autoAcc=${autoAcc}',
      getByFilter: 'api/v1/sucursales/empleados'
    },
    findSubCatByCatId: 'api/v1/catalogos/${catId}/subcatalogos/by-estado?estado=' + GV.state.active,
    dynaParams: {
      get: 'api/v1/empresas/${enterpriseId}/dynaParams',
      post: 'api/v1/empresas/${enterpriseId}/dynaParams',
      getCatalogosByTipoTabla: 'api/v1/tipoTabla/${tipoTabla}/catalogos'
    },
    jerarquiaDepa: 'api/v1/empresas/${enterId}/jerarquiaDepa',
    getEmpresasProveServicio: 'api/v1/empresas/proveServicio',
    preautorizados: {
      getList: 'api/v1/preautorizados/${idAutorizador}/${estadoservicio}/${param}',
      updateservice: 'api/v1/preautorizados/${preautorizadosId}/${estadoservicio}/${observacion}'
    }
  };
