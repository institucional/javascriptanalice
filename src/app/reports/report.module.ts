import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportRoutingModule } from './report-routing.module';
import { FastlineReportComponent } from './fastline-report/fastline-report.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EmpresaService } from '../services/component-services/empresa.service';
import { CoorpReportComponent } from './coorp-report/coorp-report.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { XlsReportService } from './services/xls-report.service';
import { PdfReportService } from './services/pdf-report.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EmpleadoService } from '../services/component-services/empleado.service';
import { CargoService } from '../services/component-services/cargo.service';
import { EmpresaControllerService } from '../services/controllers/empresa-controller.service';
import { UserService } from '../services/component-services/user.service';
import { AdminModule } from '../+admin/admin.module';
import { ReportService } from './services/report.service';
import { DynamicFormModule } from '../dynamic-form/dynamic-form.module';
import { FrSideMenuModule } from '../fr-side-menu/fr-side-menu.module';
import { MenuConsultaComponent } from './components/menu-consulta/menu-consulta.component';
import { GenericService } from './services/generic.service';
import { TableResponsiveService } from '../services/table/table-responsive.service';
import { MovilDetailComponent } from './components/movil-detail/movil-detail.component';
import { AutorizacionDetailComponent } from './components/autorizacion-detail/autorizacion-detail.component';
import { ChartPanelComponent } from './chart-panel/chart-panel.component';
import { FrDataTableModule } from '../fr-data-table/fr-data-table.module';
import { ChartReportViewComponent } from './components/chart-report-view/chart-report-view.component';

@NgModule({
  imports: [
    CommonModule,
    ReportRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    NgbModule,
    AdminModule,
    DynamicFormModule,
    FrSideMenuModule,
    FrDataTableModule
  ],
  providers: [
    EmpresaService,
    XlsReportService,
    PdfReportService,
    EmpleadoService,
    CargoService,
    EmpresaControllerService,
    UserService,
    ReportService,
    GenericService,
    TableResponsiveService
  ],
  declarations: [
    FastlineReportComponent,
    CoorpReportComponent,
    MenuConsultaComponent,
    MovilDetailComponent,
    AutorizacionDetailComponent,
    ChartPanelComponent,
    ChartReportViewComponent
  ]
})
export class ReportModule { }
