import { NgModule } from '@angular/core';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AuthGuard } from '../services/auth/auth.guard';
import { FastlineReportComponent } from './fastline-report/fastline-report.component';
import { CoorpReportComponent } from './coorp-report/coorp-report.component';
import { ChartPanelComponent } from './chart-panel/chart-panel.component';

@NgModule({
  imports: [RouterModule.forChild([
    { path: 'fastline', component: FastlineReportComponent, canActivate: [AuthGuard] },
    { path: 'coorp', component: CoorpReportComponent, canActivate: [AuthGuard] },
    { path: 'graphic-panel', component: ChartPanelComponent, canActivate: [AuthGuard] }
  ])],
  exports: [RouterModule],
  providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }],
  declarations: []
})
export class ReportRoutingModule { }
