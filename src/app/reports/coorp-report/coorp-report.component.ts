/**
 * AUTOR: FREDI ROMAN
 */
import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { FunctionService } from '../../services/functions/function.service';
import { TAGS } from '../../TextLabels';
import { GV } from '../../GeneralVars';
import { WS } from '../../WebServices';
import { Empresa } from '../../models/empresa';
import { EmpresaService } from '../../services/component-services/empresa.service';
import { Page } from '../../components/pagination/page';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Empleado } from '../../models/empleado';
import { EmpresaControllerService } from '../../services/controllers/empresa-controller.service';
import { XlsReportService } from '../services/xls-report.service';
import { FilterInterface } from '../../interfaces/filter.interface';
import { ImageManager } from '../../tools/image-manager';
import { PdfReportService } from '../services/pdf-report.service';
import { ReportService } from '../services/report.service';
import { Sucursal } from '../../models/sucursal';
import { SucursalService } from '../../services/component-services/sucursal.service';
import { NgForm } from '@angular/forms';
import { Nivel } from '../../models/nivel';
import { NivelService } from '../../services/component-services/nivel.service';
import { Departamento } from '../../models/departamento';
import { DepartamentoService } from '../../services/component-services/departamento.service';
import { TimeInterface } from '../../dynamic-form/interfaces/time.interface';
import { GenericService } from '../services/generic.service';
import { TableResponsiveService } from '../../services/table/table-responsive.service';
import { SpinnerDialogService } from '../../components/spinner-dialog/spinner-dialog.service';
import months from '../../data/months';

const LOGO = '/assets/img/logos/logo-fastline.png';
const LOGO_WIDTH = 155;
const LOGO_HEIGHT = 60;

const DAYS = ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"];

@Component({
  selector: 'app-coorp-report',
  templateUrl: './coorp-report.component.html',
  styleUrls: [
    './coorp-report.component.scss',
    '../../../../node_modules/@swimlane/ngx-datatable/release/index.css',
    '../../../vendor/libs/ngx-datatable/ngx-datatable.scss',
    '../../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css',
    '../../../vendor/libs/ngx-toastr/ngx-toastr.scss'
  ],
  encapsulation: ViewEncapsulation.None
})
export class CoorpReportComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild("tableContainer") tableContainerRef: ElementRef;
  private tableContainerElement: HTMLElement;
  private months: { monthCode: string; monthName: string }[];

  private base64Logo: any;
  private xlsReportHeaders: string[];
  private reportHeaders: string[];
  private serviceColumns: { id: number, column: string, property: string, checked: boolean }[];
  private employeeColumns: { id: number, column: string, property: string, checked: boolean }[];
  private dynaEmpColumns: { id: number, column: string, property: string, checked: boolean }[];
  private dynaDepaNiveleList: { idDepartamento: string, nombreDepa: string, idDepartPadre: string, idNivel: string, nombreNivel: string, numeroNivel: number, idEmpresa: number }[];

  public labels: any;
  public gv: any;
  public page: Page;
  public filter: { idEmpresa: number; idSucursal: number; idNivel: string; idDepartamento: string; fechaDesde: any; fechaHasta: any; horaDesde: string; horaHasta: string; estado: string; formaPago: string; ciudad: string; idEmpleado: number };
  public reportList: any[];
  public filterReportList: any[];
  public loadingIndicator: boolean;
  public timeVars: TimeInterface[];
  public tiempoHasta: TimeInterface;
  public filteredEmpleado: Empleado;
  public empresaList: Empresa[];
  public sucursalList: Sucursal[];
  public estadosCarrera: { key: string; value: string }[];
  public formasPago: { key: string; value: string }[];
  public ciudades: { key: string; value: string }[];
  public nivelesList: Nivel[];
  public depaList: Departamento[];
  public depaName: string;
  public calDesde: any;
  public calHasta: any;
  public queryCols: { id: number, column: string, property: string, checked: boolean }[];
  public showDetail: boolean;
  public showAutoriDetail: boolean;
  public numAutorizacion: string;
  public showOptions: boolean;
  public selectedRow: any;

  constructor(
    private _functionService: FunctionService,
    private _reportService: ReportService,
    private _empresaControllerService: EmpresaControllerService,
    private _sucursalService: SucursalService,
    private _empresaService: EmpresaService,
    private _nivelService: NivelService,
    private _departamentoService: DepartamentoService,
    private _xlsReportService: XlsReportService,
    private _pdfReportService: PdfReportService,
    private _genericService: GenericService,
    private _tableResponsiveService: TableResponsiveService,
    private _spinnerDialogService: SpinnerDialogService
  ) {
    this.depaName = 'Departamento';
    this.page = new Page();
    this.page.size = 5;
    this.labels = TAGS;
    this.gv = GV;
    this.loadingIndicator = true;
    this.showOptions = false;
    this.months = months;

    this.resetForm();
  }

  ngOnInit() {
    this.tableContainerElement = this.tableContainerRef.nativeElement;
    this.loadInitData();
    ImageManager.getBase64FromImgPath(LOGO, LOGO_WIDTH, LOGO_HEIGHT)
      .then((base64Img: any) => {
        this.base64Logo = base64Img;
      });
  }

  /**
   * METODO PARA MOSTRAR EL DETALLE DEL RECORRIDO DEL TAXI
   * AUTOR: FREDI ROMAN
   */
  public showAutorizacionDetail() {
    this.showAutoriDetail = null;
    setTimeout(() => {
      this.showAutoriDetail = true;
    });

    this.numAutorizacion = null;
    setTimeout(() => {
      this.numAutorizacion = this.selectedRow.autorizacion;
    }, 500);
  }

  /**
   * METODO PARA MOSTRAR EL DETALLE DEL RECORRIDO DEL TAXI
   * AUTOR: FREDI ROMAN
   */
  public showDetailModal() {
    this.showDetail = null;
    setTimeout(() => {
      this.showDetail = true;
    });

    this.numAutorizacion = null;
    setTimeout(() => {
      this.numAutorizacion = this.selectedRow.autorizacion;
    }, 500);
  }

  /**
   * METODO PARA DETECTAR LOS CAMBIOS EN LAS LISTAS DE DEFINICION DE COLUMNAS PARA LA CONSULTA
   * AUTOR: FREDI ROMAN
   * @param $event 
   */
  public noticeColumnChanges(event: any) {
    this.queryCols = null;
    setTimeout(() => {
      this.queryCols = JSON.parse(JSON.stringify(this.serviceColumns));
      this.queryCols.push(...JSON.parse(JSON.stringify(this.employeeColumns)));
      this.queryCols.push(...JSON.parse(JSON.stringify(this.dynaEmpColumns)));
      this.queryCols = this._genericService.sortReportColumns(this.queryCols);
      this.setReportHeaders();
      setTimeout(() => {
        this._tableResponsiveService.makeTableResponsive(this.tableContainerElement);
      }, 100);
    }, 100);
  }

  /**
   * METODO PARA GENERAR UN ARCHIVO PDF EN BASE AL REPORTE CONSULTADO, Y EXPORTARLO
   * AUTOR: FREDI ROMAN
   */
  public exportToPdf() {
    const columnsWidth = [22, 15, 30, 20, 40, 18, 15, 12, 57, 57];
    this._pdfReportService.generatePdfReport(this.base64Logo, "Reporte de Consumo de Servicios", this.reportHeaders, this.reportList, columnsWidth, 'reporte_serv', this._pdfReportService.LANDSCAPE);
  }

  /**
   * METODO PARA GENERAR UN ARCHIVO EXCEL EN BASE AL REPORTE CONSULTADO, Y EXPORTARLO
   * AUTOR: FREDI ROMAN
   */
  public exportToXls() {
    let filters: FilterInterface[] = [
      { name: TAGS.general.company + ":", value: this.empresaList.find(emp => emp.id == this.filter.idEmpresa).nombre },
      { name: TAGS.reports.coorpServicesReport.empleado.sucursal + ":", value: this.sucursalList.find(suc => parseInt(suc.id) == this.filter.idSucursal).nomSucursal },
      { name: TAGS.reports.coorpServicesReport.empleado.nivel + ":", value: this.nivelesList.find(niv => niv.id == this.filter.idNivel).nombre },
      { name: TAGS.reports.coorpServicesReport.empleado.nomCompleto + ":", value: (this.filter.idEmpleado) ? this.filteredEmpleado.nomCompleto : TAGS.general.totalPage },
      { name: TAGS.reports.coorpServicesReport.servicio.estado + ":", value: this.estadosCarrera.find(est => est.key == this.filter.estado).value },
      { name: TAGS.reports.coorpServicesReport.servicio.forma_pago + ":", value: this.formasPago.find(fp => fp.key == this.filter.formaPago).value },
      { name: TAGS.reports.coorpServicesReport.servicio.ciudad + ":", value: this.ciudades.find(ciu => ciu.key == this.filter.ciudad).value },
    ];
    if (this.filter.fechaDesde != null) {
      filters.splice(0, 0, { name: TAGS.reports.filter.fechaDesde + ":", value: this.filter.fechaDesde });
    }
    if (this.filter.fechaHasta != null) {
      filters.splice(1, 0, { name: TAGS.reports.filter.fechaHasta + ":", value: this.filter.fechaHasta });
    }
    if (this.filter.horaDesde != null) {
      filters.push({ name: TAGS.reports.filter.horaDesde + ":", value: this.filter.horaDesde });
    }
    if (this.filter.horaHasta != null) {
      filters.push({ name: TAGS.reports.filter.horaHasta + ":", value: this.filter.horaHasta });
    }
    if (this.depaList != null) {
      filters.splice(3, 0, { name: TAGS.reports.coorpServicesReport.empleado.departamento + ":", value: this.depaList.find(dep => dep.idDepartamento == this.filter.idDepartamento).nombreDepa });
    }


    let xlsReportData = [];
    let rowXlsRep;
    let columnsWidth = [];
    for (let repRow of this.reportList) {
      rowXlsRep = [];
      for (let col of this.queryCols) {
        if (col.checked) {
          rowXlsRep.push(repRow[col.property]);
          columnsWidth.push(25);
        }
      }
      xlsReportData.push(rowXlsRep);
    }
    this._xlsReportService.generateXlsReport(this.base64Logo, 'Reporte de Consumo de Servicios', this.xlsReportHeaders, filters, xlsReportData, columnsWidth, 'reporte_serv');
  }

  /**
   * METODO PARA DEFINIR LOS ENCABEZADOS DE LAS COLUMNAS DE LOS REPORTES XLS Y PDF
   * AUTOR: FREDI ROMAN
   */
  private setReportHeaders() {
    this.xlsReportHeaders = [];
    for (let column of this.queryCols) {
      if (column.checked) {
        this.xlsReportHeaders.push(column.column);
      }
    }

    this.reportHeaders = [
      TAGS.reports.coorpServicesReport.servicio.autorizacion,
      TAGS.reports.coorpServicesReport.servicio.automovil,
      TAGS.reports.coorpServicesReport.servicio.forma_pago,
      TAGS.reports.coorpServicesReport.servicio.nvoucher,
      TAGS.reports.coorpServicesReport.empleado.nomCompleto,
      TAGS.reports.coorpServicesReport.servicio.fechaServicio,
      TAGS.reports.coorpServicesReport.servicio.horaServicio,
      TAGS.reports.coorpServicesReport.servicio.tarifa,
      TAGS.reports.coorpServicesReport.servicio.origen,
      TAGS.reports.coorpServicesReport.servicio.destino,
    ];
  }

  /**
   * METODO PARA CAPTURAR LOS VALORES DE LAS VARIABLES DE TIEMPO
   * AUTOR: FREDI ROMAN
   * @param event 
   */
  public getTimeChanges(event: TimeInterface) {
    for (let time of this.timeVars) {
      if (event.id == time.id) time = event;
    }

    this.filter.horaDesde = (this.timeVars[0].hours !== '00') ? this.timeVars[0].hours + ":" + this.timeVars[0].minutes + ":" + this.timeVars[0].seconds : null;
    this.filter.horaHasta = (this.timeVars[1].hours !== '00') ? this.timeVars[1].hours + ":" + this.timeVars[1].minutes + ":" + this.timeVars[1].seconds : null;
  }

  /**
   * METODO PARA CAPTURAR EL VALOR DEL CALENDAR PARA LA FECHA HASTA
   * AUTOR: FREDI ROMAN
   * @param event 
   */
  public onFechaHastaChange(event: any) {
    this.calHasta = event;
  }

  /**
   * METODO PARA CAPTURAR EL VALOR DEL CALENDAR PARA LA FECHA DESDE
   * AUTOR: FREDI ROMAN
   * @param event 
   */
  public onFechaDesdeChange(event: any) {
    this.calDesde = event;
  }

  /**
   * METODO PARA CAPTAR EL OBJETO FILTRADO DESDE EL AUTOCOMPLETE
   * AUTOR: FREDI ROMAN
   * @param event
   */
  public getFilteredData(event: Empleado) {
    this.filteredEmpleado = event;
    this.filter.idEmpleado = event.id;
    this.filter.idSucursal = parseInt(event.sucursal.id);
    this.filter.idNivel = event.cargo.departamento.nivel.id;
    this.filter.idDepartamento = event.cargo.departamento.idDepartamento
    this.loadDeparmentData(false);
  }

  /**
   * METODO PARA MANIPULAR EL EVENTO CHANGE DE LISTA DE NIVELES
   * AUTOR: FREDI ROMAN
   */
  public onNivelChange() {
    if (this.filter.idNivel != '-1') {
      this.filter.idEmpleado = null;
      this.filteredEmpleado = new Empleado();
      this.loadDeparmentData();
    } else {
      this.depaList = null;
      this.filter.idDepartamento = null;
    }
  }

  /**
   * METODO PARA MANIPULAR EL EVENTO CHANGE DE LA LISTA DE EMPRESAS:
   * AUTOR: FREDI ROMAN
   */
  public onEmpresaChange() {
    this.filteredEmpleado = new Empleado();
    this.loadNivelesData();
  }

  /**
   * METODO PARA DEFINIR LAS COLUMNAS A MOSTRAR EN LA CONSULTA
   * AUTOR: FREDI ROMAN
   */
  private defineQueryColumns() {
    this.serviceColumns = !this.serviceColumns ? this._genericService.defineServiceColumns() : this.serviceColumns;
    this.employeeColumns = this._genericService.defineEmpColumns(this.nivelesList);
    this._genericService.defineDynaEmpColumns(this.filter.idEmpresa)
      .then((respData: { id: number, column: string, property: string, checked: boolean }[]) => {
        this.dynaEmpColumns = respData;

        this.queryCols = JSON.parse(JSON.stringify(this.serviceColumns));
        this.queryCols.push(...JSON.parse(JSON.stringify(this.employeeColumns)));
        this.queryCols.push(...JSON.parse(JSON.stringify(this.dynaEmpColumns)));

        this.queryCols = this._genericService.sortReportColumns(this.queryCols);
        this.setReportHeaders();
        this.onSubmit();
      });
  }

  /**
   * METODO PARA CARGAR LA INFORMACION DE LOS DEPARTAMENTOS DE UN NIVEL DE UNA EMPRESA
   * AUTOR: FREDI ROMAN
   */
  private loadDeparmentData(reinit: boolean = true) {
    if (this.filter.idNivel && this.filter.idNivel != '-1') {
      this.depaName = this.nivelesList.find(niv => niv.id == this.filter.idNivel).nombre;
      this._functionService.httpGetWithout(WS.departamento.get.replace("${nivelId}", this.filter.idNivel))
        .then((responseList: any) => {
          this.depaList = this._departamentoService.createDepartamentoList(responseList);
          this.depaList.push(new Departamento('-1', null, TAGS.general.totalPage));
          if (reinit) {
            this.filter.idDepartamento = (this.depaList[0]) ? this.depaList[this.depaList.length - 1].idDepartamento : null;
          }
        });
    } else {
      this.depaList = null;
      this.filter.idDepartamento = null;
    }
  }

  /**
   * METODO PARA CARGAR LA INFORMACION DE LOS NIVELES DE UNA EMPRESA
   * AUTOR: FREDI ROMAN
   */
  private loadNivelesData() {
    if (this.filter.idEmpresa) {
      let urlArray = [
        WS.nivel.get.replace("${enterpriseId}", this.filter.idEmpresa + ""),
        WS.departamento.getDepasNivelsByEmpId.replace("${empId}", this.filter.idEmpresa + "")
      ];
      this._functionService.httpObservableGet(urlArray)
        .then((responseList: any) => {
          this.nivelesList = this._nivelService.createNivelList(responseList[0]);
          if (this.nivelesList[0]) {
            this.nivelesList.push(new Nivel('-1', -1, TAGS.general.totalPage));
          }
          this.filter.idNivel = this.nivelesList[0] ? this.nivelesList[this.nivelesList.length - 1].id : null;

          this.dynaDepaNiveleList = responseList[1];
          this.getSucursales();
        });
    } else {
      this.nivelesList = [];
      this.filter.idNivel = null;
    }
  }

  /**
   * METODO PARA OBTENER LAS SUCURSALES DADO EL ID DE EMPRESA
   * AUTOR: FREDI ROMAN
   */
  private getSucursales() {
    if (this.filter.idEmpresa) {
      let url = WS.sucursales.sucByStateCompany.replace("${empresa}", this.filter.idEmpresa + "");
      this._functionService.httpGet(url)
        .then((responseData: any) => {
          this.sucursalList = this._sucursalService.createSucursalList(responseData.content);
          this.sucursalList.push(new Sucursal('-1', null, null, TAGS.general.totalPage));
          this.filter.idSucursal = this.sucursalList[0] ? parseInt(this.sucursalList[0].id) : null;
          this.loadDeparmentData();
          this.defineQueryColumns();
        });
    } else {
      this.sucursalList = [];
      this.filter.idSucursal = null;
    }
  }

  /******************************************************************************************
  *******************************************************************************************
   ****** FORM DEFINITION
  ********************************************************************************************
  *******************************************************************************************/

  /**
   * METODO PARA PROCESAR EL SUBMIT DEL FORMULARIO PRINCIPAL
   * AUTOR: FREDI ROMAN
   */
  public onSubmit() {
    if (this.calDesde && this.calHasta) {
      this.filter.fechaDesde = this.calDesde.year + "-" + (this.calDesde.month < 10 ? "0" + this.calDesde.month : this.calDesde.month) + "-" + (this.calDesde.day < 10 ? "0" + this.calDesde.day : this.calDesde.day);
      this.filter.fechaHasta = this.calHasta.year + "-" + (this.calHasta.month < 10 ? "0" + this.calHasta.month : this.calHasta.month) + "-" + (this.calHasta.day < 10 ? "0" + this.calHasta.day : this.calHasta.day);
    }
    this.setPaginatedTableData(0, this.page.size);
  }

  /**
   * METODO PARA CARGAR LOS DATOS DE INICIO, DESDE EL BACKEND, PARA MOSTRARLO EN EL DOM 
   * AUTOR: FREDI ROMAN
   */
  private loadInitData() {
    this.estadosCarrera = [];
    for (let property in GV.estadosCarrerasRep) {
      this.estadosCarrera.push({ key: property, value: GV.estadosCarrerasRep[property] });
    }
    this.estadosCarrera.push({ key: '-1', value: TAGS.general.totalPage });
    this.filter.estado = this.estadosCarrera[this.estadosCarrera.length - 1].key;

    this.formasPago = [];
    for (let property in GV.formasPagoCat) {
      this.formasPago.push({ key: property, value: GV.formasPagoCat[property] });
    }
    this.formasPago.push({ key: '-1', value: TAGS.general.totalPage });
    this.filter.formaPago = this.formasPago[this.formasPago.length - 1].key;

    this.ciudades = [];
    for (let property in GV.ciudadesServicios) {
      this.ciudades.push({ key: property, value: GV.ciudadesServicios[property] });
    }
    this.ciudades.push({ key: '-1', value: TAGS.general.totalPage });
    this.filter.ciudad = this.ciudades[this.ciudades.length - 1].key;

    this._empresaControllerService.getEmpresaListController()
      .then((empresaData) => {
        this.empresaList = this._empresaService.createEmpresaList(empresaData);
        this.filter.idEmpresa = this.empresaList[0] ? this.empresaList[0].id : null;
        this.loadNivelesData();
      });
  }

  /**
   * METODO PARA RESETEAR LOS ELEMENTOS DEL FORMULARIO
   * AUTOR: FREDI ROMAN
   */
  public resetForm(ngForm: NgForm = null) {
    if (ngForm) {
      //REF: https://stackoverflow.com/questions/50197347/how-to-reset-only-specific-fields-of-form-in-angular-5
      ngForm.form.controls['calenDesde'].reset();
      ngForm.form.controls['calenHasta'].reset();
      ////
    }
    let initDate: any = new Date();
    initDate = initDate.getFullYear() + '-' + (initDate.getMonth() + 1 < 10 ? '0' + (initDate.getMonth() + 1) : initDate.getMonth() + 1) + '-01';
    let endDate = initDate.split('-');
    endDate = (parseInt(endDate[1]) == 12 ? (parseInt(endDate[0]) + 1) : endDate[0]) + '-' + (parseInt(endDate[1]) == 12 ? '01' : (parseInt(endDate[1]) + 1 < 10 ? '0' + (parseInt(endDate[1]) + 1) : parseInt(endDate[1]) + 1)) + '-01';

    if (!this.filter) {
      this.filter = {
        idEmpresa: null,
        idSucursal: null,
        idNivel: null,
        idDepartamento: null,
        fechaDesde: initDate,
        fechaHasta: endDate,
        horaDesde: null,
        horaHasta: null,
        estado: null,
        formaPago: null,
        ciudad: null,
        idEmpleado: null,
      };
    } else {
      this.filter.idSucursal = (this.sucursalList[0]) ? parseInt(this.sucursalList[0].id) : null;
      this.filter.idNivel = this.nivelesList[this.nivelesList.length - 1].id;
      this.filter.idDepartamento = null;
      this.filter.fechaDesde = initDate;
      this.filter.fechaHasta = endDate;
      this.filter.horaDesde = null;
      this.filter.horaHasta = null;
      this.filter.estado = this.estadosCarrera[this.estadosCarrera.length - 1].key;
      this.filter.formaPago = this.formasPago[this.formasPago.length - 1].key;
      this.filter.ciudad = this.ciudades[this.ciudades.length - 1].key;
      this.filter.idEmpleado = null;
      this.depaList = null;
      this.setPaginatedTableData(0, this.page.size);
    }

    initDate = initDate.split('-');
    this.calDesde = { year: parseInt(initDate[0]), month: parseInt(initDate[1]), day: parseInt(initDate[2]) };

    endDate = endDate.split('-');
    this.calHasta = { year: parseInt(endDate[0]), month: parseInt(endDate[1]), day: parseInt(endDate[2]) };

    this.timeVars = [
      { id: 'htDesde', label: TAGS.reports.filter.horaDesde, hours: "00", minutes: "00", seconds: '00' },
      { id: 'htHasta', label: TAGS.reports.filter.horaHasta, hours: "00", minutes: "00", seconds: '00' }
    ];

    this.filteredEmpleado = new Empleado();
    this.showOptions = false;
  }
  /******************************************************************************************
  ******************************************************************************************/

  /******************************************************************************************
  *******************************************************************************************
   ****** TABLE DEFINITION 
  ********************************************************************************************
  *******************************************************************************************/

  /**
   * METODO PARA REALIZAR UN FILTRO EN LA TABLA
   * AUTOR: FREDI ROMAN
   * @param event 
   */
  public onTableFilter(event) {
    const val = event.target.value.toLowerCase();
    this.filterReportList = this.reportList.slice();
    const temp = this.filterReportList.filter((filterData) => {
      return filterData.empleado.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.filterReportList = temp;
    this.table.offset = 0;
  }

  /**
   * METODO PARA RECARGAR LOS DATOS DE LA TABLA AL CAMBIA EL NUMERO DE REGISTROS A SER MOTRADOS EN LA MISMA
   * AUTOR: FREDI ROMAN
   */
  public reloadTableData() {
    this.page.pageNumber = 0;
    this.setTableOffset();
  }

  /**
   * METODO PARA CAPTURAR EL EVENTO DE SELECCION DE UNA FILA DE LA TABLA
   * AUTOR: FREDI ROMAN
   * @param selecteRow 
   */
  public onSelectRow(selectedRow: { selected: any[] }) {
    this.selectedRow = selectedRow.selected[0];
    this.showOptions = true;
  }

  /**
   * METODO PARA DEFINIR LA PAGINACION DE LA TABLA, AL CAPTURAR EL EVENTO DE NEXT/PREV PAGE
   * AUTOR: FREDI ROMAN
   * @param pageInfo 
   */
  public setPage(pageInfo: any) {
    this.page.pageNumber = pageInfo.offset;
    this.setTableOffset();
  }

  /**
   * METODO PARA ESTABLECER EL OFFSET PARA LA PAGINACION DE LA TABLA
   * AUTOR: FREDI ROMAN
   */
  private setTableOffset() {
    let indexInit: number;
    if (this.page.pageNumber == 0) {
      this.filterReportList = this.reportList.slice(0, this.page.size);
    } else {
      indexInit = this.page.pageNumber * this.page.size;
      this.filterReportList = this.reportList.slice(indexInit, indexInit + this.page.size);
    }
  }

  /**
   * METODO PARA DEFINIR LOS DATOS DE LA TABLA AL INTERACTUAR CON LA PAGINACION
   * AUTOR: FREDI ROMAN
   * @param pageNumber 
   * @param size 
   */
  public setPaginatedTableData(pageNumber: any, size: any, showLoading: boolean = true) {
    this.page.pageNumber = pageNumber;
    this.page.size = size;

    if (this.filter.idEmpresa) {
      let url = WS.reportes.getRequestedServicesReport;
      //REF: https://davidwalsh.name/javascript-replace
      let stringifyFilter = JSON.stringify(this.filter).replace(/"-1"/g, "null");
      ////
      let reqFilter = { key: 'report-filter', value: stringifyFilter };
      this._reportService.httpGetWithHeaders(url, reqFilter, showLoading)
        .then((res) => {
          this.reportList = res;
          let splittedDate, dt;
          for (let rep of this.reportList) {
            splittedDate = rep.fechaServicio.split("-");
            dt = new Date(splittedDate[1] + ' ' + splittedDate[2] + ', ' + splittedDate[0] + ' 12:00:00');
            dt = dt.getUTCDay();
            dt = (dt == 0 || dt == 6) ? DAYS[dt] : '';

            Object.defineProperty(rep, 'anio', { value: splittedDate[0], writable: true });
            Object.defineProperty(rep, 'mes', { value: this.months.find(month => month.monthCode == splittedDate[1]).monthName, writable: true });
            Object.defineProperty(rep, 'diaFds', { value: dt, writable: true });

            rep.tarifa = '$ ' + rep.tarifa;
          }

          this.defineDynaDepasNiveles();
          this.filterReportList = this.reportList.slice();
          this.page.totalElements = this.reportList.length;

          setTimeout(() => {
            this._tableResponsiveService.makeTableResponsive(this.tableContainerElement);
            this.loadingIndicator = false;
          }, 100);
        });
    } else {
      this.reportList = [];
      this.filterReportList = [];
      this.page.totalElements = 0;
      this.loadingIndicator = false;
    }
  }

  /**
   * METODO PARA ESTABLECER LAS COLUMNAS DE JERARQUIA ORGANIZACIONAL PARA EL REPORTE
   * AUTOR: FREDI ROMAN
   */
  private defineDynaDepasNiveles() {
    this._spinnerDialogService.openSpinner();
    let jerData;
    for (let repRow of this.reportList) {
      jerData = this.defineDepasNivelsByRepRow(repRow.idDepartamento);
      for (let property in jerData) {
        Object.defineProperty(repRow, property, { value: jerData[property], writable: false });
      }
    }
    this._spinnerDialogService.closeSpinner();
  }

  /**
   * METODO PARA DEFINIR LAS COLUMNAS DE JERARQUIA PARA CADA FILA DEL REPORTE
   * AUTOR: FREDI ROMAN
   */
  private defineDepasNivelsByRepRow(depaId: string) {
    let jerData = "{";
    let propertyName;

    for (let i = 0; i < this.nivelesList.length; i++) {
      if (this.nivelesList[i].nombre != TAGS.general.totalPage) {
        if (propertyName) {
          jerData += ", ";
        }
        propertyName = '"' + this.nivelesList[i].nombre.toLowerCase() + '"';
        jerData += propertyName + ': "' + this.defineDepaNivel(this.nivelesList[i].id, depaId) + '"';
      }
    }
    jerData += "}";
    return JSON.parse(jerData);
  }

  /**
   * METODO PARA BUSCAR UN DEPARTAMENTO Y DEFINIR EL NOMBRE DE UN NIVEL
   * AUTOR: FREDI ROMAN
   */
  private defineDepaNivel(nivelId: string, depaId: string) {
    let dynaRow = this.dynaDepaNiveleList.find(dyn => dyn.idDepartamento == depaId && dyn.idNivel == nivelId);
    if (!dynaRow) {
      let parentDepaRow = this.dynaDepaNiveleList.find(dyn => dyn.idDepartamento == depaId);
      if (parentDepaRow) {
        return this.defineDepaNivel(nivelId, parentDepaRow.idDepartPadre);
      }
    }
    return dynaRow ? dynaRow.nombreDepa : '';
  }
  /******************************************************************************************
  ******************************************************************************************/
}
