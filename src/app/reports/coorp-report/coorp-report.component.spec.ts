import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoorpReportComponent } from './coorp-report.component';

describe('CoorpReportComponent', () => {
  let component: CoorpReportComponent;
  let fixture: ComponentFixture<CoorpReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoorpReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoorpReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
