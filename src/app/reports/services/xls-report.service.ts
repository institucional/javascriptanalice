import { Injectable } from '@angular/core';
import * as ExcelJS from 'exceljs/dist/exceljs.min.js';
import * as FileSaver from 'file-saver';
import { DatePipe } from '@angular/common';
import { FilterInterface } from '../../interfaces/filter.interface';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

const HEADER_COLOR = 'FF2D2E7E';
const HEADER_TEXT_COLOR = 'FFFFB100';
const BORDER_COLOR = 'FF333333';
const GENERIC_TEXT_COLOR = 'FF666666';

@Injectable()
export class XlsReportService {

  constructor() { }

  /**
   * METODO PARA GENERAR EL REPORTE DE CONSUMO DE SERVICIOS PARA LOS CLIENTES CORPORATIVOS
   * AUTOR: FREDI ROMAN
   * @param title 
   * @param headerLabels 
   * @param filters 
   * @param reportData 
   * @param excelFileName 
   */
  public generateXlsReport(base64Logo: any, title: string, headerLabels: string[], filters: FilterInterface[], reportData: any[], columnsWidth: number[], excelFileName: string): void {
    const workbook = new ExcelJS.Workbook();
    const worksheet = workbook.addWorksheet('Datos Reporte');

    //DEFINING REPORT CONTENT:
    this.defineReportHeader(workbook, worksheet, base64Logo, title);
    this.defineReportFilters(worksheet, filters);
    this.defineRerportColHeaders(worksheet, headerLabels);

    reportData.forEach(d => {
      var dataRow: any[] = [];
      for (let property in d) {
        dataRow.push(d[property] ? d[property] : '');
      }
      const row = worksheet.addRow(dataRow);
      row.eachCell(cell => {
        cell.font = {
          name: 'Arial',
          bold: false,
          size: 9,
          color: { argb: GENERIC_TEXT_COLOR }
        };
        cell.border = {
          top: { style: 'thin', color: { argb: BORDER_COLOR } },
          left: { style: 'thin', color: { argb: BORDER_COLOR } },
          bottom: { style: 'thin', color: { argb: BORDER_COLOR } },
          right: { style: 'thin', color: { argb: BORDER_COLOR } }
        };
      });
    });

    for (let i = 0; i < columnsWidth.length; i++) {
      worksheet.getColumn(i + 1).width = columnsWidth[i];
    }

    this.exportReportFile(workbook, excelFileName);
    /////////
  }

  /*******************************************************************************************************************************
  ********************************************************************************************************************************
   * METODOS GENERALES
  *******************************************************************************************************************************
  *******************************************************************************************************************************/

  /**
   * METODO PARA EXPORTAR EL REPORTE Y DESCAGAR EL ARCHIVO EN FORMATO XLSX
   * @param workbook 
   * @param excelFileName 
   */
  private exportReportFile(workbook: any, excelFileName: string) {
    workbook.xlsx.writeBuffer().then((buffer: any) => {
      const blob: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(blob, excelFileName + '_exported_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }

  /**
   * METODO PARA DEFINIR LAS CABECERAS DE LA TABLA DEL REPORTE
   * AUTOR: FREDI ROMAN
   */
  private defineRerportColHeaders(worksheet: any, headerLabels: string[]) {
    const headerRow = worksheet.addRow(headerLabels);
    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: HEADER_COLOR },
        bgColor: { argb: HEADER_COLOR },
      };
      cell.font = {
        name: 'Arial',
        bold: true,
        size: 10,
        color: { argb: HEADER_TEXT_COLOR }
      };
      cell.border = {
        top: { style: 'thin', color: { argb: GENERIC_TEXT_COLOR } },
        left: { style: 'thin', color: { argb: GENERIC_TEXT_COLOR } },
        bottom: { style: 'thin', color: { argb: GENERIC_TEXT_COLOR } },
        right: { style: 'thin', color: { argb: GENERIC_TEXT_COLOR } }
      };
      cell.alignment = { vertical: 'middle', horizontal: 'center' };
    });
  }

  /**
   * METODO PARA DEFINIR LOS FILTROS DEL REPORTE
   * AUTOR: FREDI ROMAN
   * @param worksheet 
   * @param filters 
   */
  private defineReportFilters(worksheet: any, filters: FilterInterface[]) {
    filters.forEach(filter => {
      let filt = worksheet.addRow([filter.name, filter.value]);
      filt.font = { name: 'Arial', family: 4, size: 10, bold: false, color: { argb: GENERIC_TEXT_COLOR } };
    });
    worksheet.addRow([]);
  }

  /**
   * PARA DEFINIR LA CABECERA DEL REPORTE
   * AUTOR: FREDI ROMAN
   * @param workbook 
   * @param base64Img 
   * @param worksheet 
   */
  private defineReportHeader(workbook: any, worksheet: any, base64Img: any, title: string) {
    let imageId2 = workbook.addImage({
      base64: base64Img,
      extension: 'png',
    });
    worksheet.addImage(imageId2, 'B1:C4');

    this.defineReportTitle(title, worksheet);
    this.defineReportDate(worksheet);
  }

  /**
   * METODO PARA DEFINIR LA FECHA DE GENERACION DEL REPORTE
   * AUTOR: FREDI ROMAN
   */
  private defineReportDate(worksheet: any) {
    let datePipe = new DatePipe('en-US');
    let fecha = worksheet.addRow(['Fecha Reporte: ' + datePipe.transform(new Date(), 'medium')]);
    fecha.font = { name: 'Arial', family: 4, size: 10, bold: true, color: { argb: GENERIC_TEXT_COLOR } };
    worksheet.mergeCells('A7:C7');
    worksheet.addRow([]);
  }

  /**
   * METODO PARA DEFINIR EL TITULO Y SU ESTILO, DEL REPORTE
   * AUTOR: FREDI ROMAN
   * @param title 
   * @param worksheet 
   */
  private defineReportTitle(title: string, worksheet: any) {
    let titleCell = worksheet.getCell('E5');
    titleCell.value = title;
    titleCell.alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getRow(5).font = { name: 'Arial', family: 4, size: 14, bold: true, color: { argb: GENERIC_TEXT_COLOR } };
    worksheet.mergeCells('E5:H5');
    worksheet.addRow([]);
  }
}
