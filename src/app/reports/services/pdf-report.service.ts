import { Injectable } from '@angular/core';
import * as jsPDF from 'jspdf';

const LEFT_MARGIN = 20;
const TOP_MARGIN = 25;
const BOTTOM_MARGIN = 20;
const HEADER_COLOR = "#2d2e7e";
const HEADER_TEXT_COLOR = "#ffb100";
const NORMAL_TEXT_COLOR = "#666666";
const GENERIC_BORDER_COLOR = "#dddddd";
const BASE_BACK_COLOR = "#ffffff";
const ALTER_BACK_COLOR = "#f6f6f6";

@Injectable()
export class PdfReportService {
  private pageNumber: number;
  private base64Img: any;
  private orientation: number;

  public PORTRAINT: number = 1;
  public LANDSCAPE: number = 2;

  constructor() { }

  /**
   * METODO PARA GENERAR EL REPORTE DE CONSUMO DE SERVICIO DE TRANSPORTE PARA CLIENTES COORPORATIVOS
   * AUTOR: FREDI ROMAN
   */
  public generatePdfReport(base64Img: any, title: string, columnHeaders: string[], reportData: any[], columnWidths: number[], fileName: string, orientationOp: number = this.PORTRAINT) {
    let pdfDoc = this.initReportManager(base64Img, title, orientationOp);
    let leftMargin = LEFT_MARGIN / 4;
    let topMargin = TOP_MARGIN + 15;

    pdfDoc.cellInitialize();
    pdfDoc.setFontSize(8);
    //pdfDoc.setLineHeightFactor(0.8);
    let lineHeight = this.defineReportColHeaders(pdfDoc, columnHeaders, columnWidths, leftMargin, topMargin);
    pdfDoc.setFontStyle('normal');
    pdfDoc.setTextColor(NORMAL_TEXT_COLOR);
    this.defineReportRows(pdfDoc, leftMargin, topMargin, lineHeight, reportData, columnWidths);

    this.exportPDF(pdfDoc, fileName);
  }

  /**
   * METODO PARA LIBERAR EN PDF EL REPORTE AL DAR CLICK EN UN BOTON
   * AUTOR: FREDI ROMAN
   */
  private exportPDF(pdfDoc: any, fileName: string) {
    //pdfDoc.save('demo-fastline.pdf');
    pdfDoc.output('dataurlnewwindow', { filename: fileName });
  }

  /**
   * METODO PARA AGREGAR LAS FILAS DE LA TABLA DEL REPORTE
   * AUTOR: FREDI ROMAN
   * @param pdfDoc 
   * @param topMargin 
   * @param lineHeight 
   */
  private defineReportRows(pdfDoc: any, leftMargin: number, topMargin: number, lineHeight: number, reportData: any[], columnWidths: number[]) {
    let auxLeftMargin: number;
    let headerKeys: any[] = [];
    let auxData: any[] = [];
    let row: string[];

    for (let data of reportData) {
      row = [];
      for (let property in data) {
        row.push(data[property] + "");
      }
      auxData.push(row);
    }
    for (let property in auxData[0]) {
      headerKeys.push(property);
    }

    let j;
    for (let i = 0; i < auxData.length; i++) {
      auxLeftMargin = leftMargin;
      topMargin = topMargin + lineHeight;
      lineHeight = pdfDoc.calculateLineHeight(headerKeys, columnWidths, auxData[i]);
      lineHeight = (lineHeight > 12.2) ? lineHeight - lineHeight / 5 : lineHeight;

      j = 0;
      if (topMargin + lineHeight > pdfDoc.internal.pageSize.height - BOTTOM_MARGIN) {
        this.addPageWithTemplate(pdfDoc);
        topMargin = TOP_MARGIN;
        pdfDoc.cellInitialize();
        pdfDoc.setFontSize(8);
      }
      for (let key of headerKeys) {
        pdfDoc.setFillColor(i % 2 == 0 ? BASE_BACK_COLOR : ALTER_BACK_COLOR);
        pdfDoc.rect(auxLeftMargin, topMargin, columnWidths[j], lineHeight, 'F');
        pdfDoc.cell(auxLeftMargin, topMargin, columnWidths[j], lineHeight, auxData[i][key] != 'null' ? auxData[i][key] : ' ', i + 2, 'left');

        auxLeftMargin = auxLeftMargin + columnWidths[j];
        j++;
      }
    }
  }

  /**
   * METODO PARA DEFINIR LOS ENCABEZADOS DE LAS COLUMNAS DEL REPORTE
   * AUTOR: FREDI ROMAN
   * @param pdfDoc 
   * @param columnHeaders 
   * @param columnWidths 
   */
  private defineReportColHeaders(pdfDoc: any, columnHeaders: any[], columnWidths: number[], leftMargin: number, topMargin: number) {
    let headerKeys: string[] = [];
    for (let property in columnHeaders) {
      headerKeys.push(property);
    }

    let lineHeight = pdfDoc.calculateLineHeight(headerKeys, columnWidths, columnHeaders);
    pdfDoc.setTextColor(HEADER_TEXT_COLOR);

    for (let i = 0; i < columnHeaders.length; i++) {
      pdfDoc.setFillColor(HEADER_COLOR);
      pdfDoc.rect(leftMargin, topMargin, columnWidths[i], lineHeight, 'F');
      pdfDoc.cell(leftMargin, topMargin, columnWidths[i], lineHeight, columnHeaders[i], 1, 'left')
      leftMargin = leftMargin + columnWidths[i];
    }

    return lineHeight;
  }

  /**
   * METODO PARA AGREGAR UNA PAGINA CON UN DISEÑO DE PLANTILLA PREDEFINIDO
   * AUTOR: FREDI ROMAN
   */
  private addPageWithTemplate(pdfDoc: any) {
    this.pageNumber++;
    pdfDoc.addPage('a4', this.orientation == this.LANDSCAPE ? 'l' : 'p');
    this.defineReportHeader(pdfDoc);
    this.defineReportFooter(pdfDoc);
  }

  /**
   * METODO PARA DEFINIR LA CABECERA DEL REPORTE A MOSTRAR
   * AUTOR: FREDI ROMAN
   */
  private defineReportFooter(pdfDoc: any) {
    let centerLeftMargin: any = pdfDoc.internal.pageSize.width / 2;
    centerLeftMargin = parseInt(centerLeftMargin);
    let bottomMargin = pdfDoc.internal.pageSize.height - 10;

    pdfDoc.setTextColor(NORMAL_TEXT_COLOR);
    pdfDoc.setFontSize(10);
    pdfDoc.text(this.pageNumber + "", centerLeftMargin, bottomMargin);
  }

  private defineReportTitle(pdfDoc: any, title: string) {
    let centerLeftMargin: any = pdfDoc.internal.pageSize.width / 2;
    centerLeftMargin = parseInt(centerLeftMargin);

    pdfDoc.setFontSize(14);
    pdfDoc.setTextColor(NORMAL_TEXT_COLOR);

    let textWidth = pdfDoc.getTextWidth(title);
    pdfDoc.text(title, centerLeftMargin - textWidth / 2, TOP_MARGIN);
  }

  /**
   * METODO PARA DEFINIR LA CABECERA DEL REPORTE A MOSTRAR
   * AUTOR: FREDI ROMAN
   */
  private defineReportHeader(pdfDoc: any, title?: string) {
    pdfDoc.addImage(this.base64Img, 'PNG', LEFT_MARGIN, 3);

    if (title) {
      this.defineReportTitle(pdfDoc, title);
    }
  }

  /**
   * METODO PARA INICIAR EL OBJETO QUE PERMITE CREAR LA APARIENCIA DEL REPORTE:
   * AUTOR: FREDI ROMAN
   */
  private initReportManager(base64Img: any, title: string, orientationOp: number) {
    var pdfDoc = new jsPDF(
      {
        orientation: orientationOp == this.LANDSCAPE ? 'l' : 'p',
        unit: 'mm',
        format: 'a4',
        hotfixes: [] // an array of hotfix strings to enable
      }
    );
    pdfDoc.setFont('Helvetica', 'bold');
    pdfDoc.setDrawColor(GENERIC_BORDER_COLOR);

    this.pageNumber = 1;
    this.base64Img = base64Img;
    this.orientation = orientationOp;
    this.defineReportHeader(pdfDoc, title);
    this.defineReportFooter(pdfDoc);

    return pdfDoc;
  }
}