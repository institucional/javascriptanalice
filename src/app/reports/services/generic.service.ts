import { Injectable } from "@angular/core";
import { FunctionService } from "../../services/functions/function.service";
import { WS } from "../../WebServices";
import { TAGS } from "../../TextLabels";
import { Nivel } from "../../models/nivel";
import * as lodash from 'lodash';

@Injectable()
export class GenericService {
    private sortBy: { property: string; position: number }[];
    private nivelesList: Nivel[];

    constructor(
        private _functionService: FunctionService
    ) { }

    /**
     * METODO PARA ORDERNAR LAS COLUMNAS DEL REPORTE:
     * AUTOR: FREDI ROMAN
     */
    public sortReportColumns(cols: { id: number, column: string, property: string, checked: boolean }[]) {
        this.sortBy = [
            { property: 'anio', position: 1 },
            { property: 'mes', position: 2 },
            { property: 'tipoGasto', position: 3 },
            { property: 'subtipoGasto', position: 4 },
            { property: 'resultado', position: 5 },
            { property: 'codEmpleado', position: 6 },
            { property: 'identificacion', position: 7 },
            { property: 'nombreCompleto', position: 8 },
            { property: 'empresa', position: 9 },
            { property: 'cargo', position: 10 },
        ];

        let pos = this.sortBy.length;
        for (let niv of this.nivelesList) {
            this.sortBy.push({ property: niv.nombre.toLowerCase(), position: pos });
            pos++;
        }

        this.sortBy.push({ property: 'custodio', position: pos });
        pos++;
        this.sortBy.push({ property: 'ciudad', position: pos });
        pos++;
        this.sortBy.push({ property: 'nvoucher', position: pos });
        pos++;
        this.sortBy.push({ property: 'automovil', position: pos });
        pos++;
        this.sortBy.push({ property: 'fechaServicio', position: pos });
        pos++;
        this.sortBy.push({ property: 'tarifa', position: pos });
        pos++;
        this.sortBy.push({ property: 'dac', position: pos });
        pos++;
        this.sortBy.push({ property: 'origen', position: pos });
        pos++;
        this.sortBy.push({ property: 'destino', position: pos });
        pos++;
        this.sortBy.push({ property: 'kmodometro', position: pos });
        pos++;
        this.sortBy.push({ property: 'tiempoespera', position: pos });
        pos++;
        this.sortBy.push({ property: 'autorizacion', position: pos });
        pos++;
        this.sortBy.push({ property: 'horaServicio', position: pos });
        pos++;
        this.sortBy.push({ property: 'horallegada', position: pos });
        pos++;
        this.sortBy.push({ property: 'horaocupado', position: pos });
        pos++;
        this.sortBy.push({ property: 'horafin', position: pos });
        pos++;
        this.sortBy.push({ property: 'tarifaEspecial', position: pos });
        pos++;
        this.sortBy.push({ property: 'diaFds', position: pos });
        pos++;

        for (let col of cols) {
            for (let sortRow of this.sortBy) {
                if (col.property == sortRow.property) {
                    col.id = sortRow.position;
                }
            }
        }
        cols = lodash.orderBy(cols, ['id'], ['asc']);
        return cols.slice();
    }

    /**
     * METODO PARA DEFINIR LAS COLUMAS DE LA TABLA SERVICIO PARA EL REPORTE DE SERVICIOS
     * AUTOR: FREDI ROMAN
     */
    public defineServiceColumns(): { id: number, column: string, property: string, checked: boolean }[] {
        return [
            { id: 1, column: 'Año', property: 'anio', checked: true },
            { id: 2, column: 'Mes', property: 'mes', checked: true },
            { id: 3, column: 'TIPO (nombre del gasto)', property: 'tipoGasto', checked: true },
            { id: 4, column: 'SUBTIPO (Consumo (+) / Descuento (-)/ Ajuste Descuento (+))', property: 'subtipoGasto', checked: true },
            { id: 5, column: 'Resultado (real / ajuste)', property: 'resultado', checked: true },
            { id: 6, column: 'Tarifa Especial', property: 'tarifaEspecial', checked: true },
            { id: 7, column: 'Día fin de semana', property: 'diaFds', checked: true },
            { id: 8, column: 'Custodio', property: 'custodio', checked: true },
            { id: 9, column: 'DAC', property: 'dac', checked: true },
            { id: 10, column: TAGS.reports.coorpServicesReport.servicio.nvoucher, property: 'nvoucher', checked: true },
            { id: 11, column: TAGS.reports.coorpServicesReport.servicio.automovil, property: 'automovil', checked: true },
            { id: 12, column: TAGS.reports.coorpServicesReport.servicio.fechaServicio, property: 'fechaServicio', checked: true },
            { id: 13, column: TAGS.reports.coorpServicesReport.servicio.horaServicio, property: 'horaServicio', checked: true },
            { id: 14, column: TAGS.reports.coorpServicesReport.servicio.tarifa, property: 'tarifa', checked: true },
            { id: 15, column: TAGS.reports.coorpServicesReport.servicio.origen, property: 'origen', checked: true },
            { id: 16, column: TAGS.reports.coorpServicesReport.servicio.destino, property: 'destino', checked: true },
            { id: 17, column: TAGS.reports.coorpServicesReport.servicio.kmodometro, property: 'kmodometro', checked: true },
            { id: 18, column: TAGS.reports.coorpServicesReport.servicio.autorizacion, property: 'autorizacion', checked: true },
            { id: 19, column: TAGS.reports.coorpServicesReport.servicio.horaaceptado, property: 'horaaceptado', checked: false },
            { id: 20, column: TAGS.reports.coorpServicesReport.servicio.horallegada, property: 'horallegada', checked: true },
            { id: 21, column: TAGS.reports.coorpServicesReport.servicio.horaocupado, property: 'horaocupado', checked: true },
            { id: 22, column: TAGS.reports.coorpServicesReport.servicio.horafin, property: 'horafin', checked: true },
            { id: 23, column: TAGS.reports.coorpServicesReport.servicio.tiempoespera, property: 'tiempoespera', checked: true },
            { id: 24, column: TAGS.reports.coorpServicesReport.servicio.forma_pago, property: 'formaPago', checked: false },
            { id: 25, column: TAGS.reports.coorpServicesReport.servicio.telefonoServicio, property: 'telefonoServicio', checked: false },
            { id: 26, column: TAGS.reports.coorpServicesReport.servicio.ciudad, property: 'ciudad', checked: true },
            { id: 27, column: TAGS.reports.coorpServicesReport.servicio.estado, property: 'estado', checked: false }
        ];
    }

    /**
     * METODO PARA DEFINIR LAS COLUMNAS DEL EMPLEADO PARA EL REPORTE DE SERVICIOS
     * AUTOR: FREDI ROMAN
     */
    public defineEmpColumns(nivelesList: Nivel[]): { id: number, column: string, property: string, checked: boolean }[] {
        this.nivelesList = nivelesList;
        let cols = [
            { id: 28, column: TAGS.reports.coorpServicesReport.empleado.codEmpleado, property: 'codEmpleado', checked: true },
            { id: 29, column: TAGS.reports.coorpServicesReport.empleado.codAlterno, property: 'codAlterno', checked: false },
            { id: 30, column: TAGS.reports.coorpServicesReport.empleado.identificacion, property: 'identificacion', checked: true },
            { id: 31, column: TAGS.reports.coorpServicesReport.empleado.primerNombre, property: 'primerNombre', checked: false },
            { id: 32, column: TAGS.reports.coorpServicesReport.empleado.segundoNombre, property: 'segundoNombre', checked: false },
            { id: 33, column: TAGS.reports.coorpServicesReport.empleado.primerApellido, property: 'primerApellido', checked: false },
            { id: 34, column: TAGS.reports.coorpServicesReport.empleado.segundoApellido, property: 'segundoApellido', checked: false },
            { id: 35, column: TAGS.reports.coorpServicesReport.empleado.nombreCompleto, property: 'nombreCompleto', checked: true },
            { id: 36, column: TAGS.reports.coorpServicesReport.empleado.celular, property: 'celular', checked: false },
            { id: 37, column: TAGS.reports.coorpServicesReport.empleado.telefonoConvencional, property: 'telefonoConvencional', checked: false },
            { id: 38, column: TAGS.reports.coorpServicesReport.empleado.correo, property: 'correo', checked: false },
            { id: 39, column: TAGS.reports.coorpServicesReport.empleado.fechaDesde, property: 'fechaDesde', checked: false },
            { id: 40, column: TAGS.reports.coorpServicesReport.empleado.fechaHasta, property: 'fechaHasta', checked: false },
            { id: 41, column: TAGS.reports.coorpServicesReport.empleado.estadoEmpleado, property: 'estadoEmpleado', checked: false },
            { id: 42, column: TAGS.reports.coorpServicesReport.empleado.cargo, property: 'cargo', checked: true },
        ];

        let nextId = cols[cols.length - 1].id + 1;
        let depaName: string;
        let splitted: string[];
        for (let i = 0; i < nivelesList.length; i++) {
            if (nivelesList[i].nombre != TAGS.general.totalPage) {
                depaName = nivelesList[i].nombre.toLowerCase();
                if (depaName.includes(' ')) {
                    splitted = depaName.split(' ');
                    depaName = '';
                    for (let value of splitted) {
                        if (depaName != '') {
                            depaName += ' ';
                        }
                        depaName += value.charAt(0).toUpperCase() + value.substring(1);
                    }
                } else {
                    depaName = depaName.charAt(0).toUpperCase() + depaName.substring(1);
                }

                cols.push({ id: nextId, column: depaName, property: depaName.toLowerCase(), checked: true });
                nextId++;
            }
        }
        cols.push({ id: nextId, column: TAGS.reports.coorpServicesReport.empleado.sucursal, property: 'sucursal', checked: false });
        nextId++;
        cols.push({ id: nextId, column: TAGS.reports.coorpServicesReport.empleado.empresa, property: 'empresa', checked: true });

        return cols;
    }

    /**
     * METODO PARA DEFINIR LAS COLUMNAS PARA LOS ATRIBUTOS DINAMICOS DEL EMPLEADO PARA EL REPORTE DE SERVICIOS
     * AUTOR: FREDI ROMAN
     */
    public defineDynaEmpColumns(idEmpresa: number) {
        if (idEmpresa) {
            return this._functionService.httpGetWithout(WS.dynaParams.get.replace('${enterpriseId}', idEmpresa + ''))
                .then((respData: any) => {
                    let cols: { id: number, column: string, property: string, checked: boolean }[] = [];
                    let val = 33;
                    for (let row of respData) {
                        cols.push({ id: val, column: row.etiqueta, property: row.id, checked: false });
                        val++;
                    }
                    return cols;
                });
        } else {
            return new Promise((resolve, reject) => {
                resolve([]);
            });
        }
    }
}