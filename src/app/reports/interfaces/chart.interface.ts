export interface ChartInterface {
    title: string;
    repLabels?: any[];
    repData?: any[];
    backColorData?: any[];
    borderColorData?: any[];
}