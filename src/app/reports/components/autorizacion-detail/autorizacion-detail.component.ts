import { Component, OnInit, ViewChild, ElementRef, Input, SimpleChanges } from '@angular/core';
import { FunctionService } from '../../../services/functions/function.service';
import { TAGS } from '../../../TextLabels';
import { Autorizacion } from '../../../models/autorizacion';

declare var $: any;

@Component({
  selector: 'autorizacion-detail',
  templateUrl: './autorizacion-detail.component.html',
  styleUrls: ['./autorizacion-detail.component.scss']
})
export class AutorizacionDetailComponent implements OnInit {

  @ViewChild('modalContainer') modalContainerRef: ElementRef;
  private modalContainerHtml: HTMLElement;
  public detalleAutorizacion: Autorizacion;

  @Input() showDetail: boolean;
  @Input() numAutorizacion: string;

  public labels: any

  constructor(
    private _functionService: FunctionService
  ) {
    this.labels = TAGS;
    this.detalleAutorizacion = new Autorizacion();
  }

  ngOnInit() {
    this.modalContainerHtml = this.modalContainerRef.nativeElement;
    this.modalContainerHtml.classList.add('d-none');
    this.onCloseModal();
  }

  /**
   * METODO PARA CERRAR EL MODAL DE CONFIRMACION DE CONTRASENIA
   * AUTOR: FREDI ROMAN
   */
  public closeModal() {
    let closeBtn: any = this.modalContainerHtml.querySelector(".btn-secondary");
    closeBtn.click();
  }

  /**
   * METODO QUE ESCUCHA EL EVENTO DE CIERRE DEL MODAL AL DAR CLICK FUERA DEL MISMO O EN EL BOTON CERRAR
   * AUTOR: FREDI ROMAN
   */
  public onCloseModal() {
    $(this.modalContainerHtml).on("hidden.bs.modal", () => {
      this.modalContainerHtml.classList.add('d-none');
    });
  }

  /**
   * METODO PARA ABRIR EL MODAL
   * AUTOR: FREDI ROMAN
   */
  public showModal() {
    $(this.modalContainerHtml).modal('show');
    this.modalContainerHtml.classList.remove('d-none');
  }

  /**
   * METODO PARA DETECTAR LOS CAMBIOS EN LOS PARAMETROS INPUT
   * AUTOR: FREDI ROMAN
   */
  ngOnChanges(changes: SimpleChanges) {
    for (let property in changes) {
      if (property == 'showDetail') {
        if (changes[property].currentValue == true) {
          this.showModal();
        }
      } else if (property == 'numAutorizacion') {
        if (changes[property].currentValue) {
          console.log("numAutorizacion", this.numAutorizacion);
        }
      }
    }
  }

}
