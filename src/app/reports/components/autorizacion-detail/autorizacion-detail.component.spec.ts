import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutorizacionDetailComponent } from './autorizacion-detail.component';

describe('AutorizacionDetailComponent', () => {
  let component: AutorizacionDetailComponent;
  let fixture: ComponentFixture<AutorizacionDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutorizacionDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutorizacionDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
