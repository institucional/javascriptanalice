import { Component, OnInit, ElementRef, ViewChild, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { SwitchInputInterface } from '../../../dynamic-form/interfaces/switch-input.interface';
import { SWITCH_SIZE } from '../../../dynamic-form/data/switch-size';
import { SW_LIST_TYPE } from '../../../dynamic-form/data/switch-list-type';

const OPEN_CLASS = 'fr-open-menu';
const DISPLAY_CLASS = 'fr-show';
const OVER_CLASS = 'fr-over-hidden';

@Component({
  selector: 'menu-consulta',
  templateUrl: './menu-consulta.component.html',
  styleUrls: ['./menu-consulta.component.scss']
})
export class MenuConsultaComponent implements OnInit, OnChanges {
  @Input() serviceColumns: { id: string, column: string, checked: boolean }[];
  @Input() employeeColumns: { id: string, column: string, checked: boolean }[];
  @Input() dynaEmpColumns: { id: string, column: string, checked: boolean }[];
  @Output() onColumSelectionChange: EventEmitter<boolean>;
  @ViewChild("menuContainer") menuContainerRef: ElementRef;
  private menuContainer: HTMLElement;
  private changedCols: boolean;

  public switchInsServ: SwitchInputInterface[];
  public switchInsEmp: SwitchInputInterface[];
  public switchInsDynaEmp: SwitchInputInterface[];
  public switchListType: number;

  constructor() {
    this.switchListType = SW_LIST_TYPE.checkbox;
    this.onColumSelectionChange = new EventEmitter<boolean>();
  }

  ngOnInit() {
    this.menuContainer = this.menuContainerRef.nativeElement;
  }

  /**
   * METODO PARA CAPTURAR EL TIPO DE CONTROL PRESUPUESTARIO ESCOGIDO POR EL USUARIO:
   * @param event ID QUE VIENE POR EL OUTPUT EVENT EMITTER DEL COMPONENTE HIJO
   */
  public getSelectedSwitchValue(event: string) {
    this.updateQueryColumns();
    this.changedCols = true;
  }

  /**
   * METODO PARA ACTUALIZAR LA LISTA DE COLUMNAS PARA MOSTRAR EN LA CONSULTA:
   * AUTOR: FREDI ROMAN
   */
  private updateQueryColumns() {
    let index;
    for (let i = 0; i < this.switchInsServ.length; i++) {
      index = this.serviceColumns.findIndex(col => col.id == this.switchInsServ[i].id);
      this.serviceColumns[index].checked = this.switchInsServ[i].checked;
    }
    for (let i = 0; i < this.switchInsEmp.length; i++) {
      index = this.employeeColumns.findIndex(col => col.id == this.switchInsEmp[i].id);
      this.employeeColumns[index].checked = this.switchInsEmp[i].checked;
    }
    for (let i = 0; i < this.switchInsDynaEmp.length; i++) {
      index = this.dynaEmpColumns.findIndex(col => col.id == this.switchInsDynaEmp[i].id);
      this.dynaEmpColumns[index].checked = this.switchInsDynaEmp[i].checked;
    }
  }

  /**
   * METODO PARA DEFINIR EL ARRAY DE TIPO SWITCH-INPUT PARA LAS COLUMNAS DE SERVICIO
   * AUTOR: FREDI ROMAN
   */
  private defineServSwitchInputs() {
    this.switchInsServ = [];
    for (let col of this.serviceColumns) {
      this.switchInsServ.push({ id: col.id, label: col.column, checked: col.checked, size: SWITCH_SIZE.tiny, customClass: 'switch-normal' });
    }
  }

  /**
   * METODO PARA DEFINIR EL ARRAY DE TIPO SWITCH-INPUT PARA LAS COLUMNAS DE EMPLEADO
   * AUTOR: FREDI ROMAN
   */
  private defineEmpSwitchInputs() {
    this.switchInsEmp = []
    for (let col of this.employeeColumns) {
      this.switchInsEmp.push({ id: col.id, label: col.column, checked: col.checked, size: SWITCH_SIZE.tiny, customClass: 'switch-normal' });
    }
  }

  /**
   * METODO PARA DEFINIR EL ARRAY DE TIPO SWITCH-INPUT PARA LAS COLUMNAS DE LOS ATRIBUTOS DINAMICOS PARA EMPLEADO
   * AUTOR: FREDI ROMAN
   */
  private defineDynaEmpSwitchInputs() {
    this.switchInsDynaEmp = [];
    for (let col of this.dynaEmpColumns) {
      this.switchInsDynaEmp.push({ id: col.id, label: col.column, checked: col.checked, size: SWITCH_SIZE.tiny, customClass: 'switch-normal' });
    }
  }

  /**
   * METODO PARA ABRIR O CERRAR EL MENU AL DAR CLICK EN EL BOTON DE APERTURA O EN LA PANTALLA DE CIERRE DEL MENU
   * AUTOR: FREDI ROMAN
   * 
   * @param event 
   * @param open 
   */
  public openHideMenu(event: any = null, open: boolean = true) {
    if (event) {
      event.preventDefault();
    }
    let menu = this.menuContainer.querySelector('.fr-side-menu');
    let backMenu = this.menuContainer.querySelector('.fr-cristal-back');
    let body = document.querySelector('body');
    if (open) {
      //body.classList.add(OVER_CLASS);
      menu.classList.add(OPEN_CLASS);
      backMenu.classList.add(DISPLAY_CLASS);
      backMenu.classList.add(OPEN_CLASS);
    } else {
      //body.classList.remove(OVER_CLASS);
      menu.classList.remove(OPEN_CLASS);
      backMenu.classList.remove(OPEN_CLASS);
      setTimeout(() => {
        backMenu.classList.remove(DISPLAY_CLASS);
        if (this.changedCols) {
          this.onColumSelectionChange.emit(this.changedCols);
          this.changedCols = false;
        }
      }, 500);
    }
  }

  /**
   * METODO PARA DETECTAR LOS CAMBIOS EN LOS VALORES @INPUT
   * AUTOR: FREDI ROMAN
   * @param changes 
   */
  ngOnChanges(changes: SimpleChanges) {
    for (let property in changes) {
      if (changes[property].currentValue) {
        switch (property) {
          case 'serviceColumns':
            this.defineServSwitchInputs();
            break;
          case 'employeeColumns':
            this.defineEmpSwitchInputs();
            break;
          case 'dynaEmpColumns':
            this.defineDynaEmpSwitchInputs();
            break;
        }
      }
    }
  }
}
