import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovilDetailComponent } from './movil-detail.component';

describe('MovilDetailComponent', () => {
  let component: MovilDetailComponent;
  let fixture: ComponentFixture<MovilDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovilDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovilDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
