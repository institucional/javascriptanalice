import { Component, OnInit, ElementRef, ViewChild, SimpleChanges, Input } from '@angular/core';
import { FunctionService } from '../../../services/functions/function.service';
import { WS } from '../../../WebServices';
import { TAGS } from '../../../TextLabels';
import { ColumnInterface } from '../../../fr-data-table/interfaces/column.interface';

declare var $: any;

@Component({
  selector: 'movil-detail',
  templateUrl: './movil-detail.component.html',
  styleUrls: ['./movil-detail.component.scss']
})
export class MovilDetailComponent implements OnInit {
  @ViewChild('modalContainer') modalContainerRef: ElementRef;
  private modalContainerHtml: HTMLElement;

  @Input() showDetail: boolean;
  @Input() numAutorizacion: string;

  public labels: any
  public movilDetailList: any[];
  public pageSize: number;
  public tableHeaders: ColumnInterface[];
  public tableRows: string[][];

  constructor(
    private _functionService: FunctionService
  ) {
    this.labels = TAGS;
    this.pageSize = 10;
  }

  ngOnInit() {
    this.modalContainerHtml = this.modalContainerRef.nativeElement;
    this.modalContainerHtml.classList.add('d-none');
    this.onCloseModal();
  }

  /**
   * METODO PARA FORMATEAR LOS DATOS DE LA TABLA
   * AUTOR: FREDI ROMAN
   */
  private defineTableData() {
    this.tableHeaders = [
      { label: "N°", columnWidth: 10 },
      { label: this.labels.reports.movilDetail.autorizacion, columnWidth: 60 },
      { label: this.labels.reports.movilDetail.descripcion, columnWidth: 250 },
      { label: this.labels.reports.movilDetail.direccion, columnWidth: 350 },
      { label: this.labels.reports.movilDetail.fechahora, columnWidth: 200 },
      { label: this.labels.reports.movilDetail.latitud, columnWidth: 60 },
      { label: this.labels.reports.movilDetail.longitud, columnWidth: 60 },
      { label: this.labels.reports.movilDetail.movil, columnWidth: 80 },
      { label: this.labels.reports.movilDetail.secuencial, columnWidth: 50 },
      { label: this.labels.reports.movilDetail.zona, columnWidth: 50 }
    ];

    this.tableRows = [];
    let row;
    for (let data of this.movilDetailList) {
      row = [
        data.rowNum,
        data.autorizacion,
        data.descripcion,
        data.direccion,
        data.fechahora,
        data.latitud,
        data.longitud,
        data.movil,
        data.secuencial,
        data.zona
      ];
      this.tableRows.push(row);
    }
  }

  /**
   * METODO PARA MOSTRAR EL DETALLE DE MOVIMIENTO DEL VEHICULO EN EL TRANSCURSO DE LA CARRERA
   * AUTOR: FREDI ROMAN
   */
  private loadMovilDetail() {
    if (this.numAutorizacion) {
      let url = WS.reportes.getMovilDetail.replace("${numAutorizacion}", this.numAutorizacion);
      this._functionService.httpGetWithout(url)
        .then((respData: any[]) => {
          let i = 1;
          for (let data of respData) {
            for (let property in data) {
              data[property] = data[property] == 'null' ? '' : data[property];
            }
            Object.defineProperty(data, "rowNum", { value: i });
            i++;
          }
          this.movilDetailList = respData;
          this.defineTableData();
        });
    }
  }

  /**
   * METODO PARA CERRAR EL MODAL DE CONFIRMACION DE CONTRASENIA
   * AUTOR: FREDI ROMAN
   */
  public closeModal() {
    let closeBtn: any = this.modalContainerHtml.querySelector(".btn-secondary");
    closeBtn.click();
  }

  /**
   * METODO QUE ESCUCHA EL EVENTO DE CIERRE DEL MODAL AL DAR CLICK FUERA DEL MISMO O EN EL BOTON CERRAR
   * AUTOR: FREDI ROMAN
   */
  public onCloseModal() {
    $(this.modalContainerHtml).on("hidden.bs.modal", () => {
      this.modalContainerHtml.classList.add('d-none');
    });
  }

  /**
   * METODO PARA ABRIR EL MODAL
   * AUTOR: FREDI ROMAN
   */
  public showModal() {
    $(this.modalContainerHtml).modal('show');
    this.modalContainerHtml.classList.remove('d-none');
  }

  /**
   * METODO PARA DETECTAR LOS CAMBIOS EN LOS PARAMETROS INPUT
   * AUTOR: FREDI ROMAN
   */
  ngOnChanges(changes: SimpleChanges) {
    for (let property in changes) {
      if (property == 'showDetail') {
        if (changes[property].currentValue == true) {
          this.showModal();
        }
      } else if (property == 'numAutorizacion') {
        if (changes[property].currentValue) {
          this.loadMovilDetail();
        }
      }
    }
  }

}
