import { Component, OnInit, Input, ViewChild, ElementRef, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { ChartInterface } from '../../interfaces/chart.interface';
import { Chart } from 'chart.js';
import { TableDataInterface } from '../../../fr-data-table/interfaces/table-data.interface';

@Component({
  selector: 'chart-report-view',
  templateUrl: './chart-report-view.component.html',
  styleUrls: ['./chart-report-view.component.scss']
})
export class ChartReportViewComponent implements OnInit, OnChanges {
  @Input() tableData: TableDataInterface;
  @Input() chartData: ChartInterface;
  @Input() viewIndex: number;
  @Input() reportHeader: { empresa: string; desde: string; hasta: string; formaPago: string; estado: string, departamento: string };
  @Output() onGraphicItemClick: EventEmitter<{ viewIndex: number; recordIndex: number }>;

  @ViewChild("repCanvasCont") repCanvasContRef: ElementRef;
  private repCanvasCont: HTMLElement;
  private canvas: any;

  public chart: any;
  public showChart: boolean;

  constructor() {
    this.onGraphicItemClick = new EventEmitter<{ viewIndex: number; recordIndex: number }>();
  }

  ngOnInit() {
    this.repCanvasCont = this.repCanvasContRef.nativeElement;
  }

  /**
   * METODO PARA OBTENER LA INFORMACION DE LA FILA DE LA TABLA DEL REPORTE, QUE SE HA DADO CLICK
   * AUTOR: FREDI ROMAN
   * @param event 
   */
  public getClickedRow(event: { index: number; row: string[] }) {
    this.onGraphicItemClick.emit({ viewIndex: this.viewIndex, recordIndex: event.index });
  }

  /**
   * METODO PARA CONTROLAR EL CLICK SOBRE LOS ELEMENTOS DEL GRAFICO
   * AUTOR: FREDI ROMAN
   */
  private handleChartEvent() {
    this.canvas.onclick = (evt) => {
      var activePoints = this.chart.getElementsAtEvent(evt);
      if (activePoints[0]) {
        var chartData = activePoints[0]['_chart'].config.data;
        var idx = activePoints[0]['_index'];

        var label = chartData.labels[idx];
        var value = chartData.datasets[0].data[idx];

        this.onGraphicItemClick.emit({ viewIndex: this.viewIndex, recordIndex: idx });
      }
    };
  }

  private defineCharts() {
    this.showChart = false;

    setTimeout(() => {
      this.showChart = true;

      setTimeout(() => {
        this.canvas = this.repCanvasCont.querySelector("canvas");
        this.chart = new Chart(this.canvas, {
          type: 'bar',
          data: {
            labels: this.chartData.repLabels,
            datasets: [{
              label: 'Valor Consumo',
              data: this.chartData.repData,
              backgroundColor: this.chartData.backColorData,
              borderColor: this.chartData.borderColorData,
              borderWidth: 1
            }]
          },
          options: {
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero: true
                }
              }]
            }
          }
        });

        this.handleChartEvent();
      }, 100);
    }, 200);
  }

  ngOnChanges(changes: SimpleChanges) {
    for (let property in changes) {
      if (changes[property].currentValue) {
        if (property == 'chartData') {
          this.chartData = changes[property].currentValue;
          this.defineCharts();
        }
      }
    }
  }
}
