import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartReportViewComponent } from './chart-report-view.component';

describe('ChartReportViewComponent', () => {
  let component: ChartReportViewComponent;
  let fixture: ComponentFixture<ChartReportViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartReportViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartReportViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
