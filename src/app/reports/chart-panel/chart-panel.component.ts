import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { GV } from '../../GeneralVars';
import { TAGS } from '../../TextLabels';
import { FunctionService } from '../../services/functions/function.service';
import { EmpresaService } from '../../services/component-services/empresa.service';
import { EmpresaControllerService } from '../../services/controllers/empresa-controller.service';
import { TimeInterface } from '../../dynamic-form/interfaces/time.interface';
import { Empresa } from '../../models/empresa';
import { WS } from '../../WebServices';
import { NgForm } from '@angular/forms';
import { ReportService } from '../services/report.service';
import { ChartInterface } from '../interfaces/chart.interface';
import { TableDataInterface } from '../../fr-data-table/interfaces/table-data.interface';
import { DecimalManager } from '../../tools/decimal-manager';
import { Nivel } from '../../models/nivel';
import { NivelService } from '../../services/component-services/nivel.service';

const BACK_COLOR = 'rgba(54, 162, 235, 0.2)';
const BORDER_COLOR = 'rgba(54, 162, 235, 1)';
const PAGE_SIZE = 5;

const PREV_CLASS = 'fr-prev';
const ACTIVE_CLASS = 'fr-active';
const NEXT_CLASS = 'fr-next';

const PREFIX_ID = 'rv-';

@Component({
  selector: 'app-chart-panel',
  templateUrl: './chart-panel.component.html',
  styleUrls: ['./chart-panel.component.scss']
})
export class ChartPanelComponent implements OnInit {
  @ViewChild("reportViews") reportViewRef: ElementRef;
  private reportView: HTMLElement;
  private nivelesList: Nivel[];

  public labels: any;
  public tiempoHasta: TimeInterface;
  public empresaList: Empresa[];
  public estadosCarrera: { key: string; value: string }[];
  public formasPago: { key: string; value: string }[];
  public ciudades: { key: string; value: string }[];
  public calDesde: any;
  public calHasta: any;
  public filter: { idEmpresa: any; fechaDesde: any; fechaHasta: any; estado: any; formaPago: any; ciudad: any; idDepaPadre?: any };
  public reportHeader: { empresa: string; ciudad: string; desde: string; hasta: string; formaPago: string; estado: string, nivelDepa: string, departamento: string };
  public consumoReportList: any[];
  public prefix: string;

  public reportViewsArray: {
    chartData: ChartInterface;
    tableData: TableDataInterface;
    reportData: any[];
    trasitionClass: string;
    dataParetId?: string;
    dataParetNivel?: string;
    dataParetDepa?: string;
  }[];

  constructor(
    private _functionService: FunctionService,
    private _empresaControllerService: EmpresaControllerService,
    private _empresaService: EmpresaService,
    private _reportService: ReportService,
    private _nivelService: NivelService
  ) {
    this.labels = TAGS;
    this.prefix = PREFIX_ID;
    this.initReportViewArray();
  }

  ngOnInit() {
    this.reportView = this.reportViewRef.nativeElement;

    this.loadInitData();
    this.resetForm();
    this.defineChartData(0);
  }

  /**
   * METODO PARA CARGAR LOS NIVELES DE UNA EMPRESA AL CAMBIAR DE SELECCION
   * AUTOR: FREDI ROMAN
   */
  public onEnterpriseChange() {
    this.loadNivelesData();
  }

  /**
   * METODO PARA INICIALIZAR EL ARRAY DE REPORT-VIEW
   * AUTOR: FREDI ROMAN
   */
  private initReportViewArray() {
    this.reportViewsArray = [{
      chartData: { title: 'Gráfica Cosumo' },
      tableData: {
        title: 'Resumen Consumo',
      },
      reportData: null,
      trasitionClass: ACTIVE_CLASS
    }];
  }

  /**
   * METODO PARA DEFINIR EL ENCABEZADO DEL REPORTE
   * AUTOR: FREDI ROMAN
   */
  private defineReportHeader() {
    this.reportHeader = {
      empresa: this.empresaList.find(emp => emp.id == this.filter.idEmpresa).nombreEmpresa,
      ciudad: this.ciudades.find(ciu => ciu.key == this.filter.ciudad).value,
      desde: this.filter.fechaDesde,
      hasta: this.filter.fechaHasta,
      formaPago: this.formasPago.find(fp => fp.key == this.filter.formaPago).value,
      estado: this.estadosCarrera.find(ec => ec.key == this.filter.estado).value,
      nivelDepa: null,
      departamento: null
    };
  }

  /**
   * METODO PARA RETROCEDER HACIA LA VISTA DE REPORTE ANTERIOR
   * AUTOR: FREDI ROMAN
   */
  public goBack(event: any, index: number) {
    event.preventDefault();
    this.reportHeader.nivelDepa = this.reportViewsArray[index - 1].dataParetNivel;
    this.reportHeader.departamento = this.reportViewsArray[index - 1].dataParetDepa;
    this.changeCarouselView(false, index);
    setTimeout(() => {
      this.reportViewsArray.splice(this.reportViewsArray.length - 1, 1);
    }, 950);
  }

  /**
   * METODO PARA CAMBIAR LA VISTA DEL CARRUSEL
   * AUTOR: FREDI ROMAN
   */
  private changeCarouselView(nextPrev: boolean, index?: number) {
    if (nextPrev) {
      let activeView = this.reportView.querySelector(".fr-carousel." + ACTIVE_CLASS);
      let firstNextView = this.reportView.querySelector(".fr-carousel." + NEXT_CLASS);
      activeView.classList.remove(ACTIVE_CLASS);
      activeView.classList.add(PREV_CLASS);
      firstNextView.classList.remove(NEXT_CLASS);
      firstNextView.classList.add(ACTIVE_CLASS);
    } else {
      if (index) {
        let activeView = this.reportView.querySelector("." + this.prefix + index + ".fr-carousel." + ACTIVE_CLASS);
        let firstPrevView = this.reportView.querySelector("." + this.prefix + (index - 1) + ".fr-carousel." + PREV_CLASS);
        activeView.classList.remove(ACTIVE_CLASS);
        activeView.classList.add(NEXT_CLASS);
        firstPrevView.classList.remove(PREV_CLASS);
        firstPrevView.classList.add(ACTIVE_CLASS);
      }
    }
  }

  /**
   * METODO PARA OBTENER EL ID DEL ITEM SELECCIOANDO DEL GRAFICO
   * AUTOR: FREDI ROMAN
   * @param event 
   */
  public getItemId(event: { viewIndex: number, recordIndex: number }) {
    if (this.reportViewsArray.length + 1 <= this.nivelesList.length) {
      let rowItem = this.reportViewsArray[event.viewIndex].reportData[event.recordIndex];
      if (parseFloat(rowItem.valor)) {
        rowItem.nombreNivel = rowItem.nombreNivel.charAt(0) + rowItem.nombreNivel.substring(1).toLowerCase();
        if (rowItem.nombreNivel.charAt(rowItem.nombreNivel.length - 1) == 's') {
          rowItem.nombreNivel = rowItem.nombreNivel.substring(0, rowItem.nombreNivel.length - 1);
        }
        if (rowItem.nombreNivel.charAt(rowItem.nombreNivel.length - 1) == 'e') {
          rowItem.nombreNivel = rowItem.nombreNivel.substring(0, rowItem.nombreNivel.length - 1);
        }

        this.reportViewsArray.push({
          chartData: { title: 'Gráfica Servicios' },
          tableData: { title: 'Resumen Servicios' },
          reportData: null,
          trasitionClass: NEXT_CLASS,
          dataParetId: rowItem.idDepartamento,
          dataParetNivel: rowItem.nombreNivel,
          dataParetDepa: rowItem.nombreDepa
        });

        setTimeout(() => {
          this.changeCarouselView(true);
          setTimeout(() => {
            this.reportHeader.departamento = rowItem.nombreDepa;
            this.reportHeader.nivelDepa = rowItem.nombreNivel;
            this.filter.idDepaPadre = rowItem.idDepartamento;
            this.getReportData(true, this.reportViewsArray.length - 1);
          }, 1000);
        }, 100);
      }
    }
  }

  private defineChartData(index: number) {
    this.reportViewsArray[index].chartData = null;

    setTimeout(() => {
      this.reportViewsArray[index].chartData = {
        title: 'Gráfica Servicios',
        repLabels: [], repData: [], backColorData: [], borderColorData: []
      };

      if (this.consumoReportList) {
        for (let data of this.consumoReportList) {
          this.reportViewsArray[index].chartData.repLabels.push(data.nombreDepa);
          this.reportViewsArray[index].chartData.repData.push(data.valor);
          this.reportViewsArray[index].chartData.backColorData.push(BACK_COLOR);
          this.reportViewsArray[index].chartData.borderColorData.push(BORDER_COLOR);
        }
      }
    });
  }

  /**
   * METODO PARA FORMATEAR LOS DATOS DE LA TABLA
   * AUTOR: FREDI ROMAN
   */
  private defineTableData(index: number) {
    let rowList: string[][] = [];
    let row;
    let total: any = 0;
    for (let data of this.consumoReportList) {
      row = [
        data.rowNum,
        data.nombreDepa,
        data.nombreNivel,
        '$ ' + data.valor,
      ];
      total += parseFloat(data.valor);
      rowList.push(row);
    }
    total = DecimalManager.precise_round(total, 2);

    this.reportViewsArray[index].tableData = null;
    setTimeout(() => {
      this.reportViewsArray[index].tableData = {
        title: 'Resumen Servicios',
        tableHeaders: [
          { label: "N°", columnWidth: 20 },
          { label: this.nivelesList[index].nombre.charAt(0) + this.nivelesList[index].nombre.substring(1).toLowerCase(), columnWidth: 200 },
          { label: this.labels.reports.consumoReport.nivel, columnWidth: 200 },
          { label: this.labels.reports.consumoReport.valor, columnWidth: 100 },
        ],
        tableRows: rowList,
        tableFooter: [{ label: 'Total', colspan: '3' }, { label: '$ ' + total }],
        pageSize: PAGE_SIZE
      }
    });
  }

  /**
   * METODO PARA PROCESAR EL SUBMIT DEL FORMULARIO PRINCIPAL
   * AUTOR: FREDI ROMAN
   */
  public onSubmit(showLoading: boolean = true) {
    if (this.calDesde && this.calHasta) {
      this.filter.fechaDesde = this.calDesde.year + "-" + (this.calDesde.month < 10 ? "0" + this.calDesde.month : this.calDesde.month) + "-" + (this.calDesde.day < 10 ? "0" + this.calDesde.day : this.calDesde.day);
      this.filter.fechaHasta = this.calHasta.year + "-" + (this.calHasta.month < 10 ? "0" + this.calHasta.month : this.calHasta.month) + "-" + (this.calHasta.day < 10 ? "0" + this.calHasta.day : this.calHasta.day);
    }
    delete this.filter.idDepaPadre;

    this.initReportViewArray();
    this.defineReportHeader();
    this.getReportData(showLoading, 0);
  }

  /**
   * METODO PARA OBTENER EL REPORTE DE CONSUMO LUEGO DE APLICAR EL FILTRO
   */
  private getReportData(showLoading: boolean = true, index: number) {
    let url = WS.reportes.getReporteConsumo;
    let stringifyFilter = JSON.stringify(this.filter);

    let reqFilter = { key: 'report-filter', value: stringifyFilter };
    this._reportService.httpGetWithHeaders(url, reqFilter, showLoading)
      .then((respData) => {
        let i = 1;
        for (let data of respData) {
          for (let property in data) {
            data[property] = data[property] == 'null' ? '' : data[property];
          }
          Object.defineProperty(data, "rowNum", { value: i, writable: true });
          i++;
        }
        this.consumoReportList = respData;
        this.reportViewsArray[index].reportData = JSON.parse(JSON.stringify(this.consumoReportList));
        this.defineTableData(index);
        this.defineChartData(index);
      });
  }

  /**
   * METODO PARA RESETEAR LOS ELEMENTOS DEL FORMULARIO
   * AUTOR: FREDI ROMAN
   */
  public resetForm(ngForm: NgForm = null) {
    if (ngForm) {
      //REF: https://stackoverflow.com/questions/50197347/how-to-reset-only-specific-fields-of-form-in-angular-5
      ngForm.form.controls['calenDesde'].reset();
      ngForm.form.controls['calenHasta'].reset();
      ////
    }
    let initDate: any = new Date();
    initDate = initDate.getFullYear() + '-' + (initDate.getMonth() + 1 < 10 ? '0' + (initDate.getMonth() + 1) : initDate.getMonth() + 1) + '-01';
    let endDate = initDate.split('-');
    endDate = (parseInt(endDate[1]) == 12 ? (parseInt(endDate[0]) + 1) : endDate[0]) + '-' + (parseInt(endDate[1]) == 12 ? '01' : (parseInt(endDate[1]) + 1 < 10 ? '0' + (parseInt(endDate[1]) + 1) : parseInt(endDate[1]) + 1)) + '-01';

    if (!this.filter) {
      this.filter = {
        idEmpresa: null,
        fechaDesde: initDate,
        fechaHasta: endDate,
        estado: null,
        formaPago: null,
        ciudad: null,
      };
    } else {
      this.filter.fechaDesde = initDate;
      this.filter.fechaHasta = endDate;
      this.filter.estado = this.estadosCarrera ? this.estadosCarrera[this.estadosCarrera.length - 1].key : null;
      this.filter.formaPago = this.formasPago ? this.formasPago[0].key : null;
      this.filter.ciudad = this.ciudades ? this.ciudades[0].key : null;
      this.filter.idDepaPadre = null;
    }

    initDate = initDate.split('-');
    this.calDesde = { year: parseInt(initDate[0]), month: parseInt(initDate[1]), day: parseInt(initDate[2]) };

    endDate = endDate.split('-');
    this.calHasta = { year: parseInt(endDate[0]), month: parseInt(endDate[1]), day: parseInt(endDate[2]) };
  }

  /**
   * METODO PARA CARGAR LA INFORMACION DE LOS NIVELES DE UNA EMPRESA
   * AUTOR: FREDI ROMAN
   */
  private loadNivelesData() {
    if (this.filter.idEmpresa) {
      this._functionService.httpGet(WS.nivel.get.replace("${enterpriseId}", this.filter.idEmpresa + ""))
        .then((responseList: any) => {
          this.nivelesList = this._nivelService.createNivelList(responseList);
          this.onSubmit(false);
        });
    } else {
    }
  }

  /**
   * METODO PARA CARGAR LOS DATOS DE INICIO, DESDE EL BACKEND, PARA MOSTRARLO EN EL DOM 
   * AUTOR: FREDI ROMAN
   */
  private loadInitData() {
    this._empresaControllerService.getEmpresaListController()
      .then((empresaData) => {
        this.empresaList = this._empresaService.createEmpresaList(empresaData);
        this.filter.idEmpresa = this.empresaList[0] ? this.empresaList[0].id : null;

        let urlArray = [
          WS.findSubCatByCatId.replace("${catId}", GV.catalogos.estadosCarreras),
          WS.findSubCatByCatId.replace("${catId}", GV.catalogos.formasPago),
          WS.findSubCatByCatId.replace("${catId}", GV.catalogos.ciudadFast)
        ];
        this._functionService.httpObservableGet(urlArray)
          .then((respDataList: any[]) => {
            this.estadosCarrera = [];
            for (let data of respDataList[0].content) {
              this.estadosCarrera.push({ key: data.id, value: data.nomSubCatalogo });
            }
            this.filter.estado = this.estadosCarrera[this.estadosCarrera.length - 1].key;

            this.formasPago = [];
            for (let data of respDataList[1].content) {
              this.formasPago.push({ key: data.id, value: data.nomSubCatalogo });
            }
            this.filter.formaPago = this.formasPago[0].key;

            this.ciudades = [];
            for (let data of respDataList[2].content) {
              this.ciudades.push({ key: data.id, value: data.nomSubCatalogo });
            }
            this.filter.ciudad = this.ciudades[0].key;

            this.loadNivelesData();
          });
      });
  }
}
