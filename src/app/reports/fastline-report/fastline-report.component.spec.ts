import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FastlineReportComponent } from './fastline-report.component';

describe('FastlineReportComponent', () => {
  let component: FastlineReportComponent;
  let fixture: ComponentFixture<FastlineReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FastlineReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FastlineReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
