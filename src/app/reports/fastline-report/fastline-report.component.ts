import { Component, OnInit } from '@angular/core';
import { WS } from '../../WebServices';
import { FunctionService } from '../../services/functions/function.service';
import { EmpresaService } from '../../services/component-services/empresa.service';
import { Empresa } from '../../models/empresa';
import { TAGS } from '../../TextLabels';

@Component({
  selector: 'app-fastline-report',
  templateUrl: './fastline-report.component.html',
  styleUrls: ['./fastline-report.component.scss']
})
export class FastlineReportComponent implements OnInit {
  public labels: any;
  public empresaList: Empresa[];
  public mainEmpresaId: number;

  constructor(
    private _functionService: FunctionService,
    private _empresaService: EmpresaService
  ) {
    this.labels = TAGS;
  }

  ngOnInit() {
    this.loadInitData();
  }

  /**
   * METODO PARA CARGAR LOS DATOS DE INICIO, DESDE EL BACKEND, PARA MOSTRARLO EN EL DOM 
   * AUTOR: FREDI ROMAN
   */
  public loadInitData() {
    const array = [WS.empresa.empByState];
    this._functionService.httpObservableGet(array).then((responseList) => {
      this.empresaList = this._empresaService.createEmpresaList(responseList[0].content);
      this.mainEmpresaId = this.empresaList[0].id;
    });
  }
}
