import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PerfilComponent } from './perfil.component';
import { TAGS } from '../../TextLabels';

@Injectable()
export class PerfilService {

  constructor(private modalService: NgbModal) { }

  public confirm(
    title: string,
    btnOkText: string = TAGS.general.validate.toUpperCase(),
    btnCancelText: string = TAGS.general.cancel.toUpperCase(),
    dialogSize: 'sm'|'lg' = 'sm'): Promise<boolean> {
    const modalRef = this.modalService.open(PerfilComponent, { size: dialogSize });
    modalRef.componentInstance.title = title;
    modalRef.componentInstance.btnOkText = btnOkText;
    modalRef.componentInstance.btnCancelText = btnCancelText;

    return modalRef.result;
  }

}
