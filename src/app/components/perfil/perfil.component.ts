import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {FunctionService} from '../../services/functions/function.service';
import {GV} from '../../GeneralVars';
import {WS} from '../../WebServices';
import {TAGS} from '../../TextLabels'
import {ToastrService} from 'ngx-toastr';
import {AppService} from '../../app.service';
import {HttpClient} from '@angular/common/http';
import {SpinnerDialogService} from '../../components/spinner-dialog/spinner-dialog.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: [
    '../../../vendor/libs/ngx-toastr/ngx-toastr.scss',
    './perfil.component.css'
  ],
  encapsulation: ViewEncapsulation.None
})
export class PerfilComponent implements OnInit {

  @Input() title: string;
  @Input() btnOkText: string;
  @Input() btnCancelText: string;

  constructor(
    private activeModal: NgbActiveModal,
    private functionService: FunctionService,
    public toastrService: ToastrService,
    private appService: AppService,
    private http: HttpClient,
    private spinner: SpinnerDialogService
  ) { }

  banMail = false;
  banCell = false;
  banId = false;
  banTerm = false;

  banNombre = false;
  banCorreo = false;
  banCelular = false;

  nombre = '';
  correo = '';
  celular = '';
  identificacion = '';
  tipoIdentificacion = '';

  tiposIdentificacion = [];
  intTIpoID = 0;

  labels = null;
  aceptTerms = false;

  pin = '';

  options_notitications = {
    tapToDismiss: true,
    closeButton: false,
    progressBar: true,
    positionClass: 'toast-top-right',
    rtl: this.appService.isRTL
  };

  ngOnInit() {
    this.labels = TAGS;
    let dataSesion = localStorage.getItem('userToken');
    dataSesion = this.functionService.decode(dataSesion);

    const array = [WS.employees.identTypesLogin];
    this.functionService.httpObservableGet(array).then((responseList) => {
      this.tiposIdentificacion = responseList[0].content;
      if (dataSesion['valid_cell'] != 1) {
        this.banCell = false;
      } else {
        this.banCell = true;
      }
      if (dataSesion['valid_email'] != 1) {
        this.banMail = false;
      } else {
        this.banMail = true;
      }
      if (dataSesion['acept_terms'] != 1) {
        this.banTerm = false;
      } else {
        this.banTerm = true;
        this.aceptTerms = true;
      }
      if (dataSesion['identification'] == null || dataSesion['identification'] == '') {
        this.banId = false;
        this.tipoIdentificacion = this.tiposIdentificacion[0].id;
      } else {
        this.banId = true;
        this.tipoIdentificacion = dataSesion['tipo_id'];
      }
      this.identificacion = dataSesion['identification'];
      this.banNombre = true;
      this.nombre = dataSesion['user_name'];
      if (dataSesion['email'] == null || dataSesion['email'] == '') {
        this.banCorreo = false;
      } else {
        this.banCorreo = true;
      }
      this.correo = dataSesion['email'];
      if (dataSesion['cell_phone'] == null || dataSesion['cell_phone'] == '') {
        this.banCelular = false;
      } else {
        this.banCelular = true;
      }
      this.celular = dataSesion['cell_phone'];
    });
  }

  validarCorreo() {
    if (this.correo == '' || this.correo == null) {
      this.toastrService['error']('Porfavor ingrese su correo electrónico.', this.appService.pageTitle, this.options_notitications);
      return;
    }

    if (this.celular == '' || this.celular == null) {
      this.toastrService['error']('Porfavor ingrese su celular.', this.appService.pageTitle, this.options_notitications);
      return;
    }

    if (this.identificacion == '' || this.identificacion == null) {
      this.toastrService['error']('Porfavor ingrese su identificación.', this.appService.pageTitle, this.options_notitications);
      return;
    }

    if (!this.aceptTerms) {
      this.toastrService['error']('Debe aceptar los términos y condiciones para continuar.',
        this.appService.pageTitle, this.options_notitications);
      return;
    }
    switch (this.intTIpoID) {
      case 0:
        if (!this.functionService.validateIdentificationCard(this.identificacion, 0)) {
          this.toastrService['error'](this.labels.messages.invalidCardId, this.appService.pageTitle, this.options_notitications);
          return;
        }
        break;
      case 1:
        if (!this.functionService.validateIdentificationCard(this.identificacion, 1)) {
          this.toastrService['error'](this.labels.messages.invalidRuc, this.appService.pageTitle, this.options_notitications);
          return;
        }
        break;
    }

    let url = WS.login.register;
    url = url.replace('${ban}', 'true');
    const user = {
      email: this.correo,
      password: 'val',
      firstName: this.nombre,
      lastName: this.nombre,
      phone: this.celular,
      fecult: this.functionService.getToday().toString(),
      ipMaquina: 'localhost',
      tipoIdentificacion: this.tipoIdentificacion,
      identificacion: this.identificacion
    };
    this.spinner.openSpinner();
    this.http.post(url, user).subscribe(res => {
      this.toastrService['success']('Correo enviado de manera correcta.', this.appService.pageTitle, this.options_notitications);
      localStorage.removeItem('userToken');
      localStorage.removeItem('dataSesion');
      this.spinner.closeSpinner();
    }, err => {
      this.toastrService['error']('Error al momento de mandar el correo.', this.appService.pageTitle, this.options_notitications);
      this.spinner.closeSpinner();
    });
  }

  sendPin() {
    let url = WS.login.sendPin;
    url = url.replace('${cell}', this.celular);
    this.spinner.openSpinner();
    this.http.get(url).subscribe(res => {
      this.toastrService['success']('PIN enviado de manera correcta.', this.appService.pageTitle, this.options_notitications);
      this.spinner.closeSpinner();
    }, err => {
      this.toastrService['error']('Error al momento de mandar el PIN.', this.appService.pageTitle, this.options_notitications);
      this.spinner.closeSpinner();
    });
  }

  validarCelular() {
    if (this.correo == '' || this.correo == null) {
      this.toastrService['error']('Porfavor ingrese su correo electrónico.', this.appService.pageTitle, this.options_notitications);
      return;
    }

    if (this.celular == '' || this.celular == null) {
      this.toastrService['error']('Porfavor ingrese su celular.', this.appService.pageTitle, this.options_notitications);
      return;
    }

    if (this.identificacion == '' || this.identificacion == null) {
      this.toastrService['error']('Porfavor ingrese su identificación.', this.appService.pageTitle, this.options_notitications);
      return;
    }

    if (!this.aceptTerms) {
      this.toastrService['error']('Debe aceptar los términos y condiciones para continuar.',
        this.appService.pageTitle, this.options_notitications);
      return;
    }
    switch (this.intTIpoID) {
      case 0:
        if (!this.functionService.validateIdentificationCard(this.identificacion, 0)) {
          this.toastrService['error'](this.labels.messages.invalidCardId, this.appService.pageTitle, this.options_notitications);
          return;
        }
        break;
      case 1:
        if (!this.functionService.validateIdentificationCard(this.identificacion, 1)) {
          this.toastrService['error'](this.labels.messages.invalidRuc, this.appService.pageTitle, this.options_notitications);
          return;
        }
        break;
    }

    if (this.pin == '') {
      this.toastrService['error']('Debe ingresar el PIN para continuar.', this.appService.pageTitle, this.options_notitications);
      return;
    }
    let url = WS.login.validatePin;
    url = url.replace('${cell}', this.celular);
    url = url.replace('${pin}', this.pin);
    url = url.replace('${correo}', this.correo);
    url = url.replace('${tipoid}', this.tipoIdentificacion);
    url = url.replace('${identificacion}', this.identificacion);
    this.spinner.openSpinner();
    this.http.get(url).subscribe(res => {
      if (res['status'] == 0) {
        this.toastrService['success']('Proceso realizado de manera correcta.', this.appService.pageTitle, this.options_notitications);
        localStorage.removeItem('userToken');
        localStorage.removeItem('dataSesion');
      } else {
        this.toastrService['error']('El PIN ingresado es incorrecto.', this.appService.pageTitle, this.options_notitications);
      }
      this.spinner.closeSpinner();
    }, err => {
      this.toastrService['error']('Error al momento de validar el PIN.', this.appService.pageTitle, this.options_notitications);
      this.spinner.closeSpinner();
    });
  }

  onChangeDate(event) {
    this.tipoIdentificacion = event.target.value;
    for (let i = 0; i < this.tiposIdentificacion.length; i++) {
      if (event.target.value.toString() === this.tiposIdentificacion[i].id.toString()) {
        switch (this.tiposIdentificacion[i].nomSubCatalogo.toUpperCase()) {
          case GV.tiposIdentificacion.cedula:
            this.intTIpoID = 0;
            break;
          case GV.tiposIdentificacion.ruc:
            this.intTIpoID = 1;
            break;
          default:
            this.intTIpoID = 2;
        }
      }
    }
  }

  public decline() {
    const result = {
      boolean: false
    };
    this.activeModal.close(result);
  }

  public accept() {
    const result = {
      boolean: true
    };
    this.activeModal.close(result);
  }

  public dismiss() {
    this.activeModal.dismiss();
  }

}
