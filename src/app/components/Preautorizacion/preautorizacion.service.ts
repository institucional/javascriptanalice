import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PreautorizacionComponent } from './preautorizacion.component';
import { TAGS } from '../../TextLabels';

@Injectable()
export class PreautorizacionService {

  constructor(private modalService: NgbModal) { }

  modal = null;

  public confirm(
    title: string,
    idServicio: string,
    nombre: string,
    origen: string,
    destino: string,
    fecha: string,
    hora: string,
    btnOkText: string = TAGS.general.save.toUpperCase(),
    btnCancelText: string = TAGS.general.cancel.toUpperCase(),
    dialogSize: 'sm'|'lg' = 'sm'): Promise<boolean> {
    const modalRef = this.modalService.open(PreautorizacionComponent, { size: dialogSize });
    modalRef.componentInstance.title = title;
    modalRef.componentInstance.idServicio = idServicio;
    modalRef.componentInstance.nombre = nombre;
    modalRef.componentInstance.origen = origen;
    modalRef.componentInstance.destino = destino;
    modalRef.componentInstance.fecha = fecha;
    modalRef.componentInstance.hora = hora;
    modalRef.componentInstance.btnOkText = btnOkText;
    modalRef.componentInstance.btnCancelText = btnCancelText;
    this.modal = modalRef;

    return modalRef.result;
  }

  public close() {
    this.modal.close();
  }

}
