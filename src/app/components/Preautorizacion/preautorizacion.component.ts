import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-calificaviaje',
  templateUrl: './preautorizacion.component.html',
  styleUrls: ['preautorizacion.component.css']
})
export class PreautorizacionComponent implements OnInit {

  @Input() title: string;
  @Input() idServicio: string;
  @Input() nombre: string;
  @Input() origen: string;
  @Input() destino: string;
  @Input() fecha: string;
  @Input() hora: string;
  @Input() btnOkText: string;
  @Input() btnCancelText: string;

  constructor(private activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  public decline(id_val) {
    const value = (<HTMLInputElement>document.getElementById(id_val)).value;
    const result = {
      bandera: false,
      idServicio: this.idServicio,
      observacion: value
    };
    this.activeModal.close(result);
  }

  public accept(id_val) {
    const value = (<HTMLInputElement>document.getElementById(id_val)).value;
    const result = {
      bandera: true,
      idServicio: this.idServicio,
      observacion: value
    };
    this.activeModal.close(result);
  }

  public dismiss() {
    this.activeModal.dismiss();
  }

}
