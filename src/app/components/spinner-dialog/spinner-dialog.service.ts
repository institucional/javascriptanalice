import { Injectable } from '@angular/core';
import { SpinnerDialogComponent } from './spinner-dialog.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Injectable()
export class SpinnerDialogService {
  modalRef = null;
  constructor(private modalService: NgbModal) { }

  public openSpinner(
    color = '#F9B100',
    bdColor = 'rgba(51,51,51,0.8)',
    size = 'medium',
    type = 'ball-spin'
  ) {
    setTimeout(() => {
      this.modalRef = this.modalService.open(SpinnerDialogComponent, { size: 'lg', backdrop: 'static' });
      this.modalRef.componentInstance.color = color;
      this.modalRef.componentInstance.bdColor = bdColor;
      this.modalRef.componentInstance.size = size;
      this.modalRef.componentInstance.type = type;
    });
  }

  public closeSpinner() {
    setTimeout(() => {
      this.modalRef.close();
    });
  }
}
