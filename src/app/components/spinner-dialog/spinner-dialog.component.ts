import {Component, Input, OnInit} from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-spinner-dialog',
  templateUrl: './spinner-dialog.component.html'
})
export class SpinnerDialogComponent implements OnInit {

  @Input() bdColor: string;
  @Input() size: string;
  @Input() color: string;
  @Input() type: string;

  constructor(
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.initSpinner();
  }

  public dismiss() {
    this.hideSpinner();
  }

  initSpinner() {
    this.spinner.show();
  }

  hideSpinner() {
    this.spinner.hide();
  }
}
