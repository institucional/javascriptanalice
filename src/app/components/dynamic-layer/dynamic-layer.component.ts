/**
 * AUTOR: FREDI ROMAN
 */
import { Component, OnInit, OnDestroy, ViewChild, ViewContainerRef, ComponentFactoryResolver, ComponentRef, ComponentFactory } from '@angular/core';
import { ModalService } from '../../services/modal-service/modal.service';
import { Subscription } from 'rxjs';
import { Modal } from '../../interfaces/modal.interface';
import { ModalComponent } from '../modal/modal.component';
import { DynaComponentService } from '../../services/component-services/dyna-component.service';

const MODAL = 1;

@Component({
  selector: 'dynamic-layer',
  templateUrl: './dynamic-layer.component.html',
  styleUrls: ['./dynamic-layer.component.scss']
})
export class DynamicLayerComponent implements OnInit, OnDestroy {
  @ViewChild("dynamicLayer", { read: ViewContainerRef }) dynamicLayer: ViewContainerRef;
  private subscriber: Subscription;

  constructor(
    private _modalService: ModalService,
    private _dynaComponentService: DynaComponentService
  ) { }

  ngOnInit() {
    this.listenToNewDynamicContent();
  }

  /**
   * METODO PARA ESCUCHAR LA SOLICITUD DE APERTURA DE UN NUEVO COMPONENTE DINAMICO
   * AUTOR: FREDI ROMAN
   */
  private listenToNewDynamicContent() {
    this.subscriber = this._modalService.$openModal.subscribe((modalData: Modal) => {
      if (modalData) {
        this.addDynamicComponent(MODAL, modalData);
      }
    });
  }

  /**
   * METODO PARA AGREGAR UN COMPONENTE DINAMICAMENTE EN EL DOM
   * AUTOR: FREDI ROMAN
   */
  private addDynamicComponent(type: number, componentData: any) {
    switch (type) {
      case MODAL:
        this._dynaComponentService.addDynamicComponent(ModalComponent, this.dynamicLayer, componentData);
        break;
    }
  }

  ngOnDestroy() {
    this.subscriber.unsubscribe();
  }
}
