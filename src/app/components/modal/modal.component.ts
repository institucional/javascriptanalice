/**
 * AUTOR: FREDI ROMAN
 */
import { Component, OnInit, ComponentRef, ViewChild, ElementRef, ViewContainerRef, OnDestroy, AfterViewInit } from '@angular/core';
import { Modal } from '../../interfaces/modal.interface';
import { BTN_CLICK_ACTIONS } from '../../data/button-click-actions';
import { BODY_TYPES } from '../../data/modal-body-type';
import { DynaComponentService } from '../../services/component-services/dyna-component.service';
import { ModalService } from '../../services/modal-service/modal.service';
import { Subscription } from 'rxjs';

declare var $: any;

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild("modalContainer") modalContainer: ElementRef;
  @ViewChild("dynaTemplate", { read: ViewContainerRef }) bodyDynaTemplate: ViewContainerRef;
  private nativeModalContainer: HTMLElement;
  private subscriber: Subscription;

  public _selfReference: ComponentRef<ModalComponent>;
  public _dynamicData: Modal;
  public btnActions: any;
  public componentTypes: any;
  public showBody: boolean;

  constructor(
    private _dynaComponentService: DynaComponentService,
    private _modalService: ModalService,
  ) {
    this.componentTypes = BODY_TYPES;
    this.btnActions = BTN_CLICK_ACTIONS;
    this.showBody = true;
  }

  ngOnInit() {
    this.listenToRefreshBody();
  }
  
  ngAfterViewInit() {
    setTimeout(() => {
      this.initModal();
    }, 50);
  }

  /**
   * METODO PARA ESCUCHAR CUANDO EL COMPONENTE BASE DESEA QUE SE REFRESQUE EL BODY DEL MODAL
   * AUTOR: FREDI ROMAN
   */
  private listenToRefreshBody() {
    this.subscriber = this._modalService.refreshModalBody$.subscribe((refreshData: { baseComponentId: string, refresh: boolean }) => {
      if (refreshData && refreshData.baseComponentId == this._dynamicData.baseComponentId) {
        this.showBody = false;
        setTimeout(() => {
          this.showBody = true;
          setTimeout(() => {
            this.showDynaComponent();
          });
        }, 50);
      }
    });
  }

  /**
   * METODO PARA DETECTAR EL CLICK DEL BOTON ESCOGIDO
   * AUTOR: FREDI ROMAN
   */
  public onButtonClick(index: number) {
    if (this._dynamicData.btnList[index].onClickAction != this.btnActions.close) {
      for (let property in this.btnActions) {
        if (this.btnActions[property] == this._dynamicData.btnList[index].onClickAction) {
          console.log("[MODAL COMPONENT] - READY TO LET THE BUTTON ACTION GET PROPAGATED, THE ACTION IS: ", property);
          this._modalService.onModalBtnClick({ baseComponentId: this._dynamicData.baseComponentId, actionType: this.btnActions[property] });
        }
      }
    }
  }

  /**
   * METODO QUE ESCUCHA EL EVENTO DE CIERRE DEL MODAL AL DAR CLICK FUERA DEL MISMO O EN EL BOTON CERRAR
   * AUTOR: FREDI ROMAN
   */
  public onCloseModal() {
    $(this.nativeModalContainer).on("hidden.bs.modal", () => {
      setTimeout(() => {
        this.selfDestroy();
      }, 300);
    });
  }

  /**
   * METODO PARA ABRIR EL MODAL
   * AUTOR: FREDI ROMAN
   */
  public initModal() {
    this.nativeModalContainer = <HTMLElement>this.modalContainer.nativeElement;
    $(this.nativeModalContainer).modal('show');
    this.onCloseModal();
    this.showDynaComponent();
  }

  /**
   * METODO PARA AGREGAR EL COMPONENTE DINAMICO, EN CASO DE QUE EL TIPO CONTENIDO A MOSTRAR EN EL BODY DEL MODAL,
   * SEA UN COMPONENTE DINAMICO
   */
  private showDynaComponent() {
    if (this._dynamicData.componentType == BODY_TYPES.angularComponent) {
      this._dynaComponentService.addDynamicComponent(this._dynamicData.body, this.bodyDynaTemplate, this._dynamicData.bodyData);
    }
  }

  /**
   * METODO PARA ELIMINAR LA REFERENCIA DE ESTE COMPONENTE DINAMICO DENTRO DE TODA LA APP
   * AUTOR: FREDI ROMAN
   */
  private selfDestroy() {
    this._selfReference.destroy();
  }

  ngOnDestroy() {
    this.subscriber.unsubscribe();
    this._modalService.onModalBtnClick(null);
  }
}
