import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-calificaviaje',
  templateUrl: './calificaviaje.component.html',
  styleUrls: ['calificaviaje.component.css']
})
export class CalificaviajeComponent implements OnInit {

  @Input() title: string;
  @Input() message: string;
  @Input() btnOkText: string;
  @Input() btnCancelText: string;

  calificacion = '0';

  constructor(private activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  public decline() {
    const result = {
      boolean: false,
      calificacion: '0'
    };
    this.activeModal.close(result);
  }

  public accept() {
    const result = {
      boolean: true,
      calificacion: this.calificacion
    };
    this.activeModal.close(result);
  }

  changeCalification(val) {
    this.calificacion = val;
    console.log(this.calificacion);
  }

  public dismiss() {
    this.activeModal.dismiss();
  }

}
