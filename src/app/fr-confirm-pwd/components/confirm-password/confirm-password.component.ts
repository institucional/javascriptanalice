import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges, ViewChild, ElementRef } from '@angular/core';
import { CryptoManager } from '../../../tools/crypto-manager';
import { FunctionService } from '../../../services/functions/function.service';
import { WS } from '../../../WebServices';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AppService } from '../../../app.service';
import { TAGS } from '../../../TextLabels';

declare var $: any;

@Component({
  selector: 'confirm-password',
  templateUrl: './confirm-password.component.html',
  styleUrls: ['./confirm-password.component.scss']
})
export class ConfirmPasswordComponent implements OnInit, OnChanges {
  @ViewChild('modalContainer') modalContainerRef: ElementRef;
  @ViewChild('frmConfirmPwd') frmConfirmPwd: NgForm;
  private modalContainerHtml: HTMLElement;
  private options_notitications: any;
  private userEmail: string;
  private intentos: number;

  @Input() showConfirm: boolean
  @Output() confirmedPwd: EventEmitter<boolean>;

  public password: string;

  constructor(
    private _functionService: FunctionService,
    private _appService: AppService,
    private _toastrService: ToastrService
  ) {
    this.confirmedPwd = new EventEmitter<boolean>();
    this.options_notitications = {
      tapToDismiss: true,
      closeButton: false,
      progressBar: true,
      positionClass: 'toast-top-right',
      rtl: this._appService.isRTL
    };
    this.intentos = 1;
  }

  ngOnInit() {
    this.modalContainerHtml = this.modalContainerRef.nativeElement;
    this.modalContainerHtml.classList.add('d-none');
    this.onCloseModal();
    this.userEmail = this._functionService.decode(localStorage.getItem('userToken')).email;
  }

  /**
   * METODO PARA CONFIRMAR LA CONTRASENIA DEL USUARIO ANTES DE PROCEDER CON EL PROCESO SOLICITADO
   * AUTOR: FREDI ROMAN
   */
  public confirmPwd() {
    if (this.intentos > 3) {
      this._toastrService['warning'](TAGS.messages.exededTries, this._appService.pageTitle, this.options_notitications);
      this.frmConfirmPwd.form.controls['pwd'].reset();
      this.closeModal();
    } else {
      let encPass = CryptoManager.encrypt(this.password);
      let url = WS.usuario.confirmPwd;
      let letPayload = {
        email: this.userEmail,
        password: encPass
      }
      this._functionService.httpPost(url, letPayload).then((respData: any) => {
        if (respData.exists == true) {
          this.confirmedPwd.emit(respData.exists);
          this.closeModal();
        } else {
          this._toastrService['error'](TAGS.messages.invalidPwd, this._appService.pageTitle, this.options_notitications);
          this.intentos++;
        }
        this.frmConfirmPwd.form.controls['pwd'].reset();
      });
    }
  }

  /**
   * METODO PARA CERRAR EL MODAL DE CONFIRMACION DE CONTRASENIA
   * AUTOR: FREDI ROMAN
   */
  public closeModal() {
    let closeBtn: any = this.modalContainerHtml.querySelector(".btn-secondary");
    closeBtn.click();
  }

  /**
   * METODO QUE ESCUCHA EL EVENTO DE CIERRE DEL MODAL AL DAR CLICK FUERA DEL MISMO O EN EL BOTON CERRAR
   * AUTOR: FREDI ROMAN
   */
  public onCloseModal() {
    $(this.modalContainerHtml).on("hidden.bs.modal", () => {
      this.modalContainerHtml.classList.add('d-none');
    });
  }

  /**
   * METODO PARA ABRIR EL MODAL
   * AUTOR: FREDI ROMAN
   */
  public showModal() {
    $(this.modalContainerHtml).modal('show');
    this.modalContainerHtml.classList.remove('d-none');
  }

  /**
   * METODO PARA DETECTAR LOS CAMBIOS EN LOS PARAMETROS INPUT
   * AUTOR: FREDI ROMAN
   */
  ngOnChanges(changes: SimpleChanges) {
    for (let property in changes) {
      if (property == 'showConfirm') {
        if (changes[property].currentValue == true) {
          this.showModal();
        }
      }
    }
  }
}
