import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmPasswordComponent } from './components/confirm-password/confirm-password.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    ConfirmPasswordComponent
  ],
  exports: [
    ConfirmPasswordComponent
  ]
})
export class FrConfirmPwdModule { }
