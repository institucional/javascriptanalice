import { Component } from '@angular/core';
import { AppService } from '../app.service'; 

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html'
})
export class ContactComponent {

  constructor(private appService: AppService) {
    this.appService.pageTitle = 'Contacto';
  }

}
