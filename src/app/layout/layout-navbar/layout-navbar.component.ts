import { Component, Input } from '@angular/core';
import { AppService } from '../../app.service';
import { LayoutService } from '../../layout/layout.service';
import { AuthService } from '../../services/auth/auth.service';
import { FunctionService } from '../../services/functions/function.service';

@Component({
  selector: 'app-layout-navbar',
  templateUrl: './layout-navbar.component.html',
  styles: [':host { display: block; }'],
  styleUrls: ['./stylesnav.css'],
  host: { '[class.layout-navbar]': 'true' }
})
export class LayoutNavbarComponent {
  isExpanded = false;
  isRTL: boolean;
  nombre = '';
  cellPhone = '';

  @Input() sidenavToggle: boolean = true;

  constructor(
    private appService: AppService,
    private layoutService: LayoutService,
    public authService: AuthService,
    private functionService: FunctionService) {
    this.isRTL = appService.isRTL;
    let dataSesion = localStorage.getItem('userToken');
    dataSesion = this.functionService.decode(dataSesion);
    this.nombre = dataSesion['user_name'];
    this.cellPhone = dataSesion['cell_phone'];
  }

  currentBg() {
    return `bg-${this.appService.layoutNavbarBg}`;
  }

  toggleSidenav() {
    this.layoutService.toggleCollapsed();
  }
}
