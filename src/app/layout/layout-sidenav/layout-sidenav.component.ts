import {Component, Input, ChangeDetectionStrategy, ViewEncapsulation} from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from '../../app.service';
import { LayoutService } from '../layout.service';
import { AuthService } from '../../services/auth/auth.service';
import {WS} from '../../WebServices';
import {GV} from '../../GeneralVars';
import {Globals} from '../../globals';
import { Howl } from 'howler';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import {environment} from '../../../environments/environment';
import {FunctionService} from '../../services/functions/function.service';
import {PreautorizacionService} from '../../components/Preautorizacion/preautorizacion.service';
import {TAGS} from '../../TextLabels';
import {ResponsePreautorizacionService} from '../../components/ResponsePreautorizacion/responsepreautorizacion.service';

@Component({
  selector: 'app-layout-sidenav',
  templateUrl: './layout-sidenav.component.html',
  styles: [':host { display: block; }'],
  host: {
    '[class.layout-sidenav]': 'orientation !== "horizontal"',
    '[class.layout-sidenav-horizontal]': 'orientation === "horizontal"',
    '[class.flex-grow-0]': 'orientation === "horizontal"'
  },
  encapsulation: ViewEncapsulation.None
})
export class LayoutSidenavComponent {
  @Input() orientation = 'vertical';
  isRTL: boolean;
  private serverUrl = environment.socketHost;
  private stompClient;
  dataSesion = null;
  nombre = '';
  cellPhone = '';
  arrayServicios = {};
  sizeAyyar = 0;
  observacion = 'OBSERVACION';
  labels = null;

  constructor(
    private router: Router,
    private appService: AppService,
    private layoutService: LayoutService,
    private functionService: FunctionService,
    private preautorizado: PreautorizacionService,
    private responsepreauorizado: ResponsePreautorizacionService,
    public authService: AuthService) {
    this.isRTL = appService.isRTL;
  }

  ngAfterViewInit() {
    // Safari bugfix
    this.layoutService._redrawLayoutSidenav();
  }

  getClasses() {
    let bg = this.appService.layoutSidenavBg;

    if (this.orientation === 'horizontal' && (bg.indexOf(' sidenav-dark') !== -1 || bg.indexOf(' sidenav-light') !== -1)) {
      bg = bg
        .replace(' sidenav-dark', '')
        .replace(' sidenav-light', '')
        .replace('-darker', '')
        .replace('-dark', '');
    }

    return `${this.orientation === 'horizontal' ? 'container-p-x ' : ''} bg-${bg}`;
  }

  isActive(url) {
    return this.router.isActive(url, true);
  }

  isMenuActive(url) {
    return this.router.isActive(url, false);
  }

  isMenuOpen(url) {
    return this.router.isActive(url, false) && this.orientation !== 'horizontal';
  }

  toggleSidenav() {
    this.layoutService.toggleCollapsed();
  }

  initializeWebSocketConnection() {
    if (Globals.websocket) {
      Globals.websocket.close();
    }
    Globals.websocket = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(Globals.websocket);
    let that = this;
    this.stompClient.connect({}, (frame) => {
      console.log(frame);
      that.stompClient.subscribe('/autorizador/' + this.dataSesion['user_id'], (message) => {
        console.log(message);
        if (message.body) {
          let urllist = WS.preautorizados.getList;
          urllist = urllist.replace('${idAutorizador}', this.dataSesion['user_id']);
          urllist = urllist.replace('${estadoservicio}', GV.estadopreautorizados.autorizar.toString());
          urllist = urllist.replace('${param}', 'true');
          this.functionService.httpGetWithout(urllist).then((resPre: any[]) => {
            this.arrayServicios = resPre;
            this.sizeAyyar = resPre.length;
            if (this.sizeAyyar > 0) {
              this.playAudio();
              this.preautorizado.confirm(
                'SERVICIO POR AUTORIZAR',
                resPre[0]['idServicio'],
                resPre[0]['nomEmpleado'],
                resPre[0]['origen'],
                resPre[0]['destino'],
                resPre[0]['fecha'],
                resPre[0]['hora']
                )
                .then((confirmed) => {
                  if (confirmed['bandera']) {
                    let observacionSend = "";
                    if (confirmed['observacion'] == '') {
                      observacionSend = 'Servicio Autorizado';
                    } else {
                      observacionSend = confirmed['observacion'];
                    }
                    this.autorizarServPopUp(confirmed['idServicio'], observacionSend);
                  } else {
                    let observacionSend = "";
                    if (confirmed['observacion'] == '') {
                      observacionSend = 'Servicio Rechazado';
                    } else {
                      observacionSend = confirmed['observacion'];
                    }
                    this.rechazarServPopUp(confirmed['idServicio'], observacionSend);
                  }
                }).catch(() => {
                });
            }
            console.log(this.arrayServicios);
          });
        }
      });
      that.stompClient.subscribe('/responsepreauto/' + this.dataSesion['user_id'], (responsePre) => {
        console.log(responsePre);
        this.playAudio();
        if (responsePre.body) {
          var obj = JSON.parse(responsePre.body);
          this.responsepreauorizado.confirm(
            obj.estadoServicio,
            obj.idServicio,
            obj.nomEmpleado,
            obj.origen,
            obj.destino,
            obj.fecha,
            obj.hora,
            obj.estadoServicioNom
          );
        }
      });
      console.log(frame);
    }, (message) => {
      // this.toastrService['error']('Servidor desconectado. Inicie nuevamente.', this.appService.pageTitle, this.options_notitications);
      // this.authService.logout();
      console.log('Servidor Desconectado.');
    });
  }

  playAudio() {
    let sound = new Howl({ src: ['assets/sounds/sound.mp3'], html5: true });
    sound.play();
    setTimeout(() => {
      sound = new Howl({ src: ['assets/sounds/sound2.mp3'], html5: true });
      sound.play();
    }, 1000);
  }

  rechazarServPopUp(idServicio, observacion) {
    let urlUpdateServce = WS.preautorizados.updateservice;
    urlUpdateServce = urlUpdateServce.replace('${preautorizadosId}', idServicio);
    urlUpdateServce = urlUpdateServce.replace('${estadoservicio}', GV.estadopreautorizados.rechazado.toString());
    urlUpdateServce = urlUpdateServce.replace('${observacion}', observacion);
    this.functionService.httpPut(urlUpdateServce, null).then((res) => {
      console.log(res);
    });
  }

  autorizarServPopUp(idServicio, observacion) {
    let urlUpdateServce = WS.preautorizados.updateservice;
    urlUpdateServce = urlUpdateServce.replace('${preautorizadosId}', idServicio);
    urlUpdateServce = urlUpdateServce.replace('${estadoservicio}', GV.estadopreautorizados.autorizado.toString());
    urlUpdateServce = urlUpdateServce.replace('${observacion}', observacion);
    this.functionService.httpPut(urlUpdateServce, null).then((res) => {
      console.log(res);
    });
  }

  rechazarServ(idServicio, id_obs) {
    const value = (<HTMLInputElement>document.getElementById(id_obs)).value;
    let observacionSend = '';
    if (value == '') {
      observacionSend = 'Servicio Rechazado';
    } else {
      observacionSend = value;
    }
    let urlUpdateServce = WS.preautorizados.updateservice;
    urlUpdateServce = urlUpdateServce.replace('${preautorizadosId}', idServicio);
    urlUpdateServce = urlUpdateServce.replace('${estadoservicio}', GV.estadopreautorizados.rechazado.toString());
    urlUpdateServce = urlUpdateServce.replace('${observacion}', observacionSend);
    this.functionService.httpPut(urlUpdateServce, null).then((res) => {
      console.log(res);
    });
  }

  autorizarServ(idServicio, id_obs) {
    const value = (<HTMLInputElement>document.getElementById(id_obs)).value;
    let observacionSend = '';
    if (value == '') {
      observacionSend = 'Servicio Rechazado';
    } else {
      observacionSend = value;
    }
    let urlUpdateServce = WS.preautorizados.updateservice;
    urlUpdateServce = urlUpdateServce.replace('${preautorizadosId}', idServicio);
    urlUpdateServce = urlUpdateServce.replace('${estadoservicio}', GV.estadopreautorizados.autorizado.toString());
    urlUpdateServce = urlUpdateServce.replace('${observacion}', observacionSend);
    this.functionService.httpPut(urlUpdateServce, null).then((res) => {
      console.log(res);
    });
  }


  ngOnInit() {
    this.labels = TAGS;
    this.sizeAyyar = 0;
    let dataSesion = localStorage.getItem('userToken');
    dataSesion = this.functionService.decode(dataSesion);
    this.nombre = dataSesion['user_name'];
    this.cellPhone = dataSesion['cell_phone'];
    this.dataSesion = localStorage.getItem('userToken');
    this.dataSesion = this.functionService.decode(this.dataSesion);
    this.initializeWebSocketConnection();
    let urllist = WS.preautorizados.getList;
    urllist = urllist.replace('${idAutorizador}', this.dataSesion['user_id']);
    urllist = urllist.replace('${estadoservicio}', GV.estadopreautorizados.autorizar.toString());
    urllist = urllist.replace('${param}', 'true');
    this.functionService.httpGetWithout(urllist).then((resPre: any[]) => {
      this.arrayServicios = resPre;
      this.sizeAyyar = resPre.length;
      console.log(this.arrayServicios);
    });
  }

}
