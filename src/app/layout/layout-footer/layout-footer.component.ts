import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from '../../app.service';

@Component({
  selector: 'app-layout-footer',
  templateUrl: './layout-footer.component.html',
  styles: [':host { display: block; }'],
  host: { '[class.layout-footer]': 'true' }
})
export class LayoutFooterComponent {
  constructor(private appService: AppService, private router: Router) {}

  currentBg() {
    return `bg-${this.appService.layoutFooterBg}`;
  }

  isActive(url) {
    return this.router.isActive(url, true);
  }
}
