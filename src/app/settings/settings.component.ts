import {Component, ViewEncapsulation} from '@angular/core';
import { AppService } from '../app.service';
import { TAGS } from '../TextLabels';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FunctionService} from '../services/functions/function.service';
import {WS} from '../WebServices';
import {ToastrService} from 'ngx-toastr';
import {AuthService} from '../services/auth/auth.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['../../vendor/libs/ngx-toastr/ngx-toastr.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SettingsComponent {

  constructor(
    private appService: AppService,
    private functions: FunctionService,
    private formBuilder: FormBuilder,
    public toastrService: ToastrService,
    private authService: AuthService
  ) {
    this.appService.pageTitle = 'Preferencias';
  }

  options_notitications = {
    tapToDismiss: true,
    closeButton: false,
    progressBar: true,
    positionClass: 'toast-top-right',
    rtl: this.appService.isRTL
  };

  submitedPerfil: Boolean = false;
  formPerfil: FormGroup;
  labels = null;
  newPassword = '';
  confirmPassword = '';
  dataSesion = null;

  perfil = {
    id: 0,
    nombres: '',
    apellidos: '',
    mail: '',
    celular: '',
    identificacion: '',
    password: '',
    oldPassword: '',
    status: ''
  };

  save() {
    if (this.formPerfil.valid) {
      this.submitedPerfil = false;
      if (this.newPassword !== '') {
        if (this.newPassword.length < 7) {
          this.toastrService['error']('La nueva contraseña debe tener al menos 7 caracteres.',
            this.appService.pageTitle, this.options_notitications);
          return;
        }
        if (this.newPassword !== this.confirmPassword) {
          this.toastrService['error']('Las cotraseñas deben coincidir para continuar.',
            this.appService.pageTitle, this.options_notitications);
          return;
        } else {
          this.perfil.password = this.newPassword;
        }
      }
      const url = WS.usuario.update;
      this.functions.httpPut(url, this.perfil).then((response) => {
        if (response['status'] === 'OK') {
          this.toastrService['success']('Datos actualizados de manera correcta.',
            this.appService.pageTitle, this.options_notitications);
        } else if (response['status'] === 'OKPASS') {
          this.toastrService['success']('Contraseña actualizada de manera correcta.',
            this.appService.pageTitle, this.options_notitications);
          this.authService.logout();
        } else if (response['status'] === 'PASSERROR') {
          this.toastrService['error']('La contraseña anterior es incorrecta.',
            this.appService.pageTitle, this.options_notitications);
        }
      });
    } else {
      this.submitedPerfil = true;
    }
  }

  clear() {
    let url = WS.usuario.get;
    url = url.replace('${usuarioId}', this.dataSesion['user_id']);
    this.functions.httpGet(url).then((response) => {
      this.perfil.id = response['id'];
      this.perfil.nombres = response['nombres'];
      this.perfil.apellidos = response['apellidos'];
      this.perfil.mail = response['mail'];
      this.perfil.celular = response['celular'];
      this.perfil.identificacion = response['identificacion'];
      this.perfil.password = response['password'];
      this.perfil.oldPassword = response['oldPassword'];
      this.perfil.status = response['status'];
    });
    this.confirmPassword = '';
    this.newPassword = '';
    this.formPerfil.controls.mail.setValue('');
    this.formPerfil.controls.phone.setValue('');
    this.formPerfil.controls.name.setValue('');
    this.formPerfil.controls.last.setValue('');
    this.formPerfil.controls.ident.setValue('');
    this.formPerfil.controls.pass.setValue('');
  }

  ngOnInit() {
    this.labels = TAGS;
    this.formPerfil = this.formBuilder.group({
      mail: [null, [Validators.required, Validators.minLength(5)]],
      phone: [null, [Validators.required, Validators.minLength(5)]],
      name: [null, [Validators.required, Validators.minLength(5)]],
      last: [null, [Validators.required, Validators.minLength(5)]],
      ident: [null, [Validators.required, Validators.minLength(5)]],
      pass: [null, [Validators.required, Validators.minLength(7)]],
    });
    this.dataSesion = localStorage.getItem('userToken');
    this.dataSesion = this.functions.decode(this.dataSesion);
    let url = WS.usuario.get;
    url = url.replace('${usuarioId}', this.dataSesion['user_id']);
    this.functions.httpGet(url).then((response) => {
      this.perfil.id = response['id'];
      this.perfil.nombres = response['nombres'];
      this.perfil.apellidos = response['apellidos'];
      this.perfil.mail = response['mail'];
      this.perfil.celular = response['celular'];
      this.perfil.identificacion = response['identificacion'];
      this.perfil.password = response['password'];
      this.perfil.oldPassword = response['oldPassword'];
      this.perfil.status = response['status'];
    });
  }

  get fc() { return this.formPerfil.controls; }

}
