import { Component } from '@angular/core';
import { AppService } from '../app.service'; 

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html'
})
export class HelpComponent {
  accordionsCount = 24;
  accordionStates: boolean[] = [];

  constructor(private appService: AppService) {
    this.appService.pageTitle = 'Ayuda';
    for (let i = 0; i < this.accordionsCount; i++) this.accordionStates.push(true);
  }

}
