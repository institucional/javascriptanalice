export default [
    {
        monthCode: '01',
        monthName: 'Enero'
    },
    {
        monthCode: '02',
        monthName: 'Febrero'
    },
    {
        monthCode: '03',
        monthName: 'Marzo'
    },
    {
        monthCode: '04',
        monthName: 'Abril'
    },
    {
        monthCode: '05',
        monthName: 'Mayo'
    },
    {
        monthCode: '06',
        monthName: 'Junio'
    },
    {
        monthCode: '07',
        monthName: 'Julio'
    },
    {
        monthCode: '08',
        monthName: 'Agosto'
    },
    {
        monthCode: '09',
        monthName: 'Septiembre'
    },
    {
        monthCode: '10',
        monthName: 'Octubre'
    },
    {
        monthCode: '11',
        monthName: 'Noviembre'
    },
    {
        monthCode: '12',
        monthName: 'Diciembre'
    },
]