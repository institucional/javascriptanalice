import { Injectable } from '@angular/core';
import { User } from '../../models/user';
import { GV } from '../../GeneralVars';
import { FunctionService } from '../../services/functions/function.service';

@Injectable()
export class UserService {
  private userId: number;

  constructor(
    private functioService: FunctionService
  ) { }

  /**
   * METODO PARA OBTENER EL ID DEL USUARIO LOGEADO, A PARTIR DEL LOCAL STORAGE
   * AUTROR: FREDI ROMAN
   */
  public getUserId() {
    let dataSesion = localStorage.getItem('userToken');
    dataSesion = this.functioService.decode(dataSesion);
    this.userId = this.userId ? this.userId : dataSesion['user_id'];
    return this.userId;
  }

  /**
   * METODO PARA CREAR UNA LISTA DE USER, A PARTIR DE UN ARRAY JSON
   * AUTOR: FREDI ROMAN
   * @param userJsonArray
   */
  public createUserlList(userJsonArray: any[]): User[] {
    let userList: User[] = [];
    for (let userJson of userJsonArray) {
      userList.push(this.extractUserData(userJson));
    }
    return userList;
  }

  /**
   * METODO PARA EXTRAER LOS ELEMENTOS DE UN OBJETO JSON PARA CREAR UN OBJETO USER
   * AUTOR: FREDI ROMAN
   * @param userData
   */
  public extractUserData(userData: any): User {
    return new User(
      userData.id,
      userData.email,
      userData.firstName,
      userData.lastName,
      userData.phone,
      userData.estado == GV.state.active ? true : false
    );
  }
}
