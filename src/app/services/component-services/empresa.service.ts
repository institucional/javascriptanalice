/**
 * AUTOR: FREDI ROMAN
 */
import { Injectable } from '@angular/core';
import { Empresa } from '../../models/empresa';

@Injectable()
export class EmpresaService {

  constructor() { }

  /**
   * METODO PARA CREAR UNA LISTA DE TIPO EMPRESA, A PARTIR DE UN ARRAY JSON
   * AUTOR: FREDI ROMAN
   * @param empresaJsonArray 
   */
  public createEmpresaList(empresaJsonArray: any[]): Empresa[] {
    let empresaList: Empresa[] = [];
    for (let empresaJson of empresaJsonArray) {
      empresaList.push(this.extractEmpresaData(empresaJson));
    }
    return empresaList;
  }

  /**
   * METODO PARA EXTRAER LOS ELEMENTOS DE UN OBJETO JSON PARA CREAR UN OBJETO EMPRESA
   * AUTOR: FREDI ROMAN
   * @param empresaData 
   */
  public extractEmpresaData(empresaData: any): Empresa {
    return new Empresa(
      empresaData.id,
      empresaData.ruc,
      empresaData.calificacion,
      empresaData.vip,
      empresaData.nombreEmpresa,
      empresaData.nombreEmpresa,
      empresaData.nombreComercial,
      empresaData.representanteLegal,
      empresaData.tipoEmpresa,
      empresaData.ciudad,
      empresaData.direccion,
      empresaData.telefono,
      empresaData.fax,
      empresaData.correo,
      empresaData.sector,
      empresaData.motivoDeshabilitacion,
      empresaData.obsDeshabilitacion
    );
  }
}
