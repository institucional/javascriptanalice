/**
 * AUTOR: FREDI ROMAN
 */
import { Injectable } from '@angular/core';
import { Empleado } from '../../models/empleado';
import { Cargo } from '../../models/cargo';
import { Departamento } from '../../models/departamento';
import { Sucursal } from '../../models/sucursal';
import { Empresa } from '../../models/empresa';
import { DynaParam } from '../../models/dyna-param';
import { WS } from '../../WebServices';
import { DynamicInputForm } from '../../dynamic-form/models/dynamic-input-form';
import { INPUT_TYPES } from '../../dynamic-form/data/input-types';
import { TextInput } from '../../dynamic-form/models/text-input';
import { TextareaInput } from '../../dynamic-form/models/textarea-input';
import { DateInput } from '../../dynamic-form/models/date-input';
import { TimeInput } from '../../dynamic-form/models/time-input';
import { BooleanInput } from '../../dynamic-form/models/boolean-input';
import { SelectInput } from '../../dynamic-form/models/select-input';
import { SWITCH_SIZE } from '../../dynamic-form/data/switch-size';

@Injectable()
export class EmpleadoService {

  constructor() { }

  /**
   * METODO PARA DEFINIR UN ARRAY DE TIPO DynamicInputForm<any>
   * AUTOR: FREDI ROMAN
   * @param dynaParamList 
   */
  public defineDynaInputForm(dynaParamList: DynaParam[]): DynamicInputForm<any>[] {
    let inputFormList: DynamicInputForm<any>[] = [];
    for (let i = 0; i < dynaParamList.length; i++) {
      switch (dynaParamList[i].idTipoDato) {
        case INPUT_TYPES.entero:
          inputFormList.push(
            new TextInput(
              {
                key: dynaParamList[i].id,
                label: dynaParamList[i].etiqueta,
                value: dynaParamList[i].valor,
                required: dynaParamList[i].obligatorio,
                erroMsg: dynaParamList[i].obligatorio ? 'Este campo es obligatorio.' : null,
                order: (i + 1),
                placeholder: dynaParamList[i].etiqueta
              }, {
                type: 'number'
              }
            ));
          break;
        case INPUT_TYPES.decimal:
          inputFormList.push(
            new TextInput(
              {
                key: dynaParamList[i].id,
                label: dynaParamList[i].etiqueta,
                value: dynaParamList[i].valor,
                prependString: '0.00',
                required: dynaParamList[i].obligatorio,
                erroMsg: dynaParamList[i].obligatorio ? 'Este campo es obligatorio.' : null,
                order: (i + 1),
                placeholder: dynaParamList[i].etiqueta
              }, {
                type: 'number'
              }
            ));
          break;
        case INPUT_TYPES.dinero:
          inputFormList.push(
            new TextInput(
              {
                key: dynaParamList[i].id,
                label: dynaParamList[i].etiqueta,
                value: dynaParamList[i].valor,
                prependString: '$',
                required: dynaParamList[i].obligatorio,
                erroMsg: dynaParamList[i].obligatorio ? 'Este campo es obligatorio.' : null,
                order: (i + 1),
                placeholder: dynaParamList[i].etiqueta
              }, {
                type: 'number'
              }
            ));
          break;
        case INPUT_TYPES.porcentaje:
          inputFormList.push(
            new TextInput(
              {
                key: dynaParamList[i].id,
                label: dynaParamList[i].etiqueta,
                value: dynaParamList[i].valor,
                prependString: '%',
                required: dynaParamList[i].obligatorio,
                erroMsg: dynaParamList[i].obligatorio ? 'Este campo es obligatorio.' : null,
                order: (i + 1),
                placeholder: dynaParamList[i].etiqueta
              }, {
                type: 'number'
              }
            ));
          break;
        case INPUT_TYPES.alfanumerico:
          inputFormList.push(
            new TextInput(
              {
                key: dynaParamList[i].id,
                label: dynaParamList[i].etiqueta,
                value: dynaParamList[i].valor,
                required: dynaParamList[i].obligatorio,
                erroMsg: dynaParamList[i].obligatorio ? 'Este campo es obligatorio.' : null,
                order: (i + 1),
                placeholder: dynaParamList[i].etiqueta
              }, {
                pattern: '[a-zA-Z|0-9]+',
                type: 'text'
              }
            ));
          break;
        case INPUT_TYPES.texto_corto:
          inputFormList.push(
            new TextInput(
              {
                key: dynaParamList[i].id,
                label: dynaParamList[i].etiqueta,
                value: dynaParamList[i].valor,
                required: dynaParamList[i].obligatorio,
                erroMsg: dynaParamList[i].obligatorio ? 'Este campo es obligatorio.' : null,
                order: (i + 1),
                placeholder: dynaParamList[i].etiqueta
              }, { type: 'text' }
            ));
          break;
        case INPUT_TYPES.texto_largo:
          inputFormList.push(
            new TextareaInput(
              {
                key: dynaParamList[i].id,
                label: dynaParamList[i].etiqueta,
                value: dynaParamList[i].valor,
                required: dynaParamList[i].obligatorio,
                erroMsg: dynaParamList[i].obligatorio ? 'Este campo es obligatorio.' : null,
                order: (i + 1),
                placeholder: dynaParamList[i].etiqueta
              }, {}
            ));
          break;
        case INPUT_TYPES.fecha:
          inputFormList.push(
            new DateInput(
              {
                key: dynaParamList[i].id,
                label: dynaParamList[i].etiqueta,
                value: dynaParamList[i].valor,
                required: dynaParamList[i].obligatorio,
                erroMsg: dynaParamList[i].obligatorio ? 'Este campo es obligatorio.' : null,
                order: (i + 1), placeholder: dynaParamList[i].etiqueta
              }, {
                max: '2000-12-31', min: '1940-01-01'
              }
            ));
          break;
        case INPUT_TYPES.hora:
          dynaParamList[i].valor = dynaParamList[i].valor ? dynaParamList[i].valor.split(":") : ['00', '00'];
          inputFormList.push(
            new TimeInput(
              {
                key: dynaParamList[i].id,
                label: dynaParamList[i].etiqueta,
                required: dynaParamList[i].obligatorio,
                erroMsg: dynaParamList[i].obligatorio ? 'Este campo es obligatorio.' : null,
                order: (i + 1), placeholder: dynaParamList[i].etiqueta
              }, {
                id: '',
                label: '',
                hours: dynaParamList[i].valor[0],
                minutes: dynaParamList[i].valor[1],
                seconds: '00',
                hideLabel: true
              }
            ));
          break;
        case INPUT_TYPES.booleano:
          inputFormList.push(
            new BooleanInput(
              {
                key: dynaParamList[i].id,
                label: dynaParamList[i].etiqueta,
                value: dynaParamList[i].valor ? dynaParamList[i].valor : true,
                required: dynaParamList[i].obligatorio,
                erroMsg: dynaParamList[i].obligatorio ? 'Este campo es obligatorio.' : null,
                order: (i + 1), placeholder: dynaParamList[i].etiqueta
              }, {
                id: dynaParamList[i].id,
                label: dynaParamList[i].etiqueta,
                checked: dynaParamList[i].valor == false ? false : true,
                size: SWITCH_SIZE.xs,
                customClass: 'switch-dark',
              }
            ));
          break;
        case INPUT_TYPES.subcatalogo:
          inputFormList.push(
            new SelectInput(
              {
                key: dynaParamList[i].id,
                label: dynaParamList[i].etiqueta,
                value: dynaParamList[i].valor,
                required: dynaParamList[i].obligatorio,
                erroMsg: dynaParamList[i].obligatorio ? 'Este campo es obligatorio.' : null,
                order: (i + 1), placeholder: dynaParamList[i].etiqueta
              }, { dataList: null, value: null },
              dynaParamList[i].idCatalogo ? WS.findSubCatByCatId.replace('${catId}', dynaParamList[i].idCatalogo + '') : null
            ));
          break;
      }
    }
    return inputFormList;
  }

  /**
   * METODO PARA CREAR UNA LISTA DE TIPO EMPLEADO, A PARTIR DE UN ARRAY JSON
   * AUTOR: FREDI ROMAN
   * @param empleadoJsonArray 
   */
  public createEmpleadoList(empleadoJsonArray: any[]): Empleado[] {
    let empleadoList: Empleado[] = [];
    for (let empleadoJson of empleadoJsonArray) {
      empleadoList.push(this.extractEmpleadoData(empleadoJson));
    }
    return empleadoList;
  }

  /**
   * METODO PARA EXTRAER LOS ELEMENTOS DE UN OBJETO JSON PARA CREAR UN OBJETO EMPLEADO
   * AUTOR: FREDI ROMAN
   * @param empleadoData 
   */
  public extractEmpleadoData(empleadoData: any): Empleado {
    let depa = new Departamento(empleadoData.idDepartamento, null, empleadoData.nombreDepa);
    depa.nivel.id = empleadoData.idNivel;
    let sucursal = new Sucursal(empleadoData.idSucursales, null, null, empleadoData.nomSucursal);
    let emp = new Empresa(empleadoData.idEmpresa, null, null, null, empleadoData.nombreEmpresa);
    sucursal.empresa = emp;

    return new Empleado(
      empleadoData.idEmpleado,
      empleadoData.codEmpleado,
      empleadoData.codAlterno,
      empleadoData.tipoIdentificacion,
      empleadoData.identificacion,
      empleadoData.primerNombre,
      empleadoData.segundoNombre,
      empleadoData.primerApellido,
      empleadoData.segundoApellido,
      empleadoData.nomCompleto,
      empleadoData.telefonoCelular,
      empleadoData.telefonoConvencional,
      empleadoData.correo,
      new Cargo(empleadoData.idCargo, null, empleadoData.nombreCargo, depa),
      empleadoData.codOperadora,
      sucursal,
      null,
      empleadoData.fechaDesde,
      empleadoData.fechaHasta,
      empleadoData.estado,
      null, null, null, null,
      empleadoData.valoresAtributos ? empleadoData.valoresAtributos : null
    );
  }
}
