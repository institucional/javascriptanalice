/**
 * AUTOR: FREDI ROMAN
 */
import { Injectable } from '@angular/core';
import { Convenio } from '../../models/convenio';
import { GV } from '../../GeneralVars';
import { EmpresaService } from './empresa.service';

@Injectable()
export class ConvenioService {

    constructor(
        private _empresaService: EmpresaService
    ) { }

    /**
    * METODO PARA CREAR UNA LISTA DE TIPO CONVENIO, A PARTIR DE UN ARRAY JSON
    * AUTOR: FREDI ROMAN
    * @param convenioJsonArray 
    */
    public createConvenioList(convenioJsonArray: any[]): Convenio[] {
        let convenioList: Convenio[] = [];
        for (let convenioJson of convenioJsonArray) {
            convenioList.push(this.extractConvenioData(convenioJson));
        }
        return convenioList;
    }

    /**
     * METODO PARA EXTRAER LOS DATOS DE LOS TIPOS DE DATO TIME QUE VIENEN COMO JSON DESDE LA BASE DE DATOS
     * AUTOR: FREDI ROMAN
     */
    public extractTimeData(convenioJson: any) {
        let jsonData;
        let hours, minutes;
        let timeVars = []

        jsonData = convenioJson.poliTaxiLaborable.desde;
        jsonData = jsonData.split(":");
        hours = jsonData[0];
        minutes = jsonData[1];
        timeVars.push({ id: 'labDesde', label: 'Desde', hours: hours, minutes: minutes, seconds: '00' });

        jsonData = convenioJson.poliTaxiLaborable.hasta;
        jsonData = jsonData.split(":");
        hours = jsonData[0];
        minutes = jsonData[1];
        timeVars.push({ id: 'labHasta', label: 'Hasta', hours: hours, minutes: minutes, seconds: '00' });

        jsonData = convenioJson.poliTaxiFeriaFds.desde;
        jsonData = jsonData.split(":");
        hours = jsonData[0];
        minutes = jsonData[1];
        timeVars.push({ id: 'ferDesde', label: 'Desde', hours: hours, minutes: minutes, seconds: '00' });

        jsonData = convenioJson.poliTaxiFeriaFds.hasta;
        jsonData = jsonData.split(":");
        hours = jsonData[0];
        minutes = jsonData[1];
        timeVars.push({ id: 'ferHasta', label: 'Hasta', hours: hours, minutes: minutes, seconds: '00' });

        return timeVars;
    }

    /**
     * METODO PARA EXTRAER LOS ELEMENTOS DE UN OBJETO JSON PARA CREAR UN OBJETO CONVENIO
     * AUTOR: FREDI ROMAN
     * @param convenioData 
     */
    public extractConvenioData(convenioData: any): Convenio {
        let laborable = JSON.parse(convenioData.poliTaxiLaborable);
        laborable.desde = laborable.desde.split(":")[0] + ":" + laborable.desde.split(":")[1];
        laborable.hasta = laborable.hasta.split(":")[0] + ":" + laborable.hasta.split(":")[1];

        let feriadoFds = JSON.parse(convenioData.poliTaxiFeriaFds);
        if (feriadoFds.desde == '00:00:00') {
            feriadoFds = null;
        } else {
            feriadoFds.desde = feriadoFds.desde.split(":")[0] + ":" + feriadoFds.desde.split(":")[1];
            feriadoFds.hasta = feriadoFds.hasta.split(":")[0] + ":" + feriadoFds.hasta.split(":")[1];
        }

        let convenio = new Convenio(
            convenioData.id,
            convenioData.codConvenio,
            convenioData.montoLimite,
            convenioData.numeroVoucherLimite,
            convenioData.controlHoras,
            convenioData.numeroHorasAprobar,
            convenioData.fechaSuscripcion,
            convenioData.fechaVencimiento,
            convenioData.plazo,
            this._empresaService.extractEmpresaData(convenioData.empresa),
            convenioData.tipoControl,
            null,
            laborable,
            feriadoFds
        );
        convenio.tipoControlDesc = this.defineControlTypeDesc(convenioData.tipoControl);
        return convenio;
    }

    /**
     * METODO PARA DEFINIR LA DESCRIPCION DEL TIPO DE CONTROL DEL CONVENIO
     * @param tipoControl 
     */
    private defineControlTypeDesc(tipoControl: string): string {
        for (let property in GV.controlConvenios) {
            if (property == tipoControl)
                return GV.controlConvenios[property];
        }
    }
}
