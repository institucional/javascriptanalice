import { Injectable } from "@angular/core";
import { DynaParam } from "../../models/dyna-param";

@Injectable()
export class DynaParamService {

    constructor() { }

    /**
   * METODO PARA CREAR UNA LISTA DE TIPO DYNA-PARAM, A PARTIR DE UN ARRAY JSON
   * AUTOR: FREDI ROMAN
   * @param dynaParamJsonArray 
   */
    public createDynaParamList(dynaParamJsonArray: any[]): DynaParam[] {
        let dynaParamList: DynaParam[] = [];
        for (let dynaParamJson of dynaParamJsonArray) {
            dynaParamList.push(this.extractDynaParamData(dynaParamJson));
        }
        return dynaParamList;
    }

    /**
     * METODO PARA EXTRAER LOS ELEMENTOS DE UN OBJETO JSON PARA CREAR UN OBJETO DYNA-PARAM
     * AUTOR: FREDI ROMAN
     * @param dynaParamData 
     */
    public extractDynaParamData(dynaParamData: any): DynaParam {
        return new DynaParam(
            dynaParamData.id,
            dynaParamData.etiqueta,
            dynaParamData.idTipoDato,
            dynaParamData.tipoDato,
            dynaParamData.idCatalogo,
            dynaParamData.catalogo,
            dynaParamData.obligatorio,
            dynaParamData.activo
        );
    }
}