/**
 * AUTOR: FREDI ROMAN
 */
import { Injectable } from '@angular/core';
import { GV } from '../../GeneralVars';
import { Departamento } from '../../models/departamento';
import { NivelService } from './nivel.service';

@Injectable()
export class DepartamentoService {

  constructor(
    private _nivelService: NivelService
  ) { }

  /**
   * METODO PARA CREAR UNA LISTA DE TIPO DEPARTAMENTO, A PARTIR DE UN ARRAY JSON
   * AUTOR: FREDI ROMAN
   * @param departamentoJsonArray 
   */
  public createDepartamentoList(departamentoJsonArray: any[]): Departamento[] {
    let departamentoList: Departamento[] = [];
    for (let departamentoJson of departamentoJsonArray) {
      departamentoList.push(this.extractDepartamentoData(departamentoJson));
    }
    return departamentoList;
  }

  /**
   * METODO PARA EXTRAER LOS ELEMENTOS DE UN OBJETO JSON PARA CREAR UN OBJETO DEPARTAMENTO
   * AUTOR: FREDI ROMAN
   * @param departamentoData 
   */
  public extractDepartamentoData(departamentoData: any): Departamento {
    return new Departamento(
      departamentoData.idDepartamento,
      departamentoData.codigoDepa,
      departamentoData.nombreDepa,
      departamentoData.codigoAlterno,
      departamentoData.departamentoPadre ? this.extractDepartamentoData(departamentoData.departamentoPadre) : null,
      this._nivelService.extractNivelData(departamentoData.nivel),
      departamentoData.estado == GV.state.active ? true : false,
      departamentoData.estado
    );
  }
}
