/**
 * AUTOR: FREDI ROMAN
 */
import { Injectable } from '@angular/core';
import { ResponsableContacto } from '../../models/responsable-contacto';
import { EmpleadoService } from './empleado.service';
import { GV } from '../../GeneralVars';
import { Convenio } from '../../models/convenio';
import { SucursalService } from './sucursal.service';
import { PresupuestoService } from './presupuesto.service';
import { Empresa } from '../../models/empresa';

@Injectable()
export class ResponsableContactoService {

  constructor(
    private _empleadoService: EmpleadoService,
    private _sucursalService: SucursalService,
    private _presupuestoService: PresupuestoService
  ) { }

  /**
   * METODO PARA CREAR UNA LISTA DE TIPO CONTACTO, A PARTIR DE UN ARRAY JSON
   * AUTOR: FREDI ROMAN
   * @param contactoJsonArray 
   */
  public createContactoList(contactoJsonArray: any[]): ResponsableContacto[] {
    let contactoList: ResponsableContacto[] = [];
    for (let contactoJson of contactoJsonArray) {
      contactoList.push(this.extractContactoData(contactoJson));
    }
    return contactoList;
  }

  /**
   * METODO PARA EXTRAER LOS ELEMENTOS DE UN OBJETO JSON PARA CREAR UN OBJETO CONTACTO
   * AUTOR: FREDI ROMAN
   * @param contactoData 
   */
  public extractContactoData(contactoData: any): ResponsableContacto {
    let empConv = new Empresa();
    empConv.nombreEmpresa = contactoData.empresaConvenio;

    return new ResponsableContacto(
      contactoData.idResponContacto,
      contactoData.idClaseResponContacto,
      contactoData.claseResponContacto,
      contactoData.codTipoResponsable,
      contactoData.tipoResponsable,
      this._empleadoService.extractEmpleadoData(contactoData),
      this._sucursalService.extractSucursalData(contactoData),
      contactoData.idConvenio ? new Convenio(contactoData.idConvenio) : null,
      contactoData.idPresupuesto ? this._presupuestoService.extractPresupuestoData(contactoData) : null,
      contactoData.estadoRespContacto == GV.state.active ? true : false,
      contactoData.estadoRespContacto, null, contactoData.recibeNotif == 1 ? true : false,
      empConv
    );
  }
}
