/**
 * AUTOR: FREDI ROMAN
 */
import { Injectable } from '@angular/core';
import { UserService } from './user.service';
import { EmpresaService } from './empresa.service';
import { UsuarioEmpresa } from '../../models/usuario-empresa';
import { Empresa } from '../../models/empresa';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class UsuarioEmpresaService {

  constructor(
    private _userService: UserService,
    private _empresaService: EmpresaService
  ) { }

  /**
   * METODO PARA CREAR UNA LISTA DE USUARIO-EMPRESA, A PARTIR DE UN ARRAY JSON
   * AUTOR: FREDI ROMAN
   * @param usuarioEmpresaJsonArray 
   */
  public createUsuarioEmpresalList(usuarioEmpresaJsonArray: any[]): UsuarioEmpresa[] {
    let usuarioEmpresaList: UsuarioEmpresa[] = [];
    for (let usuarioEmpresaJson of usuarioEmpresaJsonArray) {
      usuarioEmpresaList.push(this.extractUsuarioEmpresaData(usuarioEmpresaJson));
    }
    return usuarioEmpresaList;
  }

  /**
   * METODO PARA EXTRAER LOS ELEMENTOS DE UN OBJETO JSON PARA CREAR UN OBJETO USUARIO-EMPRESA
   * AUTOR: FREDI ROMAN
   * @param usuarioEmpresaData 
   */
  public extractUsuarioEmpresaData(usuarioEmpresaData: any): UsuarioEmpresa {
    return new UsuarioEmpresa(
      usuarioEmpresaData.id,
      this._userService.extractUserData(usuarioEmpresaData.usuario),
      this._empresaService.extractEmpresaData(usuarioEmpresaData.empresa)
    );
  }
}
