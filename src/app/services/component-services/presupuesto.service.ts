/**
 * AUTOR: FREDI ROMAN
 */
import { Injectable } from '@angular/core';
import { GV } from '../../GeneralVars';
import { Presupuesto } from '../../models/presupuesto';
import { ConvenioService } from './convenio.service';
import months from '../../data/months';
import { DepartamentoService } from './departamento.service';
import { SucursalService } from './sucursal.service';

@Injectable()
export class PresupuestoService {

  constructor(
    private _convenioService: ConvenioService,
    private _sucursalService: SucursalService,
    private _departamentoService: DepartamentoService,
  ) { }

  /**
   * METODO PARA CREAR UNA LISTA DE TIPO PRESUPUESTO, A PARTIR DE UN ARRAY JSON
   * AUTOR: FREDI ROMAN
   * @param presupJsonArray 
   */
  public createPresupuestoList(presupJsonArray: any[]): Presupuesto[] {
    let presupuestoList: Presupuesto[] = [];
    for (let presupuestoJson of presupJsonArray) {
      presupuestoList.push(this.extractPresupuestoData(presupuestoJson));
    }
    return presupuestoList;
  }

  /**
   * METODO PARA EXTRAER LOS ELEMENTOS DE UN OBJETO JSON PARA CREAR UN OBJETO PRESUPUESTO
   * AUTOR: FREDI ROMAN
   * @param presupuestoData 
   */
  public extractPresupuestoData(presupuestoData: any): Presupuesto {
    let laborable = JSON.parse(presupuestoData.poliTaxiLaborable);
    laborable.desde = laborable.desde.split(":")[0] + ":" + laborable.desde.split(":")[1];
    laborable.hasta = laborable.hasta.split(":")[0] + ":" + laborable.hasta.split(":")[1];

    let feriadoFds = JSON.parse(presupuestoData.poliTaxiFeriaFds);
    if (feriadoFds.desde == '00:00:00') {
      feriadoFds = null;
    } else {
      feriadoFds.desde = feriadoFds.desde.split(":")[0] + ":" + feriadoFds.desde.split(":")[1];
      feriadoFds.hasta = feriadoFds.hasta.split(":")[0] + ":" + feriadoFds.hasta.split(":")[1];
    }

    let presup = new Presupuesto(
      presupuestoData.id || presupuestoData.idPresupuesto,
      presupuestoData.anio,
      presupuestoData.mes,
      months.find(month => month.monthCode == presupuestoData.mes).monthName,
      presupuestoData.valor,
      (presupuestoData.convenio) ? this._convenioService.extractConvenioData(presupuestoData.convenio) : null,
      (presupuestoData.sucursal) ? this._sucursalService.extractSucursalData(presupuestoData.sucursal) : null,
      (presupuestoData.departamento) ? this._departamentoService.extractDepartamentoData(presupuestoData.departamento) : null,
      presupuestoData.estado == GV.state.active ? true : false,
      presupuestoData.estado,
      null,
      laborable,
      feriadoFds
    );

    return presup;
  }
}
