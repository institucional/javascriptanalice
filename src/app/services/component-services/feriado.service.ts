import { Injectable } from '@angular/core';
import { SucursalService } from './sucursal.service';
import { Feriado } from '../../models/feriado';
import { GV } from '../../GeneralVars';

@Injectable()
export class FeriadoService {

  constructor(
    private _sucursalService: SucursalService
  ) { }

  /**
   * METODO PARA CREAR UNA LISTA DE TIPO FERIADO, A PARTIR DE UN ARRAY JSON
   * AUTOR: FREDI ROMAN
   * @param feriadoJsonArray 
   */
  public createFeriadoList(feriadoJsonArray: any[]): Feriado[] {
    let feriadoList: Feriado[] = [];
    for (let feriadoJson of feriadoJsonArray) {
      feriadoList.push(this.extractFeriadoData(feriadoJson));
    }
    return feriadoList;
  }

  /**
   * METODO PARA EXTRAER LOS ELEMENTOS DE UN OBJETO JSON PARA CREAR UN OBJETO FERIADO
   * AUTOR: FREDI ROMAN
   * @param feriadoData 
   */
  public extractFeriadoData(feriadoData: any): Feriado {
    return new Feriado(
      feriadoData.idFeriado,
      feriadoData.fechaFeriado,
      this._sucursalService.extractSucursalData(feriadoData.sucursal),
      feriadoData.estado == GV.state.active ? true : false,
      feriadoData.estado
    );
  }
}
