/**
 * AUTOR: FREDI ROMAN
 */
import { Injectable, ComponentFactory, ComponentRef, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';

@Injectable()
export class DynaComponentService {

  constructor(
    private _componentFactoryResolver: ComponentFactoryResolver
  ) { }

  /**
   * METODO PARA AGREGAR UN COMPONENTE DINAMICAMENTE EN EL DOM
   * AUTOR: FREDI ROMAN
   */
  public addDynamicComponent(dynamicComponent: any, viewContainerRef: ViewContainerRef, componentData: any) {
    let component: ComponentFactory<any>;
    let createdComponent: ComponentRef<any>;
    let componentInstance: any;

    component = this._componentFactoryResolver.resolveComponentFactory(dynamicComponent);
    createdComponent = viewContainerRef.createComponent(component);
    componentInstance = createdComponent.instance;
    componentInstance._selfReference = createdComponent;
    componentInstance._dynamicData = componentData;
  }
}
