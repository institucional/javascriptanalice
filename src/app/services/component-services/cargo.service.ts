/**
 * AUTOR: FREDI ROMAN
 */
import { Injectable } from '@angular/core';
import { EmpresaService } from './empresa.service';
import { GV } from '../../GeneralVars';
import { Cargo } from '../../models/cargo';
import { DepartamentoService } from './departamento.service';

@Injectable()
export class CargoService {

  constructor(
    private _departamentoService: DepartamentoService
  ) { }

  /**
   * METODO PARA CREAR UNA LISTA DE TIPO CARGO, A PARTIR DE UN ARRAY JSON
   * AUTOR: FREDI ROMAN
   * @param cargoJsonArray 
   */
  public createCargoList(cargoJsonArray: any[]): Cargo[] {
    let cargoList: Cargo[] = [];
    for (let cargoJson of cargoJsonArray) {
      cargoList.push(this.extractCargoData(cargoJson));
    }
    return cargoList;
  }

  /**
   * METODO PARA EXTRAER LOS ELEMENTOS DE UN OBJETO JSON PARA CREAR UN OBJETO CARGO
   * AUTOR: FREDI ROMAN
   * @param cargoData 
   */
  public extractCargoData(cargoData: any): Cargo {
    return new Cargo(
      cargoData.id,
      cargoData.codCargo,
      cargoData.nombre,
      this._departamentoService.extractDepartamentoData(cargoData.departamento),
      cargoData.estado == GV.state.active ? true : false,
      cargoData.estado
    );
  }
}
