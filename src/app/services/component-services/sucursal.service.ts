import { Injectable } from '@angular/core';
import { EmpresaService } from './empresa.service';
import { Sucursal } from '../../models/sucursal';
import { GV } from '../../GeneralVars';

@Injectable()
export class SucursalService {

  constructor(
    private _empresaService: EmpresaService
  ) { }

  /**
   * METODO PARA CREAR UNA LISTA DE TIPO SUCURSAL, A PARTIR DE UN ARRAY JSON
   * AUTOR: FREDI ROMAN
   * @param sucursalJsonArray 
   */
  public createSucursalList(sucursalJsonArray: any[]): Sucursal[] {
    let sucursalList: Sucursal[] = [];
    for (let sucursalJson of sucursalJsonArray) {
      sucursalList.push(this.extractSucursalData(sucursalJson));
    }
    return sucursalList;
  }

  /**
   * METODO PARA EXTRAER LOS ELEMENTOS DE UN OBJETO JSON PARA CREAR UN OBJETO SUCURSAL
   * AUTOR: FREDI ROMAN
   * @param sucursalData 
   */
  public extractSucursalData(sucursalData: any): Sucursal {
    return new Sucursal(
      sucursalData.id || sucursalData.idSucursales,
      sucursalData.codAlterno,
      sucursalData.codSucursal,
      sucursalData.nomSucursal,
      sucursalData.codCiudad,
      sucursalData.direccion,
      sucursalData.telefono,
      sucursalData.fax,
      sucursalData.correo,
      sucursalData.codSector,
      sucursalData.codZona,
      sucursalData.nomCalle1,
      sucursalData.nomCalle2,
      sucursalData.numeroCalle,
      sucursalData.empresa ? this._empresaService.extractEmpresaData(sucursalData.empresa) : null,
      sucursalData.estado == GV.state.active ? true : false,
      sucursalData.estado
    );
  }
}
