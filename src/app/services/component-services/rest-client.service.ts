import { Injectable } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";
import { AppService } from "../../app.service";
import { ToastrService } from "ngx-toastr";
import { SpinnerDialogService } from "../../components/spinner-dialog/spinner-dialog.service";
import { TAGS } from "../../TextLabels";

@Injectable()
export class RestClientService {
    private options_notitications: any;

    constructor(
        private _http: Http,
        private appService: AppService,
        public _toastrService: ToastrService,
        private _spinnerDialogService: SpinnerDialogService
    ) {
        this.options_notitications = {
            tapToDismiss: true,
            closeButton: false,
            progressBar: true,
            positionClass: 'toast-top-right',
            rtl: this.appService.isRTL
        };
    }

    /**
     * METODO PARA EJECUTAR UN HTTP REQUEST GET, CON INCLUSION DE HEADERS
     * AUTOR: FREDI ROMAN
     * @param url 
     * @param reqHeaders 
     */
    public httpGetWithHeaders(url: string, reqFilter: { key: string; value: string }, showLoading: boolean) {
        if (showLoading) {
            this._spinnerDialogService.openSpinner();
        }
        let token = localStorage.getItem("userToken");
        let headerObj = {
            'Authorization': 'Bearer ' + token,
            [reqFilter.key]: [reqFilter.value]
        };
        let reqHeaders = new Headers(headerObj);
        return this._http.get(url, { headers: reqHeaders }).toPromise()
            .then((responseData: Response) => {
                let retrievedData = responseData.json();
                if (showLoading) {
                    this._spinnerDialogService.closeSpinner();
                }
                return retrievedData;
            }).catch(err => {
                if (showLoading) {
                    this._toastrService['error'](TAGS.messages.apiError, this.appService.pageTitle, this.options_notitications);
                    this._spinnerDialogService.closeSpinner();
                }
                console.log(err);
            });
    }
}