/**
 * AUTOR: FREDI ROMAN
 */
import { Injectable } from '@angular/core';
import { EmpresaService } from './empresa.service';
import { GV } from '../../GeneralVars';
import { Nivel } from '../../models/nivel';

@Injectable()
export class NivelService {

  constructor(
    private _empresaService: EmpresaService
  ) { }

  /**
   * METODO PARA CREAR UNA LISTA DE TIPO NIVEL, A PARTIR DE UN ARRAY JSON
   * AUTOR: FREDI ROMAN
   * @param nivelJsonArray 
   */
  public createNivelList(nivelJsonArray: any[]): Nivel[] {
    let nivelList: Nivel[] = [];
    let extracted: Nivel;
    let counter = 0;
    for (let nivelJson of nivelJsonArray) {
      extracted = this.extractNivelData(nivelJson);
      if (counter == 0) {
        extracted.selected = true;
      }
      nivelList.push(extracted);
      counter++;
    }
    return nivelList;
  }

  /**
   * METODO PARA EXTRAER LOS ELEMENTOS DE UN OBJETO JSON PARA CREAR UN OBJETO NIVEL
   * AUTOR: FREDI ROMAN
   * @param nivelData 
   */
  public extractNivelData(nivelData: any): Nivel {
    let nivel = new Nivel(
      nivelData.id,
      nivelData.numero,
      nivelData.nombre,
      this._empresaService.extractEmpresaData(nivelData.empresa),
      nivelData.estado == GV.state.active ? true : false,
      nivelData.estado
    );
    nivel.depaCount = nivelData.depaCount;

    return nivel;
  }
}
