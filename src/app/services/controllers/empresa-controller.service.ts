import { Injectable } from '@angular/core';
import { FunctionService } from '../functions/function.service';
import { WS } from '../../WebServices';
import { UserService } from '../component-services/user.service';

@Injectable()
export class EmpresaControllerService {

  constructor(
    private _functionService: FunctionService,
    private _userService: UserService
  ) { }

  /**
   * METODO CONTROLADOR PARA OBTENER UNA LISTA DE EMPRESAS DIFERENCIANDO EL ROL Y PERFIL DE USUARIO EN EL SISTEMA
   * AUTOR: FREDI ROMAN
   */
  public getEmpresaListController() {
    let superAdmin = false;
    const url = superAdmin ? WS.empresa.empByState : WS.usuarioEmpresa.get.replace("${userId}", this._userService.getUserId() + "");

    return this._functionService.httpGetWithout(url).then((response: any) => {
      let empresaData = [];
      if (superAdmin) {
        empresaData = response.content;
      } else {
        for (let emp of response) {
          empresaData.push(emp.empresa);
        }
      }
      return empresaData
    });
  }
}
