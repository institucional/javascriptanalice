/**
 * AUTOR: FREDI ROMAN
 */
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Modal } from '../../interfaces/modal.interface';

@Injectable()
export class ModalService {
  private openModaSub = new BehaviorSubject<Modal>(null);
  $openModal: Observable<Modal> = this.openModaSub.asObservable();

  private modalDataSub = new BehaviorSubject<{ baseComponentId: string, data: any }>(null);
  modalData$: Observable<{ baseComponentId: string, data: any }> = this.modalDataSub.asObservable();

  private onModalBtnClickSub = new BehaviorSubject<{ baseComponentId: string, actionType: number }>(null);
  onModalBtnClick$: Observable<{ baseComponentId: string, actionType: number }> = this.onModalBtnClickSub.asObservable();

  private refreshModalBodySub = new BehaviorSubject<{ baseComponentId: string, refresh: boolean }>(null);
  refreshModalBody$: Observable<{ baseComponentId: string, refresh: boolean }> = this.refreshModalBodySub.asObservable();

  constructor() { }

  /**
   * METODO PARA INDICAR QUE SE REFRESQUE LA SECCION DEL BODY DEL MODAL ABIERTO
   * AUTOR: FREDI ROMAN
   * @param refreshModal
   */
  public refreshModalBody(refreshModal: { baseComponentId: string, refresh: boolean }) {
    this.refreshModalBodySub.next(refreshModal);
  }

  /**
   * METODO PARA INDICAR QUE SE HA HECHO CLICK EN UN BOTON ESPECIFICO DEL MODAL
   * AUTOR: FREDI ROMAN
   * @param actionType
   */
  public onModalBtnClick(modalAction: { baseComponentId: string, actionType: number }) {
    this.onModalBtnClickSub.next(modalAction);
  }

  /**
   * METODO PARA ENVIAR UN OBJETO EMPRESA SELECCIONADO DESDE EL MODAL QUE MUESTRA LA LISTA DE EMPRESAS,
   * PARA SER ASOCIADO
   * AUTOR: FREDI ROMAN
   * @param asocEnter
   */
  public sendModalData(modalData: { baseComponentId: string, data: any }) {
    this.modalDataSub.next(modalData);
  }

  /**
   * METODO PARA ORDENAR LA APERTURA DE UN NUEVO MODAL DINAMICO
   * AUTOR: FREDI ROMAN
   * @param modalData
   */
  public openModal(modalData: Modal) {
    this.openModaSub.next(modalData);
  }

}
