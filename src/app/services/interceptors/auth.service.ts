import {Injectable} from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import { FunctionService } from '../functions/function.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    private functionService: FunctionService
  ) {}
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const userToken = localStorage.getItem('userToken');
    if (userToken && userToken !== null) {
      if (this.functionService.decode(userToken)) {
        req = req.clone({
          setHeaders: {
            Authorization: `Bearer ${userToken}`
          }
        });
        return next.handle(req);
      }
    } else {
      return next.handle(req);
    }
  }
}
