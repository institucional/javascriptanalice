import { Injectable } from "@angular/core";

@Injectable()
export class TableResponsiveService {
    constructor() { }

    /**
     * METODO PARA TRANSFORMAR UNA NGX-DATA-TABLE A TABLA RESPONSIVA:
     * AUTOR: FREDI ROMAN
     */
    public makeTableResponsive(tableContainerElement: HTMLElement) {
        let ngxTable = tableContainerElement.querySelector("ngx-datatable");
        let overflowContainer = ngxTable.querySelector("div");
        let header = overflowContainer.querySelector('datatable-header');
        let footer = overflowContainer.querySelector('datatable-footer');
        if (footer) {
            ngxTable.appendChild(footer);
        }

        overflowContainer.classList.add('fr-over-x-auto');
        header.classList.add('fr-respon-table-header');
    }
}