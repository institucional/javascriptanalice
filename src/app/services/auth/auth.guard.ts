import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthService } from './auth.service';
import { ToastrService } from 'ngx-toastr';
import {TAGS} from '../../TextLabels';
import {AppService} from '../../app.service';
@Injectable()
export class AuthGuard implements CanActivate {
  options_notitications = {
    tapToDismiss: true,
    closeButton: false,
    progressBar: true,
    positionClass: 'toast-top-right',
    rtl: this.appService.isRTL
  };
  constructor(
      private router: Router,
      public toastrService: ToastrService,
      private appService: AppService,
      public auth: AuthService) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):  boolean {
    if (!this.auth.isAuthenticated()) {
        this.toastrService['error'](TAGS.general.initSesion, 'LOGIN', this.options_notitications);
        this.router.navigate(['/login']);
        return false;
    }
    return true;
  }
}
