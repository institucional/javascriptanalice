import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, URLSearchParams} from '@angular/http';
import {client_id, client_secret} from '../../Configs';
import { WS } from '../../WebServices';

@Injectable()
export class AuthService {
  constructor(
    private http: Http,
    private router: Router
  ) {}

  user = {
      id: '',
      username: '',
      name: '',
      lastName: '',
      avatar: '',
      email: '',
      password: '',
      phone: '',
      company: '',
      company_id: ''
    };
  ipAddress = null;

    login(email: string, password: string, rememberMe: boolean): any {
        const url = WS.login.oauth;
        this.user.username = email;
        const headers = new Headers();
        const body = new URLSearchParams();
        const b64aut = btoa(client_id + ':' + client_secret);
        body.append('grant_type', 'password');
        body.append('username', email);
        body.append('password', password);
        headers.append('Authorization', 'Basic ' + b64aut);
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const options = new RequestOptions({ headers: headers });
        return this.http.post(url, body, options);
    }

      register(names: string, lastNames: string, email: string, password: string, cell: string, ipAddress: string, fecult: string
      , tipoIdentificacion: string, identificacion: string): any {
        let url = WS.login.register;
        url = url.replace('${ban}', 'false');
        cell = cell.replace('(', '');
        cell = cell.replace(')', '');
        cell = cell.replace('-', '');
        cell = cell.replace(' ', '');
        const user = {
          email: email,
          password: password,
          firstName: names,
          lastName: lastNames,
          phone: cell,
          fecult: fecult,
          ipMaquina: ipAddress,
          tipoIdentificacion: tipoIdentificacion,
          identificacion: identificacion
        };
        return this.http.post(url, user);
      }

    logout() {
        localStorage.removeItem('userToken');
        localStorage.removeItem('dataSesion');
        this.router.navigate(['/login']);
    }

    public isAuthenticated(): boolean {
        return (localStorage.getItem('userToken') != null);
    }
}
