import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../auth/auth.service';
import { ToastrService } from 'ngx-toastr';
import { TAGS } from '../../TextLabels';
import { AppService } from '../../app.service';
import {Secret_Key} from '../../Configs';
import * as jwt from 'angular2-jwt-simple';
import {SpinnerDialogService} from '../../components/spinner-dialog/spinner-dialog.service';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';

@Injectable()
export class FunctionService {

  options_notitications = {
    tapToDismiss: true,
    closeButton: false,
    progressBar: true,
    positionClass: 'toast-top-right',
    rtl: this.appService.isRTL
  };

  constructor(
    private appService: AppService,
    private http: HttpClient,
    public toastrService: ToastrService,
    private authService: AuthService,
    private spinner: SpinnerDialogService
  ) { }

  decode(userToken) {
    try {
      const decode = jwt.decode(userToken, Secret_Key, true);
      const current_time = Date.now() / 1000;
      if (decode['exp'] < current_time) {
        this.toastrService['error'](TAGS.messages.expiredToken, this.appService.pageTitle, this.options_notitications);
        this.authService.logout();
        return false;
      } else {
        return decode;
      }
    } catch (err) {
      this.toastrService['error'](TAGS.messages.wsError, this.appService.pageTitle, this.options_notitications);
      this.authService.logout();
      return false;
    }
  }

  getIpAddress() {
    const promise = new Promise((resolve, reject) => {
      try {
        this.http.get('https://ipinfo.io/').subscribe( data => {
            resolve(data['ip']);
          },
          err => {
            resolve('localhost');
          });
      } catch (e) {
        resolve('localhost');
      }
    });
    return promise;
  }

  requestDataFromMultipleSources(array): Observable<any[]> {
    const arrayReturn = [];
    for (let i = 0; i < array.length; i++) {
      arrayReturn.push(this.http.get(array[i]));
    }
    return Observable.forkJoin(arrayReturn);
  }

  httpObservableGet(array) {
    this.spinner.openSpinner();
    const promise = new Promise((resolve, reject) => {
      this.requestDataFromMultipleSources(array).subscribe(responseList => {
        try {
          resolve(responseList);
          // this.toastrService['success'](TAGS.messages.catalogSuccess, this.appService.pageTitle, this.options_notitications);
        } catch (e) {
          resolve();
          this.toastrService['error'](TAGS.messages.apiError, this.appService.pageTitle, this.options_notitications);
        }
        this.spinner.closeSpinner();
      }, err => {
        resolve();
        this.toastrService['error'](TAGS.messages.catalogError, this.appService.pageTitle, this.options_notitications);
        this.spinner.closeSpinner();
      });
    });
    return promise;
  }

  httpObservableGetWithout(array) {
    const promise = new Promise((resolve, reject) => {
      this.requestDataFromMultipleSources(array).subscribe(responseList => {
        try {
          resolve(responseList);
          // this.toastrService['success'](TAGS.messages.catalogSuccess, this.appService.pageTitle, this.options_notitications);
        } catch (e) {
          resolve();
          this.toastrService['error'](TAGS.messages.apiError, this.appService.pageTitle, this.options_notitications);
        }
      }, err => {
        resolve();
        this.toastrService['error'](TAGS.messages.catalogError, this.appService.pageTitle, this.options_notitications);
      });
    });
    return promise;
  }

  requestDataFromMultipleSourcesPost(array): Observable<any[]> {
    const arrayReturn = [];
    for (let i = 0; i < array.length; i++) {
      arrayReturn.push(this.http.post(array[i].url, array[i].body));
    }
    return Observable.forkJoin(arrayReturn);
  }

  httpObservablePost(array) {
    this.spinner.openSpinner();
    const promise = new Promise((resolve, reject) => {
      this.requestDataFromMultipleSourcesPost(array).subscribe(responseList => {
        try {
          resolve(responseList);
          // this.toastrService['success'](TAGS.messages.catalogSuccess, this.appService.pageTitle, this.options_notitications);
        } catch (e) {
          resolve();
          this.toastrService['error'](TAGS.messages.apiError, this.appService.pageTitle, this.options_notitications);
        }
        this.spinner.closeSpinner();
      }, err => {
        resolve();
        this.toastrService['error'](TAGS.messages.catalogError, this.appService.pageTitle, this.options_notitications);
        this.spinner.closeSpinner();
      });
    });
    return promise;
  }

  fetch(url) {
    this.spinner.openSpinner();
    const promise = new Promise((resolve, reject) => {
      this.http.get(url).subscribe(
        res => {
          resolve(res);
          // this.toastrService['success'](TAGS.messages.catalogSuccess, this.appService.pageTitle, this.options_notitications);
          this.spinner.closeSpinner();
        },
        err => {
          resolve();
          this.toastrService['error'](TAGS.messages.processError, this.appService.pageTitle, this.options_notitications);
          this.spinner.closeSpinner();
          this.authService.logout();
        }
      );
    });
    return promise;
  }

  fetchWithout(url) {
    const promise = new Promise((resolve, reject) => {
      this.http.get(url).subscribe(
        res => {
          resolve(res);
        },
        err => {
          resolve();
          this.toastrService['error'](TAGS.messages.processError, this.appService.pageTitle, this.options_notitications);
          this.authService.logout();
        }
      );
    });
    return promise;
  }

  httpGet(url) {
    this.spinner.openSpinner();
    const promise = new Promise((resolve, reject) => {
      this.http.get(url).subscribe(
        res => {
          resolve(res);
          //this.toastrService['success'](TAGS.messages.processOk, this.appService.pageTitle, this.options_notitications);
          this.spinner.closeSpinner();
        },
        err => {
          resolve();
          this.toastrService['error'](TAGS.messages.processError, this.appService.pageTitle, this.options_notitications);
          this.spinner.closeSpinner();
          this.authService.logout();
        }
      );
    });
    return promise;
  }

  httpGetWithout(url) {
    const promise = new Promise((resolve, reject) => {
      this.http.get(url).subscribe(
        res => {
          resolve(res);
        },
        err => {
          resolve();
          this.authService.logout();
        }
      );
    });
    return promise;
  }

  httpDelete(url) {
    this.spinner.openSpinner();
    const promise = new Promise((resolve, reject) => {
      this.http.delete(url).subscribe(
        res => {
          resolve(res);
          this.toastrService['success'](TAGS.messages.processOk, this.appService.pageTitle, this.options_notitications);
          this.spinner.closeSpinner();
        },
        err => {
          resolve();
          this.toastrService['error'](TAGS.messages.processError, this.appService.pageTitle, this.options_notitications);
          this.spinner.closeSpinner();
          this.authService.logout();
        }
      );
    });
    return promise;
  }

  getService(url) {
    const promise = new Promise((resolve, reject) => {
      this.http.get(url).subscribe(
        res => {
          resolve(res);
        },
        err => {
          resolve();
          this.toastrService['error'](TAGS.messages.processError, this.appService.pageTitle, this.options_notitications);
          this.authService.logout();
        }
      );
    });
    return promise;
  }

  httpPut(url, body) {
    this.spinner.openSpinner();
    const promise = new Promise((resolve, reject) => {
      this.http.put(url, body).subscribe(
        res => {
          resolve(res);
          this.toastrService['success'](TAGS.messages.processOk, this.appService.pageTitle, this.options_notitications);
          this.spinner.closeSpinner();
        }, err => {
          resolve();
          this.toastrService['error'](TAGS.messages.processError, this.appService.pageTitle, this.options_notitications);
          this.spinner.closeSpinner();
        });
    });
    return promise;
  }

  httpPost(url, body) {
    this.spinner.openSpinner();
    const promise = new Promise((resolve, reject) => {
      this.http.post(url, body).subscribe(
        res => {
          resolve(res);
          this.toastrService['success'](TAGS.messages.processOk, this.appService.pageTitle, this.options_notitications);
          this.spinner.closeSpinner();
        },
        err => {
          if (err.error['text']) {
            if (err.error['text'].toString().toUpperCase().indexOf('DUPLICATE') > -1) {
              this.toastrService['error'](TAGS.messages.codDuplicate, this.appService.pageTitle, this.options_notitications);
              this.spinner.closeSpinner();
            } else {
              this.toastrService['error'](TAGS.messages.processError, this.appService.pageTitle, this.options_notitications);
              this.spinner.closeSpinner();
            }
          } else {
            this.toastrService['error'](TAGS.messages.wsError, this.appService.pageTitle, this.options_notitications);
            this.spinner.closeSpinner();
          }
          reject(err);
        }
      );
    });
    return promise;
  }

  httpPostWithout(url, body) {
    const promise = new Promise((resolve, reject) => {
      this.http.post(url, body).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
    return promise;
  }

  getToday() {
    const today = new Date();
    const todayFormated = today.toISOString().substr(0, 10);
    return todayFormated;
  }

  getTodayNow() {
    const today = new Date();
    return today;
  }

  getHour() {
    const today = new Date();
    const todayFormated = today.toString().substr(16, 8);
    return todayFormated;
  }

  getKilometros (lat1,lon1,lat2,lon2) {
    let rad = function(x) {
      return x*Math.PI/180;
    };
    let R = 6378.137; //Radio de la tierra en km
    let dLat = rad( lat2 - lat1 );
    let dLong = rad( lon2 - lon1 );
    let a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong/2) * Math.sin(dLong/2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    let d = R * c;
    return d.toFixed(2); //Retorna tres decimales
  }

  validateIdentificationCard(identificación, tipo) {
    let cedula = '';
    if (tipo === 1) {
      if (identificación.length === 13) {
        const val_ruc   = identificación.substring(10, 13);
        if (val_ruc.toString() !== '001') {
          return false;
        } else {
          cedula = identificación.substring(0, 10);
        }
      } else {
        return false;
      }
    } else {
      cedula = identificación;
    }
    if (cedula.length === 10) {
      const digito_region = cedula.substring(0, 2);
      if ( parseInt(digito_region, 10) >= 1 && parseInt(digito_region, 10) <= 24 ) {
        const ultimo_digito   = cedula.substring(9, 10);
        const par1 = parseInt(cedula.substring(1, 2), 10);
        const par2 = parseInt(cedula.substring(3, 4), 10);
        const par3 = parseInt(cedula.substring(5, 6), 10);
        const par4 = parseInt(cedula.substring(7, 8), 10);
        const pares = par1 + par2 + par3 + par4;
        let numero1 = parseInt(cedula.substring(0, 1), 10);
        numero1 = (numero1 * 2);
        if (numero1 > 9 ) {
          numero1 = (numero1 - 9);
        }
        let numero3 = parseInt(cedula.substring(2, 3), 10);
        numero3 = (numero3 * 2);
        if ( numero3 > 9 ) {
          numero3 = (numero3 - 9);
        }
        let numero5 = parseInt(cedula.substring(4, 5), 10);
        numero5 = (numero5 * 2);
        if ( numero5 > 9 ) {
          numero5 = (numero5 - 9);
        }
        let numero7 = parseInt(cedula.substring(6, 7), 10);
        numero7 = (numero7 * 2);
        if ( numero7 > 9 ) {
          numero7 = (numero7 - 9);
        }
        let numero9 = parseInt(cedula.substring(8, 9), 10);
        numero9 = (numero9 * 2);
        if ( numero9 > 9 ) {
          numero9 = (numero9 - 9);
        }
        const impares = numero1 + numero3 + numero5 + numero7 + numero9;
        const suma_total = (pares + impares);
        const primer_digito_suma = String(suma_total).substring(0, 1);
        const decena = (parseInt(primer_digito_suma, 10) + 1)  * 10;
        let digito_validador = decena - suma_total;
        if (digito_validador === 10) {
          digito_validador = 0;
        }
        if (digito_validador.toString() === ultimo_digito.toString()) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  validarRuc(ruc) {
    const dto = ruc.length;
    let valor = 0;
    let acu = 0;
    if (ruc.toString() === '') {
      return false;
    } else {
      for (let i = 0; i < dto; i++) {
        valor = ruc.substring(i, i + 1);
        if (valor == 0 || valor == 1 || valor == 2 || valor == 3 || valor == 4 || valor == 5 || valor == 6 || valor == 7 ||
          valor == 8 || valor == 9) {
          acu = acu + 1;
        }
      }
      if(acu == dto) {
        if (ruc.substring(10, 13).toString() !== '001' ) {
          return false;
        }
        if (ruc.substring(0, 2) > 24) {
          return false;
        }
        console.log('El RUC está escrito correctamente');
        return true;
        // let porcion1 = ruc.substring(2,3);
        // if(porcion1<6){
        //   alert('El tercer dígito es menor a 6, por lo \ntanto el usuario es una persona natural.\n');
        // } else {
        //   if(porcion1==6){
        //     alert('El tercer dígito es igual a 6, por lo \ntanto el usuario es una entidad pública.\n');
        //   } else{
        //     if(porcion1==9){
        //       alert('El tercer dígito es igual a 9, por lo \ntanto el usuario es una sociedad privada.\n');
        //     }
        //   }
        // }
      } else{
        return false;
      }
    }
  }

}
