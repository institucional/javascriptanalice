import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

// *******************************************************************************
// Layouts

import { LayoutHorizontalSidenavComponent } from './layout/layout-horizontal-sidenav/layout-horizontal-sidenav.component';
import { LayoutBlankComponent } from './layout/layout-blank/layout-blank.component';


// *******************************************************************************
// Pages

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ValidateComponent } from './validate/validate.component';
import { SettingsComponent } from './settings/settings.component';
import { ContactComponent } from './contact/contact.component';
import { HelpComponent } from './help/help.component';

// *******************************************************************************
// Libs

import { AuthGuard } from './services/auth/auth.guard';

// *******************************************************************************
// Routes

const routes: Routes = [

// Home , canActivate:[AuthGuard]
  { path: 'home', component: LayoutHorizontalSidenavComponent, pathMatch: 'full', children: [
    { path: '', component: HomeComponent },
  ]},

  // Login
  { path: '', component: LayoutBlankComponent, children: [
      { path: '', component: LoginComponent },
    ]},
// Login
  { path: 'login', component: LayoutBlankComponent, children: [
    { path: '', component: LoginComponent },
  ]},

// Admin , canActivate:[AuthGuard]
  { path: 'admin', component: LayoutHorizontalSidenavComponent, loadChildren: './+admin/admin.module#AdminModule'},

// Fastmovil , canActivate:[AuthGuard]
  { path: 'fastmovil', component: LayoutHorizontalSidenavComponent, loadChildren: './+fastmovil/fastmovil.module#FastmovilModule'},

// Validate User
  { path: 'validate', component: LayoutBlankComponent, children: [
    { path: '', component: ValidateComponent },
  ]},

// Contact , canActivate:[AuthGuard]
  { path: 'settings', component: LayoutHorizontalSidenavComponent, children: [
    { path: '', component: SettingsComponent },
  ]},

// Contact, canActivate:[AuthGuard]
  { path: 'contact', component: LayoutHorizontalSidenavComponent, children: [
    { path: '', component: ContactComponent },
  ]},

// Help, canActivate:[AuthGuard]
  { path: 'help', component: LayoutHorizontalSidenavComponent, children: [
    { path: '', component: HelpComponent},
  ]},

  { path: 'reports', component: LayoutHorizontalSidenavComponent, loadChildren: './reports/report.module#ReportModule'},

];

// *******************************************************************************
//

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
})
export class AppRoutingModule {}
