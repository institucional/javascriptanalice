import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppService } from '../../app.service';
import { GV } from '../../GeneralVars';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { WS } from '../../WebServices';
import { TAGS } from '../../TextLabels';
import { Page } from '../../components/pagination/page';
import { FunctionService } from '../../services/functions/function.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { EmpresaControllerService } from '../../services/controllers/empresa-controller.service';

@Component({
  selector: 'app-secuencia-voucher',
  templateUrl: './secuencia-voucher.component.html',
  styleUrls: [
    '../../../../node_modules/@swimlane/ngx-datatable/release/index.css',
    '../../../vendor/libs/ngx-datatable/ngx-datatable.scss',
    '../../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css',
    '../../../vendor/libs/ngx-toastr/ngx-toastr.scss'
  ],
  encapsulation: ViewEncapsulation.None
})
export class SecuenciaVoucherComponent implements OnInit {
  public navBtnList: { url: string; selected: boolean; label: string }[];

  @ViewChild(DatatableComponent) table: DatatableComponent;
  banCompany = false;
  page = new Page();
  formSecuencia: FormGroup;
  submitedSecuencia: Boolean = false;
  enableState: Boolean = true;
  loadingIndicator = true;
  rows = null;
  temp = null;
  selected = [];
  empresas = [];
  convenio = {};
  secuenciaVoucher = {
    'id': '',
    'valorMinimo': '',
    'valorMaximo': '',
    'valorActual': '',
    'SGE_Empresa_idEmpresa': '',
    'SGE_Convenio_idConvenio': '',
    'estado': '',
    'codusu': '',
    'fecult': '',
    'status': '',
    'ipMaquina': ''
  };
  valoractual = '';
  labels = null;
  gv = null;
  dataSesion = null;
  editSave: string;
  ipAddress = null;
  estados = [];
  responseList = null;
  options_notitications = {
    tapToDismiss: true,
    closeButton: false,
    progressBar: true,
    positionClass: 'toast-top-right',
    rtl: this.appService.isRTL
  };
  constructor(
    private _empresaControllerService: EmpresaControllerService,
    private appService: AppService,
    private formBuilder: FormBuilder,
    private http: HttpClient,
    public toastrService: ToastrService,
    private functionService: FunctionService,
    private _router: Router
  ) {
    this.navBtnList = [
      { url: '/admin/characteristics', selected: false, label: 'Convenios' },
      { url: '/admin/contactos', selected: false, label: 'Contactos' },
      { url: '/admin/secuenciavoucher', selected: true, label: 'Secuencias' },
      { url: '/admin/presupuesto', selected: false, label: 'Presupuestos' },
      { url: '/admin/responsables', selected: false, label: 'Responsables' }
    ];
  }
  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function (d) {
      return d.empresa.nombreEmpresa.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows = temp;
    this.table.offset = 0;
  }
  onSelect({ selected }) {
    this.banCompany = true;
    this.enableState = false;
    this.editSave = GV.editSave.edit;
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    this.secuenciaVoucher = this.selected[0];
    this.valoractual = this.secuenciaVoucher.valorActual;
    this.secuenciaVoucher.SGE_Empresa_idEmpresa = this.selected[0].empresa.id;
    this.secuenciaVoucher.SGE_Convenio_idConvenio = this.selected[0].convenio.codConvenio;
    const empresa = this.empresas.find((emp) => {
      return emp.id.toString() === this.secuenciaVoucher.SGE_Empresa_idEmpresa.toString();
    });
    this.http.post(WS.characteristics.exist, empresa).subscribe(res => {
      this.convenio = res[0];
      if (this.convenio) {
        this.secuenciaVoucher.SGE_Convenio_idConvenio = this.convenio['codConvenio'];
      } else {
        this.toastrService['error'](this.labels.secuenciaVoucher.errorConvenio,
          this.appService.pageTitle, this.options_notitications);
      }
    });
  }
  onKey(event: any, param: string) { // without type info
    this.secuenciaVoucher[param] = event.target.value;
    this.valoractual = this.secuenciaVoucher.valorMinimo;
  }
  loadData() {
    this.page.pageNumber = 0;
    this.setDataPagination(0, this.page.size);
  }
  save() {
    this.secuenciaVoucher.ipMaquina = this.ipAddress;
    this.secuenciaVoucher.fecult = this.functionService.getToday();
    this.secuenciaVoucher.codusu = this.dataSesion['user_id'];
    this.secuenciaVoucher.status = '1';
    if (this.secuenciaVoucher.valorMaximo <= this.secuenciaVoucher.valorMinimo) {
      this.toastrService['error'](this.labels.messages.errorsecval, this.appService.pageTitle, this.options_notitications);
      return;
    }
    const empresa = this.empresas.find((emp) => {
      return emp.id.toString() === this.secuenciaVoucher.SGE_Empresa_idEmpresa.toString();
    });
    this.secuenciaVoucher['convenio'] = this.convenio;
    this.secuenciaVoucher['empresa'] = empresa;
    this.secuenciaVoucher.valorActual = this.valoractual;
    if (this.editSave === GV.editSave.save) {
      if (this.formSecuencia.valid) {
        this.submitedSecuencia = false;
        this.functionService.httpPost(WS.secuenciaVoucher.save, this.secuenciaVoucher).then(() => {
          this.clear();
          setTimeout(() => {
            this._router.navigate(['admin/presupuesto']);
          }, 2000);
        });
      } else {
        this.submitedSecuencia = true;
      }
    } else {
      this.functionService.httpPost(WS.secuenciaVoucher.save, this.secuenciaVoucher).then(() => {
        this.clear();
      });
    }
  }

  changeCompany() {
    const empresa = this.empresas.find((emp) => {
      return emp.id.toString() === this.secuenciaVoucher.SGE_Empresa_idEmpresa.toString();
    });
    this.http.post(WS.characteristics.exist, empresa).subscribe(res => {
      this.convenio = res[0];
      if (this.convenio) {
        this.secuenciaVoucher.SGE_Convenio_idConvenio = this.convenio['codConvenio'];
      } else {
        this.toastrService['error'](this.labels.secuenciaVoucher.errorConvenio,
          this.appService.pageTitle, this.options_notitications);
      }
    });
    this.setDataPagination(0, this.page.size);
  }

  clear() {
    this.banCompany = false;
    this.secuenciaVoucher = {
      'id': '',
      'valorMinimo': '',
      'valorMaximo': '',
      'valorActual': '',
      'SGE_Empresa_idEmpresa': '',
      'SGE_Convenio_idConvenio': '',
      'estado': '',
      'codusu': '',
      'fecult': '',
      'status': '',
      'ipMaquina': ''
    };
    this.secuenciaVoucher.estado = this.responseList[0].content[0].id;
    this.secuenciaVoucher.SGE_Empresa_idEmpresa = (this.empresas[0]) ? this.empresas[0].id : null;
    const empresa = this.empresas.find((emp) => {
      return emp.id.toString() === this.secuenciaVoucher.SGE_Empresa_idEmpresa.toString();
    });
    this.http.post(WS.characteristics.exist, empresa).subscribe(res => {
      this.convenio = res[0];
      if (this.convenio) {
        this.secuenciaVoucher.SGE_Convenio_idConvenio = this.convenio['codConvenio'];
      } else {
        this.toastrService['error'](this.labels.messages.errorConv, this.appService.pageTitle, this.options_notitications);
      }
    });
    this.formSecuencia.controls.name.setValue('');
    this.formSecuencia.controls.desc.setValue('');
    this.submitedSecuencia = false;
    this.editSave = GV.editSave.save;
    this.loadingIndicator = true;
    this.enableState = true;
    this.valoractual = '';
    this.setDataPagination(0, 5);
  }
  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    this.setDataPagination(pageInfo.offset, pageInfo.pageSize);
  }
  setDataPagination(pageNumber, size) {
    this.page.pageNumber = pageNumber;
    this.page.size = size;
    let url = WS.secuenciaVoucher.fetchEmp.replace('${empId}', this.secuenciaVoucher.SGE_Empresa_idEmpresa);
    url = url.replace('${pageNumber}', pageNumber);
    url = url.replace('${size}', size);
    this.functionService.fetch(url).then((res) => {
      this.temp = res['content'];
      this.rows = res['content'];
      this.page.totalElements = res['totalElements'];
      this.loadingIndicator = false;
    });
  }
  ngOnInit() {
    this.dataSesion = localStorage.getItem('userToken');
    this.dataSesion = this.functionService.decode(this.dataSesion);
    this.page.pageNumber = 0;
    this.page.size = 5;
    this.labels = TAGS;
    this.gv = GV;
    this.appService.pageTitle = this.labels.secuenciaVoucher.title;
    this.formSecuencia = this.formBuilder.group({
      name: [null, [Validators.required, Validators.minLength(1)]],
      desc: [null, [Validators.required, Validators.minLength(2)]],
    });
    const array = [WS.general.status];
    this.functionService.httpObservableGet(array).then((responseList) => {
      this.estados = responseList[0].content;
      this.responseList = responseList;
      this.functionService.getIpAddress().then((res) => {
        this.ipAddress = res;
      });
      this._empresaControllerService.getEmpresaListController()
        .then((empresasList) => {
          this.empresas = empresasList;
          this.clear();
        });
    });
  }
  get fc() { return this.formSecuencia.controls; }

}
