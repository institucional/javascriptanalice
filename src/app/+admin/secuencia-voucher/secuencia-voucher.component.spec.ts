import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecuenciaVoucherComponent } from './secuencia-voucher.component';

describe('SecuenciaVoucherComponent', () => {
  let component: SecuenciaVoucherComponent;
  let fixture: ComponentFixture<SecuenciaVoucherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecuenciaVoucherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecuenciaVoucherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
