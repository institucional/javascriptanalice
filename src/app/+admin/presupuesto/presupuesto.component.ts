/**
 * AUTOR: FREDI ROMAN
 */
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { FunctionService } from '../../services/functions/function.service';
import { ConfirmationDialogService } from '../../components/confirmation-dialog/confirmation-dialog.service';
import { TAGS } from '../../TextLabels';
import { GV } from '../../GeneralVars';
import { WS } from '../../WebServices';
import { Empresa } from '../../models/empresa';
import { EmpresaService } from '../../services/component-services/empresa.service';
import { Page } from '../../components/pagination/page';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Presupuesto } from '../../models/presupuesto';
import { PresupuestoService } from '../../services/component-services/presupuesto.service';
import { Month } from '../../interfaces/month.interface';
import months from '../../data/months';
import { ConvenioService } from '../../services/component-services/convenio.service';
import { Convenio } from '../../models/convenio';
import { ModalService } from '../../services/modal-service/modal.service';
import { BODY_TYPES } from '../../data/modal-body-type';
import { DetallePresupuestoComponent } from '../detalle-presupuesto/detalle-presupuesto.component';
import { EmpresaControllerService } from '../../services/controllers/empresa-controller.service';
import { NgForm } from '@angular/forms';
import { Sucursal } from '../../models/sucursal';
import { Departamento } from '../../models/departamento';
import { Nivel } from '../../models/nivel';
import { NivelService } from '../../services/component-services/nivel.service';
import { SucursalService } from '../../services/component-services/sucursal.service';
import { DepartamentoService } from '../../services/component-services/departamento.service';
import { SwitchInputInterface } from '../../dynamic-form/interfaces/switch-input.interface';
import { TimeInterface } from '../../dynamic-form/interfaces/time.interface';
import { SWITCH_SIZE } from '../../dynamic-form/data/switch-size';

const BASE_NAME = 'PresupuestoComponent';

@Component({
  selector: 'app-presupuesto',
  templateUrl: './presupuesto.component.html',
  styleUrls: [
    './presupuesto.component.scss',
    '../../../../node_modules/@swimlane/ngx-datatable/release/index.css',
    '../../../vendor/libs/ngx-datatable/ngx-datatable.scss',
    '../../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css',
    '../../../vendor/libs/ngx-toastr/ngx-toastr.scss'
  ],
  encapsulation: ViewEncapsulation.None
})
export class PresupuestoComponent implements OnInit {
  @ViewChild("frmPresupuesto") frmPresupuesto: NgForm;
  @ViewChild(DatatableComponent) table: DatatableComponent;
  private ipAddress: string;

  public labels: any;
  public gv: any;
  public navBtnList: { url: string; selected: boolean; label: string }[];
  public empresaList: Empresa[];
  public mainEmpresaId: number;
  public nivelesList: Nivel[];
  public currentNivelIndex: number;
  public sucursalList: Sucursal[];
  public parentDepaList: Departamento[];
  public depaList: Departamento[];
  public parentDepaName: string;
  public parentDepaId: string;
  public depaName: string;
  public yearList: string[];
  public monthList: Month[];
  public presupuesto: Presupuesto;
  public saveEdit: boolean;
  public page: Page;
  public presupuestoList: Presupuesto[];
  public filterPresupuestoList: Presupuesto[];
  public localConvenio: Convenio;
  public loadingIndicator: boolean;
  public switchIns: SwitchInputInterface[];
  public timeVars: TimeInterface[];
  public selectedRows: any[];
  public autoGenPresupuesto: string;

  constructor(
    private _functionService: FunctionService,
    private _empresaControllerService: EmpresaControllerService,
    private _confirmationDialogService: ConfirmationDialogService,
    private _empresaService: EmpresaService,
    private _nivelService: NivelService,
    private _sucursalService: SucursalService,
    private _convenioService: ConvenioService,
    private _departamentoService: DepartamentoService,
    public _presupuestoService: PresupuestoService,
    public _modalService: ModalService,
  ) {
    this.navBtnList = [
      { url: '/admin/characteristics', selected: false, label: 'Convenios' },
      { url: '/admin/contactos', selected: false, label: 'Contactos' },
      { url: '/admin/secuenciavoucher', selected: false, label: 'Secuencias' },
      { url: '/admin/presupuesto', selected: true, label: 'Presupuestos' },
      { url: '/admin/responsables', selected: false, label: 'Responsables' }
    ];

    this.defineConvenio();
    this.definePresupuestoObj();

    this.selectedRows = [];
    this.page = new Page();
    this.page.size = 5;
    this.labels = TAGS;
    this.gv = GV;
    this.saveEdit = true;
    this.loadingIndicator = true;
  }

  ngOnInit() {
    this.defineTimeVars();
    this.loadInitData();
  }

  /**
   * METODO PARA CAPTURAR LOS VALORES DE LAS VARIABLES DE TIEMPO
   * AUTOR: FREDI ROMAN
   * @param event
   */
  public getTimeChanges(event: TimeInterface) {
    for (let time of this.timeVars) {
      if (event.id == time.id) time = event;
    }

    this.presupuesto.poliTaxiLaborable = JSON.stringify({
      desde: this.timeVars[0].hours + ":" + this.timeVars[0].minutes + ":" + this.timeVars[0].seconds,
      hasta: this.timeVars[1].hours + ":" + this.timeVars[1].minutes + ":" + this.timeVars[1].seconds,
    });
    this.presupuesto.poliTaxiFeriaFds = JSON.stringify({
      desde: this.timeVars[2].hours + ":" + this.timeVars[2].minutes + ":" + this.timeVars[2].seconds,
      hasta: this.timeVars[3].hours + ":" + this.timeVars[3].minutes + ":" + this.timeVars[3].seconds,
    });
  }

  /**
   * METODO PARA DEFINIR LAS VARIABLES DE TIEMPO
   * AUTOR: FREDI ROMAN
   */
  private defineTimeVars() {
    this.timeVars = [
      { id: 'labDesde', label: 'Desde', hours: "00", minutes: "00", seconds: '00' },
      { id: 'labHasta', label: 'Hasta', hours: "00", minutes: "00", seconds: '00' },
      { id: 'ferDesde', label: 'Desde', hours: "00", minutes: "00", seconds: '00' },
      { id: 'ferHasta', label: 'Hasta', hours: "00", minutes: "00", seconds: '00' },
    ]
  }

  /**
   * METODO PARA CAPTURAR EL TIPO DE GENERACION AUTOMATICA DE PRESUPUESTO ESCOGIDO POR EL USUARIO:
   * AUTOR: FREDI ROMAN
   * @param event ID QUE VIENE POR EL OUTPUT EVENT EMITTER DEL COMPONENTE HIJO
   */
  public getSelectedSwitchValue(event: string) {
    this.autoGenPresupuesto = this.switchIns.find(sw => sw.id == event).checked ? event : null;
  }

  /**
   * METODO PARA DEFINIR EL ARRAY DE TIPO SWITCH-INPUT
   * AUTOR: FREDI ROMAN
   */
  private defineSwitchInputs() {
    this.switchIns = [];
    for (let property in GV.autoGenPresupuesto) {
      this.switchIns.push({
        id: property,
        label: GV.autoGenPresupuesto[property],
        checked: false,
        size: SWITCH_SIZE.xs,
        immutable: false,
        customClass: 'switch-normal'
      });
    }
  }

  /**
   * METODO PARA ORDERNAR LA APERTURA DE UN MODAL PARA MOSTRAR EL DETALLE GENERAL DE PRESUPUESTOS:
   * AUTOR: FREDI ROMAN
   */
  public openGeneralDetail() {
    this._modalService.openModal(
      {
        baseComponentId: BASE_NAME,
        icon: '',
        title: TAGS.detallePresupuesto.title,
        componentType: BODY_TYPES.angularComponent,
        body: DetallePresupuestoComponent,
        bodyData: {
          yearList: this.yearList,
          enterpriseId: this.mainEmpresaId,
          empresaList: this.empresaList,
          sucursalId: this.presupuesto.sucursal.id,
          sucursalList: this.sucursalList,
          convenioId: this.localConvenio.id
        },
        btnList: [
          {
            icon: '',
            label: TAGS.terminos.leftModalBtn,
            btnClass: 'btn-primary',
            onClickAction: 1
          },
          {
            icon: '',
            label: TAGS.terminos.rightModalBtn,
            btnClass: 'btn-secondary',
            onClickAction: 2
          }]
      }
    );
  }
  /******************************************************************************************
  *******************************************************************************************
   ****** FORM DEFINITION
  ********************************************************************************************
  *******************************************************************************************/

  /**
   * METODO PARA PROCESAR EL SUBMIT DEL FORMULARIO DE REGISTRO Y ACTULIZACION DE PRESUPUESTO:
   * AUTOR: FREDI ROMAN
   */
  public onSubmit() {
    this.presupuesto.active = JSON.parse(this.presupuesto.active + "");
    if (!this.saveEdit && !this.presupuesto.active) {
      const text = TAGS.messages.genericInactivate;
      this._confirmationDialogService.confirm(this.labels.general.confirm.toLocaleUpperCase(), text)
        .then((confirmed) => {
          if (confirmed) { this.saveData(); }
        });
    }
    else {
      this.saveData();
    }
  }

  /**
   * METODO PARA INSERTAR O ACTUALIZAR UN REGISTRO DE PRESUPUESTO
   * AUTOR: FREDI ROMAN
   */
  public saveData() {
    let url;
    this.presupuesto.ipMaquina = this.ipAddress;
    this.presupuesto.convenio.poliTaxiLaborable = this.presupuesto.convenio.poliTaxiLaborable.toString();
    this.presupuesto.convenio.poliTaxiFeriaFds = this.presupuesto.convenio.poliTaxiFeriaFds.toString();
    if (this.saveEdit) {
      let postData: any = this.presupuesto;
      let date: Date = new Date();
      if (this.autoGenPresupuesto == GV.autoGenPresupValues.autoPresupByUnit + "") {
        url = WS.presupuesto.autoPresupByUnit.replace("${convenioId}", this.presupuesto.convenio.id + "");
        postData = {
          valor: this.presupuesto.valor,
          estado: GV.state.active,
          codusu: 1,
          fecult: date.getFullYear() + "-" + date.getMonth() + '-' + date.getDate(),
          status: 1,
          ipMaquina: this.presupuesto.ipMaquina,
          poliTaxiLaborable: this.presupuesto.poliTaxiLaborable,
          poliTaxiFeriaFds: this.presupuesto.poliTaxiFeriaFds,
          idConvenio: this.presupuesto.convenio.id,
          idSucursal: this.presupuesto.sucursal.id,
          idDepartamento: this.presupuesto.departamento.idDepartamento
        };
      } else if (this.autoGenPresupuesto == GV.autoGenPresupValues.autoPresupForAllUnits + "") {
        url = WS.presupuesto.autoPresupForAllUnits.replace("${convenioId}", this.presupuesto.convenio.id + "");
        postData = {
          valor: this.presupuesto.valor,
          estado: GV.state.active,
          codusu: 1,
          fecult: date.getFullYear() + "-" + date.getMonth() + '-' + date.getDate(),
          status: 1,
          ipMaquina: this.presupuesto.ipMaquina,
          poliTaxiLaborable: this.presupuesto.poliTaxiLaborable,
          poliTaxiFeriaFds: this.presupuesto.poliTaxiFeriaFds,
          idConvenio: this.presupuesto.convenio.id,
          idSucursal: this.presupuesto.sucursal.id,
          idNivel: this.nivelesList[this.currentNivelIndex].id
        };
      } else {
        url = WS.presupuesto.post.replace("${convenioId}", this.presupuesto.convenio.id + "");
        this.presupuesto.status = 1;
      }
      this._functionService.httpPost(url, postData).then((presupuestoData) => {
        this.setPaginatedTableData(0, this.page.size);
      });
    }
    else {
      url = WS.presupuesto.put.replace("${convenioId}", this.presupuesto.convenio.id + "").replace("${presupuestoId}", this.presupuesto.id).replace("${isActive}", this.presupuesto.active + "");
      this.presupuesto.status = 2;
      this._functionService.httpPut(url, this.presupuesto).then((presupuestoData) => {
        this.setPaginatedTableData(0, this.page.size);
      });
    }
    this.resetForm();
  }

  /**
   * METODO PARA FILTRAR LOS MESES A ESCOGER DADO LAS FECHAS DEL CONVENIO, AL SELECCIONAR UN ANIO DE LA LISTA
   * AUTOR: FREDI ROMAN
   */
  public onYearChange() {
    this.defineMonths();
  }

  /**
   * METODO PARA CAPTURAR EL EVENTO CHANGE DEL SELECT DE DEPARTAMENTOS PADRE PARA RECARGAR LOS DATOS DEPENDIENTES
   * AUTOR: FREDI ROMAN
   */
  public onChangeParentDepa() {
    this.presupuesto.departamento.idDepartamento = null;
    this.loadSubDepartamtData();
  }

  /**
   * METODO PARA PROCESAR EL EVENTO CLICK DE UN BOTON NIVEL
   * AUTOR: FREDI ROMAN
   * @param event
   * @param i
   */
  public onNivelClick(event: number) {
    this.resetForm();
    this.currentNivelIndex = event;
    if (this.currentNivelIndex > 0) {
      this.loadParentDeparmentData();
    } else {
      this.loadDeparmentData();
    }
  }

  /**
   * METODO PARA DETECTAR EL CAMBIO DE SUCURSAL PARA CARGAR DATOS CONFORME AL REGISTRO SELECCIONADO
   * AUTOR: FREDI ROMAN
   */
  public onSucursalChange() {
    this.setPaginatedTableData(0, this.page.size);
  }

  /**
   * METODO PARA CARGAR LOS DATOS DE ACUERDO A LA EMPRESA SELECCIONADA
   * AUTOR: FREDI ROMAN
   */
  public onEnterpriseChange() {
    this.loadNivelesData();
    this.loadSucursalesData();
  }

  /**
   * METOOD PARA CARGAR LOS DEPARTAMENTOS HIJOS DE UN DEPATAMENTO
   * AUTOR: FREDI ROMAN
   */
  private loadSubDepartamtData() {
    if (this.parentDepaId) {
      this.depaName = this.nivelesList[this.currentNivelIndex].nombre;
      this._functionService.httpGetWithout(WS.departamento.getByParentDepaId.replace("${parentDepaId}", this.parentDepaId))
        .then((respList: any) => {
          this.depaList = this._departamentoService.createDepartamentoList(respList);
          this.presupuesto.departamento.idDepartamento = (this.depaList[0]) ? this.depaList[0].idDepartamento : null;
        });
    } else {
      this.depaList = [];
      this.presupuesto.departamento.idDepartamento = null;
      this.loadingIndicator = false;
    }
  }

  /**
   * METODO PARA CARGAR LA INFORMACION DE LOS DEPARTAMENTOS PADRES JUNTO CON SUS SUBDEPARTAMENTOS INMEDIATOS:
   * AUTOR: FREDI ROMAN
   */
  private loadParentDeparmentData() {
    if (this.nivelesList.length > 0) {
      let parentNivelId = this.nivelesList[this.currentNivelIndex - 1].id;
      this.parentDepaName = this.nivelesList[this.currentNivelIndex - 1].nombre;
      this._functionService.httpGetWithout(WS.departamento.get.replace("${nivelId}", parentNivelId))
        .then((responseList: any) => {
          this.parentDepaList = this._departamentoService.createDepartamentoList(responseList);
          this.parentDepaId = this.parentDepaList[0] ? this.parentDepaList[0].idDepartamento : null;
          this.loadSubDepartamtData();
        });
    } else {
      this.parentDepaList = [];
      this.depaList = [];
      this.presupuesto.departamento.idDepartamento = null;
      this.loadingIndicator = false;
    }
  }

  /**
   * METODO PARA CARGAR LA INFORMACION DE LOS DEPARTAMENTOS DE UN NIVEL DE UNA EMPRESA
   * AUTOR: FREDI ROMAN
   */
  private loadDeparmentData() {
    if (this.nivelesList.length > 0) {
      let parentNivelId = this.nivelesList[this.currentNivelIndex].id;
      this.depaName = this.nivelesList[this.currentNivelIndex].nombre;
      this._functionService.httpGetWithout(WS.departamento.get.replace("${nivelId}", parentNivelId))
        .then((responseList: any) => {
          this.depaList = this._departamentoService.createDepartamentoList(responseList);
          this.presupuesto.departamento.idDepartamento = (this.depaList[0]) ? this.depaList[0].idDepartamento : null;
        });
    } else {
      this.depaList = [];
      this.presupuesto.departamento.idDepartamento = null;
      this.loadingIndicator = false;
    }
  }

  /**
   * METODO PARA CARGAR LA INFORMACION DE LOS NIVELES DE UNA EMPRESA
   * AUTOR: FREDI ROMAN
   */
  private loadNivelesData() {
    if (this.mainEmpresaId) {
      this._functionService.httpGet(WS.nivel.get.replace("${enterpriseId}", this.mainEmpresaId + ""))
        .then((responseList: any) => {
          this.nivelesList = this._nivelService.createNivelList(responseList);
          this.currentNivelIndex = 0;
          this.loadDeparmentData();
        });
    } else {
      this.nivelesList = [];
      this.loadingIndicator = false;
    }
  }

  /**
   * METODO PARA OBTENER EL CONVENIO ACTIVO DE LA EMPRESA
   * AUTOR: FREDI ROMAN
   */
  private getCurrentConvenio() {
    if (this.mainEmpresaId) {
      this._functionService.httpGetWithout(WS.presupuesto.getActiveConvenio.replace("${enterpriseId}", this.mainEmpresaId + "")).then((responseList) => {
        if (responseList) {
          this.localConvenio = this._convenioService.extractConvenioData(responseList);
          this.defineYears();
          this.definePresupuestoObj();
          this.defineMonths();
          this.defineSwitchInputs();
          this.setPaginatedTableData(0, this.page.size);
        } else {
          this.defineConvenio();
          this.presupuestoList = [];
          this.filterPresupuestoList = [];
          this.loadingIndicator = false;
        }
      });
    } else {
      this.defineConvenio();
      this.presupuestoList = [];
      this.filterPresupuestoList = [];
      this.loadingIndicator = false;
    }
  }

  /**
   * METODO PARA CARGAR LA INFORMACION DE LAS SUCURSALES DE UNA EMPRESA
   * AUTOR: FREDI ROMAN
   */
  private loadSucursalesData() {
    if (this.mainEmpresaId) {
      this._functionService.httpGetWithout(WS.sucursales.sucByStateCompany.replace("${empresa}", this.mainEmpresaId + ""))
        .then((responseList: any) => {
          this.sucursalList = this._sucursalService.createSucursalList(responseList.content);
          this.presupuesto.sucursal.id = this.sucursalList[0] ? this.sucursalList[0].id : null;
          this.getCurrentConvenio();
        });
    } else {
      this.sucursalList = [];
      this.presupuesto.sucursal.id = null;
    }
  }

  /**
   * METODO PARA CARGAR LOS DATOS DE INICIO, DESDE EL BACKEND, PARA MOSTRARLO EN EL DOM
   * AUTOR: FREDI ROMAN
   */
  private loadInitData() {
    this._empresaControllerService.getEmpresaListController()
      .then((empresaData) => {
        this.empresaList = this._empresaService.createEmpresaList(empresaData);
        this.mainEmpresaId = (this.empresaList[0]) ? this.empresaList[0].id : null;

        this.loadNivelesData();
        this.loadSucursalesData();
        this._functionService.getIpAddress().then((res: any) => {
          this.ipAddress = res;
        });
      });
  }

  /**
   * METODO PARA DEFINIR EL OBJETO PRESUPUESTO
   * AUTOR: FREDI ROMAN
   */
  private definePresupuestoObj() {
    if (!this.presupuesto) {
      this.presupuesto = new Presupuesto();
    }
    this.presupuesto.convenio = (this.localConvenio.codConvenio) ? this.localConvenio : null;
    this.presupuesto.departamento.idDepartamento = this.depaList ? this.depaList[0].idDepartamento : null;
    this.presupuesto.anio = this.yearList ? this.yearList[0] : null;
    this.presupuesto.active = true;
  }

  /**
   * METODO PARA RESETEAR LOS ELEMENTOS DEL FORMULARIO
   * AUTOR: FREDI ROMAN
   */
  public resetForm() {
    this.frmPresupuesto.form.controls["valor"].reset();
    this.selectedRows.splice(0, this.selectedRows.length);
    this.definePresupuestoObj();
    this.defineTimeVars();
    for (let sw of this.switchIns) {
      sw.checked = false;
      sw.immutable = false;
      sw.disabled = false;
    }
    this.autoGenPresupuesto = null;
    this.saveEdit = true;
  }

  /**
   * METODO PARA DEFINIR LOS MESES EN LOS CUALES SE DEFINIRA UN PRESUPUESTO, DE ACUERDO AL INTERVALO DE FECHAS
   * DEL CONVENIO ACTUAL
   * AUTOR: FREDI ROMAN
   */
  private defineMonths() {
    this.monthList = months;

    let index;
    let dateArray = this.localConvenio.fechaSuscripcion.split("-");
    if (this.presupuesto.anio == dateArray[0]) {
      index = this.monthList.findIndex(month => month.monthCode == dateArray[1]);
      this.monthList = this.monthList.slice(index);
    } else {
      dateArray = this.localConvenio.fechaVencimiento.split("-");
      if (this.presupuesto.anio == dateArray[0]) {
        index = this.monthList.findIndex(month => month.monthCode == dateArray[1]);
        this.monthList = this.monthList.slice(0, index + 1);
      }
    }

    let d = new Date();
    let m: any = (d.getMonth() + 1) + '';
    m = (m.length == 1) ? '0' + m : m;
    m = this.monthList.find(month => month.monthCode == m);
    m = !m ? this.monthList[0].monthCode : m.monthCode;
    this.presupuesto.mes = m;
  }

  /**
   * METODO PARA DEFINIR LA LISTA DE ANIOS A PARTIR DEL AÑO DE LA SUSCRIPCION DEL CONVENIO
   * AUTOR: FREDI ROMAN
   */
  private defineYears() {
    this.yearList = [];
    let yearSus;
    let yearVen;

    if (this.localConvenio.fechaSuscripcion) {
      yearSus = this.localConvenio.fechaSuscripcion.split("-")[0];
      yearVen = this.localConvenio.fechaVencimiento.split("-")[0];
      this.yearList.push(yearSus);
      if (yearVen != yearSus) {
        this.yearList.push(yearVen);
      }
    } else {
      yearSus = new Date().getFullYear();
      this.yearList.push(yearSus);
    }
  }

  /**
   * METODO PARA DEFINIR O RESETEAR EL OBJETO CONVENIO:
   * AUTOR: FREDI ROMAN
   */
  private defineConvenio() {
    this.localConvenio = new Convenio();
  }
  /******************************************************************************************
  ******************************************************************************************/

  /******************************************************************************************
  *******************************************************************************************
   ****** TABLE DEFINITION
  ********************************************************************************************
  *******************************************************************************************/

  /**
   * METODO PARA REALIZAR UN FILTRO EN LA TABLA
   * AUTOR: FREDI ROMAN
   * @param event
   */
  public onTableFilter(event) {
    const val = event.target.value.toLowerCase();
    this.filterPresupuestoList = this.presupuestoList.slice();
    const temp = this.filterPresupuestoList.filter((filterData) => {
      return filterData.anio.indexOf(val) !== -1 || filterData.mesDesc.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.filterPresupuestoList = temp;
    this.table.offset = 0;
  }

  /**
   * METODO PARA RECARGAR LOS DATOS DE LA TABLA AL CAMBIA EL NUMERO DE REGISTROS A SER MOTRADOS EN LA MISMA
   * AUTOR: FREDI ROMAN
   */
  public reloadTableData() {
    this.page.pageNumber = 0;
    this.setPaginatedTableData(0, this.page.size);
  }

  /**
   * METODO PARA CAPTURAR EL EVENTO DE SELECCION DE UNA FILA DE LA TABLA
   * AUTOR: FREDI ROMAN
   * @param selecteRow
   */
  public onSelectRow(selecteRow: { selected: any[] }) {
    this.selectedRows.splice(0, this.selectedRows.length);
    this.selectedRows.push(...selecteRow.selected);
    this.saveEdit = false;
    this.presupuesto.id = selecteRow.selected[0].id;
    this.presupuesto.anio = selecteRow.selected[0].anio;
    this.presupuesto.mes = selecteRow.selected[0].mes;
    this.presupuesto.valor = selecteRow.selected[0].valor;
    this.presupuesto.convenio = selecteRow.selected[0].convenio;
    this.presupuesto.departamento.idDepartamento = selecteRow.selected[0].departamento.idDepartamento;
    this.presupuesto.departamento.nivel.id = selecteRow.selected[0].departamento.nivel.id;
    this.presupuesto.sucursal.id = selecteRow.selected[0].sucursal.id;
    this.presupuesto.active = selecteRow.selected[0].active;
    this.timeVars = selecteRow.selected[0].poliTaxiLaborable || selecteRow.selected[0].poliTaxiFeriaFds ? this._convenioService.extractTimeData(selecteRow.selected[0]) : null;

    /***************************************CHANGING LEVEL AND DEPARTMENTS**************************/
    let index = this.nivelesList.findIndex(niv => niv.id == this.presupuesto.departamento.nivel.id);
    for (let i = 0; i < this.nivelesList.length; i++) {
      if (index == i) {
        this.nivelesList[index].selected = true;
      } else {
        this.nivelesList[i].selected = false;
      }
    }
    if (index != this.currentNivelIndex) {
      this.currentNivelIndex = index;
      if (this.currentNivelIndex > 0) {
        this.loadParentDeparmentData();
      } else {
        this.loadDeparmentData();
      }
    }
    /***********************************************************************************************
    ***********************************************************************************************/

    for (let sw of this.switchIns) {
      sw.checked = false;
      sw.immutable = true;
      sw.disabled = true;
    }
  }

  /**
   * METODO PARA DEFINIR LA PAGINACION DE LA TABLA, AL CAPTURAR EL EVENTO DE NEXT/PREV PAGE
   * AUTOR: FREDI ROMAN
   * @param pageInfo
   */
  public setPage(pageInfo: any) {
    this.page.pageNumber = pageInfo.offset;
    this.setPaginatedTableData(pageInfo.offset, pageInfo.pageSize);
  }

  /**
   * METODO PARA DEFINIR LOS DATOS DE LA TABLA AL INTERACTUAR CON LA PAGINACION
   * AUTOR: FREDI ROMAN
   * @param pageNumber
   * @param size
   */
  public setPaginatedTableData(pageNumber: any, size: any) {
    this.page.pageNumber = pageNumber;
    this.page.size = size;

    if (this.localConvenio.id) {
      let url = WS.presupuesto.fetch.replace("${convenioId}", this.localConvenio.id + "").replace("${sucId}", this.presupuesto.sucursal.id).replace('${pageNumber}', pageNumber).replace('${size}', size);
      this._functionService.fetch(url)
        .then((res) => {
          this.presupuestoList = this._presupuestoService.createPresupuestoList(res['content']);
          this.filterPresupuestoList = this.presupuestoList.slice();
          this.page.totalElements = res['totalElements'];
        });
    } else {
      this.presupuestoList = [];
      this.filterPresupuestoList = [];
      this.page.totalElements = 0;
    }
    this.loadingIndicator = false;
  }
  /******************************************************************************************
  ******************************************************************************************/
}
