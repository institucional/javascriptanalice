/**
 * AUTOR: FREDI ROMAN
 */
import { Component, OnInit, ViewEncapsulation, ViewChild, OnDestroy } from '@angular/core';
import { FunctionService } from '../../services/functions/function.service';
import { ConfirmationDialogService } from '../../components/confirmation-dialog/confirmation-dialog.service';
import { TAGS } from '../../TextLabels';
import { GV } from '../../GeneralVars';
import { WS } from '../../WebServices';
import { Page } from '../../components/pagination/page';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { User } from '../../models/user';
import { UserService } from '../../services/component-services/user.service';
import { UsuarioEmpresa } from '../../models/usuario-empresa';
import { UsuarioEmpresaService } from '../../services/component-services/usuario-empresa.service';
import { ModalService } from '../../services/modal-service/modal.service';
import { BODY_TYPES } from '../../data/modal-body-type';
import { EmpresaListComponent } from '../empresa-list/empresa-list.component';
import { Subscription } from 'rxjs';
import { BTN_CLICK_ACTIONS } from '../../data/button-click-actions';
import { ToastrService } from 'ngx-toastr';
import { ArrayManager } from '../../tools/array-manager';

const BASE_NAME = 'UsuarioEmpresa';

@Component({
  selector: 'app-usuario-empresa',
  templateUrl: './usuario-empresa.component.html',
  styleUrls: [
    './usuario-empresa.component.scss',
    '../../../../node_modules/@swimlane/ngx-datatable/release/index.css',
    '../../../vendor/libs/ngx-datatable/ngx-datatable.scss',
    '../../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css',
    '../../../vendor/libs/ngx-toastr/ngx-toastr.scss'
  ],
  encapsulation: ViewEncapsulation.None
})
export class UsuarioEmpresaComponent implements OnInit, OnDestroy {
  @ViewChild(DatatableComponent) table: DatatableComponent;
  private ipAddress: string;
  private filterPatter: string;
  private userId: number;
  private subscriber: Subscription;
  private btnSubscriber: Subscription;

  public labels: any;
  public gv: any;
  public usuarioEmpresaList: UsuarioEmpresa[];
  public currUsuarioEmpresa: UsuarioEmpresa;
  public usuarioEmpresa: UsuarioEmpresa;
  public page: Page;
  public userList: User[];
  public filterUserList: User[];
  public loadingIndicator: boolean;
  public selectedRow: any[];
  public mainEnterpriseId: number;

  constructor(
    private _functionService: FunctionService,
    private _confirmationDialogService: ConfirmationDialogService,
    private _usuarioEmpresaService: UsuarioEmpresaService,
    public _userService: UserService,
    public _modalService: ModalService,
    public _toastrService: ToastrService
  ) {
    this.usuarioEmpresa = new UsuarioEmpresa();
    this.currUsuarioEmpresa = new UsuarioEmpresa();
    this.filterPatter = "";

    this.selectedRow = [new User()];
    this.page = new Page();
    this.labels = TAGS;
    this.gv = GV;
    this.loadingIndicator = true;
  }

  ngOnInit() {
    this.setPaginatedTableData(0, 5);
    this.listenToAsocEnterprise();
    this.listenToModalClickedBtn();
  }

  /**
   * METODO PARA ESCUCHAR CUANDO UN BOTON HA SIDO CLICKEADO
   * AUTOR: FREDI ROMAN
   */
  private listenToModalClickedBtn() {
    this.btnSubscriber = this._modalService.onModalBtnClick$.subscribe((modalAction: { baseComponentId: string, actionType: number }) => {
      if (modalAction && modalAction.baseComponentId == BASE_NAME) {
        switch (modalAction.actionType) {
          case BTN_CLICK_ACTIONS.asociarEmpresa:
            this.saveData();
            break;
        }
      }
    });
  }

  /**
   * METODO PARA ESCUCHAR CUANDO UNA EMPRESA A SER ASOCIADA, SEA SELECCIONADA DESDE LA TABLA MODAL DE EMPRESAS
   * AUTOR: FREDI ROMAN
   */
  private listenToAsocEnterprise() {
    this.subscriber = this._modalService.modalData$.subscribe((modalData: { baseComponentId: string, data: any }) => {
      if (modalData && modalData.baseComponentId == BASE_NAME) {
        this.usuarioEmpresa.empresa = modalData.data;
      }
    });
  }

  /**
   * METODO PARA ABRIR EL MODAL PARA MOSTRAR LA LISTA DE EMPRESAS PARA ASOCIADARLAS AL USUARIO
   * AUTOR: FREDI ROMAN
   */
  public openEnterpriseModal() {
    this._modalService.openModal({
      baseComponentId: BASE_NAME,
      title: TAGS.usuarioEmpresa.modalAsocTitle,
      icon: '',
      componentType: BODY_TYPES.angularComponent,
      body: EmpresaListComponent,
      bodyData: { baseComponentId: BASE_NAME, userId: this.userId },
      btnList: [
        {
          icon: '',
          label: TAGS.general.add,
          btnClass: 'btn-primary',
          onClickAction: BTN_CLICK_ACTIONS.asociarEmpresa
        },
        {
          icon: '',
          label: TAGS.general.cancel,
          btnClass: 'btn-secondary',
          onClickAction: BTN_CLICK_ACTIONS.close
        }]
    });
  }

  /**
   * METODO PARA ELIMINAR UN REGISTRO SELECCIONADO
   * AUTOR: FREDI ROMAN
   */
  public onDeleteSelected() {
    const text = TAGS.messages.usuarioEmpresa.confirmDelete;
    this._confirmationDialogService.confirm(this.labels.general.confirm.toLocaleUpperCase(), text)
      .then((confirmed) => {
        if (confirmed) { this.deleteSelected(); }
      });
  }

  /**
   * METODO PARA SOLICITAR LA ELIMINACION DE UN REGISTRO SELECCIONADO, HACIA EL BACKEND
   * AUTOR: FREDI ROMAN
   */
  private deleteSelected() {
    let url = WS.usuarioEmpresa.del.replace("${enterpriseId}", this.currUsuarioEmpresa.empresa.id + "").replace("${userEnterId}", this.currUsuarioEmpresa.id);
    this._functionService.httpDelete(url).then(() => {
      ArrayManager.alterArrayData(this.usuarioEmpresaList, this.currUsuarioEmpresa, ArrayManager.REMOVE);
      this.defineCurrUsuarioEmp(this.usuarioEmpresaList.length > 0 ? this.usuarioEmpresaList[0] : new UsuarioEmpresa());
    });
  }

  /**
   * METODO PARA CAPTURAR EL EVENTO CHANGE DEL SELECT QUE CONTIENE LA LISTA DE EMPRESAS
   * AUTOR: FREDI ROMAN
   */
  public changeEnterprise() {
    this.currUsuarioEmpresa = this.usuarioEmpresaList.find(record => record.empresa.id == this.mainEnterpriseId);
  }

  /******************************************************************************************
  *******************************************************************************************
   ****** FORM DEFINITION
  ********************************************************************************************
  *******************************************************************************************/

  /**
   * METODO PARA INSERTAR O ACTUALIZAR UN REGISTRO
   * AUTOR: FREDI ROMAN
   */
  public saveData() {
    if (!this.usuarioEmpresa.empresa.id) {
      this._toastrService['error'](TAGS.messages.usuarioEmpresa.noEmpresa, TAGS.messages.processError);
      return;
    }
    let url;
    this.usuarioEmpresa.ipMaquina = this.ipAddress;
    url = WS.usuarioEmpresa.post.replace("${enterpriseId}", this.usuarioEmpresa.empresa.id + "").replace("${userId}", this.userId + "");
    this.usuarioEmpresa.status = 1;
    this._functionService.httpPost(url, this.usuarioEmpresa).then((respData) => {
      this.usuarioEmpresa = new UsuarioEmpresa();
      this.defineCurrUsuarioEmp(this._usuarioEmpresaService.extractUsuarioEmpresaData(respData));
      ArrayManager.alterArrayData(this.usuarioEmpresaList, this.currUsuarioEmpresa, ArrayManager.PREPEND);

      this._modalService.refreshModalBody(null);
      setTimeout(() => {
        this._modalService.refreshModalBody({ baseComponentId: BASE_NAME, refresh: true });
      });
    });
  }

  /**
 * METODO PARA CARGAR LOS DATOS DE INICIO, DESDE EL BACKEND, PARA MOSTRARLO EN EL DOM 
 * AUTOR: FREDI ROMAN
 */
  private loadInitData() {
    const array = [WS.usuarioEmpresa.get.replace("${userId}", this.userId + ""),];
    this._functionService.httpObservableGet(array).then((responseList) => {
      this.usuarioEmpresaList = this._usuarioEmpresaService.createUsuarioEmpresalList(responseList[0]);
      this.defineCurrUsuarioEmp(this.usuarioEmpresaList.length > 0 ? this.usuarioEmpresaList[0] : new UsuarioEmpresa());

      this._functionService.getIpAddress().then((res: any) => {
        this.ipAddress = res;
      });
    });
  }

  /**
   * METODO PARA DEFINIR LOS DATOS DE LA EMPRESA SELECCIONADA DESDE EL SELECT DE EMPRESAS ASOCIADAS AL USUARIO LOGEADO
   * @param usuarioEmp 
   */
  private defineCurrUsuarioEmp(usuarioEmp: UsuarioEmpresa) {
    this.currUsuarioEmpresa = usuarioEmp;
    this.mainEnterpriseId = this.currUsuarioEmpresa.empresa.id;
  }

  /******************************************************************************************
  ******************************************************************************************/

  /******************************************************************************************
  *******************************************************************************************
   ****** TABLE DEFINITION 
  ********************************************************************************************
  *******************************************************************************************/

  /**
   * METODO PARA REALIZAR UN FILTRO EN LA TABLA
   * AUTOR: FREDI ROMAN
   * @param event 
   */
  public onTableFilter(event) {
    const val = event.target.value.toLowerCase();
    this.filterUserList = this.userList.slice();
    const temp = this.filterUserList.filter((filterData) => {
      return (filterData.firstName + " " + filterData.lastName).toLowerCase().indexOf(val) !== -1 || filterData.email.toLowerCase().indexOf(val) !== -1 || filterData.phone.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.filterUserList = temp;
    this.table.offset = 0;
  }

  /**
   * METODO PARA RECARGAR LOS DATOS DE LA TABLA AL CAMBIA EL NUMERO DE REGISTROS A SER MOTRADOS EN LA MISMA
   * AUTOR: FREDI ROMAN
   */
  public reloadTableData() {
    this.page.pageNumber = 0;
    this.setPaginatedTableData(0, this.page.size);
  }

  /**
   * METODO PARA DEFINIR LA PAGINACION DE LA TABLA, AL CAPTURAR EL EVENTO DE NEXT/PREV PAGE
   * AUTOR: FREDI ROMAN
   * @param pageInfo 
   */
  public setPage(pageInfo: any) {
    this.page.pageNumber = pageInfo.offset;
    this.setPaginatedTableData(pageInfo.offset, pageInfo.pageSize);
  }

  /**
   * METODO PARA CAPTURAR EL EVENTO DE SELECCION DE UNA FILA DE LA TABLA
   * AUTOR: FREDI ROMAN
   * @param selecteRow 
   */
  public onSelectRow(selecteRow: { selected: any[] }) {
    this.selectedRow = selecteRow.selected;
    this.userId = this.selectedRow[0].id;
    this.loadInitData();
  }

  /**
   * METODO PARA DEFINIR LOS DATOS DE LA TABLA AL INTERACTUAR CON LA PAGINACION
   * AUTOR: FREDI ROMAN
   * @param pageNumber 
   * @param size 
   */
  public setPaginatedTableData(pageNumber: any, size: any) {
    this.page.pageNumber = pageNumber;
    this.page.size = size;

    let url = WS.usuarioEmpresa.getUsersByFilter.replace("${pattern}", this.filterPatter).replace('${isActive}', 'true').replace('${pageNumber}', this.page.pageNumber + "").replace('${size}', this.page.size + "");
    this._functionService.fetchWithout(url).then((res) => {
      this.userList = this._userService.createUserlList(res['content']);
      this.filterUserList = this.userList.slice();
      this.selectedRow[0] = this.userList[0];
      this.page.totalElements = res['totalElements'];
      this.loadingIndicator = false;

      this.userId = this.selectedRow[0].id;
      this.loadInitData();
    });
  }
  /******************************************************************************************
  ******************************************************************************************/

  ngOnDestroy() {
    this.subscriber.unsubscribe();
    this.btnSubscriber.unsubscribe();
    this._modalService.refreshModalBody(null);
  }
}
