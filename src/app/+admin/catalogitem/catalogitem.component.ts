import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import { AppService } from '../../app.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TAGS } from '../../TextLabels';
import { WS } from '../../WebServices';
import { GV } from '../../GeneralVars';
import {ConfirmationDialogService} from '../../components/confirmation-dialog/confirmation-dialog.service';
import {FunctionService} from '../../services/functions/function.service';
import {Page} from '../../components/pagination/page';

@Component({
  selector: 'tables-ngx-datatable',
  templateUrl: './catalogitem.component.html',
  styleUrls: [
    '../../../../node_modules/@swimlane/ngx-datatable/release/index.css',
    '../../../vendor/libs/ngx-datatable/ngx-datatable.scss',
    '../../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css',
    '../../../vendor/libs/ngx-toastr/ngx-toastr.scss'
  ],
  encapsulation: ViewEncapsulation.None
})


export class CatalogItemComponent {
  public id: string;
  public descripcion: string;
  formCatalogItem: FormGroup;
  submitedCatalogItem: Boolean = false;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  loadingIndicator = true;
  rows = null;
  temp = null;
  selected = [];
  catalogoitem = {
    'id' : '',
    'codSubcatalogo': '',
    'nomSubCatalogo': '',
    'estado': '',
    'desElemento': '',
    'valorNumerico': '',
    'valorAlfanumerico': '',
    'codusu': '1',
    'fecult': '',
    'status': '1',
    'ipMaquina': ''
  };
  ipAddress = null;
  editSave: string;
  labels = null;
  gv = null;
  enableState: Boolean = true;
  codCatalogo: string;
  estados = [];
  responseList = null;
  page = new Page();

  constructor(
    private confirmationDialogService: ConfirmationDialogService,
    private appService: AppService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private functionService: FunctionService
  ) {}
  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function(d) {
      return d.nomSubCatalogo.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows = temp;
    this.table.offset = 0;
  }
  onSelect({ selected }) {
    this.editSave = GV.editSave.edit;
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    this.catalogoitem = this.selected[0];
    this.formCatalogItem.controls.code.setValue(this.catalogoitem.codSubcatalogo);
    this.formCatalogItem.controls.name.setValue(this.catalogoitem.nomSubCatalogo);
    this.formCatalogItem.controls.desc.setValue(this.catalogoitem.desElemento);
    this.enableState = false;
  }
  loadData() {
    this.page.pageNumber = 0;
    this.setDataPagination(0, this.page.size);
  }
  onKey(event: any, param: string) {
    this.catalogoitem[param] = event.target.value;
    this.catalogoitem.codSubcatalogo = this.catalogoitem.codSubcatalogo.toUpperCase();
    this.catalogoitem.nomSubCatalogo = this.catalogoitem.nomSubCatalogo.toUpperCase();
    this.catalogoitem.desElemento = this.catalogoitem.desElemento.toUpperCase();
    this.catalogoitem.valorAlfanumerico = this.catalogoitem.valorAlfanumerico.toUpperCase();
  }
  clear() {
    this.catalogoitem = {
      'id' : '',
      'codSubcatalogo': '',
      'nomSubCatalogo': '',
      'estado': '',
      'desElemento': '',
      'valorNumerico': '',
      'valorAlfanumerico': '',
      'codusu': '1',
      'fecult': '',
      'status': '1',
      'ipMaquina' : ''
    };
    this.formCatalogItem.controls.code.setValue('');
    this.formCatalogItem.controls.name.setValue('');
    this.formCatalogItem.controls.desc.setValue('');
    this.submitedCatalogItem = false;
    this.editSave = GV.editSave.save;
    this.setDataPagination(0, 5);
    this.enableState = true;
    this.catalogoitem.estado = this.responseList[0].content[0].id;
  }
  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    this.setDataPagination(pageInfo.offset, pageInfo.pageSize);
  }
  setDataPagination(pageNumber, size) {
    this.page.pageNumber = pageNumber;
    this.page.size = size;
    let url = WS.subcatalog.fetch;
    url = url.replace('${idCatalogo}', this.route.snapshot.paramMap.get('id'));
    url = url.replace('${pageNumber}', pageNumber);
    url = url.replace('${size}', size);
    this.functionService.fetch(url).then((res) => {
      this.temp = res['content'];
      this.rows = res['content'];
      this.page.totalElements = res['totalElements'];
      this.loadingIndicator = false;
    });
  }
  openConfirmationDialog() {
    const text = this.labels.messages.inactivate + ' ' + this.labels.subcatalog.subText + '.  '
      + this.labels.messages.inactivateConfirm;
    this.confirmationDialogService.confirm(this.labels.general.confirm.toLocaleUpperCase(), text)
      .then((confirmed) => {
        if (confirmed) {
          this.save();
        }
      }).catch(() =>
      console.log('Dismissed.)'
      ));
  }
  confirmMethod() {
    if (this.catalogoitem.estado.toString() === GV.state.inactive) {
      this.openConfirmationDialog();
    } else {
      this.save();
    }
  }
  save() {
    this.catalogoitem.ipMaquina = this.ipAddress;
    this.catalogoitem.fecult = this.functionService.getToday();
    if (this.editSave === GV.editSave.save) {
      if (this.formCatalogItem.valid) {
        this.submitedCatalogItem = false;
        let url = WS.subcatalog.save;
        url = url.replace('${idCatalogo}', this.route.snapshot.paramMap.get('id'));
        this.functionService.httpPost(url, this.catalogoitem).then(() => {
          this.clear();
        });
      } else {
        this.submitedCatalogItem = true;
      }
    } else {
      let url = WS.subcatalog.update;
      url = url.replace('${idCatalogo}', this.route.snapshot.paramMap.get('id'));
      url = url.replace('${idSubcatalogo}', this.selected[0].id.toString());
      this.functionService.httpPut(url, this.catalogoitem).then(() => {
        this.clear();
      });
    }
  }
  ngOnInit() {
    this.page.pageNumber = 0;
    this.page.size = 5;
    this.labels = TAGS;
    this.gv = GV;
    this.appService.pageTitle = this.labels.subcatalog.title;
    this.catalogoitem.estado = GV.state.active;
    this.id = this.route.snapshot.paramMap.get('id');
    this.descripcion = this.route.snapshot.paramMap.get('desc');
    this.codCatalogo = this.route.snapshot.paramMap.get('codc');
    this.formCatalogItem = this.formBuilder.group({
      code: [null, [Validators.required, Validators.minLength(3)]],
      name: [null, [Validators.required, Validators.minLength(3)]],
      desc: [null, [Validators.required, Validators.minLength(2)]],
    });
    const array = [WS.general.status];
    this.functionService.httpObservableGet(array).then((responseList) => {
      this.estados = responseList[0].content;
      this.responseList = responseList;
      this.functionService.getIpAddress().then((res) => {
        this.ipAddress = res;
      });
      this.clear();
    });
  }
  get fc() { return this.formCatalogItem.controls; }
}
