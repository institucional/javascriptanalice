import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeachEmployeeComponent } from './seach-employee.component';

describe('SeachEmployeeComponent', () => {
  let component: SeachEmployeeComponent;
  let fixture: ComponentFixture<SeachEmployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeachEmployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeachEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
