/**
 * AUTOR: FREDI ROMAN
 */
import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef, Input } from '@angular/core';
import { WS } from '../../WebServices';
import { FunctionService } from '../../services/functions/function.service';
import { TAGS } from '../../TextLabels';
import { Empleado } from '../../models/empleado';
import { EmpleadoService } from '../../services/component-services/empleado.service';
import { RestClientService } from '../../services/component-services/rest-client.service';

const AUTO_COMPLETE_CLASS = 'show-autocomplete';

@Component({
  selector: 'seach-employee',
  templateUrl: './seach-employee.component.html',
  styleUrls: ['./seach-employee.component.scss']
})
export class SeachEmployeeComponent implements OnInit {
  @ViewChild("autoComplete") autoCompleteContRef: ElementRef;
  private autoCompleteCont: HTMLElement;
  @Input() class: string;
  @Input() enterpriseIdArray: number[];
  @Input() enterpriseId: number;
  @Input() sucId: number;
  @Input() convenioId: number;
  @Output() onDataFilter: EventEmitter<Empleado>;

  private timeout: any;
  private timeoutBlur: any;

  public filterPattern: string;
  public filteredData: Empleado[];
  public labels: any;

  constructor(
    private _functionService: FunctionService,
    private _empleadoService: EmpleadoService,
    private _restClientService: RestClientService
  ) {
    this.onDataFilter = new EventEmitter<Empleado>();
    this.labels = TAGS;
  }

  ngOnInit() {
    this.autoCompleteCont = <HTMLElement>this.autoCompleteContRef.nativeElement;
  }

  /**
   * METODO PARA SELECCIONAR UNA OPCION DEL AUTOCOMPLETE PARA ENVIARLO HACIA EL COMPONENTE PADRE
   * AUTOR: FREDI ROMAN
   * @param event 
   * @param index 
   */
  public onOptionClick(event: any, index: number) {
    event.preventDefault();
    this.onDataFilter.emit(this.filteredData[index]);
    this.showHideOptions();
  }

  /**
   * METODO PARA MOSTRAR U OCULTAR LA LISTA DE OPCIONES:
   * AUTOR: FREDI ROMAN
   * @param hide 
   */
  public showHideOptions(hide: boolean = true) {
    if (this.timeoutBlur) {
      clearTimeout(this.timeoutBlur);
    }
    this.timeoutBlur = setTimeout(() => {
      let autoComplete = this.autoCompleteCont.querySelector('.fr-auto-complete');
      let autoCompleteClass = autoComplete.classList;
      if (hide) {
        autoCompleteClass.remove(AUTO_COMPLETE_CLASS);
      }
      else {
        autoCompleteClass.add(AUTO_COMPLETE_CLASS);
      }
    }, 300);
  }

  /**
   * METODO PARA MANIPULAR EL EVENTO DE KEYUP DEL FILTRO DE BUSQUEDA DE USUARIOS:
   * AUTOR: FREDI ROMAN
   */
  public onFilteringUser() {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }

    this.timeout = setTimeout(() => {
      this.loadFilteredData();
    }, 500);
  }

  /**
   * METODO PARA CARGAR UNA LISTA DE USUARIOS DADO UN FILTRO
   * AUTOR: FREDI ROMAN
   */
  private loadFilteredData() {
    this.filteredData = [];
    let url;
    if (this.enterpriseId && !this.convenioId) {
      url = WS.reportes.filterEmpoyeeByEnterId.replace('${enterpriseId}', this.enterpriseId + "").replace("${sucId}", this.sucId + "").replace('${filter}', this.filterPattern);
      this._functionService.getService(url).then((respData: any) => {
        if (respData) {
          for (let dat of respData) {
            this.filteredData.push(this._empleadoService.extractEmpleadoData(JSON.parse(dat)));
          }
          this.showHideOptions(false);
        }
      }).catch(err => console.log("[SEARCH EMPLOYEE COMPONENT] - ERROR: ", err))
    }
  }
}
