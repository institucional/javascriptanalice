/**
 * AUTOR: FREDI ROMAN
 */
import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { FunctionService } from '../../services/functions/function.service';
import { ConfirmationDialogService } from '../../components/confirmation-dialog/confirmation-dialog.service';
import { TAGS } from '../../TextLabels';
import { GV } from '../../GeneralVars';
import { WS } from '../../WebServices';
import { Empresa } from '../../models/empresa';
import { EmpresaService } from '../../services/component-services/empresa.service';
import { Page } from '../../components/pagination/page';
import { ConvenioService } from '../../services/component-services/convenio.service';
import { Convenio } from '../../models/convenio';
import { ResponsableContacto } from '../../models/responsable-contacto';
import { ResponsableContactoService } from '../../services/component-services/responsable-contacto.service';
import { Empleado } from '../../models/empleado';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { EmpresaControllerService } from '../../services/controllers/empresa-controller.service';
import { Sucursal } from '../../models/sucursal';
import { AccordionInterface } from '../../fr-accordion/interfaces/accordion-interface';
import { SucursalService } from '../../services/component-services/sucursal.service';
import { Presupuesto } from '../../models/presupuesto';
import { TableResponsiveService } from '../../services/table/table-responsive.service';
import { SwitchInputInterface } from '../../dynamic-form/interfaces/switch-input.interface';
import { SWITCH_SIZE } from '../../dynamic-form/data/switch-size';

@Component({
  selector: 'app-resposable',
  templateUrl: './responsable.component.html',
  styleUrls: [
    './responsable.component.scss',
    '../../../../node_modules/@swimlane/ngx-datatable/release/index.css',
    '../../../vendor/libs/ngx-datatable/ngx-datatable.scss',
    '../../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css',
    '../../../vendor/libs/ngx-toastr/ngx-toastr.scss'
  ],
  encapsulation: ViewEncapsulation.None
})
export class ResponsableComponent implements OnInit, OnDestroy {
  @ViewChild("tableContainer") tableContainerRef: ElementRef;
  private tableContainerElement: HTMLElement;

  private ipAddress: string;
  private sub: Subscription;

  public labels: any;
  public gv: any;
  public navBtnList: { url: string; selected: boolean; label: string }[];
  public empresaList: Empresa[];
  public mainEmpresaId: number;
  public sucursalList: Sucursal[];
  public depaWithChildList: AccordionInterface[];
  public tipoResponsableList: { id: string; name: string }[];
  public tipoResponAuxList: { id: string; name: string }[];
  public localConvenio: Convenio;
  public responsable: ResponsableContacto;
  public responsableList: ResponsableContacto[];
  public filterResponsableList: ResponsableContacto[];
  public updateEmpData: boolean;
  public updatePresupData: boolean;
  public switchIns: SwitchInputInterface[];
  public departamentoId: string;

  public saveEdit: boolean;
  public page: Page;
  public loadingIndicator: boolean;
  public showPresup: boolean;
  public updateTable: boolean;
  public ableToSubmit: boolean;
  public unSelectRow: boolean;
  public selectedDepa: string;
  public selectedPresupId: string;
  public selectedEmpId: number;
  public selectedRow: any[];

  constructor(
    private _functionService: FunctionService,
    private _empresaControllerService: EmpresaControllerService,
    private _confirmationDialogService: ConfirmationDialogService,
    private _convenioService: ConvenioService,
    private _empresaService: EmpresaService,
    private _sucursalService: SucursalService,
    public _responsableContactoService: ResponsableContactoService,
    public _activatedRoute: ActivatedRoute,
    public _tableResponsiveService: TableResponsiveService,
  ) {
    this.navBtnList = [
      { url: '/admin/characteristics', selected: false, label: 'Convenios' },
      { url: '/admin/contactos', selected: false, label: 'Contactos' },
      { url: '/admin/secuenciavoucher', selected: false, label: 'Secuencias' },
      { url: '/admin/presupuesto', selected: false, label: 'Presupuestos' },
      { url: '/admin/responsables', selected: true, label: 'Responsables' }
    ];

    this.initResponsable();
    this.defineConvenio();
    this.getRoutingParams();

    this.page = new Page();
    this.page.size = 5;
    this.selectedRow = [];

    this.labels = TAGS;
    this.gv = GV;
    this.saveEdit = true;
    this.loadingIndicator = true;
    this.ableToSubmit = false;
  }

  ngOnInit() {
    this.tableContainerElement = this.tableContainerRef.nativeElement;
    this.defineSwitchInputs();
    this.loadInitData();
  }

  /**
   * METODO PARA CAPTURAR EL TIPO DE GENERACION DE FERIADOS AUTOMATICOS ESCOGIDO POR EL USUARIO:
   * @param event ID QUE VIENE POR EL OUTPUT EVENT EMITTER DEL COMPONENTE HIJO
   */
  public getSelectedSwitchValue(event: string) {
    this.responsable.recibeNotif = this.switchIns.find(sw => sw.id == event).checked;
  }

  /**
   * METODO PARA DEFINIR EL ARRAY DE TIPO SWITCH-INPUT
   * AUTOR: FREDI ROMAN
   */
  private defineSwitchInputs() {
    this.switchIns = [{
      id: 'sw-1',
      label: "Recibe notificaciones",
      checked: this.responsable.recibeNotif,
      size: SWITCH_SIZE.xs,
      immutable: false,
      customClass: 'switch-normal'
    }];
  }

  /**
   * METODO PARA  OBTENER EL OBJETO EMPLEADO SELECCIONADO DE LA TABLA HIJA
   * AUTOR: FREDI ROMAN
   * @param event OBJETO EMPLEADO
   */
  public getSelectedEmplooyee(event: Empleado) {
    this.responsable.empleado.id = event.id;
    if (this.responsable.empleado.id && (!this.showPresup || this.responsable.presupuesto.id)) {
      this.ableToSubmit = true;
    }
  }

  /**
   * METODO PARA OBTENER EL OBJETO PRESUPUESTO SELECCIONAD DE LA TABLA HIJA
   * AUTOR: FREDI ROMAN
   * @param event OBJETO PRESUPUESTO
   */
  public getSelectedPresup(event: Presupuesto) {
    this.responsable.presupuesto.id = event.id;
    if (this.responsable.empleado.id && (!this.showPresup || this.responsable.presupuesto.id)) {
      this.ableToSubmit = true;
    }
  }

  /**
   * METODO PARA CAPTURAR LOS VALORES DE LAS VARIABLES DE URL QUE VIENEN DESDE OTRA RUTA
   * AUTOR: FREDI ROMAN
   */
  private getRoutingParams() {
    this.sub = this._activatedRoute.params.subscribe(params => {
      this.mainEmpresaId = +params['enterpriseId']; // (+) converts string 'id' to a number
    });
  }

  /**
   * METODO PARA INICIALIZAR EL OBJETO CONTACTO
   */
  private initResponsable() {
    this.responsable = new ResponsableContacto();
    this.responsable.clase = GV.catClaseContacto.responsableCliente;
    this.responsable.recibeNotif = false;
    this.responsable.active = true;
  }

  /**
   * METODO PARA CAPTAR EL OBJETO FILTRADO DESDE EL AUTOCOMPLETE
   * AUTOR: FREDI ROMAN
   * @param event
   */
  public getFilteredData(event: Empleado) {
    this.responsable.empleado = event;
  }

  /******************************************************************************************
  *******************************************************************************************
   ****** FORM DEFINITION
  ********************************************************************************************
  *******************************************************************************************/

  /**
   * METODO PARA PROCESAR EL SUBMIT DEL FORMULARIO DE REGISTRO Y ACTULIZACION DE PRESUPUESTO:
   * AUTOR: FREDI ROMAN
   */
  public onSubmit() {
    this.responsable.active = JSON.parse(this.responsable.active + "");
    if (!this.saveEdit && !this.responsable.active) {
      const text = TAGS.messages.genericInactivate;
      this._confirmationDialogService.confirm(this.labels.general.confirm.toLocaleUpperCase(), text)
        .then((confirmed) => {
          if (confirmed) { this.saveData(); }
        });
    }
    else {
      this.saveData();
    }
  }

  /**
   * METODO PARA INSERTAR O ACTUALIZAR UN REGISTRO DE CONTACTO
   * AUTOR: FREDI ROMAN
   */
  public saveData() {
    let url;
    this.responsable.ipMaquina = this.ipAddress;
    const date = new Date();

    let reqData = {
      idResponContacto: null,
      claseResponContacto: this.responsable.clase,
      codTipoResp: this.responsable.codTipoResponsable,
      codusu: 1,
      fecult: date.getFullYear() + "-" + (date.getMonth() < 10 ? "0" + date.getMonth() : date.getMonth()) + "-" + (date.getDay() < 10 ? "0" + date.getDay() : date.getDay()),
      status: this.responsable.status,
      ipMaquina: this.responsable.ipMaquina,
      idEmpleado: this.responsable.empleado.id,
      idSucursal: this.responsable.sucursal.id,
      idConvenio: !this.showPresup ? this.localConvenio.id : null,
      idPresupuesto: this.showPresup ? this.responsable.presupuesto.id : null,
      recibeNotif: this.responsable.recibeNotif ? 1 : 0,
      estado: this.responsable.active ? GV.state.active : GV.state.inactive
    };

    if (this.saveEdit) {
      reqData.status = 1;
    }
    else {
      reqData.idResponContacto = this.responsable.id;
      reqData.status = 2;
    }
    url = WS.responsableContacto.postPut;
    this._functionService.httpPost(url, reqData).then((responsableData) => {
      this.resetForm();
      this.setPaginatedTableData(0, this.page.size);
    }).catch(err => console.log("err", err));
  }

  /**
   * METODO PARA MODIFICAR LA INTERFAZ DE ACUERDO AL TIPO DE RESPONSABLE ESCOGIDO
   * AUTOR: FREDI ROMAN
   */
  public onTipoRChange(updateDomData: boolean = false) {
    if (this.saveEdit) {
      this.resetForm();
      if (this.responsable.codTipoResponsable == GV.subCatTipoResposable.respPoliticaTaxi || this.responsable.codTipoResponsable == GV.subCatTipoResposable.respPresup) {
        this.showPresup = true;
      } else {
        this.showPresup = false;
      }

      if (updateDomData) {
        if (this.showPresup) {
          this.updatePresupData = null;
          setTimeout(() => {
            this.updatePresupData = true;
          }, 200);
        }
        this.updateTable = null;
        setTimeout(() => {
          this.setPaginatedTableData(0, this.page.size);
        }, 100);
      }
    }
  }

  /**
   * METODO PARA OBTENER EL ID DEL DEPARTAMENTO SELECCIONADO DESDE LA JERARQUIA
   * AUTOR: FREDI ROMAN
   * @param event
   */
  public getFocusedDepa(event: string) {
    if (this.departamentoId != event) {
      if (this.saveEdit) {
        this.resetForm();
      }
      this.departamentoId = event;
      this.updateEmpData = null;
      setTimeout(() => {
        this.updateEmpData = true;
        if (!this.saveEdit) {
          this.selectedEmpId = null;
          setTimeout(() => {
            this.selectedEmpId = this.responsable.empleado.id;
          }, 500);
        }
      }, 200);

      if (this.showPresup) {
        this.updatePresupData = null;
        setTimeout(() => {
          this.updatePresupData = true;
          if (!this.saveEdit) {
            this.selectedPresupId = null;
            setTimeout(() => {
              this.selectedPresupId = this.responsable.presupuesto.id;
            }, 500);
          }
        }, 200);
      }

      /**
       * VALIDACION PARA NO TENER QUE CARGAR NUEVAMENTE LA TABLA PRINCIPAL DE RESPONSABLES AL SELECCIONAR UNA
       * FILA DE LA MISMA
       */
      if (this.saveEdit) {
        this.setPaginatedTableData(0, this.page.size);
      }
    } else {
      if (!this.saveEdit) {
        this.selectedEmpId = null;
        setTimeout(() => {
          this.selectedEmpId = this.responsable.empleado.id;
        });
      }

      if (this.showPresup) {
        if (!this.saveEdit) {
          this.selectedPresupId = null;
          setTimeout(() => {
            this.selectedPresupId = this.responsable.presupuesto.id;
          });
        }
      }
    }
  }

  /**
   * METODO PARA CAPTURAR EL EVENTO CHANGE DEL SELECT DE SUCURSAL PARA RECARGAR LOS DATOS DEPENDIENTES
   * AUTOR: FREDI ROMAN
   */
  public onChangeSuc() {
    this.resetForm();
    this.updateEmpData = null;
    setTimeout(() => {
      this.updateEmpData = true;
    }, 200);
    if (this.showPresup) {
      this.updatePresupData = null;
      setTimeout(() => {
        this.updatePresupData = true;
      }, 200);
    }
    this.setPaginatedTableData(0, this.page.size);
  }

  /**
   * METODO PARA CARGAR INFORMACION DE ACUERDO A LA EMPRESA SELECCIONADA
   * AUTOR: FREDI ROMAN
   */
  public onEnterpriseChange() {
    this.resetForm();
    this.departamentoId = null;
    this.loadSucursalesData();
  }

  /**
   * METODO PARA CARGAR LA LISTA DE ITEMS DE LA JERARQUIA DEPARTAMENTAL
   * AUTOR: FREDI ROMAN
   */
  private loadDepaHierarchy() {
    let url = WS.jerarquiaDepa.replace('${enterId}', this.mainEmpresaId + '');
    this._functionService.httpGetWithout(url)
      .then((respData: any) => {
        this.depaWithChildList = []
        for (let data of respData) {
          this.depaWithChildList.push({ id: data.id, nombre: data.nombre, subItems: data.subItems });
        }
        this.getActiveConvenio();
      });
  }

  /**
   * METODO PARA CARGAR LA INFORMACION DE LAS SUCURSALES DE UNA EMPRESA
   * AUTOR: FREDI ROMAN
   */
  private loadSucursalesData() {
    if (this.mainEmpresaId) {
      this._functionService.httpGetWithout(WS.sucursales.sucByStateCompany.replace("${empresa}", this.mainEmpresaId + ""))
        .then((responseList: any) => {
          this.sucursalList = this._sucursalService.createSucursalList(responseList.content);
          this.responsable.sucursal.id = this.sucursalList[0] ? this.sucursalList[0].id : null;

          this.updateEmpData = null;
          setTimeout(() => {
            this.updateEmpData = true;
          }, 200);


          this.loadDepaHierarchy();
        });
    } else {
      this.sucursalList = [];
      this.responsable.sucursal.id = null;
      this.loadingIndicator = false;
    }
  }

  /**
   * METODO PARA CARGAR LOS DATOS DE INICIO, DESDE EL BACKEND, PARA MOSTRARLO EN EL DOM
   * AUTOR: FREDI ROMAN
   */
  private loadInitData() {
    this._empresaControllerService.getEmpresaListController()
      .then((empresaData) => {
        this.empresaList = this._empresaService.createEmpresaList(empresaData);
        for (let i = 0; i < this.empresaList.length; i++) {
          if (this.empresaList[i].tipoEmpresa == GV.tipoEmpresaProvServ) {
            this.empresaList.splice(i, 1);
            i--;
          }
        }
        this.mainEmpresaId = this.mainEmpresaId ? this.mainEmpresaId : this.empresaList[0] ? this.empresaList[0].id : null;

        const url = WS.findSubCatByCatId.replace("${catId}", GV.tipoResponsable + "");
        this._functionService.httpGet(url)
          .then((respData: any) => {
            this.tipoResponsableList = [];
            for (let tipo of respData.content) {
              this.tipoResponsableList.push({ id: tipo.id, name: tipo.nomSubCatalogo });
            }
            this.tipoResponAuxList = this.tipoResponsableList.slice();
            this.responsable.codTipoResponsable = this.tipoResponsableList[0] ? parseInt(this.tipoResponsableList[0].id) : null;
            this.onTipoRChange();
            this.loadSucursalesData();

            this._functionService.getIpAddress().then((res: any) => {
              this.ipAddress = res;
            });
          });
      });
  }

  /**
   * METODO PARA RESETEAR LOS ELEMENTOS DEL FORMULARIO
   * AUTOR: FREDI ROMAN
   */
  public resetForm() {
    this.responsable.active = true;
    this.responsable.recibeNotif = false;
    this.responsable.empleado = new Empleado();
    this.responsable.presupuesto = new Presupuesto();
    this.responsable.convenio = new Convenio();
    this.saveEdit = true;
    this.ableToSubmit = false;
    this.unSelectRow = null;
    setTimeout(() => {
      this.unSelectRow = true;
    });
    this.selectedRow = [];
    this.switchIns[0].checked = this.responsable.recibeNotif;
    this.tipoResponAuxList = this.tipoResponsableList.slice();
  }

  /**
   * METODO PARA OBTENER EL CONVENIO ACTIVO DE LA EMPRESA
   * AUTOR: FREDI ROMAN
   */
  private getActiveConvenio() {
    if (this.mainEmpresaId) {
      this._functionService.httpGetWithout(WS.presupuesto.getActiveConvenio.replace("${enterpriseId}", this.mainEmpresaId + "")).then((responseList) => {
        if (responseList) {
          this.localConvenio = this._convenioService.extractConvenioData(responseList);
          this.responsable.convenio = this.localConvenio;
          this.updateEmpData = null;
          setTimeout(() => {
            this.updateEmpData = true;
          }, 200);

          this.updatePresupData = null;
          setTimeout(() => {
            this.updatePresupData = true;
          }, 200);

          this.setPaginatedTableData(0, this.page.size);
        }
        else {
          this.defineConvenio();
          this.responsableList = [];
          this.filterResponsableList = [];
          this.loadingIndicator = false;
        }
      });
    } else {
      this.defineConvenio();
      this.responsableList = [];
      this.filterResponsableList = [];
      this.loadingIndicator = false;
    }
  }

  /**
   * METODO PARA DEFINIR O RESETEAR EL OBJETO CONVENIO:
   * AUTOR: FREDI ROMAN
   */
  private defineConvenio() {
    this.localConvenio = new Convenio();
  }
  /******************************************************************************************
  ******************************************************************************************/

  /******************************************************************************************
  *******************************************************************************************
   ****** TABLE DEFINITION
  ********************************************************************************************
  *******************************************************************************************/

  /**
   * METODO PARA REALIZAR UN FILTRO EN LA TABLA
   * AUTOR: FREDI ROMAN
   * @param event
   */
  public onTableFilter(event) {
    const val = event.target.value.toLowerCase();
    this.filterResponsableList = this.responsableList.slice();
    const temp = this.filterResponsableList.filter((filterData) => {
      return filterData.empleado.nomCompleto.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.filterResponsableList = temp;
  }

  /**
   * METODO PARA RECARGAR LOS DATOS DE LA TABLA AL CAMBIA EL NUMERO DE REGISTROS A SER MOTRADOS EN LA MISMA
   * AUTOR: FREDI ROMAN
   */
  public reloadTableData() {
    this.page.pageNumber = 0;
    this.setPaginatedTableData(0, this.page.size);
  }

  /**
   * METODO PARA CAPTURAR EL EVENTO DE SELECCION DE UNA FILA DE LA TABLA
   * AUTOR: FREDI ROMAN
   * @param selecteRow
   */
  public onSelectRow(selecteRow: { selected: any[] }) {
    this.saveEdit = false;
    this.responsable.id = selecteRow.selected[0].id;
    this.responsable.clase = selecteRow.selected[0].clase;
    this.responsable.codTipoResponsable = selecteRow.selected[0].codTipoResponsable;
    this.responsable.empleado = selecteRow.selected[0].empleado;
    this.responsable.convenio = selecteRow.selected[0].convenio;
    this.responsable.presupuesto = selecteRow.selected[0].presupuesto;
    this.responsable.recibeNotif = selecteRow.selected[0].recibeNotif;
    this.responsable.active = selecteRow.selected[0].active;

    this.switchIns[0].checked = this.responsable.recibeNotif;

    if (this.responsable.empleado.id && (!this.showPresup || this.responsable.presupuesto.id)) {
      this.ableToSubmit = true;
    }

    this.tipoResponAuxList = this.tipoResponsableList.slice();
    if (this.responsable.convenio.id) {
      for (let i = 0; i < this.tipoResponAuxList.length; i++) {
        for (let property in GV.scTipoResposablePresup) {
          if (this.tipoResponAuxList[i].id == GV.scTipoResposablePresup[property]) {
            this.tipoResponAuxList.splice(i, 1);
          }
        }
      }
    } else if (this.responsable.presupuesto.id) {
      for (let i = 0; i < this.tipoResponAuxList.length; i++) {
        for (let property in GV.scTipoResposablePresup) {
          if (this.tipoResponAuxList[i].id != GV.scTipoResposablePresup[property]) {
            this.tipoResponAuxList.splice(i, 1);
          }
        }
      }
    }

    this.selectedDepa = null;
    setTimeout(() => {
      this.selectedDepa = this.responsable.empleado.cargo.departamento.idDepartamento;
    });
  }

  /**
   * METODO PARA DEFINIR LA PAGINACION DE LA TABLA, AL CAPTURAR EL EVENTO DE NEXT/PREV PAGE
   * AUTOR: FREDI ROMAN
   * @param pageInfo
   */
  public setPage(pageInfo: any) {
    this.page.pageNumber = pageInfo.offset;
    this.setTableOffset();
  }

  /**
   * METODO PARA ESTABLECER EL OFFSET PARA LA PAGINACION DE LA TABLA
   * AUTOR: FREDI ROMAN
   */
  private setTableOffset() {
    let indexInit: number;
    if (this.page.pageNumber == 0) {
      this.filterResponsableList = this.responsableList.slice(0, this.page.size);
    } else {
      indexInit = this.page.pageNumber * this.page.size;
      this.filterResponsableList = this.responsableList.slice(indexInit, indexInit + 5);
    }
  }

  /**
   * METODO PARA DEFINIR LOS DATOS DE LA TABLA AL INTERACTUAR CON LA PAGINACION
   * AUTOR: FREDI ROMAN
   * @param pageNumber
   * @param size
   */
  public setPaginatedTableData(pageNumber: any, size: any) {
    this.loadingIndicator = true;
    this.page.pageNumber = pageNumber;
    this.page.size = size;

    if (this.localConvenio.id && this.responsable.sucursal.id) {
      let url = this.showPresup
        ? WS.responsableContacto.presupFetch.replace("${sucId}", this.responsable.sucursal.id).replace("${convId}", this.localConvenio.id + "").replace("?presupId=${presupId}", "")
        : WS.responsableContacto.fetchResponsableByConv.replace("${convId}", this.localConvenio.id + "");
      this._functionService.fetch(url)
        .then((res: any) => {
          this.updateTable = true;
          this.responsableList = this._responsableContactoService.createContactoList(res);
          this.filterResponsableList = this.responsableList.slice();
          this.page.totalElements = this.responsableList.length;

          setTimeout(() => {
            this._tableResponsiveService.makeTableResponsive(this.tableContainerElement);
            this.loadingIndicator = false;
          }, 300);
        });
    } else {
      this.responsableList = [];
      this.filterResponsableList = [];
      this.page.totalElements = 0;
    }
  }
  /******************************************************************************************
  ******************************************************************************************/

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
