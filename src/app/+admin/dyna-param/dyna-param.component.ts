/**
 * AUTOR: FREDI ROMAN
 */
import { Component, ViewEncapsulation, ViewChild, OnInit } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgForm } from '@angular/forms';
import { TAGS } from '../../TextLabels';
import { GV } from '../../GeneralVars';
import { ConfirmationDialogService } from '../../components/confirmation-dialog/confirmation-dialog.service';
import { FunctionService } from '../../services/functions/function.service';
import { WS } from '../../WebServices';
import { Page } from '../../components/pagination/page';
import { EmpresaControllerService } from '../../services/controllers/empresa-controller.service';
import { Empresa } from '../../models/empresa';
import { EmpresaService } from '../../services/component-services/empresa.service';
import { UUIDManager } from '../../tools/uuid-manager';
import { DynaParam } from '../../models/dyna-param';
import { DynaParamService } from '../../services/component-services/dyna-param.service';
import * as lodash from 'lodash';
import { SwitchInputInterface } from '../../dynamic-form/interfaces/switch-input.interface';
import { SWITCH_SIZE } from '../../dynamic-form/data/switch-size';

@Component({
  selector: 'app-dyna-param',
  templateUrl: './dyna-param.component.html',
  styleUrls: [
    './dyna-param.component.scss',
    '../../../../node_modules/@swimlane/ngx-datatable/release/index.css',
    '../../../vendor/libs/ngx-datatable/ngx-datatable.scss',
    '../../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css',
    '../../../vendor/libs/ngx-toastr/ngx-toastr.scss'
  ],
  encapsulation: ViewEncapsulation.None
})
export class DynaParamsComponent implements OnInit {
  @ViewChild("frmDynaParams") formRef: NgForm;
  @ViewChild(DatatableComponent) table: DatatableComponent;
  private dynaParamSubmitList: DynaParam[];
  private deleteRecord: boolean;

  public navBtnList: { url: string; selected: boolean; label: string }[];
  public empresaList: Empresa[];
  public mainEnterpriseId: number;
  public dynaParam: DynaParam;
  public dynaParamList: DynaParam[];
  public filterDynaParamList: DynaParam[];
  public saveEdit: boolean;
  public page: Page;
  public loadingIndicator: boolean;
  public labels: any;
  public gv: any;
  public switchIns: SwitchInputInterface[];
  public tiposDatoList: any[];
  public catalogosUsuarioList: any[];

  constructor(
    private _functionService: FunctionService,
    private _empresaControllerService: EmpresaControllerService,
    private _empresaService: EmpresaService,
    private _dynaParamService: DynaParamService,
    private _confirmationDialogService: ConfirmationDialogService,
  ) {
    this.navBtnList = [
      { url: '/admin/company', selected: false, label: 'Empresa' },
      { url: '/admin/sucursales', selected: false, label: 'Sucursal' },
      { url: '/admin/feriado', selected: false, label: 'Feriados' },
      { url: '/admin/definiciones', selected: true, label: 'Definiciones' }
    ];
    this.dynaParam = new DynaParam();
    this.dynaParam.obligatorio = true;
    this.dynaParam.activo = true;

    this.page = new Page();
    this.page.size = 5;
    this.labels = TAGS;
    this.gv = GV;
    this.saveEdit = true;
    this.deleteRecord = false;
    this.loadingIndicator = true;
  }

  ngOnInit() {
    this.defineSwitchInputs();
    this.loadInitData();
  }

  /**
   * METODO PARA ELIMINAR UN PARAMETRO DE ATRIBUTO DINAMICO PARA EMPLEADOS
   * AUTOR: FREDI ROMAN
   */
  public onParamDelete() {
    this.deleteRecord = true;
    this.onSubmit();
  }

  /**
   * METODO PARA CAPTURAR EL TIPO DE CONTROL PRESUPUESTARIO ESCOGIDO POR EL USUARIO:
   * @param event ID QUE VIENE POR EL OUTPUT EVENT EMITTER DEL COMPONENTE HIJO
   */
  public getSelectedSwitchValue(event: string) {
    this.dynaParam.obligatorio = this.switchIns.find(sw => sw.id == event).checked;
  }

  /**
   * METODO PARA DEFINIR EL ARRAY DE TIPO SWITCH-INPUT
   * AUTOR: FREDI ROMAN
   */
  private defineSwitchInputs() {
    this.switchIns = [{ id: '1', label: 'Obligatorio', checked: this.dynaParam.obligatorio, size: SWITCH_SIZE.xs, customClass: 'switch-normal' }];
  }

  /**
   * METODO PARA PROCESAR EL SUBMIT DEL FORMULARIO DE REGISTRO Y ACTULIZACION:
   * AUTOR: FREDI ROMAN
   */
  public onSubmit() {
    if (!this.saveEdit && !this.dynaParam.activo || this.deleteRecord) {
      const text = TAGS.messages.genericInactivate;
      this._confirmationDialogService.confirm(this.labels.general.confirm.toLocaleUpperCase(), text)
        .then((confirmed) => {
          if (confirmed) { this.saveData(); }
        });
    }
    else {
      this.saveData();
    }
  }

  /**
   * METODO PARA INSERTAR O ACTUALIZAR UN REGISTRO
   * AUTOR: FREDI ROMAN
   */
  public saveData() {
    let url = WS.dynaParams.post.replace("${enterpriseId}", this.mainEnterpriseId + "");
    this.dynaParam.idTipoDato = parseInt("" + this.dynaParam.idTipoDato);
    this.dynaParam.idCatalogo = this.dynaParam.idCatalogo ? parseInt("" + this.dynaParam.idCatalogo) : null;
    this.dynaParamSubmitList = this.dynaParamList.slice();

    if (this.saveEdit) {
      this.dynaParam.id = UUIDManager.generateUUID();
      this.dynaParamSubmitList.push(this.dynaParam);
    } else {
      let index = this.dynaParamSubmitList.findIndex(dyna => dyna.id == this.dynaParam.id);
      if (this.deleteRecord) {
        this.dynaParamSubmitList.splice(index, 1);
      } else {
        this.dynaParamSubmitList.splice(index, 1, this.dynaParam);
      }
    }
    this._functionService.httpPost(url, this.dynaParamSubmitList).then((resp) => {
      this.resetForm();
      this.setPaginatedTableData(0, this.page.size);
    });
  }

  /**
   * METODO PARA RESETEAR LOS ELEMENTOS DEL FORMULARIO
   * AUTOR: FREDI ROMAN
   */
  public resetForm() {
    this.formRef.form.controls['etiqueta'].reset();
    this.dynaParam.idTipoDato = this.tiposDatoList[0] ? this.tiposDatoList[0].id : null;
    this.dynaParam.tipoDato = this.tiposDatoList[0] ? this.tiposDatoList[0].nomSubCatalogo : null;
    this.dynaParam.idCatalogo = null;
    this.dynaParam.catalogo = null;
    this.dynaParam.obligatorio = true;
    this.dynaParam.activo = true;
    this.switchIns[0].checked = this.dynaParam.obligatorio;
    this.saveEdit = true;
    this.deleteRecord = false;
  }

  /**
   * METODO PARA CAPTURAR EL EVENTO CHANGE DE LA LISTA DE TIPOS DE DATO
   * AUTOR: FREDI ROMAN
   */
  public onTipoChange() {
    this.dynaParam.idCatalogo = null;
    this.dynaParam.catalogo = null;
    this.dynaParam.tipoDato = this.tiposDatoList.find(tipo => tipo.id == this.dynaParam.idTipoDato).nomSubCatalogo;
  }

  /**
   * METODO PARA CAPTURAR EL EVENTO CHANGE DE LA LISTA DE CATALOGOS DEL USUARIO
   * AUTOR: FREDI ROMAN
   */
  public onCatalogoChange() {
    this.dynaParam.catalogo = this.catalogosUsuarioList.find(cat => cat.id == this.dynaParam.idCatalogo).nomCatalogo;
  }

  /**
   * METODO PARA CAPTURAR EL EVENTO CHANGE DEL SELECT DE EMPRESA PARA RECARGAR LOS DATOS DEPENDIENTES
   * AUTOR: FREDI ROMAN
   */
  public onChangeEmpresa() {
    this.setPaginatedTableData(0, this.page.size);
    this.resetForm();
  }

  /**
   * METODO PARA CARGAR LOS DATOS DE INICIO, DESDE EL BACKEND, PARA MOSTRARLO EN EL DOM 
   * AUTOR: FREDI ROMAN
   */
  private loadInitData() {
    const array = [
      WS.findSubCatByCatId.replace('${catId}', GV.tiposDato + ""),
      WS.dynaParams.getCatalogosByTipoTabla.replace('${tipoTabla}', GV.tiposTablaUsuario + ""),
    ];
    this._functionService.httpObservableGet(array).then((responseList) => {
      this._empresaControllerService.getEmpresaListController()
        .then((empresaData) => {
          this.empresaList = this._empresaService.createEmpresaList(empresaData);
          this.mainEnterpriseId = (this.empresaList[0]) ? this.empresaList[0].id : null;
          this.setPaginatedTableData(0, this.page.size);
        });

      this.tiposDatoList = lodash.orderBy(responseList[0].content, ['valorNumerico'], ['asc']);
      this.dynaParam.idTipoDato = this.tiposDatoList[0] ? this.tiposDatoList[0].id : null;
      this.dynaParam.tipoDato = this.tiposDatoList[0] ? this.tiposDatoList[0].nomSubCatalogo : null;

      this.catalogosUsuarioList = responseList[1];
    });
  }

  /******************************************************************************************
  *******************************************************************************************
   ****** TABLE DEFINITION 
  ********************************************************************************************
  *******************************************************************************************/

  /**
   * METODO PARA REALIZAR UN FILTRO EN LA TABLA
   * AUTOR: FREDI ROMAN
   * @param event 
   */
  public onTableFilter(event) {
    const val = event.target.value.toLowerCase();
    this.filterDynaParamList = this.dynaParamList.slice();
    const temp = this.filterDynaParamList.filter((filterData) => {
      return filterData.etiqueta.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.filterDynaParamList = temp;
    this.table.offset = 0;
  }

  /**
   * METODO PARA AUMENTAR EL NUMERO DE FILAS EL LA TABLA Y CARGAR LOS DATOS DESDE EL BACKEND
   * AUTOR: FREDI ROMAN
   */
  public reloadTableData() {
    this.page.pageNumber = 0;
    this.setPaginatedTableData(0, this.page.size);
  }

  /**
   * METODO PARA CAPTURAR EL EVENTO DE SELECCION DE UNA FILA DE LA TABLA
   * AUTOR: FREDI ROMAN
   * @param selecteRow 
   */
  public onSelectRow(selecteRow: { selected: any[] }) {
    this.saveEdit = false;
    this.dynaParam.id = selecteRow.selected[0].id;
    this.dynaParam.etiqueta = selecteRow.selected[0].etiqueta;
    this.dynaParam.idTipoDato = selecteRow.selected[0].idTipoDato;
    this.dynaParam.tipoDato = selecteRow.selected[0].tipoDato;
    this.dynaParam.idCatalogo = selecteRow.selected[0].idCatalogo;
    this.dynaParam.catalogo = selecteRow.selected[0].catalogo;
    this.dynaParam.obligatorio = selecteRow.selected[0].obligatorio;
    this.dynaParam.activo = selecteRow.selected[0].activo;

    this.switchIns[0].checked = this.dynaParam.obligatorio;
  }

  /**
   * METODO PARA DEFINIR LA PAGINACION DE LA TABLA, AL CAPTURAR EL EVENTO DE NEXT/PREV PAGE
   * AUTOR: FREDI ROMAN
   * @param pageInfo 
   */
  public setPage(pageInfo: any) {
    this.page.pageNumber = pageInfo.offset;
    this.setTableOffset();
  }

  /**
   * METODO PARA ESTABLECER EL OFFSET PARA LA PAGINACION DE LA TABLA
   * AUTOR: FREDI ROMAN
   */
  private setTableOffset() {
    let indexInit: number;
    if (this.page.pageNumber == 0) {
      this.filterDynaParamList = this.dynaParamList.slice(0, this.page.size);
    } else {
      indexInit = this.page.pageNumber * this.page.size;
      this.filterDynaParamList = this.dynaParamList.slice(indexInit, indexInit + 5);
    }
  }

  /**
   * METODO PARA DEFINIR LOS DATOS DE LA TABLA AL INTERACTUAR CON LA PAGINACION
   * AUTOR: FREDI ROMAN
   * @param pageNumber 
   * @param size 
   */
  public setPaginatedTableData(pageNumber: any, size: any) {
    this.page.pageNumber = pageNumber;
    this.page.size = size;

    if (this.mainEnterpriseId) {
      let url = WS.dynaParams.get.replace("${enterpriseId}", this.mainEnterpriseId + "");
      this.dynaParamList = [];
      this.filterDynaParamList = [];
      this._functionService.fetch(url).then((res: any) => {
        this.dynaParamList = this._dynaParamService.createDynaParamList(res);
        this.filterDynaParamList = this.dynaParamList.slice();
        this.page.totalElements = this.dynaParamList.length;
      });
    } else {
      this.dynaParamList = [];
      this.filterDynaParamList = [];
      this.page.totalElements = 0;
    }
    this.loadingIndicator = false;
  }
  /******************************************************************************************
  ******************************************************************************************/
}
