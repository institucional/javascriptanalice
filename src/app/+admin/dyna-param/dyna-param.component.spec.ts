import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynaParamsComponent } from './dyna-param.component';

describe('DynaParamsComponent', () => {
  let component: DynaParamsComponent;
  let fixture: ComponentFixture<DynaParamsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynaParamsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynaParamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
