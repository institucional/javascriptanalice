import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConvenioDetailComponent } from './convenio-detail.component';

describe('ConvenioDetailComponent', () => {
  let component: ConvenioDetailComponent;
  let fixture: ComponentFixture<ConvenioDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConvenioDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConvenioDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
