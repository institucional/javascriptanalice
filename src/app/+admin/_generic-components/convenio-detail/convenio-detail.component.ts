import { Component, OnInit, Input } from '@angular/core';
import { Convenio } from '../../../models/convenio';
import { TAGS } from '../../../TextLabels';

@Component({
  selector: 'convenio-detail',
  templateUrl: './convenio-detail.component.html',
  styleUrls: ['./convenio-detail.component.scss']
})
export class ConvenioDetailComponent implements OnInit {
  @Input() convenio: Convenio;
  public labels: any;

  constructor() {
    this.labels = TAGS;
  }

  ngOnInit() {
  }

}
