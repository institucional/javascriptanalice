import { Component, OnInit, SimpleChanges, Input, Output, OnChanges, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Presupuesto } from '../../../models/presupuesto';
import { Page } from '../../../components/pagination/page';
import { TableResponsiveService } from '../../../services/table/table-responsive.service';
import { TAGS } from '../../../TextLabels';
import { WS } from '../../../WebServices';
import { FunctionService } from '../../../services/functions/function.service';
import { PresupuestoService } from '../../../services/component-services/presupuesto.service';

@Component({
  selector: 'presupuesto-list',
  templateUrl: './presupuesto-list.component.html',
  styleUrls: ['./presupuesto-list.component.scss']
})
export class PresupuestoListComponent implements OnInit, OnChanges {
  @Input() updateData: boolean;
  @Input() sucursalId: string;
  @Input() departamentoId: string;
  @Input() convenioId: number;
  @Input() unSelectRow: boolean;
  @Input() selectRowById: string;
  @Output() onPresupSelected: EventEmitter<Presupuesto>;

  @ViewChild("tableContainer") tableContainerRef: ElementRef;
  private tableContainerElement: HTMLElement;

  public presupuestoList: Presupuesto[];
  public filterPresupList: Presupuesto[];
  public page: Page;
  public labels: any;
  public loadingIndicator: boolean;
  public selectedRow: any[];

  constructor(
    private _functionService: FunctionService,
    private _presupuestoService: PresupuestoService,
    public _tableResponsiveService: TableResponsiveService,
  ) {
    this.onPresupSelected = new EventEmitter<Presupuesto>();
    this.page = new Page();
    this.page.size = 5;
    this.selectedRow = [];

    this.labels = TAGS;
    this.loadingIndicator = true;
  }

  ngOnInit() {
    this.tableContainerElement = this.tableContainerRef.nativeElement;
  }

  /**
   * METODO PARA REALIZAR UN FILTRO EN LA TABLA
   * AUTOR: FREDI ROMAN
   * @param event 
   */
  public onEmployeeTableFilter(event) {
    const val = event.target.value.toLowerCase();
    this.filterPresupList = this.presupuestoList.slice();
    const temp = this.filterPresupList.filter((filterData) => {
      return filterData.mesDesc.toLowerCase().indexOf(val) !== -1 || filterData.anio.indexOf(val) !== -1 || !val;
    });
    this.filterPresupList = temp;
  }

  /**
   * METODO PARA CAPTURAR EL EVENTO DE SELECCION DE UNA FILA DE LA TABLA
   * AUTOR: FREDI ROMAN
   * @param selecteRow 
   */
  public onSelectRow(selecteRow: { selected: any[] }) {
    this.onPresupSelected.emit(selecteRow.selected[0]);
  }

  /**
   * METODO PARA DEFINIR LA PAGINACION DE LA TABLA, AL CAPTURAR EL EVENTO DE NEXT/PREV PAGE
   * AUTOR: FREDI ROMAN
   * @param pageInfo 
   */
  public setPage(pageInfo: any) {
    this.page.pageNumber = pageInfo.offset;
    this.setTableOffset();
  }

  /**
   * METODO PARA ESTABLECER EL OFFSET PARA LA PAGINACION DE LA TABLA
   * AUTOR: FREDI ROMAN
   */
  private setTableOffset() {
    let indexInit: number;
    if (this.page.pageNumber == 0) {
      this.filterPresupList = this.presupuestoList.slice(0, this.page.size);
    } else {
      indexInit = this.page.pageNumber * this.page.size;
      this.filterPresupList = this.presupuestoList.slice(indexInit, indexInit + this.page.size);
    }
  }

  /**
   * METODO PARA DEFINIR LOS DATOS DE LA TABLA AL INTERACTUAR CON LA PAGINACION
   * AUTOR: FREDI ROMAN
   * @param pageNumber 
   * @param size 
   */
  public setTableData(pageNumber: any, size: any) {
    this.loadingIndicator = true;
    this.page.pageNumber = pageNumber;
    this.page.size = size;

    if (this.convenioId && this.sucursalId && this.departamentoId) {
      let url = WS.presupuesto.presupListBy.replace("${convenioId}", this.convenioId + "").replace("${sucId}", this.sucursalId).replace("${depaId}", this.departamentoId);
      this.presupuestoList = [];
      this.filterPresupList = [];

      this._functionService.fetchWithout(url)
        .then((res: any) => {
          this.presupuestoList = this._presupuestoService.createPresupuestoList(res.content);
          this.filterPresupList = this.presupuestoList.slice();
          this.page.totalElements = this.presupuestoList.length;

          if (this.selectRowById) {
            this.selectedRow = [
              this.presupuestoList.find(presup => presup.id == this.selectRowById)
            ];
          }

          setTimeout(() => {
            this._tableResponsiveService.makeTableResponsive(this.tableContainerElement);
            this.loadingIndicator = false;
          }, 300);
        });
    } else {
      this.presupuestoList = [];
      this.filterPresupList = [];
      this.page.totalElements = 0;
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    for (let property in changes) {
      switch (property) {
        case 'updateData':
          if (changes[property].currentValue) {
            this.setTableData(0, this.page.size);
          }
          break;
        case 'unSelectRow':
          if (changes[property].currentValue == true) {
            this.selectedRow = [];
          }
          break;
        case 'selectRowById':
          if (changes[property].currentValue) {
            this.selectedRow = [
              this.presupuestoList.find(presup => presup.id == changes[property].currentValue)
            ];
          }
          break;
      }
    }
  }
}
