import { Component, OnInit, Input, OnChanges, SimpleChanges, ElementRef, ViewChild, Output, EventEmitter } from '@angular/core';
import { Empleado } from '../../../models/empleado';
import { Page } from '../../../components/pagination/page';
import { WS } from '../../../WebServices';
import { GV } from '../../../GeneralVars';
import { RestClientService } from '../../../services/component-services/rest-client.service';
import { EmpleadoService } from '../../../services/component-services/empleado.service';
import { TableResponsiveService } from '../../../services/table/table-responsive.service';
import { TAGS } from '../../../TextLabels';

@Component({
  selector: 'employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss']
})
export class EmployeeListComponent implements OnInit, OnChanges {
  @Input() updateData: boolean;
  @Input() sucursalId: string;
  @Input() departamentoId: string;
  @Input() unSelectRow: boolean;
  @Input() selectRowById: number;
  @Output() onEmployeeSelected: EventEmitter<Empleado>;

  @ViewChild("tableContainer1") table1ContainerRef: ElementRef;
  private table1ContainerElement: HTMLElement;
  private filterDepa: boolean;

  public empleadoList: Empleado[];
  public filterEmpleadoList: Empleado[];
  public page: Page;
  public labels: any;
  public loadingIndicator: boolean;
  public selectedRow: any[];

  constructor(
    private _restClientService: RestClientService,
    private _empleadoService: EmpleadoService,
    public _tableResponsiveService: TableResponsiveService,
  ) {
    this.onEmployeeSelected = new EventEmitter<Empleado>();
    this.page = new Page();
    this.page.size = 5;
    this.selectedRow = [];

    this.labels = TAGS;
    this.loadingIndicator = true;
    this.filterDepa = false;
  }

  ngOnInit() {
    this.table1ContainerElement = this.table1ContainerRef.nativeElement;
  }

  /******************************************************************************************
  *******************************************************************************************
   ****** EMPLOYEE TABLE DEFINITION 
  ********************************************************************************************
  *******************************************************************************************/

  /**
   * METODO PARA REALIZAR UN FILTRO EN LA TABLA
   * AUTOR: FREDI ROMAN
   * @param event 
   */
  public onEmployeeTableFilter(event) {
    const val = event.target.value.toLowerCase();
    this.filterEmpleadoList = this.empleadoList.slice();
    const temp = this.filterEmpleadoList.filter((filterData) => {
      return filterData.nomCompleto.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.filterEmpleadoList = temp;
  }

  /**
   * METODO PARA CAPTURAR EL EVENTO DE SELECCION DE UNA FILA DE LA TABLA
   * AUTOR: FREDI ROMAN
   * @param selecteRow 
   */
  public onEmployeeSelectRow(selecteRow: { selected: any[] }) {
    this.onEmployeeSelected.emit(selecteRow.selected[0]);
  }

  /**
   * METODO PARA DEFINIR LA PAGINACION DE LA TABLA, AL CAPTURAR EL EVENTO DE NEXT/PREV PAGE
   * AUTOR: FREDI ROMAN
   * @param pageInfo 
   */
  public setEmployeePage(pageInfo: any) {
    this.page.pageNumber = pageInfo.offset;
    this.setEmployeeTableOffset();
  }

  /**
   * METODO PARA ESTABLECER EL OFFSET PARA LA PAGINACION DE LA TABLA
   * AUTOR: FREDI ROMAN
   */
  private setEmployeeTableOffset() {
    let indexInit: number;
    if (this.page.pageNumber == 0) {
      this.filterEmpleadoList = this.empleadoList.slice(0, this.page.size);
    } else {
      indexInit = this.page.pageNumber * this.page.size;
      this.filterEmpleadoList = this.empleadoList.slice(indexInit, indexInit + 5);
    }
  }

  /**
   * METODO PARA DEFINIR LOS DATOS DE LA TABLA AL INTERACTUAR CON LA PAGINACION
   * AUTOR: FREDI ROMAN
   * @param pageNumber 
   * @param size 
   */
  public setEmployeeTableData(pageNumber: any, size: any, withAnimation: boolean = true) {
    this.loadingIndicator = true;
    this.page.pageNumber = pageNumber;
    this.page.size = size;

    if (this.sucursalId) {
      let url = WS.empleadosSucursal.getByFilter;
      this.empleadoList = [];
      this.filterEmpleadoList = [];

      let filter = {
        idSucursal: this.sucursalId,
        numNivel: "",
        idDepartamento: this.departamentoId,
        estado: GV.state.active
      };
      this._restClientService.httpGetWithHeaders(url, { key: 'filterJson', value: JSON.stringify(filter) }, withAnimation)
        .then((res: any) => {
          this.empleadoList = this._empleadoService.createEmpleadoList(res);
          this.filterEmpleadoList = this.empleadoList.slice();
          this.page.totalElements = this.empleadoList.length;

          if (this.selectRowById) {
            this.selectedRow = [
              this.empleadoList.find(emp => emp.id == this.selectRowById)
            ];
          }

          setTimeout(() => {
            this._tableResponsiveService.makeTableResponsive(this.table1ContainerElement);
            this.loadingIndicator = false;
          }, 300);
        });
    } else {
      this.empleadoList = [];
      this.filterEmpleadoList = [];
      this.page.totalElements = 0;
    }
  }
  /******************************************************************************************
  ******************************************************************************************/
  ngOnChanges(changes: SimpleChanges) {
    for (let property in changes) {
      switch (property) {
        case 'updateData':
          if (changes[property].currentValue) {
            this.setEmployeeTableData(0, this.page.size, false);
          }
          break;
        case 'unSelectRow':
          if (changes[property].currentValue == true) {
            this.selectedRow = [];
          }
          break;
        case 'selectRowById':
          if (changes[property].currentValue) {
            this.selectedRow = [
              this.empleadoList.find(emp => emp.id == changes[property].currentValue)
            ];
          }
          break;
      }
    }
  }
}
