import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavBtnGroupComponent } from './nav-btn-group.component';

describe('NavBtnGroupComponent', () => {
  let component: NavBtnGroupComponent;
  let fixture: ComponentFixture<NavBtnGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavBtnGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavBtnGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
