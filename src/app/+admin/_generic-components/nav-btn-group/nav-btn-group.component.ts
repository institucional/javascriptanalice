import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'nav-btn-group',
  templateUrl: './nav-btn-group.component.html',
  styleUrls: ['./nav-btn-group.component.scss']
})
export class NavBtnGroupComponent implements OnInit {
  @Input() navBtnList: { url: string; selected: boolean; label: string }[];

  constructor() { }

  ngOnInit() {
  }

}
