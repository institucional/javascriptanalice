import { Component, OnInit, Input, Output, EventEmitter, ElementRef, ViewChild, SimpleChanges } from '@angular/core';
import { Convenio } from '../../../models/convenio';
import { Page } from '../../../components/pagination/page';
import { FunctionService } from '../../../services/functions/function.service';
import { ConvenioService } from '../../../services/component-services/convenio.service';
import { TableResponsiveService } from '../../../services/table/table-responsive.service';
import { TAGS } from '../../../TextLabels';
import { WS } from '../../../WebServices';

@Component({
  selector: 'convenios-list',
  templateUrl: './convenios-list.component.html',
  styleUrls: ['./convenios-list.component.scss']
})
export class ConveniosListComponent implements OnInit {
  @Input() updateData: boolean;
  @Output() onConvenioSelected: EventEmitter<Convenio>;

  @ViewChild("tableContainer") tableContainerRef: ElementRef;
  private tableContainerElement: HTMLElement;

  public convenioList: Convenio[];
  public filterConvenioList: Convenio[];
  public page: Page;
  public labels: any;
  public loadingIndicator: boolean;
  public selectedRow: any[];

  constructor(
    private _functionService: FunctionService,
    private _convenioService: ConvenioService,
    public _tableResponsiveService: TableResponsiveService,
  ) {
    this.onConvenioSelected = new EventEmitter<Convenio>();
    this.page = new Page();
    this.page.size = 5;
    this.selectedRow = [];

    this.labels = TAGS;
    this.loadingIndicator = true;
  }

  ngOnInit() {
    this.tableContainerElement = this.tableContainerRef.nativeElement;
  }

  /**
   * METODO PARA REALIZAR UN FILTRO EN LA TABLA
   * AUTOR: FREDI ROMAN
   * @param event 
   */
  public onTableFilter(event) {
    const val = event.target.value.toLowerCase();
    this.filterConvenioList = this.convenioList.slice();
    const temp = this.filterConvenioList.filter((filterData) => {
      //return filterData..toLowerCase().indexOf(val) !== -1 || filterData.anio.indexOf(val) !== -1 || !val;
    });
    this.filterConvenioList = temp;
  }

  /**
   * METODO PARA CAPTURAR EL EVENTO DE SELECCION DE UNA FILA DE LA TABLA
   * AUTOR: FREDI ROMAN
   * @param selecteRow 
   */
  public onSelectRow(selecteRow: { selected: any[] }) {
    this.onConvenioSelected.emit(selecteRow.selected[0]);
  }

  /**
   * METODO PARA DEFINIR LA PAGINACION DE LA TABLA, AL CAPTURAR EL EVENTO DE NEXT/PREV PAGE
   * AUTOR: FREDI ROMAN
   * @param pageInfo 
   */
  public setPage(pageInfo: any) {
    this.page.pageNumber = pageInfo.offset;
    this.setTableData(pageInfo.pageNumber, pageInfo.size);
  }

  /**
   * METODO PARA DEFINIR LOS DATOS DE LA TABLA AL INTERACTUAR CON LA PAGINACION
   * AUTOR: FREDI ROMAN
   * @param pageNumber 
   * @param size 
   */
  public setTableData(pageNumber: any, size: any) {
    this.loadingIndicator = true;
    this.page.pageNumber = pageNumber;
    this.page.size = size;

    let url = WS.responsableContacto.convenioFetch;
    this.convenioList = [];
    this.filterConvenioList = [];

    this._functionService.fetchWithout(url)
      .then((res: any) => {
        this.convenioList = this._convenioService.createConvenioList(res.content);
        this.filterConvenioList = this.convenioList.slice();
        this.page.totalElements = this.convenioList.length;

        setTimeout(() => {
          this._tableResponsiveService.makeTableResponsive(this.tableContainerElement);
          this.loadingIndicator = false;
          this.selectedRow = [
            this.convenioList[0]
          ];
          this.onConvenioSelected.emit(this.convenioList[0]);
        }, 300);
      });
  }

  ngOnChanges(changes: SimpleChanges) {
    for (let property in changes) {
      switch (property) {
        case 'updateData':
          if (changes[property].currentValue) {
            this.setTableData(0, this.page.size);
          }
          break;
      }
    }
  }
}
