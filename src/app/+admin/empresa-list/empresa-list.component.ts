import { Component, OnInit, ComponentRef, OnDestroy, ViewChild } from '@angular/core';
import { Empresa } from '../../models/empresa';
import { Page } from '../../components/pagination/page';
import { FunctionService } from '../../services/functions/function.service';
import { EmpresaService } from '../../services/component-services/empresa.service';
import { TAGS } from '../../TextLabels';
import { WS } from '../../WebServices';
import { ModalService } from '../../services/modal-service/modal.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-empresa-list',
  templateUrl: './empresa-list.component.html',
  styleUrls: ['./empresa-list.component.scss'],
  providers: [EmpresaService]
})
export class EmpresaListComponent implements OnInit, OnDestroy {
  @ViewChild(DatatableComponent) table: DatatableComponent;
  
  public _selfReference: ComponentRef<EmpresaListComponent>;
  public _dynamicData: { baseComponentId: string, userId: number };
  public labels: any
  public empresaList: Empresa[];
  public filterEmpresaList: Empresa[];
  public page: Page;
  public loadingIndicator: boolean;

  constructor(
    private _functionService: FunctionService,
    private _empresaService: EmpresaService,
    private _modalService: ModalService
  ) {
    this.labels = TAGS;
    this.page = new Page();
    this.loadingIndicator = true;
  }

  ngOnInit() {
    setTimeout(() => {
      this.loadEmpresasToAsocList(0, 5);
    }, 500)
  }

  /**
   * METODO PARA REALIZAR UN FILTRO EN LA TABLA
   * AUTOR: FREDI ROMAN
   * @param event 
   */
  public onTableFilter(event) {
    const val = event.target.value.toLowerCase();
    this.filterEmpresaList = this.empresaList.slice();
    const temp = this.filterEmpresaList.filter((filterData) => {
      return filterData.ruc.toLowerCase().indexOf(val) !== -1 || filterData.nombreEmpresa.toLowerCase().indexOf(val) !== -1 || filterData.representanteLegal.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.filterEmpresaList = temp;
    this.table.offset = 0;
  }

  /**
   * METODO PARA RECARGAR LOS DATOS DE LA TABLA AL CAMBIA EL NUMERO DE REGISTROS A SER MOTRADOS EN LA MISMA
   * AUTOR: FREDI ROMAN
   */
  public reloadTableData() {
    this.page.pageNumber = 0;
    this.loadEmpresasToAsocList(0, this.page.size);
  }

  /**
   * METODO PARA ESPECIFICAR LA PAGINA DE LA TABLA
   * @param pageInfo 
   */
  public setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    this.loadEmpresasToAsocList(pageInfo.offset, pageInfo.pageSize);
  }

  /**
   * METODO PARA CAPTURAR EL EVENTO DE SELECCION DE UNA FILA DE LA TABLA
   * AUTOR: FREDI ROMAN
   * @param selecteRow 
   */
  public onSelectRow(selecteRow: { selected: any[] }) {
    this._modalService.sendModalData({ baseComponentId: this._dynamicData.baseComponentId, data: selecteRow.selected[0] });
  }

  /**
   * METODO PARA CARGAR UNA LISTA DE EMPRESAS DISPONIBLES PARA SER ASOCIADAS AL USUARIO LOGEADO,
   * DEFINE LOS DATOS DE LA TABLA AL INTERACTUAR CON LA PAGINACION
   * AUTOR: FREDI ROMAN
   * @param pageNumber 
   * @param size 
   */
  private loadEmpresasToAsocList(pageNumber: any, size: any) {
    this.page.pageNumber = pageNumber;
    this.page.size = size;
    let url = WS.usuarioEmpresa.getEmpresasToAsoc.replace("${userId}", this._dynamicData.userId + "");
    url = url.replace('${pageNumber}', pageNumber);
    url = url.replace('${size}', size);
    this._functionService.fetch(url)
      .then((res: any) => {
        this.empresaList = this._empresaService.createEmpresaList(res['content']);
        this.filterEmpresaList = this.empresaList;
        this.page.totalElements = res['totalElements'];
        this.loadingIndicator = false;
      });
  }

  ngOnDestroy() {
    this._modalService.sendModalData(null);
  }
}
