/**
 * AUTOR: FREDI ROMAN
 */
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { FunctionService } from '../../services/functions/function.service';
import { ConfirmationDialogService } from '../../components/confirmation-dialog/confirmation-dialog.service';
import { TAGS } from '../../TextLabels';
import { GV } from '../../GeneralVars';
import { WS } from '../../WebServices';
import { Empresa } from '../../models/empresa';
import { EmpresaService } from '../../services/component-services/empresa.service';
import { Page } from '../../components/pagination/page';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Cargo } from '../../models/cargo';
import { CargoService } from '../../services/component-services/cargo.service';
import { EmpresaControllerService } from '../../services/controllers/empresa-controller.service';
import { NgForm } from '@angular/forms';
import { Nivel } from '../../models/nivel';
import { NivelService } from '../../services/component-services/nivel.service';
import { Departamento } from '../../models/departamento';
import { DepartamentoService } from '../../services/component-services/departamento.service';

@Component({
  selector: 'app-cargo',
  templateUrl: './cargo.component.html',
  styleUrls: [
    './cargo.component.scss',
    '../../../../node_modules/@swimlane/ngx-datatable/release/index.css',
    '../../../vendor/libs/ngx-datatable/ngx-datatable.scss',
    '../../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css',
    '../../../vendor/libs/ngx-toastr/ngx-toastr.scss'
  ],
  encapsulation: ViewEncapsulation.None
})
export class CargoComponent implements OnInit {
  @ViewChild("frmCargo") formRef: NgForm;
  @ViewChild(DatatableComponent) table: DatatableComponent;
  private ipAddress: string;

  public navBtnList: { url: string; selected: boolean; label: string }[];
  public mainEnterpriseId: number;
  public labels: any;
  public gv: any;
  public empresaList: Empresa[];
  public nivelesList: Nivel[];
  public depaList: Departamento[];
  public cargo: Cargo;
  public page: Page;
  public cargoList: Cargo[];
  public filterCargoList: Cargo[];
  public saveEdit: boolean;
  public loadingIndicator: boolean;
  public depaName: string;

  constructor(
    private _functionService: FunctionService,
    private _empresaControllerService: EmpresaControllerService,
    private _confirmationDialogService: ConfirmationDialogService,
    private _empresaService: EmpresaService,
    public _cargoService: CargoService,
    private _nivelService: NivelService,
    private _departamentoService: DepartamentoService
  ) {
    this.navBtnList = [
      { url: '/admin/departamentos', selected: false, label: 'Departamentos' },
      { url: '/admin/cargo', selected: true, label: 'Cargos' },
      { url: '/admin/employees', selected: false, label: 'Empleados' }
    ];

    this.cargo = new Cargo();
    this.cargo.active = true;

    this.page = new Page();
    this.page.size = 5;
    this.labels = TAGS;
    this.gv = GV;
    this.saveEdit = true;
    this.loadingIndicator = true;
  }

  ngOnInit() {
    this.loadInitData();
  }

  /**
   * METODO PARA DETECTAR EL CAMBIO DE DEPARTAMENTO
   * AUTOR: FREDI ROMAN
   */
  public onChangeDepa() {
    this.setPaginatedTableData(0, this.page.size);
  }

  /**
   * METODO PARA PROCESAR EL EVENTO CLICK DE UN BOTON NIVEL
   * AUTOR: FREDI ROMAN
   * @param event 
   * @param i 
   */
  public onNivelClick(event: number) {
    this.resetForm();
    this.loadDeparmentData(event, true);
  }

  /**
   * METODO PARA CARGAR LA INFORMACION DE LOS NIVELES DE UNA EMPRESA
   * AUTOR: FREDI ROMAN
   */
  private loadDeparmentData(index: number, showLoading: boolean = false) {
    if (this.nivelesList.length > 0) {
      let parentNivelId = this.nivelesList[index].id;
      this.depaName = this.nivelesList[index].nombre;

      let request: any;
      if (showLoading) {
        request = this._functionService.httpGet(WS.departamento.get.replace("${nivelId}", parentNivelId));
      } else {
        request = this._functionService.httpGetWithout(WS.departamento.get.replace("${nivelId}", parentNivelId));
      }

      request.then((responseList: any) => {
        this.depaList = this._departamentoService.createDepartamentoList(responseList);
        this.cargo.departamento.idDepartamento = (this.depaList[0]) ? this.depaList[0].idDepartamento : null;
        this.setPaginatedTableData(0, this.page.size);
      });
    } else {
      this.loadingIndicator = false;
    }
  }

  /**
   * METODO PARA CARGAR LA INFORMACION DE LOS NIVELES DE UNA EMPRESA
   * AUTOR: FREDI ROMAN
   */
  private loadNivelesData(showLoading: boolean = false) {
    if (this.mainEnterpriseId) {
      this._functionService.httpGet(WS.nivel.get.replace("${enterpriseId}", this.mainEnterpriseId + ""))
        .then((responseList: any) => {
          this.nivelesList = this._nivelService.createNivelList(responseList);
          this.loadDeparmentData(0, showLoading);
        });
    } else {
      this.loadingIndicator = false;
    }
  }

  /**
   * METODO PARA CAPTURAR EL EVENTO CHANGE DEL SELECT DE EMPRESA PARA RECARGAR LOS DATOS DE LA TABLA
   * AUTOR: FREDI ROMAN
   */
  public onChangeEmpresa() {
    this.loadNivelesData();
  }

  /******************************************************************************************
  *******************************************************************************************
   ****** FORM DEFINITION
  ********************************************************************************************
  *******************************************************************************************/

  /**
   * METODO PARA PROCESAR EL SUBMIT DEL FORMULARIO DE REGISTRO Y ACTULIZACION DE CARGO:
   * AUTOR: FREDI ROMAN
   */
  public onSubmit() {
    this.cargo.active = JSON.parse(this.cargo.active + "");
    if (!this.saveEdit && !this.cargo.active) {
      const text = TAGS.messages.genericInactivate;
      this._confirmationDialogService.confirm(this.labels.general.confirm.toLocaleUpperCase(), text)
        .then((confirmed) => {
          if (confirmed) { this.saveData(); }
        });
    }
    else {
      this.saveData();
    }
  }

  /**
   * METODO PARA INSERTAR O ACTUALIZAR UN REGISTRO DE CARGO
   * AUTOR: FREDI ROMAN
   */
  public saveData() {
    let url;
    this.cargo.ipMaquina = this.ipAddress;
    if (this.saveEdit) {
      url = WS.cargos.post.replace("${depaId}", this.cargo.departamento.idDepartamento + "");
      this.cargo.status = 1;
      this._functionService.httpPost(url, this.cargo).then((cargoData) => {
        this.setPaginatedTableData(0, this.page.size);
      }).catch(err => console.log(err));
    }
    else {
      url = WS.cargos.put.replace("${depaId}", this.cargo.departamento.idDepartamento + "").replace("${cargoId}", this.cargo.id).replace("${isActive}", this.cargo.active + "");
      this.cargo.status = 2;
      this._functionService.httpPut(url, this.cargo).then((cargoData) => {
        this.setPaginatedTableData(0, this.page.size);
      });
    }
    this.resetForm();
  }

  /**
   * METODO PARA CARGAR LOS DATOS DE INICIO, DESDE EL BACKEND, PARA MOSTRARLO EN EL DOM 
   * AUTOR: FREDI ROMAN
   */
  private loadInitData() {
    this._empresaControllerService.getEmpresaListController()
      .then((empresaData) => {
        this.empresaList = this._empresaService.createEmpresaList(empresaData);
        this.mainEnterpriseId = (this.empresaList[0]) ? this.empresaList[0].id : null;
        this.loadNivelesData(true);

        this._functionService.getIpAddress().then((res: any) => {
          this.ipAddress = res;
        });
      });
  }

  /**
   * METODO PARA RESETEAR LOS ELEMENTOS DEL FORMULARIO
   * AUTOR: FREDI ROMAN
   */
  public resetForm() {
    this.formRef.form.controls['nombre'].reset();
    this.formRef.form.controls['codCargo'].reset();
    this.cargo.active = true;
    this.saveEdit = true;
  }
  /******************************************************************************************
  ******************************************************************************************/

  /******************************************************************************************
  *******************************************************************************************
   ****** TABLE DEFINITION 
  ********************************************************************************************
  *******************************************************************************************/

  /**
   * METODO PARA REALIZAR UN FILTRO EN LA TABLA
   * AUTOR: FREDI ROMAN
   * @param event 
   */
  public onTableFilter(event) {
    const val = event.target.value.toLowerCase();
    this.filterCargoList = this.cargoList.slice();
    const temp = this.filterCargoList.filter((filterData) => {
      return filterData.nombre.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.filterCargoList = temp;
    this.table.offset = 0;
  }

  /**
   * METODO PARA AUMENTAR EL NUMERO DE FILAS EL LA TABLA Y CARGAR LOS DATOS DESDE EL BACKEND
   * AUTOR: FREDI ROMAN
   */
  public reloadTableData() {
    this.page.pageNumber = 0;
    this.setPaginatedTableData(0, this.page.size);
  }

  /**
   * METODO PARA CAPTURAR EL EVENTO DE SELECCION DE UNA FILA DE LA TABLA
   * AUTOR: FREDI ROMAN
   * @param selecteRow 
   */
  public onSelectRow(selecteRow: { selected: any[] }) {
    this.saveEdit = false;
    this.cargo.id = selecteRow.selected[0].id;
    this.cargo.codCargo = selecteRow.selected[0].codCargo;
    this.cargo.nombre = selecteRow.selected[0].nombre;
    this.cargo.departamento.idDepartamento = selecteRow.selected[0].departamento.idDepartamento;
    this.cargo.active = selecteRow.selected[0].active;
  }

  /**
   * METODO PARA DEFINIR LA PAGINACION DE LA TABLA, AL CAPTURAR EL EVENTO DE NEXT/PREV PAGE
   * AUTOR: FREDI ROMAN
   * @param pageInfo 
   */
  public setPage(pageInfo: any) {
    this.page.pageNumber = pageInfo.offset;
    this.setPaginatedTableData(pageInfo.offset, pageInfo.pageSize);
  }

  /**
   * METODO PARA DEFINIR LOS DATOS DE LA TABLA AL INTERACTUAR CON LA PAGINACION
   * AUTOR: FREDI ROMAN
   * @param pageNumber 
   * @param size 
   */
  public setPaginatedTableData(pageNumber: any, size: any) {
    this.page.pageNumber = pageNumber;
    this.page.size = size;

    if (this.cargo.departamento.idDepartamento) {
      let url = WS.cargos.fetchByDepaId.replace("${depaId}", this.cargo.departamento.idDepartamento + "").replace('${pageNumber}', pageNumber).replace('${size}', size);
      this._functionService.fetch(url).then((res) => {
        this.cargoList = (res && res['content']) ? this._cargoService.createCargoList(res['content']) : [];
        this.filterCargoList = this.cargoList.slice();
        this.page.totalElements = (res && res['content']) ? res['totalElements'] : 0;
      });
    } else {
      this.cargoList = [];
      this.filterCargoList = [];
      this.page.totalElements = 0;
    }
    this.loadingIndicator = false;
  }
  /******************************************************************************************
  ******************************************************************************************/
}
