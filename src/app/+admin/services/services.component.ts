import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import { AppService } from '../../app.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { TAGS } from '../../TextLabels';
import { GV } from '../../GeneralVars';
import { WS } from '../../WebServices';
import {ActivatedRoute, Router} from '@angular/router';
import { ConfirmationDialogService } from '../../components/confirmation-dialog/confirmation-dialog.service';
import { FunctionService } from '../../services/functions/function.service';
import {Page} from '../../components/pagination/page';

@Component({
  selector: '[tables-ngx-datatable]',
  templateUrl: './services.component.html',
  styleUrls: [
    '../../../../node_modules/@swimlane/ngx-datatable/release/index.css',
    '../../../vendor/libs/ngx-datatable/ngx-datatable.scss',
    '../../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css',
    '../../../vendor/libs/ngx-toastr/ngx-toastr.scss'
  ],
  encapsulation: ViewEncapsulation.None
})


export class ServicesComponent {
  @ViewChild(DatatableComponent) table: DatatableComponent;
  viewEdit: Boolean = false;
  selected = [];
  movilizaciones = [];
  estadoBuscar = '';
  service = '';
  valor = '';
  idServicio = '';
  idSubcatalogo = '';
  idEmpresa = '';
  idConvenio = '';
  options_notitications = {
    tapToDismiss: true,
    closeButton: false,
    progressBar: true,
    positionClass: 'toast-top-right',
    rtl: this.appService.isRTL
  };

  loadingIndicator = true;
  loadingIndicatorMov = true;
  rows = null;
  temp = null;
  labels = null;
  gv = null;
  idConv: string;
  codConv: string;
  empServ: string;
  codEmp: string;
  servicesSave = [];
  changeStateVal: Boolean = true;
  ipAddress = null;
  estados = [];
  responseList = null;
  page = new Page();

  constructor(
    private confirmationDialogService: ConfirmationDialogService,
    private route: ActivatedRoute,
    private appService: AppService,
    private http: HttpClient,
    public toastrService: ToastrService,
    private functionService: FunctionService,
    private _router: Router
  ) {
    this.toastrService.toastrConfig.newestOnTop = true;
    this.toastrService.toastrConfig.preventDuplicates = true;
  }
  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function(d) {
      return d.descripcionServicio.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows = temp;
    this.table.offset = 0;
  }
  changeState() {
    this.changeStateVal = true;
  }
  confirmMethod() {
    if (this.changeStateVal && this.estadoBuscar.toString() === GV.state.inactive) {
      this.openConfirmationDialog();
    } else {
      this.save();
    }
  }
  openConfirmationDialog() {
    const text = this.labels.messages.inactivate + ' ' + this.labels.services.service + '.  ' + this.labels.messages.inactivateConfirm;
    this.confirmationDialogService.confirm(this.labels.general.confirm.toLocaleUpperCase(), text)
      .then((confirmed) => {
        if (confirmed) {
          this.save();
        }
      }).catch(() =>
      console.log('Dismissed.)'
      ));
  }
  save () {
    if (this.viewEdit) {
      if (this.servicesSave.length > 0) {
        const arrayPost = [];
        for (let i = 0; i < this.servicesSave.length; i++) {
          let val = 0;
          if (
            this.servicesSave[i].valorServicio !== '' &&
            this.servicesSave[i].valorServicio !== null &&
            this.servicesSave[i].valorServicio.toLocaleUpperCase() !== 'NAN') {
            val = this.servicesSave[i].valorServicio;
          }
          const srv = {
            Id: this.servicesSave[i].Id,
            descripcionServicio: this.servicesSave[i].descripcionServicio.toString(),
            valorServicio: val.toString(),
            estado: this.estadoBuscar.toString(),
            codusu: '1',
            fecult: this.functionService.getToday(),
            status: '1',
            ipMaquina : this.ipAddress
          };
          let url = WS.services.save;
          url = url.replace('${idServicio}', this.servicesSave[i].Id);
          const array = {
            url: url,
            body: {
              servicio: srv,
              idEmpresa: this.servicesSave[i].SGE_Empresa_idEmpresa.toString(),
              idConvenio: this.servicesSave[i].SGE_Convenio_idConvenio.toString()
            }
          };
          arrayPost.push(array);
        }
        this.functionService.httpObservablePost(arrayPost).then(() => {
          this.clear();
          setTimeout(() => {
            this._router.navigate(['admin/secuenciavoucher']);
          }, 2000);
        });
      } else {
        this.toastrService['error'](this.labels.messages.selectElemnts, this.appService.pageTitle, this.options_notitications);
      }
    } else {
      const srv = {
        idsubcatalogo : this.idSubcatalogo,
        descripcionServicio: this.service,
        valorServicio : this.valor,
        estado : this.estadoBuscar,
        codusu: '1',
        fecult: this.functionService.getToday(),
        status: '1',
        ipMaquina : this.ipAddress
      };
      let url = WS.services.update;
      url = url.replace('${idServicio}', this.idServicio);
      this.functionService.httpPut(url, {
        servicio: srv,
        idEmpresa : this.idEmpresa,
        idConvenio : this.idConvenio
      }).then(() => {
        this.clear();
      });
    }
  }
  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    this.setDataPagination(pageInfo.offset, pageInfo.pageSize);
  }
  setDataPagination(pageNumber, size) {
    this.page.pageNumber = pageNumber;
    this.page.size = size;
    let url = WS.services.fetch;
    url = url.replace('${pageNumber}', pageNumber);
    url = url.replace('${size}', size);
    url = url.replace('${idConvenio}', this.idConv);
    this.functionService.fetch(url).then((res) => {
      this.temp = res['content'];
      this.rows = res['content'];
      this.loadingIndicator = false;
      this.page.totalElements = res['totalElements'];
    });
  }
  loadData() {
    this.page.pageNumber = 0;
    this.setDataPagination(0, this.page.size);
  }
  clear () {
    this.viewEdit = true;
    this.service = '';
    this.valor = '';
    this.setDataPagination(0, 5);
    for (let i = 0; i < this.servicesSave.length; i++) {
      document.getElementById('inp' + this.servicesSave[i].rowIndex).setAttribute('disabled', 'true');
      (<HTMLInputElement>document.getElementById('inp' + this.servicesSave[i].rowIndex)).value = '';
      (<HTMLInputElement>document.getElementById('che' + this.servicesSave[i].rowIndex)).checked = false;
    }
    this.servicesSave = [];
    this.estadoBuscar = this.responseList[1].content[0].id;
    this.changeStateVal = false;
  }
  keyDown (e) {
    if (!((e.keyCode > 95 && e.keyCode < 106)
      || (e.keyCode > 47 && e.keyCode < 58)
      || e.keyCode.toString() === '8' || e.keyCode.toString() === '190' || e.keyCode.toString() === '110')) {
      return false;
    }
  }
  onSelect({ selected }) {
    this.viewEdit = false;
    this.service = selected[0].descripcionServicio;
    this.valor = selected[0].valorServicio;
    this.estadoBuscar = selected[0].estado;
    this.idServicio = selected[0].id;
    this.idSubcatalogo = selected[0].idsubcatalogo;
    this.idEmpresa = selected[0].empresa.id.toString();
    this.idConvenio = selected[0].convenio.id.toString();
  }
  checkElement(event, cell, rowIndex) {
    if (event.target.checked) {
      this.servicesSave.push({
        'Id': cell.id,
        'descripcionServicio': cell.desElemento,
        'valorServicio': '',
        'SGE_Empresa_idEmpresa': this.codEmp,
        'SGE_Convenio_idConvenio': this.idConv,
        'rowIndex': rowIndex});
      // document.getElementById('inp' + rowIndex).removeAttribute('disabled');
      // document.getElementById('inp' + rowIndex).focus();
    } else {
      for (let i = 0; i < this.servicesSave.length; i++) {
        if (this.servicesSave[i].Id === cell.id) {
          this.servicesSave.splice(i, 1);
          break;
        }
      }
      // document.getElementById('inp' + rowIndex).setAttribute('disabled', 'true');
      // (<HTMLInputElement>document.getElementById('inp' + rowIndex)).value = '';
    }
  }
  updateValue(event, cell, rowIndex) {
    const dec = parseFloat(event.target.value).toFixed(2);
    for (let i = 0; i < this.servicesSave.length; i++) {
      if (this.servicesSave[i].Id === cell.id) {
        this.servicesSave[i].valorServicio = dec;
        break;
      }
    }
    (<HTMLInputElement>document.getElementById('inp' + rowIndex)).value = dec;
  }
  updateValueEdit(event) {
    const decEdit = parseFloat(event.target.value).toFixed(2);
    (<HTMLInputElement>document.getElementById('inpEdit')).value = decEdit;
  }
  ngOnInit() {
    this.page.pageNumber = 0;
    this.page.size = 5;
    this.labels = TAGS;
    this.gv = GV;
    this.viewEdit = true;
    this.service = '';
    this.valor = '';
    this.appService.pageTitle = this.labels.characteristics.subtitle1;
    this.idConv = this.route.snapshot.paramMap.get('idc');
    this.codConv = this.route.snapshot.paramMap.get('cod');
    this.codEmp = this.route.snapshot.paramMap.get('emp');
    this.empServ = this.route.snapshot.paramMap.get('nom');
    const array = [WS.services.services, WS.general.status];
    this.loadingIndicatorMov = true;
    this.functionService.httpObservableGet(array).then((responseList) => {
      this.movilizaciones = responseList[0].content;
      this.estados = responseList[1].content;
      this.responseList = responseList;
      this.functionService.getIpAddress().then((res) => {
        this.ipAddress = res;
      });
      this.clear();
      this.loadingIndicatorMov = false;
    });
  }
}
