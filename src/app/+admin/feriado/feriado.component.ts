/**
 * AUTOR: FREDI ROMAN
 */
import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { FunctionService } from '../../services/functions/function.service';
import { ConfirmationDialogService } from '../../components/confirmation-dialog/confirmation-dialog.service';
import { TAGS } from '../../TextLabels';
import { GV } from '../../GeneralVars';
import { WS } from '../../WebServices';
import { Empresa } from '../../models/empresa';
import { EmpresaService } from '../../services/component-services/empresa.service';
import { Page } from '../../components/pagination/page';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Feriado } from '../../models/feriado';
import { FeriadoService } from '../../services/component-services/feriado.service';
import { Sucursal } from '../../models/sucursal';
import { SucursalService } from '../../services/component-services/sucursal.service';
import { EmpresaControllerService } from '../../services/controllers/empresa-controller.service';
import { NgForm } from '@angular/forms';
import { SwitchInputInterface } from '../../dynamic-form/interfaces/switch-input.interface';
import { SWITCH_SIZE } from '../../dynamic-form/data/switch-size';

@Component({
  selector: 'app-feriado',
  templateUrl: './feriado.component.html',
  styleUrls: [
    './feriado.component.scss',
    '../../../../node_modules/@swimlane/ngx-datatable/release/index.css',
    '../../../vendor/libs/ngx-datatable/ngx-datatable.scss',
    '../../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css',
    '../../../vendor/libs/ngx-toastr/ngx-toastr.scss'
  ],
  encapsulation: ViewEncapsulation.None
})
export class FeriadoComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild("frmFeriado") frmFeriado: NgForm;
  private ipAddress: string;
  private autoGenFeriado: string;

  public labels: any;
  public gv: any;
  public navBtnList: { url: string; selected: boolean; label: string }[];
  public empresaList: Empresa[];
  public sucursalList: Sucursal[];
  public feriado: Feriado;
  public saveEdit: boolean;
  public page: Page;
  public feriadoList: Feriado[];
  public filterFeriadoList: Feriado[];
  public loadingIndicator: boolean;
  public mainEnterpriseId: number;
  public switchIns: SwitchInputInterface[];

  constructor(
    private _functionService: FunctionService,
    private _empresaControllerService: EmpresaControllerService,
    private _confirmationDialogService: ConfirmationDialogService,
    private _empresaService: EmpresaService,
    private _sucursalService: SucursalService,
    public _feriadoService: FeriadoService
  ) {
    this.navBtnList = [
      { url: '/admin/company', selected: false, label: 'Empresa' },
      { url: '/admin/sucursales', selected: false, label: 'Sucursal' },
      { url: '/admin/feriado', selected: true, label: 'Feriados' },
      { url: '/admin/definiciones', selected: false, label: 'Definiciones' }
    ];

    this.feriado = new Feriado();
    this.feriado.active = true;

    this.page = new Page();
    this.labels = TAGS;
    this.gv = GV;
    this.saveEdit = true;
    this.loadingIndicator = true;
  }

  ngOnInit() {
    this.defineSwitchInputs();
    this.loadInitData();
  }

  /**
   * METODO PARA CAPTURAR EL TIPO DE GENERACION DE FERIADOS AUTOMATICOS ESCOGIDO POR EL USUARIO:
   * @param event ID QUE VIENE POR EL OUTPUT EVENT EMITTER DEL COMPONENTE HIJO
   */
  public getSelectedSwitchValue(event: string) {
    this.autoGenFeriado = this.switchIns.find(sw => sw.id == event).checked ? event : null;
  }

  /**
   * METODO PARA DEFINIR EL ARRAY DE TIPO SWITCH-INPUT
   * AUTOR: FREDI ROMAN
   */
  private defineSwitchInputs() {
    this.switchIns = []
    for (let property in GV.autoGenFeriados) {
      this.switchIns.push({
        id: property,
        label: GV.autoGenFeriados[property],
        checked: false,
        size: SWITCH_SIZE.xs,
        immutable: false,
        customClass: 'switch-normal'
      });
    }
  }

  /**
   * METODO PARA ELIMINAR UN REGISTRO DE FERIADO
   * AUTOR: FREDI ROMAN
   */
  public onDeleteFeriado() {
    const text = TAGS.messages.genericDelete;
    this._confirmationDialogService.confirm(this.labels.general.confirm.toLocaleUpperCase(), text)
      .then((confirmed) => {
        if (confirmed) { this.deleteFeriado(); }
      });
  }

  /**
   * METODO PARA SOLICITAR LA ELIMINACION DE UN REGISTRO DE FERIADO, HACIA EL BACKEND
   * AUTOR: FREDI ROMAN
   */
  private deleteFeriado() {
    let url = WS.feriado.del.replace("${sucursalId}", this.feriado.sucursal.id + "").replace("${feriadoId}", this.feriado.idFeriado).replace("${isActive}", this.feriado.active + "");
    this._functionService.httpDelete(url).then(() => {
      this.setPaginatedTableData(0, 5);
      this.resetForm();
    });
  }

  /**
   * METODO PARA DETECTAR EL CAMBIO DE FECHA
   * AUTOR: FREDI ROMAN
   */
  public onDateChange(event: any) {
    this.feriado.fechaFeriado = event;
  }

  /**
   * METODO PARA CAPTURAR EL EVENTO CHANGE DEL SELECT QUE CONTIENE LA LISTA DE SUCURSALES
   * AUTOR: FREDI ROMAN
   */
  public changeSucursal() {
    this.setPaginatedTableData(0, 5);
  }

  /**
   * METODO PARA CAPTURAR EL EVENTO CHANGE DEL SELECT QUE CONTIENE LA LISTA DE EMPRESAS
   * AUTOR: FREDI ROMAN
   */
  public changeEnterprise() {
    this.loadSucursalData();
  }

  /******************************************************************************************
  *******************************************************************************************
   ****** FORM DEFINITION
  ********************************************************************************************
  *******************************************************************************************/

  /**
   * METODO PARA PROCESAR EL SUBMIT DEL FORMULARIO DE REGISTRO Y ACTUALIZACION:
   * AUTOR: FREDI ROMAN
   */
  public onSubmit() {
    this.feriado.active = JSON.parse(this.feriado.active + "");
    if (!this.saveEdit && !this.feriado.active) {
      const text = TAGS.messages.genericInactivate;
      this._confirmationDialogService.confirm(this.labels.general.confirm.toLocaleUpperCase(), text)
        .then((confirmed) => {
          if (confirmed) { this.saveData(); }
        });
    }
    else {
      this.saveData();
    }
  }

  /**
   * METODO PARA INSERTAR O ACTUALIZAR UN REGISTRO
   * AUTOR: FREDI ROMAN
   */
  public saveData() {
    let url, feriadoData: any;
    this.feriado.ipMaquina = this.ipAddress;
    this.feriado.fechaFeriado = this.feriado.fechaFeriado.year + "-" + (this.feriado.fechaFeriado.month < 10 ? "0" + this.feriado.fechaFeriado.month : this.feriado.fechaFeriado.month) + "-" + (this.feriado.fechaFeriado.day < 10 ? "0" + this.feriado.fechaFeriado.day : this.feriado.fechaFeriado.day);
    if (this.saveEdit) {
      feriadoData = this.feriado;
      if (this.autoGenFeriado) {
        url = WS.feriado.autoFeriadosForAllEnter;
        let date = new Date();
        feriadoData = {
          fechaFeriado: this.feriado.fechaFeriado,
          estado: GV.state.active,
          codusu: 1,
          fecult: date.getFullYear() + "-" + date.getMonth() + "-" + date.getDay(),
          status: 1,
          ipMaquina: this.feriado.ipMaquina,
          idEmpresa: this.mainEnterpriseId,
          idSucursal: this.feriado.sucursal.id,
          tipoFeriado: this.autoGenFeriado
        };
      } else {
        url = WS.feriado.post.replace("${sucursalId}", this.feriado.sucursal.id + "");
        this.feriado.status = 1;
      }
      this._functionService.httpPost(url, feriadoData).then((feriadoData) => {
        this.setPaginatedTableData(0, this.page.size);
        this.resetForm();
      });
    }
    else {
      url = WS.feriado.put.replace("${sucursalId}", this.feriado.sucursal.id + "").replace("${feriadoId}", this.feriado.idFeriado).replace("${isActive}", this.feriado.active + "");
      this.feriado.status = 2;
      this._functionService.httpPut(url, this.feriado).then((feriadoData) => {
        this.setPaginatedTableData(0, this.page.size);
        this.resetForm();
      });
    }
  }

  /**
   * METODO PARA CARGAR LA INFORMACION DE LAS SUCURSALES DE UNA EMPRESA
   */
  private loadSucursalData() {
    if (this.mainEnterpriseId) {
      this._functionService.httpGetWithout(WS.sucursales.sucByStateCompany.replace("${empresa}", this.mainEnterpriseId + ""))
        .then((responseList: any) => {
          this.sucursalList = this._sucursalService.createSucursalList(responseList.content);
          this.feriado.sucursal.id = (this.sucursalList[0]) ? this.sucursalList[0].id : null;

          this.setPaginatedTableData(0, 5);
        });
    } else {
      this.loadingIndicator = false;
    }
  }

  /**
   * METODO PARA CARGAR LOS DATOS DE INICIO, DESDE EL BACKEND, PARA MOSTRARLO EN EL DOM 
   * AUTOR: FREDI ROMAN
   */
  private loadInitData() {
    this._empresaControllerService.getEmpresaListController()
      .then((empresaData) => {
        this.empresaList = this._empresaService.createEmpresaList(empresaData);
        this.mainEnterpriseId = (this.empresaList[0]) ? this.empresaList[0].id : null;
        this.loadSucursalData();

        this._functionService.getIpAddress().then((res: any) => {
          this.ipAddress = res;
        });
      });
  }

  /**
   * METODO PARA RESETEAR LOS ELEMENTOS DEL FORMULARIO
   * AUTOR: FREDI ROMAN
   */
  public resetForm() {
    this.frmFeriado.form.controls['fechaFeriado'].reset();
    this.feriado.active = true;
    for (let sw of this.switchIns) {
      sw.checked = false;
      sw.immutable = false;
      sw.disabled = false;
    }
    this.autoGenFeriado = null;
    this.saveEdit = true;
  }
  /******************************************************************************************
  ******************************************************************************************/

  /******************************************************************************************
  *******************************************************************************************
   ****** TABLE DEFINITION 
  ********************************************************************************************
  *******************************************************************************************/

  /**
   * METODO PARA REALIZAR UN FILTRO EN LA TABLA
   * AUTOR: FREDI ROMAN
   * @param event 
   */
  public onTableFilter(event) {
    const val = event.target.value.toLowerCase();
    this.filterFeriadoList = this.feriadoList.slice();
    const temp = this.filterFeriadoList.filter((filterData) => {
      return filterData.sucursal.nomSucursal.toLowerCase().indexOf(val) !== -1 || filterData.fechaFeriado.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.filterFeriadoList = temp;
    this.table.offset = 0;
  }

  /**
   * METODO PARA AUMENTAR EL NUMERO DE FILAS EL LA TABLA Y CARGAR LOS DATOS DESDE EL BACKEND
   * AUTOR: FREDI ROMAN
   */
  public reloadTableData() {
    this.page.pageNumber = 0;
    this.setPaginatedTableData(0, this.page.size);
  }

  /**
   * METODO PARA CAPTURAR EL EVENTO DE SELECCION DE UNA FILA DE LA TABLA
   * AUTOR: FREDI ROMAN
   * @param selecteRow 
   */
  public onSelectRow(selecteRow: { selected: any[] }) {
    this.saveEdit = false;
    this.feriado.idFeriado = selecteRow.selected[0].idFeriado;

    let date = selecteRow.selected[0].fechaFeriado;
    date = date.split("-");
    this.feriado.fechaFeriado = { year: parseInt(date[0]), month: parseInt(date[1]), day: parseInt(date[2]) };

    this.feriado.sucursal = selecteRow.selected[0].sucursal;
    this.feriado.active = selecteRow.selected[0].active;
    for (let sw of this.switchIns) {
      sw.checked = false;
      sw.immutable = true;
      sw.disabled = true;
    }
  }

  /**
   * METODO PARA DEFINIR LA PAGINACION DE LA TABLA, AL CAPTURAR EL EVENTO DE NEXT/PREV PAGE
   * AUTOR: FREDI ROMAN
   * @param pageInfo 
   */
  public setPage(pageInfo: any) {
    this.page.pageNumber = pageInfo.offset;
    this.setPaginatedTableData(pageInfo.offset, pageInfo.pageSize);
  }

  /**
   * METODO PARA DEFINIR LOS DATOS DE LA TABLA AL INTERACTUAR CON LA PAGINACION
   * AUTOR: FREDI ROMAN
   * @param pageNumber 
   * @param size 
   */
  public setPaginatedTableData(pageNumber: any, size: any) {
    this.page.pageNumber = pageNumber;
    this.page.size = size;

    if (this.feriado.sucursal.id) {
      let url = WS.feriado.fetch.replace("${sucursalId}", this.feriado.sucursal.id + "").replace('${pageNumber}', pageNumber).replace('${size}', size);
      this._functionService.fetch(url).then((res) => {
        this.filterFeriadoList = this._feriadoService.createFeriadoList(res['content']);
        this.filterFeriadoList = this.filterFeriadoList.slice();
        this.page.totalElements = res['totalElements'];
      });
    } else {
      this.filterFeriadoList = [];
      this.filterFeriadoList = [];
      this.page.totalElements = 0;
    }
    this.loadingIndicator = false;
  }
  /******************************************************************************************
  ******************************************************************************************/
}
