import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { AuthGuard } from '../services/auth/auth.guard';

// *******************************************************************************
//

import { CatalogComponent } from './catalog/catalog.component';
import { CatalogItemComponent } from './catalogitem/catalogitem.component';
import { UserComponent } from './user/user.component';
import { CharacteristicsComponent } from './characteristics/characteristics.component';
import { ServicesComponent } from './services/services.component';
import { EmployeesComponent } from './employees/employees.component';
import { SucursalesComponent } from './sucursales/sucursales.component';
import { CompanyComponent } from './company/company.component';
import { CargoComponent } from './cargo/cargo.component';
import { PresupuestoComponent } from './presupuesto/presupuesto.component';
import { FeriadoComponent } from './feriado/feriado.component';
import { UsuarioEmpresaComponent } from './usuario-empresa/usuario-empresa.component';
import { SecuenciaVoucherComponent } from './secuencia-voucher/secuencia-voucher.component';
import { DepartamentoComponent } from './departamento/departamento.component';
import { DynaParamsComponent } from './dyna-param/dyna-param.component';
import { ResponsableComponent } from './responsable/responsable.component';
import { ContactoFastlineComponent } from './contacto-fastline/contacto-fastline.component';

// *******************************************************************************
//

@NgModule({
  imports: [RouterModule.forChild([
    { path: 'catalog', component: CatalogComponent, canActivate: [AuthGuard] },
    { path: 'catalogitem/:id/:desc/:codc', component: CatalogItemComponent, canActivate: [AuthGuard] },
    { path: 'user', component: UserComponent, canActivate: [AuthGuard] },
    { path: 'characteristics', component: CharacteristicsComponent, canActivate: [AuthGuard] },
    { path: 'services/:idc/:cod/:emp/:nom', component: ServicesComponent, canActivate: [AuthGuard] },
    { path: 'employees', component: EmployeesComponent, canActivate: [AuthGuard] },
    { path: 'sucursales', component: SucursalesComponent, canActivate: [AuthGuard] },
    { path: 'sucursales/:emp', component: SucursalesComponent, canActivate: [AuthGuard] },
    { path: 'company', component: CompanyComponent, canActivate: [AuthGuard] },
    { path: 'cargo', component: CargoComponent, canActivate: [AuthGuard] },
    { path: 'presupuesto', component: PresupuestoComponent, canActivate: [AuthGuard] },
    { path: 'contactos', component: ContactoFastlineComponent, canActivate: [AuthGuard] },
    { path: 'responsables', component: ResponsableComponent, canActivate: [AuthGuard] },
    { path: 'feriado', component: FeriadoComponent, canActivate: [AuthGuard] },
    { path: 'usuarioEmpresa', component: UsuarioEmpresaComponent, canActivate: [AuthGuard] },
    { path: 'departamentos', component: DepartamentoComponent, canActivate: [AuthGuard] },
    { path: 'secuenciavoucher', component: SecuenciaVoucherComponent, canActivate: [AuthGuard] },
    { path: 'definiciones', component: DynaParamsComponent, canActivate: [AuthGuard] }
  ])],
  exports: [RouterModule],
  providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }],
})
export class AdminRoutingModule { }
