import { Component, ViewEncapsulation, ViewChild, OnInit } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgForm } from '@angular/forms';
import { TAGS } from '../../TextLabels';
import { GV } from '../../GeneralVars';
import * as textMaskAddons from 'text-mask-addons/dist/textMaskAddons';
import { ConfirmationDialogService } from '../../components/confirmation-dialog/confirmation-dialog.service';
import { FunctionService } from '../../services/functions/function.service';
import { WS } from '../../WebServices';
import { Page } from '../../components/pagination/page';
import { EmpresaControllerService } from '../../services/controllers/empresa-controller.service';
import { Empresa } from '../../models/empresa';
import { Nivel } from '../../models/nivel';
import { Departamento } from '../../models/departamento';
import { Empleado } from '../../models/empleado';
import { Cargo } from '../../models/cargo';
import { EmpresaService } from '../../services/component-services/empresa.service';
import { NivelService } from '../../services/component-services/nivel.service';
import { DepartamentoService } from '../../services/component-services/departamento.service';
import { CargoService } from '../../services/component-services/cargo.service';
import { Sucursal } from '../../models/sucursal';
import { SucursalService } from '../../services/component-services/sucursal.service';
import { EmpleadoService } from '../../services/component-services/empleado.service';
import { DynaParam } from '../../models/dyna-param';
import { DynaParamService } from '../../services/component-services/dyna-param.service';
import { DynaParamValue } from '../../interfaces/dyna-param-value';
import { SwitchInputInterface } from '../../dynamic-form/interfaces/switch-input.interface';
import { DynamicInputForm } from '../../dynamic-form/models/dynamic-input-form';
import { SWITCH_SIZE } from '../../dynamic-form/data/switch-size';
import { RestClientService } from '../../services/component-services/rest-client.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employees.component.html',
  styleUrls: [
    '../../../../node_modules/@swimlane/ngx-datatable/release/index.css',
    '../../../vendor/libs/ngx-datatable/ngx-datatable.scss',
    '../../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css',
    '../../../vendor/libs/ngx-toastr/ngx-toastr.scss'
  ],
  encapsulation: ViewEncapsulation.None
})

export class EmployeesComponent implements OnInit {
  /***********************************************************************************************************
   * REFACTORIZACION DE DEPARTAMENTOS, CARGOS, EMPLEADOS
   * AUTOR FREDI ROMAN
   **********************************************************************************************************/
  @ViewChild("frmEmploye") formRef: NgForm;
  @ViewChild(DatatableComponent) table: DatatableComponent;
  private ipAddress: string;
  private currentNivelIndex: number;
  private generarCuenta: boolean;

  public navBtnList: { url: string; selected: boolean; label: string }[];
  public empresaList: Empresa[];
  public mainEnterpriseId: number;
  public nivelesList: Nivel[];
  public sucursalList: Sucursal[];
  public depaList: Departamento[];
  public mainDepaId: number;
  public cargoList: Cargo[];
  public empleado: Empleado;
  public empleadoList: Empleado[];
  public filterEmpleadoList: Empleado[];
  public saveEdit: boolean;
  public page: Page;
  public loadingIndicator: boolean;
  public labels: any;
  public gv: any;
  public operadorasList: any[];
  public tiposIdList: any[];
  public estadosList: any[];
  public emailMaskOptions;
  public switchIns: SwitchInputInterface[];
  public depaName: string;
  public dynaParamList: DynaParam[];
  public dynaParamValues: DynaParamValue[];

  public inputFormList: DynamicInputForm<any>[];
  public letSubmit: boolean;
  public letReset: boolean;
  public pushedInputValue: { key: string; value: string };
  public selectedRow: any[];

  constructor(
    private _functionService: FunctionService,
    private _restClientService: RestClientService,
    private _empresaControllerService: EmpresaControllerService,
    private _confirmationDialogService: ConfirmationDialogService,
    private _empresaService: EmpresaService,
    private _nivelService: NivelService,
    private _sucursalService: SucursalService,
    private _departamentoService: DepartamentoService,
    private _cargoService: CargoService,
    private _empleadoService: EmpleadoService,
    private _dynaParamService: DynaParamService
  ) {
    this.navBtnList = [
      { url: '/admin/departamentos', selected: false, label: 'Departamentos' },
      { url: '/admin/cargo', selected: false, label: 'Cargos' },
      { url: '/admin/employees', selected: true, label: 'Empleados' }
    ];
    this.empleado = new Empleado();
    this.empleado.estado = parseInt(GV.state.active);

    this.dynaParamList = [];
    this.generarCuenta = true;
    this.emailMaskOptions = {
      mask: textMaskAddons.emailMask
    };
    this.page = new Page();
    this.page.size = 5;
    this.labels = TAGS;
    this.gv = GV;
    this.saveEdit = true;
    this.loadingIndicator = true;
    this.inputFormList = [];
    this.selectedRow = [];
  }

  ngOnInit() {
    this.defineSwitchInputs();
    this.loadInitData();
  }

  /***********************************************************************************************
  ************************************************************************************************
   * FORMULARIO DINAMICO PARA LOS ATRIBUTOS DINAMICOS DEL EMPLEADO
   * AUTOR: FREDI ROMAN
  ************************************************************************************************
  ***********************************************************************************************/

  /**
   * METODO PARA OBTENER LOS DATOS DE LOS CAMPOS DEL DYNAMIC-FORM
   * AUTOR: FREDI ROMAN
   * @param jsonFormData 
   */
  public getSumitedData(jsonFormData: JSON) {
    this.dynaParamValues = [];
    for (let property in jsonFormData) {
      this.dynaParamValues.push({ idAtributo: property, valor: jsonFormData[property] });
    }
    this.saveData();
  }

  /**
   * METODO PARA DEFINIR LOS DATOS DEL DYNAMIC-FORM
   * AUTOR: FREDI ROMAN
   */
  private defineDynaForm() {
    this.inputFormList = this._empleadoService.defineDynaInputForm(this.dynaParamList);
  }
  /************************************************************************************************
  ***********************************************************************************************/

  /**
   * METODO PARA CAPTURAR EL TIPO DE CONTROL PRESUPUESTARIO ESCOGIDO POR EL USUARIO:
   * @param event ID QUE VIENE POR EL OUTPUT EVENT EMITTER DEL COMPONENTE HIJO
   */
  public getSelectedSwitchValue(event: string) {
    this.generarCuenta = this.switchIns.find(sw => sw.id == event).checked;
  }

  /**
   * METODO PARA DEFINIR EL ARRAY DE TIPO SWITCH-INPUT
   * AUTOR: FREDI ROMAN
   */
  private defineSwitchInputs() {
    this.switchIns = [{ id: '1', label: 'Crear cuenta de usuario', checked: true, size: SWITCH_SIZE.xs, customClass: 'switch-normal' }];
  }

  /**
   * METODO PARA PROCESAR EL SUBMIT DEL FORMULARIO DE REGISTRO Y ACTULIZACION:
   * AUTOR: FREDI ROMAN
   */
  public onSubmit() {
    if (!this.saveEdit && this.empleado.estado == parseInt(GV.state.inactive)) {
      const text = TAGS.messages.genericInactivate;
      this._confirmationDialogService.confirm(this.labels.general.confirm.toLocaleUpperCase(), text)
        .then((confirmed) => {
          if (confirmed) {
            if (this.inputFormList.length > 0) {
              this.letSubmit = null;
              setTimeout(() => {
                this.letSubmit = true;
              });
            } else {
              this.saveData();
            }
          }
        });
    }
    else {
      if (this.inputFormList.length > 0) {
        this.letSubmit = null;
        setTimeout(() => {
          this.letSubmit = true;
        });
      } else {
        this.saveData();
      }
    }
  }

  /**
   * METODO PARA INSERTAR O ACTUALIZAR UN REGISTRO
   * AUTOR: FREDI ROMAN
   */
  public saveData() {
    let url;
    const date = new Date();
    let empData =
    {
      empleado: {
        idEmpleado: null,
        codEmpleado: this.empleado.codEmpleado,
        tipoIdentificacion: this.empleado.tipoIdentif,
        identificacion: this.empleado.identificacion,
        primerNombre: this.empleado.primerNombre,
        segundoNombre: this.empleado.segundoNombre,
        primerApellido: this.empleado.primerApellido,
        segundoApellido: this.empleado.segundoApellido,
        nomCompleto: this.empleado.nomCompleto,
        codOperadora: this.empleado.codOperadora,
        telefonoCelular: this.empleado.telefonoCelular,
        telefonoConvencional: this.empleado.telefonoConvencional,
        correo: this.empleado.correo,
        idEmpresa: this.mainEnterpriseId
      },
      idSucursal: this.empleado.sucursal.id,
      idCargo: this.empleado.cargo.id,
      fechaDesde: (this.empleado.fechaDesde) ? this.empleado.fechaDesde.year + "-" + (this.empleado.fechaDesde.month < 10 ? "0" + this.empleado.fechaDesde.month : this.empleado.fechaDesde.month) + "-" + (this.empleado.fechaDesde.day < 10 ? "0" + this.empleado.fechaDesde.day : this.empleado.fechaDesde.day) : null,
      estado: this.empleado.estado,
      codusu: this.empleado.codusu,
      fecult: date.getFullYear() + "-" + (date.getMonth() < 10 ? "0" + date.getMonth() : date.getMonth()) + "-" + (date.getDay() < 10 ? "0" + date.getDay() : date.getDay()),
      status: this.empleado.status,
      ipMaquina: this.ipAddress,
      atribDinamicos: this.dynaParamValues
    };

    if (this.saveEdit) {
      url = WS.empleadosSucursal.postPut.replace("${sucId}", this.empleado.sucursal.id + "").replace("${autoAcc}", this.generarCuenta + "");
      if (this.generarCuenta) {
        console.log("LA CUENTA SE CREARÁ AUTOMATICAMENTE");
      } else {
        console.log("NO SE CREARÁ LA CUENTA");
      }
      this._functionService.httpPost(url, empData).then((resp) => {
        this.setPaginatedTableData(0, this.page.size);
      });
    }
    else {
      url = WS.empleadosSucursal.postPut.replace("${sucId}", this.empleado.sucursal.id + "").replace("${autoAcc}", "false");
      empData.empleado.idEmpleado = this.empleado.id;
      empData.status = 2;
      this._functionService.httpPut(url, empData).then((resp) => {
        this.setPaginatedTableData(0, this.page.size);
      });
    }
    this.resetForm();
  }

  /**
   * METODO PARA CONCATER EL NOMBRE COMPLETO DEL EMPLEADO EN UN SOLO INPUT
   * AUTOR: JUAN MOLINA
   */
  public fullNameComplete() {
    this.empleado.primerNombre = this.empleado.primerNombre ? this.empleado.primerNombre.toUpperCase() : null;
    this.empleado.segundoNombre = this.empleado.segundoNombre ? this.empleado.segundoNombre.toUpperCase() : null;
    this.empleado.primerApellido = this.empleado.primerApellido ? this.empleado.primerApellido.toUpperCase() : null;
    this.empleado.segundoApellido = this.empleado.segundoApellido ? this.empleado.segundoApellido.toUpperCase() : null;
    const nom = (this.empleado.primerNombre ? this.empleado.primerNombre : '') + ' ' + (this.empleado.segundoNombre ? this.empleado.segundoNombre : '');
    const ape = (this.empleado.primerApellido ? this.empleado.primerApellido : '') + ' ' + (this.empleado.segundoApellido ? this.empleado.segundoApellido : '');
    this.empleado.nomCompleto = nom + ' ' + ape;
  }

  /**
   * METODO PARA RESETEAR LOS ELEMENTOS DEL FORMULARIO
   * AUTOR: FREDI ROMAN
   */
  public resetForm() {
    this.formRef.form.controls['codEmp'].reset();
    this.formRef.form.controls['empId'].reset();
    this.formRef.form.controls['nombre'].reset();
    this.formRef.form.controls['segNombre'].reset();
    this.formRef.form.controls['apellido'].reset();
    this.formRef.form.controls['segApellido'].reset();
    this.formRef.form.controls['nomComp'].reset();
    this.formRef.form.controls['celular'].reset();
    this.formRef.form.controls['telefono'].reset();
    this.formRef.form.controls['correo'].reset();
    this.formRef.form.controls['codAlt'].reset();
    this.formRef.form.controls['fecDesde'].reset();
    this.formRef.form.controls['fecHasta'].reset();
    this.empleado.estado = parseInt(GV.state.active);
    this.saveEdit = true;
    this.selectedRow = [];

    for (let attr of this.dynaParamList) {
      attr.valor = null;
    }
    this.defineDynaForm();
  }

  /**
   * METODO PARA CAPTURAR EL EVENTO CHANGE DEL SELECT DE DEPARTAMENTO PARA RECARGAR LOS DATOS DEPENDIENTES
   * AUTOR: FREDI ROMAN
   */
  public onChangeDepa() {
    this.empleado.cargo.id = null;
    this.resetForm();
    this.loadCargosData();
  }

  /**
   * METODO PARA CAPTURAR EL EVENTO CHANGE DEL SELECT DE SUCURSAL PARA RECARGAR LOS DATOS DEPENDIENTES
   * AUTOR: FREDI ROMAN
   */
  public onChangeSuc() {
    this.resetForm();
    this.empleado.cargo.departamento.idDepartamento = this.depaList[0].idDepartamento;
    this.cargoList = [];
    this.setPaginatedTableData(0, this.page.size);
  }

  /**
   * METODO PARA PROCESAR EL EVENTO CLICK DE UN BOTON NIVEL
   * AUTOR: FREDI ROMAN
   * @param event 
   * @param i 
   */
  public onNivelClick(event: number) {
    this.resetForm();
    this.currentNivelIndex = event;
    this.loadDeparmentData();
  }

  /**
   * METODO PARA CAPTURAR EL EVENTO CHANGE DEL SELECT DE EMPRESA PARA RECARGAR LOS DATOS DEPENDIENTES
   * AUTOR: FREDI ROMAN
   */
  public onChangeEmpresa() {
    this.resetForm();
    this.loadDynaParams();
    this.loadNivelesData();
  }

  /**
   * METODO PARA CARGAR LA INFORMACION DE LOS CARGOS DE UN DEPARTAMENTO DE UN NIVEL DE UNA EMPRESA
   * AUTOR: FREDI ROMAN
   */
  private loadCargosData(reloadTableData: boolean = true) {
    if (this.empleado.cargo.departamento.idDepartamento) {
      if (this.empleado.cargo.departamento.idDepartamento != '-1') {
        this._functionService.httpGet(WS.cargos.get.replace("${depaId}", this.empleado.cargo.departamento.idDepartamento))
          .then((responseList: any) => {
            this.cargoList = this._cargoService.createCargoList(responseList);
            this.empleado.cargo.id = (!this.empleado.cargo.id) ? ((this.cargoList[0]) ? this.cargoList[0].id : null) : this.empleado.cargo.id;
            if (reloadTableData) {
              this.setPaginatedTableData(0, this.page.size);
            }
          });
      } else {
        this.cargoList = [];
        this.empleado.cargo.id = null;
        this.setPaginatedTableData(0, this.page.size);
      }
    } else {
      this.cargoList = [];
      this.empleado.cargo.id = null;
      this.loadingIndicator = false;
    }
  }

  /**
   * METODO PARA CARGAR LA INFORMACION DE LOS DEPARTAMENTOS DE UN NIVEL DE UNA EMPRESA
   * AUTOR: FREDI ROMAN
   */
  private loadDeparmentData() {
    if (this.nivelesList.length > 0) {
      let parentNivelId = this.nivelesList[this.currentNivelIndex].id;
      this.depaName = this.nivelesList[this.currentNivelIndex].nombre;
      this.depaName = this.depaName.charAt(0) + this.depaName.substring(1).toLowerCase();
      this._functionService.httpGet(WS.departamento.get.replace("${nivelId}", parentNivelId))
        .then((responseList: any) => {
          this.depaList = this._departamentoService.createDepartamentoList(responseList);
          this.depaList.splice(0, 0, new Departamento('-1', null, TAGS.general.totalPage));
          this.empleado.cargo.departamento.idDepartamento = (this.depaList[0]) ? this.depaList[0].idDepartamento : null;
          this.empleado.cargo.id = null;
          this.loadCargosData();
        });
    } else {
      this.depaList = [];
      this.empleado.cargo.departamento.idDepartamento = null;
      this.loadingIndicator = false;
    }
  }

  /**
   * METODO PARA CARGAR LA INFORMACION DE LOS NIVELES DE UNA EMPRESA
   * AUTOR: FREDI ROMAN
   */
  private loadNivelesData() {
    if (this.mainEnterpriseId) {
      this._functionService.httpGetWithout(WS.nivel.get.replace("${enterpriseId}", this.mainEnterpriseId + ""))
        .then((responseList: any) => {
          this.nivelesList = this._nivelService.createNivelList(responseList);
          this.currentNivelIndex = 0;
          this.loadSucursalesData();
        });
    } else {
      this.nivelesList = [];
      this.loadingIndicator = false;
    }
  }

  /**
   * METODO PARA CARGAR LA INFORMACION DE LAS SUCURSALES DE UNA EMPRESA
   * AUTOR: FREDI ROMAN
   */
  private loadSucursalesData() {
    if (this.mainEnterpriseId) {
      this._functionService.httpGet(WS.sucursales.sucByStateCompany.replace("${empresa}", this.mainEnterpriseId + ""))
        .then((responseList: any) => {
          this.sucursalList = this._sucursalService.createSucursalList(responseList.content);
          this.empleado.sucursal.id = (this.sucursalList[0]) ? this.sucursalList[0].id : null;
          this.loadDeparmentData();
        });
    } else {
      this.sucursalList = [];
      this.empleado.sucursal.id = null;
      this.loadingIndicator = false;
    }
  }

  /**;
   * METODO PARA CARGAR LA LISTA DE PARAMETROS DE ATRIBUTOS ADICIONALES PARA EL EMPLEADO
   * AUTOR: FREDI ROMAN
   */
  private loadDynaParams() {
    if (this.mainEnterpriseId) {
      let url = WS.dynaParams.get.replace("${enterpriseId}", this.mainEnterpriseId + "");
      this._functionService.fetchWithout(url).then((res: any) => {
        this.dynaParamList = this._dynaParamService.createDynaParamList(res);
        this.defineDynaForm();
      });
    } else {
      this.dynaParamList = [];
      this.inputFormList = [];
    }
  }

  /**
   * METODO PARA CARGAR LOS DATOS DE INICIO, DESDE EL BACKEND, PARA MOSTRARLO EN EL DOM 
   * AUTOR: FREDI ROMAN
   */
  private loadInitData() {
    const array = [
      WS.empleadosSucursal.operators, WS.empleadosSucursal.identificationTypes, WS.general.status
    ];
    this._functionService.httpObservableGet(array).then((responseList) => {
      this._empresaControllerService.getEmpresaListController()
        .then((empresaData) => {
          this.empresaList = this._empresaService.createEmpresaList(empresaData);
          this.mainEnterpriseId = (this.empresaList[0]) ? this.empresaList[0].id : null;
          this.loadDynaParams();
          this.loadNivelesData();
        });
      this.operadorasList = responseList[0].content;
      this.empleado.codOperadora = (this.operadorasList[0]) ? this.operadorasList[0].id : null;

      this.tiposIdList = responseList[1].content;
      this.empleado.tipoIdentif = (this.tiposIdList[0]) ? this.tiposIdList[0].id : null;

      this.estadosList = responseList[2].content;

      this._functionService.getIpAddress().then((res: any) => {
        this.ipAddress = res;
      });
    });
  }

  /******************************************************************************************
  *******************************************************************************************
   ****** TABLE DEFINITION 
  ********************************************************************************************
  *******************************************************************************************/

  /**
   * METODO PARA REALIZAR UN FILTRO EN LA TABLA
   * AUTOR: FREDI ROMAN
   * @param event 
   */
  public onTableFilter(event) {
    const val = event.target.value.toLowerCase();
    this.filterEmpleadoList = this.empleadoList.slice();
    const temp = this.filterEmpleadoList.filter((filterData) => {
      return filterData.nomCompleto.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.filterEmpleadoList = temp;
    this.table.offset = 0;
  }

  /**
   * METODO PARA AUMENTAR EL NUMERO DE FILAS EL LA TABLA Y CARGAR LOS DATOS DESDE EL BACKEND
   * AUTOR: FREDI ROMAN
   */
  public reloadTableData() {
    this.page.pageNumber = 0;
    this.setTableOffset();
  }

  /**
   * METODO PARA CAPTURAR EL EVENTO DE SELECCION DE UNA FILA DE LA TABLA
   * AUTOR: FREDI ROMAN
   * @param selecteRow 
   */
  public onSelectRow(selecteRow: { selected: any[] }) {
    let auxDepaId;
    this.saveEdit = false;
    this.empleado.id = selecteRow.selected[0].id;
    this.empleado.codEmpleado = selecteRow.selected[0].codEmpleado;
    this.empleado.codAlterno = selecteRow.selected[0].codAlterno;
    this.empleado.primerNombre = selecteRow.selected[0].primerNombre;
    this.empleado.segundoNombre = selecteRow.selected[0].segundoNombre;
    this.empleado.primerApellido = selecteRow.selected[0].primerApellido;
    this.empleado.segundoApellido = selecteRow.selected[0].segundoApellido;
    this.empleado.nomCompleto = selecteRow.selected[0].nomCompleto;
    this.empleado.tipoIdentif = selecteRow.selected[0].tipoIdentif;
    this.empleado.identificacion = selecteRow.selected[0].identificacion;
    this.empleado.codOperadora = selecteRow.selected[0].codOperadora;
    this.empleado.telefonoCelular = selecteRow.selected[0].telefonoCelular;
    this.empleado.telefonoCelular = selecteRow.selected[0].telefonoCelular;
    this.empleado.telefonoConvencional = selecteRow.selected[0].telefonoConvencional;
    this.empleado.telefonoConvencional = selecteRow.selected[0].telefonoConvencional;
    this.empleado.usuario = selecteRow.selected[0].usuario;
    this.empleado.correo = selecteRow.selected[0].correo;
    this.empleado.cargo.id = selecteRow.selected[0].cargo.id;

    auxDepaId = this.empleado.cargo.departamento.idDepartamento;
    this.empleado.cargo.departamento.idDepartamento = selecteRow.selected[0].cargo.departamento.idDepartamento;

    this.empleado.sucursal = selecteRow.selected[0].sucursal;
    this.empleado.dynaAttrib = selecteRow.selected[0].dynaAttrib;

    if (this.empleado.dynaAttrib) {
      for (let attr of this.empleado.dynaAttrib) {
        this.dynaParamList.find(dyna => dyna.id == attr.idAtributo).valor = attr.valor;
      }

    } else {
      for (let attr of this.dynaParamList) {
        attr.valor = null;
      }
    }
    this.defineDynaForm();

    let date = selecteRow.selected[0].fechaDesde;
    date = (date) ? date.split("-") : date;
    this.empleado.fechaDesde = (date) ? { year: parseInt(date[0]), month: parseInt(date[1]), day: parseInt(date[2]) } : date;

    date = selecteRow.selected[0].fechaHasta;
    date = (date) ? date.split("-") : date;
    this.empleado.fechaHasta = (date) ? { year: parseInt(date[0]), month: parseInt(date[1]), day: parseInt(date[2]) } : date;

    this.empleado.estado = selecteRow.selected[0].estado;
    if (auxDepaId != this.empleado.cargo.departamento.idDepartamento) {
      this.loadCargosData(false);
    }
  }

  /**
   * METODO PARA DEFINIR LA PAGINACION DE LA TABLA, AL CAPTURAR EL EVENTO DE NEXT/PREV PAGE
   * AUTOR: FREDI ROMAN
   * @param pageInfo 
   */
  public setPage(pageInfo: any) {
    this.page.pageNumber = pageInfo.offset;
    this.setTableOffset();
  }

  /**
   * METODO PARA ESTABLECER EL OFFSET PARA LA PAGINACION DE LA TABLA
   * AUTOR: FREDI ROMAN
   */
  private setTableOffset() {
    let indexInit: number;
    if (this.page.pageNumber == 0) {
      this.filterEmpleadoList = this.empleadoList.slice(0, this.page.size);
    } else {
      indexInit = this.page.pageNumber * this.page.size;
      this.filterEmpleadoList = this.empleadoList.slice(indexInit, indexInit + this.page.size);
    }
  }

  /**
   * METODO PARA DEFINIR LOS DATOS DE LA TABLA AL INTERACTUAR CON LA PAGINACION
   * AUTOR: FREDI ROMAN
   * @param pageNumber 
   * @param size 
   */
  public setPaginatedTableData(pageNumber: any, size: any, withAnimation: boolean = true) {
    this.page.pageNumber = pageNumber;
    this.page.size = size;

    if (this.empleado.sucursal.id) {
      let url = WS.empleadosSucursal.getByFilter;
      this.empleadoList = [];
      this.filterEmpleadoList = [];

      let filter = {
        idSucursal: this.empleado.sucursal.id,
        numNivel: this.nivelesList[this.currentNivelIndex].numero,
        idDepartamento: this.empleado.cargo.departamento.idDepartamento != '-1' ? this.empleado.cargo.departamento.idDepartamento : null
      };

      this._restClientService.httpGetWithHeaders(url, { key: 'filterJson', value: JSON.stringify(filter) }, withAnimation)
        .then((res: any) => {
          this.empleadoList = this._empleadoService.createEmpleadoList(res);
          this.filterEmpleadoList = this.empleadoList.slice();
          this.page.totalElements = this.empleadoList.length;
        });
    } else {
      this.empleadoList = [];
      this.filterEmpleadoList = [];
      this.page.totalElements = 0;
    }
    this.loadingIndicator = false;
  }
  /******************************************************************************************
  ******************************************************************************************/

  /********************************************************************************************************
  ********************************************************************************************************/
}
