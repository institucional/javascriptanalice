import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import { AppService } from '../../app.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Http } from '@angular/http';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'tables-ngx-datatable',
  templateUrl: './user.component.html',
  styleUrls: [
    '../../../../node_modules/@swimlane/ngx-datatable/release/index.css',
    '../../../vendor/libs/ngx-datatable/ngx-datatable.scss',
    '../../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css',
    '../../../vendor/libs/ngx-toastr/ngx-toastr.scss'
  ],
  encapsulation: ViewEncapsulation.None
})


export class UserComponent {
  @ViewChild(DatatableComponent) table: DatatableComponent;
  formUser: FormGroup;
  submitedUser: Boolean = false;

  options_notitications = {
    tapToDismiss: true,
    closeButton: false,
    progressBar: true,
    positionClass: 'toast-top-right',
    rtl: this.appService.isRTL
  };

  loadingIndicator = true;
  rows = [];
  temp = [];
  selected = [];
  user = {
    'codigo': '',
    'email': '',
    'nombre': '',
    'telefono': '',
    'verificado': '0',
    'estado': '1',
    'nivel': '100',
    'grupo': '',
    'tipoIdentificacion': '',
    'identificacion': ''
  };

  constructor(private appService: AppService, private http: Http, public toastrService: ToastrService, private formBuilder: FormBuilder) {
    this.appService.pageTitle = 'Administración - Usuarios';
    this.fetch((data) => {
      this.temp = [...data];
      this.rows = data;
      setTimeout(() => { this.loadingIndicator = false; }, 1500);
    });
    this.toastrService.toastrConfig.newestOnTop = true;
    this.toastrService.toastrConfig.preventDuplicates = true;
  }

  fetch(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', `assets/json/users.json`);
    req.onload = () => {
      const data = JSON.parse(req.response);
      cb(data);
    };
    req.send();
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function(d) {
      return d.nombre.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows = temp;
    this.table.offset = 0;
  }

  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    this.user = this.selected[0]
    this.formUser.controls.email.setValue(this.user.email);
    this.formUser.controls.name.setValue(this.user.nombre);
    this.formUser.controls.phone.setValue(this.user.telefono);
  }

  onKey(event: any, param: string) { // without type info
    this.user[param] = event.target.value;
  }

  save() {
     if (this.formUser.valid) {
      this.submitedUser = false;
      this.http.post("api/usario/salvar", {
          codCatalogo : this.user.codigo,
          nomCatalogo: this.user.nombre,
          descripcion: this.user.email,
          tipoTabla: this.user.telefono,
          verificado: this.user.verificado,
          estado: this.user.estado,
        }).subscribe(
          res => {
            this.toastrService['success']("Usuario salvado", this.appService.pageTitle, this.options_notitications);
            this.clear()
            this.fetch((data) => {
            this.temp = [...data];
            this.rows = data;
              setTimeout(() => { this.loadingIndicator = false; }, 1500);
            });
          },
          err => {
            this.toastrService['error']("Error, usuario no salvado", this.appService.pageTitle, this.options_notitications);
          }
        );
    } else {
      this.submitedUser = true;
    }
  }

  clear() {
    this.user = {
      'codigo': '',
      'email': '',
      'nombre': '',
      'telefono': '',
      'verificado': '0',
      'estado': '1',
      'nivel': '100',
      'grupo': '',
      'tipoIdentificacion': '',
      'identificacion': ''
    };
    this.formUser.controls.email.setValue("");
    this.formUser.controls.name.setValue("");
    this.formUser.controls.phone.setValue("");
    this.submitedUser = false;
  }

  ngOnInit() {
    this.formUser = this.formBuilder.group({
      email: [null, [Validators.required, Validators.minLength(3)]],
      name: [null, [Validators.required, Validators.minLength(5)]],
      phone: [null, [Validators.required, Validators.minLength(5)]],
    });
  }

  get fu() { return this.formUser.controls; }

  group(group: number){
    switch (group) {
      case 0:
        return "Administrador"
      case 100:
        return "Usuario sin privilegios"
      default:
        return "Error"
    }
  }

}
