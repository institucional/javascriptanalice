import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// *******************************************************************************
//

import { AdminRoutingModule } from './admin-routing.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxMaskModule } from 'ngx-mask';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TextMaskModule } from 'angular2-text-mask';

// *******************************************************************************
// Page components

import { CatalogComponent } from './catalog/catalog.component';
import { CatalogItemComponent } from './catalogitem/catalogitem.component';
import { UserComponent } from './user/user.component';
import { CharacteristicsComponent } from './characteristics/characteristics.component';
import { ServicesComponent } from './services/services.component';
import { EmployeesComponent } from './employees/employees.component';
import { AuthInterceptor } from '../services/interceptors/auth.service';
import { SucursalesComponent } from './sucursales/sucursales.component';
import { CompanyComponent } from './company/company.component';
import { EmpresaService } from '../services/component-services/empresa.service';
import { CargoComponent } from './cargo/cargo.component';
import { CargoService } from '../services/component-services/cargo.service';
import { PresupuestoComponent } from './presupuesto/presupuesto.component';
import { PresupuestoService } from '../services/component-services/presupuesto.service';
import { ConvenioService } from '../services/component-services/convenio.service';
import { EmpleadoService } from '../services/component-services/empleado.service';
import { SeachEmployeeComponent } from './seach-employee/seach-employee.component';
import { FeriadoComponent } from './feriado/feriado.component';
import { FeriadoService } from '../services/component-services/feriado.service';
import { SucursalService } from '../services/component-services/sucursal.service';
import { UsuarioEmpresaComponent } from './usuario-empresa/usuario-empresa.component';
import { UsuarioEmpresaService } from '../services/component-services/usuario-empresa.service';
import { UserService } from '../services/component-services/user.service';
import { SecuenciaVoucherComponent } from './secuencia-voucher/secuencia-voucher.component';
import { EmpresaControllerService } from '../services/controllers/empresa-controller.service';
import { NivelComponent } from './nivel/nivel.component';
import { NivelService } from '../services/component-services/nivel.service';
import { DepartamentoService } from '../services/component-services/departamento.service';
import { NavBtnGroupComponent } from './_generic-components/nav-btn-group/nav-btn-group.component';
import { DepartamentoComponent } from './departamento/departamento.component';
import { DynaParamsComponent } from './dyna-param/dyna-param.component';
import { DynaParamService } from '../services/component-services/dyna-param.service';
import { DynamicFormModule } from '../dynamic-form/dynamic-form.module';
import { FrAccordionModule } from '../fr-accordion/fr-accordion.module';
import { TableResponsiveService } from '../services/table/table-responsive.service';
import { ConvenioDetailComponent } from './_generic-components/convenio-detail/convenio-detail.component';
import { EmployeeListComponent } from './_generic-components/employee-list/employee-list.component';
import { PresupuestoListComponent } from './_generic-components/presupuesto-list/presupuesto-list.component';
import { FrConfirmPwdModule } from '../fr-confirm-pwd/fr-confirm-pwd.module';
import { ResponsableComponent } from './responsable/responsable.component';
import { ContactoFastlineComponent } from './contacto-fastline/contacto-fastline.component';
import { ResponsableContactoService } from '../services/component-services/responsable-contacto.service';
import { ConveniosListComponent } from './_generic-components/convenios-list/convenios-list.component';

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    NgxDatatableModule,
    NgxMaskModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    TextMaskModule,
    HttpClientModule,
    DynamicFormModule,
    FrAccordionModule,
    FrConfirmPwdModule
  ],
  providers: [
    EmpresaService,
    CargoService,
    ConvenioService,
    PresupuestoService,
    EmpleadoService,
    ResponsableContactoService,
    SucursalService,
    FeriadoService,
    UsuarioEmpresaService,
    UserService,
    EmpresaControllerService,
    NivelService,
    DepartamentoService,
    DynaParamService,
    TableResponsiveService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  declarations: [
    CatalogComponent,
    CatalogItemComponent,
    UserComponent,
    CharacteristicsComponent,
    ServicesComponent,
    EmployeesComponent,
    SucursalesComponent,
    CompanyComponent,
    CargoComponent,
    PresupuestoComponent,
    ResponsableComponent,
    FeriadoComponent,
    UsuarioEmpresaComponent,
    SecuenciaVoucherComponent,
    SeachEmployeeComponent,
    NivelComponent,
    NavBtnGroupComponent,
    DepartamentoComponent,
    DynaParamsComponent,
    ConvenioDetailComponent,
    EmployeeListComponent,
    PresupuestoListComponent,
    ContactoFastlineComponent,
    ConveniosListComponent
  ],
  exports: [
    SeachEmployeeComponent
  ]
})
export class AdminModule { }
