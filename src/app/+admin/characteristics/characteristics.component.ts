import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import { AppService } from '../../app.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TAGS } from '../../TextLabels';
import { GV } from '../../GeneralVars';
import { WS } from '../../WebServices';
import * as moment from 'moment';
import { ConfirmationDialogService } from '../../components/confirmation-dialog/confirmation-dialog.service';
import { FunctionService } from '../../services/functions/function.service';
import { Page } from '../../components/pagination/page';
import { ConvenioService } from '../../services/component-services/convenio.service';
import { Router } from '@angular/router';
import { EmpresaControllerService } from '../../services/controllers/empresa-controller.service';
import { SwitchInputInterface } from '../../dynamic-form/interfaces/switch-input.interface';
import { TimeInterface } from '../../dynamic-form/interfaces/time.interface';
import { SWITCH_SIZE } from '../../dynamic-form/data/switch-size';

@Component({
  selector: '[tables-ngx-datatable] [ngbd-datepicker-popup]',
  templateUrl: './characteristics.component.html',
  styleUrls: [
    '../../../../node_modules/@swimlane/ngx-datatable/release/index.css',
    '../../../vendor/libs/ngx-datatable/ngx-datatable.scss',
    '../../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css',
    '../../../vendor/libs/ngx-toastr/ngx-toastr.scss'
  ],
  encapsulation: ViewEncapsulation.None
})

export class CharacteristicsComponent {
  /***************************************************************************************************************************************
   * AUTOR: FREDI ROMAN
   **************************************************************************************************************************************/
  public switchIns: SwitchInputInterface[];
  public timeVars: TimeInterface[];
  public navBtnList: { url: string; selected: boolean; label: string }[];

  /**
   * METODO PARA ABRIR EL MENU DE CONTACTOS
   */
  public openContacts() {
    this._router.navigate(["/admin/contacto", this.caracteristica.SGE_Empresa_idEmpresa]);
  }

  changeAutorizacion() {
    if (this.caracteristica.requiereAutorizacion == GV.afirmation.si) {
      this.banAutorizador = false;
    } else {
      this.banAutorizador = true;
    }
  }

  /**
   * METODO PARA VERIFICAR SI EL USUARIO HA ESCOGIDO SI O NO:
   */
  public verifyControl() {
    if (this.caracteristica.controlHoras == GV.afirmation.no) {
      this.caracteristica.poliTaxiLaborable = JSON.stringify({
        desde: "00:00:00",
        hasta: "00:00:00",
      });
      this.caracteristica.poliTaxiFeriaFds = JSON.stringify({
        desde: "00:00:00",
        hasta: "00:00:00",
      });
    } else {
      this.defineTimeVars();
    }
  }

  /**
   * METODO PARA CAPTURAR LOS VALORES DE LAS VARIABLES DE TIEMPO
   * AUTOR: FREDI ROMAN
   * @param event
   */
  public getTimeChanges(event: TimeInterface) {
    for (let time of this.timeVars) {
      if (event.id == time.id) time = event;
    }

    this.caracteristica.poliTaxiLaborable = JSON.stringify({
      desde: this.timeVars[0].hours + ":" + this.timeVars[0].minutes + ":" + this.timeVars[0].seconds,
      hasta: this.timeVars[1].hours + ":" + this.timeVars[1].minutes + ":" + this.timeVars[1].seconds,
    });
    this.caracteristica.poliTaxiFeriaFds = JSON.stringify({
      desde: this.timeVars[2].hours + ":" + this.timeVars[2].minutes + ":" + this.timeVars[2].seconds,
      hasta: this.timeVars[3].hours + ":" + this.timeVars[3].minutes + ":" + this.timeVars[3].seconds,
    });
  }

  /**
   * METODO PARA DEFINIR LAS VARIABLES DE TIEMPO
   * AUTOR: FREDI ROMAN
   */
  private defineTimeVars() {
    this.timeVars = [
      { id: 'labDesde', label: 'Desde', hours: "00", minutes: "00", seconds: '00' },
      { id: 'labHasta', label: 'Hasta', hours: "00", minutes: "00", seconds: '00' },
      { id: 'ferDesde', label: 'Desde', hours: "00", minutes: "00", seconds: '00' },
      { id: 'ferHasta', label: 'Hasta', hours: "00", minutes: "00", seconds: '00' },
    ]
  }

  /**
   * METODO PARA SELECCIONAR UN DETERMINADO SWITCH INPUT DEPENDIENDO DEL VALOR SELECCIONADO DESDE LA TABLA
   * AUTOR: FREDI ROMAN
   */
  private selectSwitchInValue() {
    let index = this.switchIns.findIndex(switchIn => switchIn.id == this.caracteristica.tipoControl);
    for (let i = 0; i < this.switchIns.length; i++) {
      if (i == index) this.switchIns[i].checked = true;
      else this.switchIns[i].checked = false;
    }
    this.switchIns.find(switchIn => switchIn.id != this.caracteristica.tipoControl).checked = false;
  }

  /**
   * METODO PARA CAPTURAR EL TIPO DE CONTROL PRESUPUESTARIO ESCOGIDO POR EL USUARIO:
   * @param event ID QUE VIENE POR EL OUTPUT EVENT EMITTER DEL COMPONENTE HIJO
   */
  public getSelectedSwitchValue(event: string) {
    this.caracteristica.tipoControl = event;
  }

  /**
   * METODO PARA DEFINIR EL ARRAY DE TIPO SWITCH-INPUT
   * AUTOR: FREDI ROMAN
   */
  private defineSwitchInputs() {
    let i = 0;
    this.switchIns = []
    if (this.responseList[7]) {
      for (let tipo of this.responseList[7].content) {
        this.switchIns.push({
          id: tipo.id,
          label: tipo.nomSubCatalogo,
          checked: i == 0 ? true : false,
          size: SWITCH_SIZE.xs,
          immutable: true,
          customClass: 'switch-normal'
        });
        i += 1;
      }
      this.caracteristica.tipoControl = this.switchIns[0].id;
    }

  }
  /**************************************************************************************************************************************/
  /**************************************************************************************************************************************/

  banCompany = false;
  banAutorizador = false;

  fechaSuscripcion = { year: 2017, month: 8, day: 8 };
  fechaVencimiento = { year: 2017, month: 8, day: 8 };
  @ViewChild(DatatableComponent) table: DatatableComponent;
  submitedCharacteristic: Boolean = false;
  pfielcump: Boolean = false;
  prespciv: Boolean = false;
  nivserv: Boolean = false;
  penali: Boolean = false;
  multa: Boolean = false;
  formCharacteristic: FormGroup;
  empresas = [];
  tiposReportes = [];
  periodosFacturacion = [];
  facturasImpresas = [];
  formasRegistro = [];
  tiposFacturacion = [];
  selected = [];
  responseList = null;
  options_notitications = {
    tapToDismiss: true,
    closeButton: false,
    progressBar: true,
    positionClass: 'toast-top-right',
    rtl: this.appService.isRTL
  };

  loadingIndicator = true;
  rows = null;
  temp = null;
  estadoBuscar: '';
  caracteristicaPru = null;
  caracteristica = {
    'id': '',
    'SGE_Empresa_idEmpresa': '',
    'fechaSuscripcion': '',
    'estado': '',
    'codConvenio': '',
    'fechaVencimiento': null,
    'desagregaConcumoCC': '',
    'tipoReporte': '',
    'porcentajeServicio': '',
    'periodoFacturacion': '',
    'validaCedula': '',
    'valorServicio': '',
    'facturaImpresa': '',
    'origenServicio': '',
    'tipoFacturacion': '',
    'controlHoras': '',
    'porcentajeMora': '0',
    'numeroHorasAprobar': '',
    'pagaMora': '23',
    'valorMora': '0',
    'alertaMensaje': '',
    'carteravencida': '',
    'plazovencimiento': '0',
    'montoLimite': 0,
    'numeroVoucherLimite': '',
    'plazo': '',
    'polizaFielcumplimiento': '-1',
    'polizaResponsabilidadcivil': '-1',
    'nivelServicio': '-1',
    'penalizacion': '-1',
    'multasSanciones': '-1',
    'adenda': '-1',
    'observaciones': '',
    'nombreEmpresa': '',
    'nomSubCatalogo': '',
    'codusu': '1',
    'fecult': '',
    'status': '1',
    'ipMaquina': '',
    'tipoControl': '',
    'poliTaxiLaborable': undefined,
    'poliTaxiFeriaFds': undefined,
    'tiempoEspera': '0',
    'requiereAutorizacion': '',
    'numServiciosExtra': '0'
  };
  labels = null;
  gv = null;
  viewServices = null;
  editSave: string;
  enableState: Boolean = true;
  changeStateVal: Boolean = true;
  inactiveState: Boolean = false;
  ipAddress = null;
  estados = [];
  afirmaciones = [];
  page = new Page();

  constructor(
    private functionService: FunctionService,
    private _empresaControllerService: EmpresaControllerService,
    private confirmationDialogService: ConfirmationDialogService,
    private appService: AppService,
    private http: HttpClient,
    public toastrService: ToastrService,
    private formBuilder: FormBuilder,
    private _convenioService: ConvenioService,
    private _router: Router
  ) {
    this.navBtnList = [
      { url: '/admin/characteristics', selected: true, label: 'Convenios' },
      { url: '/admin/contactos', selected: false, label: 'Contactos' },
      { url: '/admin/secuenciavoucher', selected: false, label: 'Secuencias' },
      { url: '/admin/presupuesto', selected: false, label: 'Presupuestos' },
      { url: '/admin/responsables', selected: false, label: 'Responsables' }
    ];

    this.defineTimeVars();
    this.toastrService.toastrConfig.newestOnTop = true;
    this.toastrService.toastrConfig.preventDuplicates = true;
  }

  /**
   * METODO PARA CAPTURAR EL EVENTO CHANGE DEL SELECT DE EMPRESA PARA RECARGAR LOS DATOS DE LA TABLA
   * AUTOR: FREDI ROMAN
   */
  public onChangeEmpresa() {
    this.setDataPagination(0, this.page.size);
  }

  openConfirmationDialog() {
    const text = this.labels.messages.inactivate + ' '
      + this.labels.characteristics.agreement + '.  '
      + this.labels.messages.inactivateConfirm;
    this.confirmationDialogService.confirm(this.labels.general.confirm.toLocaleUpperCase(), text)
      .then((confirmed) => {
        if (confirmed) {
          this.save();
        }
      }).catch(() =>
        console.log('Dismissed.)'
        ));
  }
  changeState() {
    this.changeStateVal = true;
  }
  confirmMethod() {
    if (this.changeStateVal) {
      this.openConfirmationDialog();
    } else {
      this.save();
    }
  }
  onChangeDate(event) {
    let year = this.fechaSuscripcion.year;
    let month = this.fechaSuscripcion.month;
    let day = this.fechaSuscripcion.day;
    const start = moment(year + '-' + month + '-' + day);
    year = this.fechaVencimiento.year;
    month = this.fechaVencimiento.month;
    day = this.fechaVencimiento.day;
    const end = moment(year + '-' + month + '-' + day);
    const dif = end.diff(start, 'days');
    this.caracteristica.plazo = dif.toString();
  }
  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function (d) {
      return d.empresa.nombreEmpresa.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows = temp;
    this.table.offset = 0;
  }
  updateFilterState() {
    const temp = this.temp.filter((d) => {
      if (this.estadoBuscar === this.labels.general.all) {
        return d;
      } else {
        return d['estado'].toString() === this.estadoBuscar;
      }
    });
    this.rows = temp;
    this.table.offset = 0;
  }
  save() {
    this.caracteristica.plazovencimiento = this.caracteristica.plazovencimiento == '' ? '0' : this.caracteristica.plazovencimiento;
    //let validaPoliza = false;
    let validaPoliza = true;
    this.caracteristica.fecult = this.functionService.getToday();
    this.caracteristica.ipMaquina = this.ipAddress;
    //let validaOtros = false;
    let validaOtros = true;
    /*if (this.caracteristica.polizaResponsabilidadcivil.toString() === GV.afirmation.si ||
      this.caracteristica.polizaFielcumplimiento.toString() === GV.afirmation.si) {
      validaPoliza = true;
    } else {
      this.toastrService['error'](this.labels.messages.selectVal + ' '
        + this.labels.characteristics.polizas.title + '.', this.appService.pageTitle, this.options_notitications);
      return;
    }*/
    /*if (this.caracteristica.multasSanciones.toString() === GV.afirmation.si ||
      this.caracteristica.penalizacion.toString() === GV.afirmation.si ||
      this.caracteristica.nivelServicio.toString() === GV.afirmation.si) {
      validaOtros = true;
    } else {
      this.toastrService['error'](this.labels.messages.selectVal + ' '
        + this.labels.characteristics.otros.title + '.', this.appService.pageTitle, this.options_notitications);
      return;
    }*/
    if (this.formCharacteristic.valid && validaOtros && validaPoliza) {
      if (this.caracteristica.plazo.toString() !== '0') {
        this.submitedCharacteristic = false;
        let year = this.fechaSuscripcion['year'];
        let month = this.fechaSuscripcion['month'];
        let day = this.fechaSuscripcion['day'];
        this.caracteristica.fechaSuscripcion = year + '-' + (month < 10 ? '0' + month : month) + '-' + (day < 10 ? '0' + day : day);
        year = this.fechaVencimiento['year'];
        month = this.fechaVencimiento['month'];
        day = this.fechaVencimiento['day'];
        this.caracteristica.fechaVencimiento = year + '-' + (month < 10 ? '0' + month : month) + '-' + (day < 10 ? '0' + day : day);

        if (this.editSave === GV.editSave.save) {
          const empresa = this.empresas.find((emp) => {
            return emp.id.toString() === this.caracteristica.SGE_Empresa_idEmpresa.toString();
          });
          this.http.post(WS.characteristics.exist, empresa).subscribe(res => {
            const arr = res[0];
            if (!arr) {
              this.caracteristica['empresa'] = empresa;
              this.functionService.httpPost(WS.characteristics.save, this.caracteristica).then((conv) => {
                this.caracteristicaPru = conv;
                const id = this.caracteristicaPru['id'];
                const codConvenio = this.caracteristicaPru['codConvenio'];
                const idEmpresa = this.caracteristicaPru['empresa']['id'];
                const nombreEmpresa = this.caracteristicaPru['empresa']['nombreEmpresa'];
                this.clear();
                setTimeout(() => {
                  this._router.navigate(['admin/services', id, codConvenio, idEmpresa, nombreEmpresa]);
                }, 2000);
              });
            } else {
              this.toastrService['error'](this.labels.messages.convenioAct, this.appService.pageTitle, this.options_notitications);
            }
          }, err => {
            this.toastrService['error'](this.labels.messages.processError, this.appService.pageTitle, this.options_notitications);
          });
        } else {
          if (this.caracteristica.estado.toString() === GV.state.inactive && this.changeStateVal) {
            const body = {
              convenio: this.caracteristica,
              estado: GV.state.inactive
            };
            this.functionService.httpPost(WS.characteristics.inactive, body).then(() => {
              this.clear();
            });
          } else {
            const empresa = this.empresas.find((emp) => {
              return emp.id.toString() === this.caracteristica.SGE_Empresa_idEmpresa;
            });
            this.caracteristica['empresa'] = empresa;
            let url = WS.characteristics.update;
            url = url.replace('${idConvenio}', this.selected[0].id);
            this.functionService.httpPut(url, this.caracteristica).then(() => {
              this.clear();
            });
          }
        }
      } else {
        this.submitedCharacteristic = true;
        this.toastrService['error'](this.labels.messages.lowerPlazo, this.appService.pageTitle, this.options_notitications);
      }
    } else {
      this.submitedCharacteristic = true;
      this.toastrService['error'](this.labels.messages.reviewForm, this.appService.pageTitle, this.options_notitications);
    }
  }
  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    this.setDataPagination(pageInfo.offset, pageInfo.pageSize);
  }
  setDataPagination(pageNumber, size) {
    this.page.pageNumber = pageNumber;
    this.page.size = size;

    if (this.caracteristica.SGE_Empresa_idEmpresa) {
      let url = WS.characteristics.fetchByEnterpriseId.replace("${enterpriseId}", this.caracteristica.SGE_Empresa_idEmpresa);
      url = url.replace('${pageNumber}', pageNumber);
      url = url.replace('${size}', size);
      this.functionService.fetch(url).then((res) => {
        this.temp = res['content'];
        this.rows = res['content'];
        this.page.totalElements = res['totalElements'];
      });
    } else {
      this.temp = [];
      this.rows = [];
      this.page.totalElements = 0;
    }
    this.loadingIndicator = false;
  }
  loadData() {
    this.page.pageNumber = 0;
    this.setDataPagination(0, this.page.size);
  }
  clear() {
    this.banCompany = false;
    const now = new Date();
    this.caracteristica.SGE_Empresa_idEmpresa = (this.empresas[0]) ? this.empresas[0].id : null;
    this.setDataPagination(0, 5);
    this.formCharacteristic.controls.code.setValue('');
    this.formCharacteristic.controls.plaz.setValue('');
    //this.formCharacteristic.controls.inter.setValue('');
    //this.formCharacteristic.controls.numh.setValue('');
    // this.formCharacteristic.controls.mont.setValue('');
    this.formCharacteristic.controls.dec.setValue('');
    //this.formCharacteristic.controls.dec1.setValue('');
    //this.formCharacteristic.controls.dec2.setValue('');
    //this.formCharacteristic.controls.dec3.setValue('');
    this.formCharacteristic.controls.fecsus.setValue('');
    this.formCharacteristic.controls.fecven.setValue('');
    this.estadoBuscar = this.labels.general.all;
    this.caracteristica.id = '';
    // this.caracteristica.SGE_Empresa_idEmpresa = this.responseList[0].content[0].id;
    this.caracteristica.codConvenio = '';
    this.caracteristica.plazo = '';
    this.caracteristica.fechaSuscripcion = '';
    this.caracteristica.fechaVencimiento = '';
    this.caracteristica.desagregaConcumoCC = this.responseList[6].content[0].id;
    this.caracteristica.validaCedula = this.responseList[6].content[0].id;
    this.caracteristica.tipoReporte = this.responseList[1].content[0].id;
    this.caracteristica.origenServicio = this.responseList[3].content[0].id;
    this.caracteristica.adenda = this.responseList[6].content[0].id;
    this.caracteristica.numeroVoucherLimite = '0';
    this.caracteristica.observaciones = '';
    this.caracteristica.tipoFacturacion = this.responseList[4].content[0].id;
    this.caracteristica.alertaMensaje = this.responseList[6].content[1].id;
    this.caracteristica.requiereAutorizacion = this.responseList[6].content[1].id;
    this.banAutorizador = true;
    this.caracteristica.carteravencida = this.responseList[6].content[0].id;
    this.caracteristica.porcentajeServicio = '';
    //this.caracteristica.porcentajeMora = '';
    this.caracteristica.controlHoras = this.responseList[6].content[1].id;
    this.verifyControl();
    this.caracteristica.montoLimite = 0;
    this.caracteristica.valorServicio = '0';
    this.caracteristica.tiempoEspera = '0';
    this.caracteristica.numServiciosExtra = '0';
    //this.caracteristica.valorMora = '';
    this.caracteristica.numeroHorasAprobar = '0';
    this.caracteristica.periodoFacturacion = this.responseList[1].content[0].id;
    this.caracteristica.facturaImpresa = this.responseList[2].content[0].id;
    //this.caracteristica.pagaMora = this.responseList[7].content[0].id;
    /*this.caracteristica.polizaFielcumplimiento = GV.afirmation.no;
    this.caracteristica.polizaResponsabilidadcivil = GV.afirmation.no;
    this.caracteristica.nivelServicio = GV.afirmation.no;
    this.caracteristica.penalizacion = GV.afirmation.no;
    this.caracteristica.multasSanciones = GV.afirmation.no;*/
    this.caracteristica.estado = this.responseList[5].content[0].id;
    this.fechaSuscripcion = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    this.fechaVencimiento = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    this.submitedCharacteristic = false;
    this.viewServices = true;
    this.editSave = GV.editSave.save;
    this.pfielcump = false;
    this.prespciv = false;
    this.nivserv = false;
    this.penali = false;
    this.multa = false;
    this.enableState = true;
    this.changeStateVal = false;
    this.inactiveState = false;
  }
  changed = (evt, element) => {
    if (evt.target.checked) {
      this.caracteristica[element] = GV.afirmation.si;
    } else {
      this.caracteristica[element] = GV.afirmation.no;
    }
  }
  onSelect({ selected }) {
    this.banCompany = true;
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    this.caracteristica = this.selected[0];
    this.caracteristica.SGE_Empresa_idEmpresa = this.caracteristica['empresa'].id.toString();
    this.caracteristica.nombreEmpresa = this.caracteristica['empresa'].nombreEmpresa.toString();
    this.caracteristica.plazo = this.caracteristica.plazo.toString();
    this.caracteristica.numeroHorasAprobar = this.caracteristica.numeroHorasAprobar.toString();
    this.caracteristica.numeroVoucherLimite = this.caracteristica.numeroVoucherLimite.toString();
    this.caracteristica.numeroHorasAprobar = this.caracteristica.numeroHorasAprobar.toString();
    // this.caracteristica.montoLimite = this.caracteristica.montoLimite.toString();
    this.caracteristica.porcentajeServicio = this.caracteristica.porcentajeServicio.toString();
    this.caracteristica.valorServicio = this.caracteristica.valorServicio.toString();
    this.caracteristica.tipoControl = this.caracteristica.tipoControl.toString();
    this.caracteristica.tiempoEspera = this.caracteristica.tiempoEspera.toString();
    this.caracteristica.numServiciosExtra = this.caracteristica.numServiciosExtra.toString();
    if (this.caracteristica.requiereAutorizacion == GV.afirmation.si) {
      this.banAutorizador = false;
    } else {
      this.banAutorizador = true;
    }
    this.selectSwitchInValue();

    /*this.caracteristica.porcentajeMora = this.caracteristica.porcentajeMora.toString();
    this.caracteristica.valorMora = this.caracteristica.valorMora.toString();*/
    let year = parseInt(this.caracteristica.fechaSuscripcion.split('-')[0], 10);
    let month = parseInt(this.caracteristica.fechaSuscripcion.split('-')[1], 10);
    let day = parseInt(this.caracteristica.fechaSuscripcion.split('-')[2], 10);
    this.fechaSuscripcion = { 'year': year, 'month': month, 'day': day };
    year = parseInt(this.caracteristica.fechaVencimiento.split('-')[0], 10);
    month = parseInt(this.caracteristica.fechaVencimiento.split('-')[1], 10);
    day = parseInt(this.caracteristica.fechaVencimiento.split('-')[2], 10);
    this.fechaVencimiento = { 'year': year, 'month': month, 'day': day };
    /*this.pfielcump = this.caracteristica.polizaFielcumplimiento.toString() === GV.afirmation.si ? true : false;
    this.prespciv = this.caracteristica.polizaResponsabilidadcivil.toString() === GV.afirmation.si ? true : false;
    this.nivserv = this.caracteristica.nivelServicio.toString() === GV.afirmation.si ? true : false;
    this.penali = this.caracteristica.penalizacion.toString() === GV.afirmation.si ? true : false;
    this.multa = this.caracteristica.multasSanciones.toString() === GV.afirmation.si ? true : false;*/
    this.viewServices = false;
    this.editSave = GV.editSave.edit;
    this.enableState = false;
    this.inactiveState = this.caracteristica.estado.toString() === GV.state.inactive ? true : false;

    this.caracteristica.poliTaxiLaborable = JSON.parse(this.selected[0].poliTaxiLaborable);
    this.caracteristica.poliTaxiFeriaFds = JSON.parse(this.selected[0].poliTaxiFeriaFds);

    this.timeVars = this._convenioService.extractTimeData(this.selected[0]);
    //
  }
  addDays() {
    if (this.fechaSuscripcion && this.caracteristica.plazo && this.caracteristica.plazo !== '') {
      const year = this.fechaSuscripcion.year;
      const month = this.fechaSuscripcion.month;
      const day = this.fechaSuscripcion.day;
      const addDay = new Date(year, month, day);
      const fixedDate = new Date();
      fixedDate.setDate(addDay.getDate() + parseInt(this.caracteristica.plazo, 10));
      this.fechaVencimiento = { year: fixedDate.getFullYear(), month: fixedDate.getMonth() + 1, day: fixedDate.getDate() };
    } else {
      this.fechaVencimiento = null;
    }
  }
  ngOnInit() {
    this.page.pageNumber = 0;
    this.page.size = 5;
    this.labels = TAGS;
    this.gv = GV;
    this.viewServices = true;
    this.editSave = GV.editSave.save;
    this.appService.pageTitle = this.labels.characteristics.subtitle1;
    this.formCharacteristic = this.formBuilder.group({
      code: [null, [Validators.required, Validators.minLength(3)]],
      plaz: [null, [Validators.required, Validators.minLength(1)]],
      //inter: [null, [Validators.required, Validators.minLength(1)]],
      //numh: [null, [Validators.required, Validators.minLength(1)]],
      // mont: [null, [Validators.required, Validators.minLength(1), Validators.pattern('^\\d+(\\.\\d{1,2})?$')]],
      dec: [null, [Validators.required, Validators.minLength(1), Validators.max(100), Validators.pattern('^\\d+(\\.\\d{1,2})?$')]],
      //dec1: [null, [Validators.required, Validators.minLength(1), Validators.max(100), Validators.pattern('^\\d+(\\.\\d{1,2})?$')]],
      //dec2: [null, [Validators.required, Validators.minLength(1), Validators.pattern('^\\d+(\\.\\d{1,2})?$')]],
      //dec3: [null, [Validators.required, Validators.minLength(1), Validators.pattern('^\\d+(\\.\\d{1,2})?$')]],
      fecsus: [null, [Validators.required, Validators.minLength(10)]],
      fecven: [null, [Validators.required, Validators.minLength(10)]]
    });
    const array = [
      WS.characteristics.typeReports, WS.characteristics.facPeriod, WS.characteristics.facPrint,
      WS.characteristics.registerForm, WS.characteristics.typeFact, WS.general.status, WS.general.afirmation,
      WS.findSubCatByCatId.replace('${catId}', GV.catalogTipoControlPresup + '')
    ];
    this.functionService.httpObservableGet(array).then((responseList) => {
      this.tiposReportes = responseList[0].content;
      this.periodosFacturacion = responseList[1].content;
      this.facturasImpresas = responseList[2].content;
      this.formasRegistro = responseList[3].content;
      this.tiposFacturacion = responseList[4].content;
      this.estados = responseList[5].content;
      this.afirmaciones = responseList[6].content;
      this.responseList = responseList;
      this.defineSwitchInputs();
      this.functionService.getIpAddress().then((res) => {
        this.ipAddress = res;
      });

      this._empresaControllerService.getEmpresaListController()
        .then((empresasList) => {
          this.empresas = empresasList;
          this.clear();
        });
    });
  }
  get fc() { return this.formCharacteristic.controls; }
}
