import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactoFastlineComponent } from './contacto-fastline.component';

describe('ContactoFastlineComponent', () => {
  let component: ContactoFastlineComponent;
  let fixture: ComponentFixture<ContactoFastlineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactoFastlineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactoFastlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
