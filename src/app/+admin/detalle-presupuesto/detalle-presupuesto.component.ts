import { Component, OnInit, ComponentRef } from '@angular/core';
import { TAGS } from '../../TextLabels';
import { FunctionService } from '../../services/functions/function.service';
import { WS } from '../../WebServices';
import { Page } from '../../components/pagination/page';
import { DecimalManager } from '../../tools/decimal-manager';
import { AccordionInterface } from '../../fr-accordion/interfaces/accordion-interface';
import { Empresa } from '../../models/empresa';
import { Sucursal } from '../../models/sucursal';
import { RestClientService } from '../../services/component-services/rest-client.service';

@Component({
  selector: 'app-detalle-presupuesto',
  templateUrl: './detalle-presupuesto.component.html',
  styleUrls: [
    './detalle-presupuesto.component.scss'
  ]
})
export class DetallePresupuestoComponent implements OnInit {
  private _dynamicData: { yearList: string[], enterpriseId: number, empresaList: Empresa[], sucursalId: string, sucursalList: Sucursal[], convenioId: number };

  public _selfReference: ComponentRef<DetallePresupuestoComponent>;
  public labels: any
  public detalleGenList: any[];
  public filterDetalleGenList: any[];
  public page: Page;
  public loadingIndicator: boolean;
  public totals: any;
  public depaWithChildList: AccordionInterface[];

  public filterYear: string;
  public enterpriseId: number;
  public sucursalId: string;
  public convenioId: number;
  private departamentoId: string;

  public yearList: string[];
  public empresaList: Empresa[];
  public sucursalList: Sucursal[];

  constructor(
    private _functionService: FunctionService,
    private _restClientService: RestClientService
  ) {
    this.labels = TAGS;
    this.page = new Page();
    this.loadingIndicator = true;
    this.initTotals();
  }

  ngOnInit() {
    this.empresaList = this._dynamicData.empresaList;
    this.sucursalList = this._dynamicData.sucursalList;
    this.yearList = this._dynamicData.yearList;
    this.enterpriseId = this._dynamicData.enterpriseId;
    this.sucursalId = this._dynamicData.sucursalId;
    this.filterYear = this._dynamicData.yearList[0];
    this.convenioId = this._dynamicData.convenioId;

    this.loadDepaHierarchy();
  }

  /**
   * METODO PARA OBTENER EL ID DEL DEPARTAMENTO SELECCIONADO DESDE LA JERARQUIA
   * AUTOR: FREDI ROMAN
   * @param event
   */
  public getFocusedDepa(event: string) {
    if (this.departamentoId != event) {
      this.departamentoId = event;
      this.loadGeneralDetail();
    }
  }

  /**
   * METODO PARA MANIPULAR EL EVENTO ON CHANGE DEL INPUT, PARA PODER REALIZAR UN FILTRO EN LA TABLA
   * DETALLE GENERAL DE PRESUPUESTOS
   * AUTOR: FREDI ROMAN
   */
  public onChangeYearFilter() {
    this.loadGeneralDetail();
  }

  /**
   * METODO PARA RENOVAR LA LISTA DE LOS PRESUPUESTOS DADO EL CAMBIO DE SUCURSAL
   * AUTOR: FREDI ROMAN
   */
  public onSucursalChange() {
    this.loadGeneralDetail();
  }

  /**
   * METODO PARA ACTUALIZAR LA JERARQUIA DEPARTAMENTAL AL CAMBIAR LA EMPRESA
   * AUTOR: FREDI ROMAN
   */
  public onEnterpriseChange() {
    this.loadDepaHierarchy();
  }

  /**
   * METODO PARA CARGAR EL DETALLE GENERAL DE PRESUPUESTOS DADO UN AÑO DETERMINADO
   * AUTOR: FREDI ROMAN
   */
  private loadGeneralDetail() {
    this.loadingIndicator = true;
    let url = WS.presupuesto.getGeneralDetail;
    let filter: any = {
      idEmpresa: this.enterpriseId,
      idConvenio: this._dynamicData.convenioId,
      anio: this.filterYear,
      idSucursal: parseInt(this.sucursalId),
      idDepartamento: this.departamentoId
    };

    filter = JSON.stringify(filter);
    this._restClientService.httpGetWithHeaders(url, { key: 'filter', value: filter }, false)
      .then((responseData: any) => {
        let totalRow;
        this.detalleGenList = [];
        this.initTotals();
        for (let json of responseData) {
          json.valor_ene = json.valor_ene || '0.00';
          json.valor_feb = json.valor_feb || '0.00';
          json.valor_mar = json.valor_mar || '0.00';
          json.valor_abr = json.valor_abr || '0.00';
          json.valor_may = json.valor_may || '0.00';
          json.valor_jun = json.valor_jun || '0.00';
          json.valor_jul = json.valor_jul || '0.00';
          json.valor_ago = json.valor_ago || '0.00';
          json.valor_sep = json.valor_sep || '0.00';
          json.valor_oct = json.valor_oct || '0.00';
          json.valor_nov = json.valor_nov || '0.00';
          json.valor_dic = json.valor_dic || '0.00';
          totalRow = this.defineTotalRow([json.valor_ene, json.valor_feb, json.valor_mar, json.valor_abr, json.valor_may, json.valor_jun, json.valor_jul, json.valor_ago, json.valor_sep, json.valor_oct, json.valor_nov, json.valor_dic]);
          Object.defineProperty(json, 'valor_total', { value: totalRow, writable: false });
          this.detalleGenList.push(json);
          this.defineTotals(json);
        }
        this.filterDetalleGenList = this.detalleGenList.slice();
        this.page.totalElements = this.filterDetalleGenList.length;
        setTimeout(() => {
          this.loadingIndicator = false;
        }, 500);
      });
  }

  /**
   * METODO PARA CALCULAR EL TOTAL POR FILAS
   * AUTOR: FREDI ROMAN
   */
  private defineTotalRow(row: any[]) {
    let total = 0;
    for (let value of row) {
      total += parseFloat(value);
    }
    return DecimalManager.precise_round(total, 2);
  }

  /**
   * METODO PARA DEFINIR LAS CANTIDADES TOTALES DEL DETALLE GENERAL
   * AUTOR: FREDI ROMAN
   * @param row 
   */
  private defineTotals(row: any) {
    this.totals.ene = DecimalManager.precise_round(parseFloat(this.totals.ene) + parseFloat(row.valor_ene), 2);
    this.totals.feb = DecimalManager.precise_round(parseFloat(this.totals.feb) + parseFloat(row.valor_feb), 2);
    this.totals.mar = DecimalManager.precise_round(parseFloat(this.totals.mar) + parseFloat(row.valor_mar), 2);
    this.totals.abr = DecimalManager.precise_round(parseFloat(this.totals.abr) + parseFloat(row.valor_abr), 2);
    this.totals.may = DecimalManager.precise_round(parseFloat(this.totals.may) + parseFloat(row.valor_may), 2);
    this.totals.jun = DecimalManager.precise_round(parseFloat(this.totals.jun) + parseFloat(row.valor_jun), 2);
    this.totals.jul = DecimalManager.precise_round(parseFloat(this.totals.jul) + parseFloat(row.valor_jul), 2);
    this.totals.ago = DecimalManager.precise_round(parseFloat(this.totals.ago) + parseFloat(row.valor_ago), 2);
    this.totals.sep = DecimalManager.precise_round(parseFloat(this.totals.sep) + parseFloat(row.valor_sep), 2);
    this.totals.oct = DecimalManager.precise_round(parseFloat(this.totals.oct) + parseFloat(row.valor_oct), 2);
    this.totals.nov = DecimalManager.precise_round(parseFloat(this.totals.nov) + parseFloat(row.valor_nov), 2);
    this.totals.dic = DecimalManager.precise_round(parseFloat(this.totals.dic) + parseFloat(row.valor_dic), 2);
    this.totals.total = DecimalManager.precise_round(parseFloat(this.totals.total) + parseFloat(row.valor_total), 2);
  }

  /**
   * METODO PARA INICIALIZAR EL OBJETO DE LOS TOTALES
   * AUTOR: FREDI ROMAN
   */
  private initTotals() {
    this.totals = { ene: '0.00', feb: '0.00', mar: '0.00', abr: '0.00', may: '0.00', jun: '0.00', jul: '0.00', ago: '0.00', sep: '0.00', oct: '0.00', nov: '0.00', dic: '0.00', total: '0.00' };
  }

  /**
   * METODO PARA CARGAR LA LISTA DE ITEMS DE LA JERARQUIA DEPARTAMENTAL
   * AUTOR: FREDI ROMAN
   */
  private loadDepaHierarchy() {
    let url = WS.jerarquiaDepa.replace('${enterId}', this.enterpriseId + '');
    this._functionService.httpGetWithout(url)
      .then((respData: any) => {
        this.depaWithChildList = []
        for (let data of respData) {
          this.depaWithChildList.push({ id: data.id, nombre: data.nombre, subItems: data.subItems });
        }
      });
  }
}