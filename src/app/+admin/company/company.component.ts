import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import { TAGS } from '../../TextLabels';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FunctionService} from '../../services/functions/function.service';
import { WS } from '../../WebServices';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {Page} from '../../components/pagination/page';
import {GV} from '../../GeneralVars';
import {ConfirmationDialogService} from '../../components/confirmation-dialog/confirmation-dialog.service';
import {ToastrService} from 'ngx-toastr';
import {AppService} from '../../app.service';
import * as textMaskAddons from 'text-mask-addons/dist/textMaskAddons';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: '[tables-ngx-datatable]',
  templateUrl: './company.component.html',
  styleUrls: [
    '../../../../node_modules/@swimlane/ngx-datatable/release/index.css',
    '../../../vendor/libs/ngx-datatable/ngx-datatable.scss',
    '../../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css',
    '../../../vendor/libs/ngx-toastr/ngx-toastr.scss'
  ],
  encapsulation: ViewEncapsulation.None
})
export class CompanyComponent implements OnInit {
  /*****************************************************************************************
  ******************************************************************************************
   * AUTOR: FREDI ROMAN
  ******************************************************************************************
  *****************************************************************************************/
  public navBtnList: { url: string; selected: boolean; label: string }[];
  /*****************************************************************************************
  *****************************************************************************************/

  modalRef = null;
  @ViewChild(DatatableComponent) table: DatatableComponent;
  loadingIndicator = true;
  saveSucursal = false;
  changeStateVal: Boolean = true;
  inactiveState: Boolean = false;
  banDesactivar: Boolean = false;
  labels = null;
  formCompany: FormGroup;
  formSucursal: FormGroup;
  responseList = null;
  ipAddress = null;
  submitedCompany: Boolean = null;
  submitedSucursales: Boolean = null;
  enableState: Boolean = true;
  viewEdit: Boolean = false;
  temp = null;
  rows = null;
  gv = null;
  dataSesion = null;
  viewServices = null;
  timeout = null;
  arrayCalle1 = {};
  arrayCalle2 = {};
  calificaciones = [];
  afirmaciones = [];
  motivos = [];
  estados = [];
  tipoempresas = [];
  ciudades = [];
  sectores = [];
  selected = [];
  citySend = GV.ciudadesFastline.quito;
  page = new Page();
  options_notitications = {
    tapToDismiss: true,
    closeButton: false,
    progressBar: true,
    positionClass: 'toast-top-right',
    rtl: this.appService.isRTL
  };
  emailMaskOptions = {
    mask: textMaskAddons.emailMask
  };
  sucursal = {
    'id': '',
    'codAlterno': '',
    'codSucursal': '',
    'nomSucursal': '',
    'codCiudad': '',
    'direccion': '',
    'telefono': '',
    'fax': '',
    'correo': '',
    'codSector': '',
    'codZona': '',
    'nomCalle1': '',
    'nomCalle2': '',
    'numeroCalle': '',
    'estado': '',
    'codusu': '',
    'fecult': '',
    'status': '',
    'ipMaquina': '',
    'SGE_Empresa_idEmpresa': '',
    'empresa': null
  };
  empresa = {
    'id': '',
    'ruc': '',
    'calificacion': '',
    'vip': '',
    'nombreEmpresa': '',
    'nombreComercial': '',
    'representanteLegal': '',
    'tipoEmpresa': '',
    'ciudad': '',
    'direccion': '',
    'telefono': '',
    'fax': '',
    'correo': '',
    'sector': '',
    'motivoDeshabilitacion': 0,
    'obsDeshabilitacion': '',
    'estado': '',
    'codusu': '',
    'fecult': null,
    'status': '',
    'ipMaquina': ''
  };

  constructor(
    private functionService: FunctionService,
    private formBuilder: FormBuilder,
    private confirmationDialogService: ConfirmationDialogService,
    public toastrService: ToastrService,
    private appService: AppService,
    private modalService: NgbModal,
    private _router: Router
  ) {
    this.navBtnList = [
      { url: '/admin/company', selected: true, label: 'Empresa' },
      { url: '/admin/sucursales', selected: false, label: 'Sucursal' },
      { url: '/admin/feriado', selected: false, label: 'Feriados' },
      { url: '/admin/definiciones', selected: false, label: 'Definiciones' }
    ];
  }

  clear() {
    this.sucursal = {
      'id': '',
      'codAlterno': '',
      'codSucursal': '',
      'nomSucursal': '',
      'codCiudad': '',
      'direccion': '',
      'telefono': '',
      'fax': '',
      'correo': '',
      'codSector': '',
      'codZona': '',
      'nomCalle1': '',
      'nomCalle2': '',
      'numeroCalle': '',
      'estado': '',
      'codusu': '',
      'fecult': '',
      'status': '',
      'ipMaquina': '',
      'SGE_Empresa_idEmpresa': '',
      'empresa': null
    };
    this.empresa = {
      'id': '',
      'ruc': '',
      'calificacion': '',
      'vip': '',
      'nombreEmpresa': '',
      'nombreComercial': '',
      'representanteLegal': '',
      'tipoEmpresa': '',
      'ciudad': '',
      'direccion': '',
      'telefono': '',
      'fax': '',
      'correo': '',
      'sector': '',
      'motivoDeshabilitacion': 0,
      'obsDeshabilitacion': '',
      'estado': '',
      'codusu': '',
      'fecult': null,
      'status': '',
      'ipMaquina': ''
    };
    this.clearSucursal();
    this.banDesactivar = false;
    this.submitedCompany = false;
    this.submitedSucursales = false;
    this.viewEdit = true;
    this.setDataPagination(0, 5);
    this.formCompany.controls.iden.setValue('');
    this.formCompany.controls.full.setValue('');
    this.formCompany.controls.ncor.setValue('');
    this.formCompany.controls.rep.setValue('');
    this.formCompany.controls.dir.setValue('');
    this.formCompany.controls.mail.setValue('');
    this.formCompany.controls.tlf.setValue('');
    this.formSucursal.controls.cds.setValue('');
    this.formSucursal.controls.cdn.setValue('');
    this.formSucursal.controls.dsp.setValue('');
    this.formSucursal.controls.str1.setValue('');
    this.formSucursal.controls.str2.setValue('');
    this.formSucursal.controls.nstr.setValue('');
    this.formSucursal.controls.tlf.setValue('');
    this.empresa.calificacion = this.responseList[0].content[0].id;
    this.empresa.vip = this.responseList[1].content[0].id;
    this.empresa.estado = this.responseList[2].content[0].id;
    this.empresa.motivoDeshabilitacion = this.responseList[3].content[0].id;
    this.empresa.tipoEmpresa = this.responseList[4].content[0].id;
    this.empresa.ciudad = this.responseList[5].content[0].id;
    this.empresa.sector = this.responseList[6].content[0].id;
    this.changeStateVal = false;
    this.enableState = true;
    this.inactiveState = false;
    this.viewServices = true;
  }

  open(content) {
    // this.clearSucursal();
    this.modalRef = this.modalService.open(content);
  }

  closeModal() {
    // this.clearSucursal();
    this.modalRef.close();
  }

  saveModal() {
    let url = WS.sucursales.getCodSucursal;
    url = url.replace('${codigosucursal}', this.sucursal.codSucursal);
    this.functionService.httpGetWithout(url).then((response) => {
      if (!response) {
        if (this.formSucursal.valid) {
          this.submitedSucursales = false;
          this.saveSucursal = true;
          this.modalRef.close();
        } else {
          this.submitedSucursales = true;
        }
      } else {
        this.toastrService['error'](this.labels.messages.duplicateCodSuc, this.appService.pageTitle, this.options_notitications);
      }
    });
  }

  clearSucursal() {
    this.sucursal = {
      'id': '',
      'codAlterno': '',
      'codSucursal': '',
      'nomSucursal': '',
      'codCiudad': '',
      'direccion': '',
      'telefono': '',
      'fax': '',
      'correo': '',
      'codSector': '',
      'codZona': '',
      'nomCalle1': '',
      'nomCalle2': '',
      'numeroCalle': '',
      'estado': '',
      'codusu': '',
      'fecult': '',
      'status': '',
      'ipMaquina': '',
      'SGE_Empresa_idEmpresa': '',
      'empresa': null
    };
    this.formSucursal.controls.cds.setValue('');
    this.formSucursal.controls.cdn.setValue('');
    this.formSucursal.controls.dsp.setValue('');
    this.formSucursal.controls.str1.setValue('');
    this.formSucursal.controls.str2.setValue('');
    this.formSucursal.controls.nstr.setValue('');
    this.formSucursal.controls.tlf.setValue('');
    this.sucursal.codCiudad = this.responseList[5].content[0].id;
    this.sucursal.codSector = this.responseList[6].content[0].id;
    this.submitedSucursales = false;
    this.saveSucursal = false;
  }

  cancel() {
    this.clearSucursal();
    // this.modalRef.close();
  }

  changeCity(event) {
    this.sucursal.nomCalle1 = '';
    this.sucursal.nomCalle2 = '';
    this.sucursal.codZona = '';
    console.log( event.target.value.toLowerCase());
    if (event.target.value.toString() === '13') {
      this.citySend = GV.ciudadesFastline.guayaquil;
    } else {
      this.citySend = GV.ciudadesFastline.quito;
    }
  }

  save() {
    let url = WS.empresa.getEmpresaRuc;
    url = url.replace('${ruc}', this.empresa.ruc);
    this.functionService.httpGet(url).then((response) => {
      if (!response) {
        if (this.formCompany.valid) {
          if (!this.functionService.validarRuc(this.empresa.ruc)) {
            this.toastrService['error'](this.labels.messages.invalidRuc, this.appService.pageTitle, this.options_notitications);
            return;
          }
          this.submitedCompany = false;
          this.empresa.fecult = this.functionService.getToday();
          this.sucursal.fecult = this.functionService.getToday();
          this.empresa.ipMaquina = this.ipAddress;
          this.sucursal.ipMaquina = this.ipAddress;
          this.empresa.codusu = this.dataSesion['user_id'];;
          this.sucursal.codusu = this.dataSesion['user_id'];
          this.empresa.status = '1';
          this.sucursal.status = '1';
          this.sucursal.estado = GV.state.active;
          this.sucursal.empresa = this.empresa;
          if (this.viewEdit) {
            if (!this.saveSucursal) {
              this.toastrService['error'](this.labels.messages.addSucursal, this.appService.pageTitle, this.options_notitications);
              return;
            } else {
              const empresaCtx = {
                empresa: this.empresa,
                sucursal: this.sucursal
              };
              this.functionService.httpPost(WS.empresa.save, empresaCtx).then((res) => {
                this.clear();
                setTimeout(() => {
                  this._router.navigate(['admin/sucursales']);
                }, 2000);
              }).catch(err =>
                console.log(err)
              );
            }
          } else {
            let url = WS.empresa.update;
            if (this.empresa.estado.toString() === GV.state.inactive && this.changeStateVal) {
              url = url.replace('${empresaId}', this.empresa.id);
              this.functionService.httpPut(url, this.empresa).then(() => {
                this.clear();
              });
            } else {
              url = url.replace('${empresaId}', this.empresa.id);
              this.functionService.httpPut(url, this.empresa).then(() => {
                this.clear();
              });
            }
          }
        } else {
          this.submitedCompany = true;
        }
      } else {
        this.toastrService['error'](this.labels.messages.duplicateRuc, this.appService.pageTitle, this.options_notitications);
      }
    });
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function(d) {
      return d.nombreEmpresa.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows = temp;
    this.table.offset = 0;
  }

  loadData() {
    this.page.pageNumber = 0;
    this.setDataPagination(0, this.page.size);
  }

  setDataPagination(pageNumber, size) {
    this.page.pageNumber = pageNumber;
    this.page.size = size;
    let url = WS.empresa.fetch;
    url = url.replace('${pageNumber}', pageNumber);
    url = url.replace('${size}', size);
    this.functionService.fetch(url).then((res) => {
      this.temp = res['content'];
      this.rows = res['content'];
      this.page.totalElements = res['totalElements'];
      this.loadingIndicator = false;
    });
  }

  confirmMethod() {
    if (this.changeStateVal && this.empresa.estado.toString() === GV.state.inactive) {
      this.openConfirmationDialog();
    } else {
      this.save();
    }
  }

  onBlurStreet() {
    if (this.sucursal.nomCalle1.toString() !== '' && this.sucursal.nomCalle2.toString() !== '') {
      let url1 = WS.geo.crossStreet;
      url1 = url1.replace('${street1}', this.sucursal.nomCalle1);
      url1 = url1.replace('${street2}', this.sucursal.nomCalle2);
      url1 = url1.replace('${ciudad}', this.citySend);
      if (!url1.toString().includes('undefined')) {
        this.functionService.getService(url1).then((org) => {
          if (org[0]) {
            this.sucursal.codZona = org[0]['zona'];
          } else {
            this.toastrService['error'](this.labels.messages.errorCruce, this.appService.pageTitle, this.options_notitications);
            this.sucursal.codZona = '';
          }
        });
      }
    }
  }

  keyUpOrSt1() {
    this.sucursal.direccion = this.sucursal.nomCalle1.toUpperCase() + ' Y ' + this.sucursal.nomCalle2.toUpperCase();
    this.sucursal.nomCalle1 = this.sucursal.nomCalle1.toUpperCase();
    if (this.sucursal.nomCalle1.toString() !== '' && this.sucursal.nomCalle1.toString() !== ' ') {
      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        let url = WS.geo.findStreet;
        if (this.sucursal.nomCalle1.toString() !== '') {
          url = url.replace('${street}', this.sucursal.nomCalle1);
          url = url.replace('${ciudad}', this.citySend);
          this.functionService.getService(url).then((responseList) => {
            this.arrayCalle1 = responseList;
          });
        }
      }, 500, 'JavaScript');
    } else {
      this.sucursal.nomCalle1 = '';
    }
  }

  keyUpOrSt2() {
    this.sucursal.direccion = this.sucursal.nomCalle1.toUpperCase() + ' Y ' + this.sucursal.nomCalle2.toUpperCase();
    this.sucursal.nomCalle2 = this.sucursal.nomCalle2.toUpperCase();
    if (this.sucursal.nomCalle2.toString() !== '' && this.sucursal.nomCalle2.toString() !== ' ') {
      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        let url = WS.geo.findStreet;
        if (this.sucursal.nomCalle2.toString() !== '') {
          url = url.replace('${street}', this.sucursal.nomCalle2);
          url = url.replace('${ciudad}', this.citySend);
          this.functionService.getService(url).then((responseList) => {
            this.arrayCalle2 = responseList;
          });
        }
      }, 500, 'JavaScript');
    } else {
      this.sucursal.nomCalle2 = '';
    }
  }

  changeState() {
    this.changeStateVal = true;
    if (this.empresa.estado.toString() === GV.state.inactive.toString()) {
      this.banDesactivar = true;
    } else {
      this.banDesactivar = false;
    }
  }

  onSelect({ selected }) {
    this.enableState = false;
    this.viewEdit = false;
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    this.empresa = this.selected[0];
    this.empresa.motivoDeshabilitacion = parseInt(this.selected[0].motivoDeshabilitacion, 10);
    this.inactiveState = this.empresa.estado.toString() === GV.state.inactive ? true : false;
    this.viewServices = false;
  }

  openConfirmationDialog() {
    const text = this.labels.messages.inactivate + ' ' + this.labels.company.companyName + '.  ' + this.labels.messages.inactivateConfirm;
    this.confirmationDialogService.confirm(this.labels.general.confirm.toLocaleUpperCase(), text)
      .then((confirmed) => {
        if (confirmed) {
          this.save();
        }
      }).catch(() =>
      console.log('Dismissed.)'
      ));
  }

  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    this.setDataPagination(pageInfo.offset, pageInfo.pageSize);
  }

  ngOnInit() {
    this.citySend = GV.ciudadesFastline.quito;
    this.dataSesion = localStorage.getItem('userToken');
    this.dataSesion = this.functionService.decode(this.dataSesion);
    this.labels = TAGS;
    this.gv = GV;
    this.viewEdit = true;
    this.saveSucursal = false;
    this.viewServices = true;
    this.banDesactivar = false;
    this.formSucursal = this.formBuilder.group({
      cds: [null, [Validators.required, Validators.minLength(3)]],
      cdn: [null, [Validators.required]],
      dsp: [null, [Validators.required]],
      str1: [null, [Validators.required]],
      str2: [null, [Validators.required]],
      nstr: [null, [Validators.required]],
      tlf: [null, [Validators.required]]
    });
    this.formCompany = this.formBuilder.group({
      iden: [null, [Validators.required, Validators.minLength(5)]],
      full: [null, [Validators.required, Validators.minLength(3)]],
      ncor: [null, [Validators.required, Validators.minLength(3)]],
      rep: [null, [Validators.required, Validators.minLength(3)]],
      dir: [null, [Validators.required, Validators.minLength(3)]],
      mail: [null, [Validators.required, Validators.minLength(3)]],
      tlf: [null, [Validators.required, Validators.minLength(3)]]
    });
    const array = [
      WS.empresa.qualifications, WS.general.afirmation, WS.general.status, WS.empresa.motives, WS.empresa.companyTypes,
      WS.general.cities, WS.empresa.sectors
    ];
    this.functionService.httpObservableGet(array).then((responseList) => {
      this.calificaciones = responseList[0].content;
      this.afirmaciones = responseList[1].content;
      this.estados = responseList[2].content;
      this.motivos = responseList[3].content;
      this.tipoempresas = responseList[4].content;
      this.ciudades = responseList[5].content;
      this.sectores = responseList[6].content;
      this.responseList = responseList;
      this.functionService.getIpAddress().then((res) => {
        this.ipAddress = res;
      });
      this.clear();
    });
  }

  get fc() { return this.formCompany.controls; }
  get fd() { return this.formSucursal.controls; }
}
