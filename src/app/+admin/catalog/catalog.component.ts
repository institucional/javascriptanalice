import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import { AppService } from '../../app.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TAGS } from '../../TextLabels';
import { WS } from '../../WebServices';
import { GV } from '../../GeneralVars';
import { FunctionService } from '../../services/functions/function.service';
import {Page} from '../../components/pagination/page';
@Component({
  selector: 'tables-ngx-datatable',
  templateUrl: './catalog.component.html',
  styleUrls: [
    '../../../../node_modules/@swimlane/ngx-datatable/release/index.css',
    '../../../vendor/libs/ngx-datatable/ngx-datatable.scss',
    '../../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css',
    '../../../vendor/libs/ngx-toastr/ngx-toastr.scss'
  ],
  encapsulation: ViewEncapsulation.None
})
export class CatalogComponent {
  @ViewChild(DatatableComponent) table: DatatableComponent;
  page = new Page();
  formCatalog: FormGroup;
  submitedCatalog: Boolean = false;
  loadingIndicator = true;
  rows = null;
  temp = null;
  selected = [];
  catalogo = {
    'id': '',
    'codCatalogo': '',
    'nomCatalogo': '',
    'descripcion': '',
    'tipoTabla': '',
    'estado': '',
    'codusu': '1',
    'fecult': '',
    'status': '1',
    'ipMaquina' : ''
  };
  labels = null;
  gv = null;
  editSave: string;
  ipAddress = null;
  estados = [];
  tiposCat = [];
  responseList = null;
  constructor(
    private appService: AppService,
    private formBuilder: FormBuilder,
    private functionService: FunctionService
    ) {}
  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function(d) {
      return d.nomCatalogo.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows = temp;
    this.table.offset = 0;
  }
  onSelect({ selected }) {
    this.editSave = GV.editSave.edit;
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    this.catalogo = this.selected[0];
    this.catalogo.tipoTabla = this.catalogo.tipoTabla.toString();
    this.formCatalog.controls.code.setValue(this.catalogo.codCatalogo);
    this.formCatalog.controls.name.setValue(this.catalogo.nomCatalogo);
    this.formCatalog.controls.desc.setValue(this.catalogo.descripcion);
  }
  onKey(event: any, param: string) { // without type info
    this.catalogo[param] = event.target.value;
    this.catalogo.nomCatalogo = this.catalogo.nomCatalogo.toUpperCase();
    this.catalogo.descripcion = this.catalogo.descripcion.toUpperCase();
  }
  loadData() {
    this.page.pageNumber = 0;
    this.setDataPagination(0, this.page.size);
  }
  save() {
    this.catalogo.ipMaquina = this.ipAddress;
    this.catalogo.fecult = this.functionService.getToday();
    if (this.editSave === GV.editSave.save) {
      if (this.formCatalog.valid) {
        this.submitedCatalog = false;
        this.functionService.httpPost(WS.catalog.save, this.catalogo).then(() => {
          this.clear();
        });
      } else {
        this.submitedCatalog = true;
      }
    } else {
      let url = WS.catalog.update;
      url = url.replace('${idCatalogo}', this.selected[0].id);
      this.functionService.httpPut( url, this.catalogo).then(() => {
        this.clear();
      });
    }
  }
  clear() {
    this.catalogo = {
      'id': '',
      'codCatalogo': '',
      'nomCatalogo': '',
      'descripcion': '',
      'tipoTabla': '',
      'estado': '',
      'codusu': '1',
      'fecult': '',
      'status': '1',
      'ipMaquina' : ''
    };
    this.catalogo.estado = this.responseList[0].content[0].id;
    this.catalogo.tipoTabla = this.responseList[1].content[1].id;
    this.formCatalog.controls.code.setValue('');
    this.formCatalog.controls.name.setValue('');
    this.formCatalog.controls.desc.setValue('');
    this.submitedCatalog = false;
    this.editSave = GV.editSave.save;
    this.loadingIndicator = true;
    this.setDataPagination(0, 5);
  }
  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    this.setDataPagination(pageInfo.offset, pageInfo.pageSize);
  }
  setDataPagination(pageNumber, size) {
    this.page.pageNumber = pageNumber;
    this.page.size = size;
    let url = WS.catalog.fetch;
    url = url.replace('${pageNumber}', pageNumber);
    url = url.replace('${size}', size);
    this.functionService.fetch(url).then((res) => {
      this.temp = res['content'];
      this.rows = res['content'];
      this.page.totalElements = res['totalElements'];
      this.loadingIndicator = false;
    });
  }
  ngOnInit() {
    this.page.pageNumber = 0;
    this.page.size = 5;
    this.labels = TAGS;
    this.gv = GV;
    this.appService.pageTitle = this.labels.catalog.title;
    this.formCatalog = this.formBuilder.group({
      code: [null, [Validators.required, Validators.minLength(3)]],
      name: [null, [Validators.required, Validators.minLength(5)]],
      desc: [null, [Validators.required, Validators.minLength(5)]],
    });
    const array = [WS.general.status, WS.catalog.type];
    this.functionService.httpObservableGet(array).then((responseList) => {
      this.estados = responseList[0].content;
      this.tiposCat = responseList[1].content;
      this.responseList = responseList;
      this.functionService.getIpAddress().then((res) => {
        this.ipAddress = res;
      });
      this.clear();
    });
  }
  get fc() { return this.formCatalog.controls; }
}
