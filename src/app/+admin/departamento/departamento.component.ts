/**
 * AUTOR: FREDI ROMAN
 */
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { FunctionService } from '../../services/functions/function.service';
import { Page } from '../../components/pagination/page';
import { TAGS } from '../../TextLabels';
import { GV } from '../../GeneralVars';
import { Empresa } from '../../models/empresa';
import { EmpresaControllerService } from '../../services/controllers/empresa-controller.service';
import { EmpresaService } from '../../services/component-services/empresa.service';
import { WS } from '../../WebServices';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgForm } from '@angular/forms';
import { Nivel } from '../../models/nivel';
import { NivelService } from '../../services/component-services/nivel.service';
import { Departamento } from '../../models/departamento';
import { DepartamentoService } from '../../services/component-services/departamento.service';
import { ConfirmationDialogService } from '../../components/confirmation-dialog/confirmation-dialog.service';

@Component({
  selector: 'app-departamento',
  templateUrl: './departamento.component.html',
  styleUrls: [
    './departamento.component.scss',
    '../../../../node_modules/@swimlane/ngx-datatable/release/index.css',
    '../../../vendor/libs/ngx-datatable/ngx-datatable.scss',
    '../../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css',
    '../../../vendor/libs/ngx-toastr/ngx-toastr.scss'
  ],
  encapsulation: ViewEncapsulation.None
})
export class DepartamentoComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild("frmDepart") frmDepart: NgForm;
  private ipAddress: string;
  private currentNivelIndex: number;

  public navBtnList: { url: string; selected: boolean; label: string }[];
  public empresaList: Empresa[];
  public mainEnterpriseId: number;
  public nivelesList: Nivel[];
  public parentDepartList: Departamento[];
  public departamento: Departamento;
  public departamentoList: Departamento[];
  public filterDepartamentoList: Departamento[];
  public page: Page;
  public loadingIndicator: boolean;
  public labels: any;
  public gv: any;
  public saveEdit: boolean;
  public depaName: string;
  public showParentCol: boolean;

  constructor(
    private _functionService: FunctionService,
    private _empresaControllerService: EmpresaControllerService,
    private _empresaService: EmpresaService,
    private _nivelService: NivelService,
    private _departamentoService: DepartamentoService,
    private _confirmationDialogService: ConfirmationDialogService
  ) {
    this.navBtnList = [
      { url: '/admin/departamentos', selected: true, label: 'Departamentos' },
      { url: '/admin/cargo', selected: false, label: 'Cargos' },
      { url: '/admin/employees', selected: false, label: 'Empleados' }
    ];

    this.currentNivelIndex = 0;
    this.departamento = new Departamento();
    this.departamento.active = true;
    this.page = new Page();
    this.page.size = 5;
    this.labels = TAGS;
    this.gv = GV;
    this.saveEdit = true;
    this.loadingIndicator = true;
  }

  ngOnInit() {
    this.loadInitData();
  }

  /**
   * METODO PARA RECIBIR UN REGISTRO QUE HAYA SIDO INGRESADO A TRAVES DEL COMPONENTE NIVEL
   * AUTOR: FREDI ROMAN
   * @param $event 
   */
  public onAddNivel(event: Nivel) {
    let nivelAux = this.nivelesList.findIndex(niv => niv.id == event.id);
    if (nivelAux == -1) {
      this.nivelesList.push(event);
    }
  }

  /**
   * METODO PARA CAPTURAR EL EVENTO CHANGE DEL SELECT DE EMPRESA PARA RECARGAR LOS DATOS DE LA TABLA
   * AUTOR: FREDI ROMAN
   */
  public changeEnterprise() {
    this.loadNivelesData();
  }

  /**
   * METODO PARA PROCESAR EL SUBMIT DEL FORMULARIO DE REGISTRO Y ACTUALIZACION:
   * AUTOR: FREDI ROMAN
   */
  public onSubmit() {
    this.departamento.active = JSON.parse(this.departamento.active + "");
    if (!this.saveEdit && !this.departamento.active) {
      const text = TAGS.messages.genericInactivate;
      this._confirmationDialogService.confirm(this.labels.general.confirm.toLocaleUpperCase(), text)
        .then((confirmed) => {
          if (confirmed) { this.saveData(); }
        });
    }
    else {
      this.saveData();
    }
  }

  /**
   * METODO PARA INSERTAR O ACTUALIZAR UN REGISTRO
   * AUTOR: FREDI ROMAN
   */
  public saveData() {
    let url;
    this.departamento.ipMaquina = this.ipAddress;
    if (this.currentNivelIndex == 0) {
      this.departamento.departamentoPadre = null;
    }

    if (this.saveEdit) {
      url = WS.departamento.post.replace("${nivelId}", this.departamento.nivel.id + "");
      this.departamento.status = 1;
      this._functionService.httpPost(url, this.departamento).then((nivelData) => {
        this.setPaginatedTableData(0, this.page.size);
        this.resetForm();
      });
    }
    else {
      url = WS.departamento.put.replace("${nivelId}", this.departamento.nivel.id + "").replace("${depaId}", this.departamento.idDepartamento).replace("${isActive}", this.departamento.active + "");
      this.departamento.status = 2;
      this._functionService.httpPut(url, this.departamento).then((nivelData) => {
        this.setPaginatedTableData(0, this.page.size);
        this.resetForm();
      });
    }
  }

  /**
   * METODO PARA RESETEAR LOS ELEMENTOS DEL FORMULARIO
   * AUTOR: FREDI ROMAN
   */
  public resetForm() {
    this.frmDepart.form.controls['codigoDepa'].reset();
    this.frmDepart.form.controls['nombreDepa'].reset();
    this.frmDepart.form.controls['codigoAlterno'].reset();
    this.departamento.active = true;
    this.saveEdit = true;
  }

  /**
   * METODO PARA PROCESAR EL EVENTO CLICK DE UN BOTON NIVEL
   * AUTOR: FREDI ROMAN
   * @param event 
   * @param i 
   */
  public onNivelClick(event: number) {
    this.currentNivelIndex = event;
    this.departamento.nivel = this.nivelesList[this.currentNivelIndex];
    this.showParentCol = null;
    setTimeout(() => {
      this.showParentCol = this.departamento.nivel.numero > 1 ? true : false;
    }, 100);
    this.resetForm();
    if (event > 0) {
      if (!this.departamento.departamentoPadre) {
        this.departamento.departamentoPadre = new Departamento();
      }
      this.loadParentDepaData(true);
    } else {
      this.setPaginatedTableData(0, this.page.size);
    }
  }

  /**
   * METODO PARA CARGAR LA INFORMACION DE LOS NIVELES DE UNA EMPRESA
   * AUTOR: FREDI ROMAN
   */
  private loadParentDepaData(showLoading: boolean = false) {
    if (this.departamento.nivel.id) {
      let parentNivelId = this.nivelesList[this.currentNivelIndex - 1].id;
      this.depaName = this.nivelesList[this.currentNivelIndex - 1].nombre;

      let request: any;
      if (showLoading) {
        request = this._functionService.httpGet(WS.departamento.get.replace("${nivelId}", parentNivelId));
      } else {
        request = this._functionService.httpGetWithout(WS.departamento.get.replace("${nivelId}", parentNivelId));
      }
      request.then((responseList: any) => {
        this.parentDepartList = this._departamentoService.createDepartamentoList(responseList);
        this.departamento.departamentoPadre.idDepartamento = (this.parentDepartList[0]) ? this.parentDepartList[0].idDepartamento : this.departamento.departamentoPadre.idDepartamento;

        this.setPaginatedTableData(0, this.page.size);
      });
    } else {
      this.loadingIndicator = false;
    }
  }

  /**
   * METODO PARA CARGAR LA INFORMACION DE LOS NIVELES DE UNA EMPRESA
   * AUTOR: FREDI ROMAN
   */
  private loadNivelesData() {
    if (this.mainEnterpriseId) {
      this._functionService.httpGet(WS.nivel.get.replace("${enterpriseId}", this.mainEnterpriseId + ""))
        .then((responseList: any) => {
          this.nivelesList = this._nivelService.createNivelList(responseList);
          this.departamento.nivel = (this.nivelesList[0]) ? this.nivelesList[0] : new Nivel();
          this.setPaginatedTableData(0, this.page.size);
        });
    } else {
      this.loadingIndicator = false;
    }
  }

  /**
   * METODO PARA CARGAR LOS DATOS DE INICIO, DESDE EL BACKEND, PARA MOSTRARLO EN EL DOM 
   * AUTOR: FREDI ROMAN
   */
  private loadInitData() {
    this._empresaControllerService.getEmpresaListController()
      .then((empresaData) => {
        this.empresaList = this._empresaService.createEmpresaList(empresaData);
        this.mainEnterpriseId = (this.empresaList[0]) ? this.empresaList[0].id : null;
        this.loadNivelesData();

        this._functionService.getIpAddress().then((res: any) => {
          this.ipAddress = res;
        });
      });
  }

  /******************************************************************************************
  *******************************************************************************************
   ****** TABLE DEFINITION 
  ********************************************************************************************
  *******************************************************************************************/

  /**
   * METODO PARA REALIZAR UN FILTRO EN LA TABLA
   * AUTOR: FREDI ROMAN
   * @param event 
   */
  public onTableFilter(event) {
    const val = event.target.value.toLowerCase();
    this.filterDepartamentoList = this.departamentoList.slice();
    const temp = this.filterDepartamentoList.filter((filterData) => {
      return filterData.nombreDepa.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.filterDepartamentoList = temp;
    this.table.offset = 0;
  }

  /**
   * METODO PARA AUMENTAR EL NUMERO DE FILAS EL LA TABLA Y CARGAR LOS DATOS DESDE EL BACKEND
   * AUTOR: FREDI ROMAN
   */
  public reloadTableData() {
    this.page.pageNumber = 0;
    this.setPaginatedTableData(0, this.page.size);
  }

  /**
   * METODO PARA CAPTURAR EL EVENTO DE SELECCION DE UNA FILA DE LA TABLA
   * AUTOR: FREDI ROMAN
   * @param selecteRow 
   */
  public onSelectRow(selecteRow: { selected: any[] }) {
    this.saveEdit = false;
    this.departamento.idDepartamento = selecteRow.selected[0].idDepartamento;
    this.departamento.codigoDepa = selecteRow.selected[0].codigoDepa;
    this.departamento.codigoAlterno = selecteRow.selected[0].codigoAlterno;
    this.departamento.nombreDepa = selecteRow.selected[0].nombreDepa;
    this.departamento.departamentoPadre = selecteRow.selected[0].departamentoPadre;
    this.departamento.nivel = selecteRow.selected[0].nivel;
    this.departamento.active = selecteRow.selected[0].active;
  }

  /**
   * METODO PARA DEFINIR LA PAGINACION DE LA TABLA, AL CAPTURAR EL EVENTO DE NEXT/PREV PAGE
   * AUTOR: FREDI ROMAN
   * @param pageInfo 
   */
  public setPage(pageInfo: any) {
    this.page.pageNumber = pageInfo.offset;
    this.setPaginatedTableData(pageInfo.offset, pageInfo.pageSize);
  }

  /**
   * METODO PARA DEFINIR LOS DATOS DE LA TABLA AL INTERACTUAR CON LA PAGINACION
   * AUTOR: FREDI ROMAN
   * @param pageNumber 
   * @param size 
   */
  public setPaginatedTableData(pageNumber: any, size: any) {
    this.page.pageNumber = pageNumber;
    this.page.size = size;

    if (this.departamento.nivel.id) {
      let url = WS.departamento.fetchByNivelId.replace("${nivelId}", this.departamento.nivel.id + "").replace('${pageNumber}', pageNumber).replace('${size}', size);
      this._functionService.fetch(url)
        .then((res) => {
          this.departamentoList = (res && res['content']) ? this._departamentoService.createDepartamentoList(res['content']) : [];
          this.filterDepartamentoList = this.departamentoList.slice();
          this.page.totalElements = (res && res['content']) ? res['totalElements'] : 0;
        });
    } else {
      this.departamentoList = [];
      this.filterDepartamentoList = [];
      this.page.totalElements = 0;
    }
    this.loadingIndicator = false;
  }
  /******************************************************************************************
  ******************************************************************************************/

}
