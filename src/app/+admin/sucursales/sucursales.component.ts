import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { TAGS } from '../../TextLabels';
import { WS } from '../../WebServices';
import { GV } from '../../GeneralVars';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FunctionService } from '../../services/functions/function.service';
import { ConfirmationDialogService } from '../../components/confirmation-dialog/confirmation-dialog.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Page } from '../../components/pagination/page';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import * as textMaskAddons from 'text-mask-addons/dist/textMaskAddons';
import {ToastrService} from 'ngx-toastr';
import { EmpresaControllerService } from '../../services/controllers/empresa-controller.service';
import {AppService} from '../../app.service';

@Component({
  selector: '[app-sucursales tables-ngx-datatable]',
  templateUrl: './sucursales.component.html',
  styleUrls: [
    '../../../../node_modules/@swimlane/ngx-datatable/release/index.css',
    '../../../vendor/libs/ngx-datatable/ngx-datatable.scss',
    '../../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css',
    '../../../vendor/libs/ngx-toastr/ngx-toastr.scss'
  ],
  encapsulation: ViewEncapsulation.None
})
export class SucursalesComponent implements OnInit {
  /*****************************************************************************************
  ******************************************************************************************
  * AUTOR: FREDI ROMAN
  ******************************************************************************************
  *****************************************************************************************/
  public navBtnList: { url: string; selected: boolean; label: string }[];
  /*****************************************************************************************
  *****************************************************************************************/

  @ViewChild(DatatableComponent) table: DatatableComponent;
  enableState: Boolean = true;
  changeStateVal: Boolean = true;
  viewEdit: Boolean = false;
  submitedSucursales: Boolean = null;
  loadingIndicator = true;
  formSucursal: FormGroup;
  labels = null;
  ipAddress = null;
  responseList = null;
  rows = null;
  temp = null;
  gv = null;
  dataSesion = null;
  empresas = [];
  estados = [];
  ciudades = [];
  zonas = [];
  sectores = [];
  selected = [];
  timeout = null;
  arrayCalle1 = {};
  arrayCalle2 = {};
  page = new Page();
  citySend = GV.ciudadesFastline.quito;
  emailMaskOptions = {
    mask: textMaskAddons.emailMask
  };
  sucursal = {
    'id': '',
    'codAlterno': '',
    'codSucursal': '',
    'nomSucursal': '',
    'codCiudad': '',
    'direccion': '',
    'telefono': '',
    'fax': '',
    'correo': '',
    'codSector': '',
    'codZona': '',
    'nomCalle1': '',
    'nomCalle2': '',
    'numeroCalle': '',
    'estado': '',
    'codusu': '',
    'fecult': '',
    'status': '',
    'ipMaquina': '',
    'SGE_Empresa_idEmpresa': '',
    'empresa': null
  };
  options_notitications = {
    tapToDismiss: true,
    closeButton: false,
    progressBar: true,
    positionClass: 'toast-top-right',
    rtl: this.appService.isRTL
  };
  constructor(
    private functionService: FunctionService,
    private _empresaControllerService: EmpresaControllerService,
    private formBuilder: FormBuilder,
    private confirmationDialogService: ConfirmationDialogService,
    private route: ActivatedRoute,
    public toastrService: ToastrService,
    private appService: AppService,
    private _router: Router
  ) {
    this.navBtnList = [
      { url: '/admin/company', selected: false, label: 'Empresa' },
      { url: '/admin/sucursales', selected: true, label: 'Sucursal' },
      { url: '/admin/feriado', selected: false, label: 'Feriados' },
      { url: '/admin/definiciones', selected: false, label: 'Definiciones' }
    ];
  }

  /**
   * METODO PARA CAPTURAR EL EVENTO CHANGE DEL SELECT DE EMPRESA PARA RECARGAR LOS DATOS DE LA TABLA
   * AUTOR: FREDI ROMAN
   */
  public onChangeEmpresa() {
    this.setDataPagination(0, this.page.size);
  }

  changeState() {
    this.changeStateVal = true;
  }

  confirmMethod() {
    if (this.changeStateVal && this.sucursal.estado.toString() === GV.state.inactive) {
      this.openConfirmationDialog();
    } else {
      this.save();
    }
  }

  openConfirmationDialog() {
    const text = this.labels.messages.inactivate + ' ' + this.labels.employees.employee + '.  ' + this.labels.messages.inactivateConfirm;
    this.confirmationDialogService.confirm(this.labels.general.confirm.toLocaleUpperCase(), text)
      .then((confirmed) => {
        if (confirmed) {
          this.save();
        }
      }).catch(() =>
        console.log('Dismissed.)'
        ));
  }

  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    this.setDataPagination(pageInfo.offset, pageInfo.pageSize);
  }

  loadData() {
    this.page.pageNumber = 0;
    this.setDataPagination(0, this.page.size);
  }

  setDataPagination(pageNumber, size) {
    this.page.pageNumber = pageNumber;
    this.page.size = size;

    if (this.sucursal.SGE_Empresa_idEmpresa) {
      let url = WS.sucursales.fetchByEnterpriseId.replace("${enterpriseId}", this.sucursal.SGE_Empresa_idEmpresa);
      url = url.replace('${pageNumber}', pageNumber);
      url = url.replace('${size}', size);
      this.functionService.fetch(url).then((res) => {
        this.temp = res['content'];
        this.rows = res['content'];
        this.page.totalElements = res['totalElements'];
      });
    } else {
      this.temp = [];
      this.rows = [];
      this.page.totalElements = 0;
    }
    this.loadingIndicator = false;
  }

  clear() {
    this.sucursal = {
      'id': '',
      'codAlterno': '',
      'codSucursal': '',
      'nomSucursal': '',
      'codCiudad': '',
      'direccion': '',
      'telefono': '',
      'fax': '',
      'correo': '',
      'codSector': '',
      'codZona': '',
      'nomCalle1': '',
      'nomCalle2': '',
      'numeroCalle': '',
      'estado': '',
      'codusu': '',
      'fecult': '',
      'status': '',
      'ipMaquina': '',
      'SGE_Empresa_idEmpresa': '',
      'empresa': null
    };
    ////
    this.sucursal.SGE_Empresa_idEmpresa = (this.empresas[0]) ? this.empresas[0].id : null;
    ////
    this.setDataPagination(0, 5);
    this.submitedSucursales = false;
    this.viewEdit = true;
    this.sucursal.estado = this.responseList[0].content[0].id;
    this.sucursal.codCiudad = this.responseList[1].content[0].id;
    this.sucursal.codSector = this.responseList[2].content[0].id;
    this.formSucursal.controls.cds.setValue('');
    this.formSucursal.controls.cdn.setValue('');
    this.formSucursal.controls.dsp.setValue('');
    this.formSucursal.controls.str1.setValue('');
    this.formSucursal.controls.str2.setValue('');
    this.formSucursal.controls.nstr.setValue('');
    this.formSucursal.controls.tlf.setValue('');
    this.formSucursal.controls.zone.setValue('');
    this.enableState = true;
    this.changeStateVal = false;
  }

  onSelect({ selected }) {
    this.enableState = false;
    this.viewEdit = false;
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    this.sucursal = this.selected[0];
    this.sucursal.SGE_Empresa_idEmpresa = this.sucursal['empresa'].id;
    this.sucursal.codSucursal = this.sucursal.codSucursal.toString();
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function (d) {
      return d.empresa.nombreEmpresa.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows = temp;
    this.table.offset = 0;
  }

  onBlurStreet() {
    if (this.sucursal.nomCalle1.toString() !== '' && this.sucursal.nomCalle2.toString() !== '') {
      let url1 = WS.geo.crossStreet;
      url1 = url1.replace('${street1}', this.sucursal.nomCalle1);
      url1 = url1.replace('${street2}', this.sucursal.nomCalle2);
      url1 = url1.replace('${ciudad}', this.citySend);
      if (!url1.toString().includes('undefined')) {
        this.functionService.getService(url1).then((org) => {
          if (org[0]) {
            this.sucursal.codZona = org[0]['zona'];
          } else {
            this.toastrService['error'](this.labels.messages.errorCruce, this.appService.pageTitle, this.options_notitications);
            this.sucursal.codZona = '';
          }
        });
      }
    }
  }

  keyUpOrSt1() {
    this.sucursal.direccion = this.sucursal.nomCalle1.toUpperCase() + ' Y ' + this.sucursal.nomCalle2.toUpperCase();
    this.sucursal.nomCalle1 = this.sucursal.nomCalle1.toUpperCase();
    if (this.sucursal.nomCalle1.toString() !== '' && this.sucursal.nomCalle1.toString() !== ' ') {
      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        let url = WS.geo.findStreet;
        if (this.sucursal.nomCalle1.toString() !== '') {
          url = url.replace('${street}', this.sucursal.nomCalle1);
          url = url.replace('${ciudad}', this.citySend);
          this.functionService.getService(url).then((responseList) => {
            this.arrayCalle1 = responseList;
          });
        }
      }, 500, 'JavaScript');
    } else {
      this.sucursal.nomCalle1 = '';
    }
  }

  keyUpOrSt2() {
    this.sucursal.direccion = this.sucursal.nomCalle1.toUpperCase() + ' Y ' + this.sucursal.nomCalle2.toUpperCase();
    this.sucursal.nomCalle2 = this.sucursal.nomCalle2.toUpperCase();
    if (this.sucursal.nomCalle2.toString() !== '' && this.sucursal.nomCalle2.toString() !== ' ') {
      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        let url = WS.geo.findStreet;
        if (this.sucursal.nomCalle2.toString() !== '') {
          url = url.replace('${street}', this.sucursal.nomCalle2);
          url = url.replace('${ciudad}', this.citySend);
          this.functionService.getService(url).then((responseList) => {
            this.arrayCalle2 = responseList;
          });
        }
      }, 500, 'JavaScript');
    } else {
      this.sucursal.nomCalle2 = '';
    }
  }

  changeCity(event) {
    this.sucursal.nomCalle1 = '';
    this.sucursal.nomCalle2 = '';
    this.sucursal.codZona = '';
    console.log( event.target.value.toLowerCase());
    if (event.target.value.toString() === '13') {
      this.citySend = GV.ciudadesFastline.guayaquil;
    } else {
      this.citySend = GV.ciudadesFastline.quito;
    }
  }

  save() {
    let url = WS.sucursales.getCodSucursal;
    url = url.replace('${codigosucursal}', this.sucursal.codSucursal);
    this.functionService.httpGet(url).then((response) => {
      if (!response) {
        if (this.formSucursal.valid) {
          this.submitedSucursales = false;
          const empresa = this.empresas.find((emp) => {
            return emp.id.toString() === this.sucursal.SGE_Empresa_idEmpresa.toString();
          });
          this.sucursal.fecult = this.functionService.getToday();
          this.sucursal.ipMaquina = this.ipAddress;
          this.sucursal.codusu = this.dataSesion['user_id'];
          this.sucursal.status = '1';
          this.sucursal.empresa = empresa;
          if (this.viewEdit) {
            this.functionService.httpPost(WS.sucursales.save, this.sucursal).then(() => {
              this.clear();
            });
          } else {
            let url = WS.sucursales.update;
            url = url.replace('${sucursalId}', this.sucursal.id);
            this.functionService.httpPut(url, this.sucursal).then(() => {
              this.clear();
            });
          }
        } else {
          this.submitedSucursales = true;
        }
      } else {
        this.toastrService['error'](this.labels.messages.duplicateCodSuc, this.appService.pageTitle, this.options_notitications);
      }
    });
  }

  ngOnInit() {
    this.citySend = GV.ciudadesFastline.quito;
    this.dataSesion = localStorage.getItem('userToken');
    this.dataSesion = this.functionService.decode(this.dataSesion);
    this.labels = TAGS;
    this.gv = GV;
    this.viewEdit = true;
    this.formSucursal = this.formBuilder.group({
      cds: [null, [Validators.required, Validators.minLength(3)]],
      cdn: [null, [Validators.required]],
      dsp: [null, [Validators.required]],
      str1: [null, [Validators.required]],
      str2: [null, [Validators.required]],
      nstr: [null, [Validators.required]],
      tlf: [null, [Validators.required]],
      zone: [null, [Validators.required]]
    });
    const array = [
      WS.general.status, WS.general.cities, WS.empresa.sectors
    ];
    this.functionService.httpObservableGet(array).then((responseList) => {
      this.estados = responseList[0].content;
      this.ciudades = responseList[1].content;
      this.sectores = responseList[2].content;
      this.responseList = responseList;
      this.functionService.getIpAddress().then((res) => {
        this.ipAddress = res;
      });
      this._empresaControllerService.getEmpresaListController()
        .then((empresasList) => {
          this.empresas = empresasList;
          this.clear();
        });
      if (this.route.snapshot.paramMap.get('emp')) {
        this.sucursal.SGE_Empresa_idEmpresa = this.route.snapshot.paramMap.get('emp');
      }
    });
  }
  get fc() { return this.formSucursal.controls; }
}
