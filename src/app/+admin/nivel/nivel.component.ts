/**
 * AUTOR: FREDI ROMAN
 */
import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Nivel } from '../../models/nivel';
import { FunctionService } from '../../services/functions/function.service';
import { WS } from '../../WebServices';
import { NivelService } from '../../services/component-services/nivel.service';
import { ConfirmationDialogService } from '../../components/confirmation-dialog/confirmation-dialog.service';
import { TAGS } from '../../TextLabels';

@Component({
  selector: 'nivel',
  templateUrl: './nivel.component.html',
  styleUrls: ['./nivel.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NivelComponent implements OnInit, OnChanges {
  @Input() empresaId: number;
  @Input() nivelesList: Nivel[];
  @Input() showEditableControls: boolean;
  @Output() nivelClickedIndex: EventEmitter<number>;
  @Output() onAddNivel: EventEmitter<Nivel>;

  private delNivelByIndex: number;

  public newNivel: Nivel;
  public addCancel: boolean;
  public showConfirm: boolean;

  constructor(
    private _functionService: FunctionService,
    private _nivelService: NivelService,
    private _confirmationDialogService: ConfirmationDialogService,
  ) {
    this.nivelClickedIndex = new EventEmitter<number>();
    this.onAddNivel = new EventEmitter<Nivel>();
    this.addCancel = true;
  }

  ngOnInit() { }

  /**
   * METODO PARA DETECTAR LA ACCION DE ELIMINAR NIVEL
   * AUTOR: FREDI ROMAN
   * @param event 
   */
  public deleteNivel(event: any, index: number) {
    event.preventDefault();
    this._confirmationDialogService.confirm(TAGS.general.confirm.toLocaleUpperCase(), TAGS.messages.nivel.confirmDelete)
      .then((confirmed) => {
        if (confirmed) {
          this.delNivelByIndex = index;
          if (this.delNivelByIndex >= 0) {
            let url = WS.nivel.delete.replace("${nivelId}", this.nivelesList[this.delNivelByIndex].id);
            this._functionService.httpDelete(url)
              .then((respData: any) => {
                this.nivelesList.splice(this.delNivelByIndex, 1);
              });
          }
        } else {
          this.delNivelByIndex = null;
        }
      });
  }

  /**
   * METODO PARA ANADIR Y GUARDAR UN NIVEL 
   * AUTOR: FREDI ROMAN
  */
  public saveNivel(event: any, index: number) {
    event.preventDefault();
    let url, request;
    if (this.nivelesList[index].id) {
      url = WS.nivel.put.replace("${enterpriseId}", this.nivelesList[index].empresa.id + "").replace('${nivelId}', this.nivelesList[index].id).replace('${isActive}', 'true');
      request = this._functionService.httpPut(url, this.nivelesList[index]);
    } else {
      url = WS.nivel.post.replace("${enterpriseId}", this.empresaId + "");
      request = this._functionService.httpPost(url, this.nivelesList[index]);
    }

    request.then((resp) => {
      this.newNivel = this._nivelService.extractNivelData(resp);
      this.nivelesList.splice(index, 1, this.newNivel);
      this.onAddNivel.emit(this.newNivel);
      this.newNivel = null;
      this.addCancel = true;
      this.onNivelClick(null, index);
    });
  }

  /**
   * METODO PARA AGREGAR UN INPUT PARA REGISTRAR UN NUEVO NIVEL
   * AUTOR: FREDI ROMAN
   * @param event 
   */
  public addModifyNivel(event: any = null, index: number = undefined) {
    if (event) {
      event.preventDefault();
    }
    if (this.addCancel) {
      this.addCancel = false;
      this.newNivel = new Nivel();
      if (index != undefined) {
        this.newNivel.id = this.nivelesList[index].id;
        this.newNivel.nombre = this.nivelesList[index].nombre;
        this.nivelesList[index].editable = true;
      } else {
        this.newNivel.editable = true;
        this.newNivel.numero = this.nivelesList.length + 1;
        this.nivelesList.push(this.newNivel);
      }
    } else {
      this.addCancel = true;
      for (let i = 0; i < this.nivelesList.length; i++) {
        if (!this.nivelesList[i].id) {
          this.nivelesList.splice(i, 1);
        } else {
          this.nivelesList[i].editable = false;
        }
        if (this.newNivel.id) {
          if (this.nivelesList[i].id == this.newNivel.id) {
            this.nivelesList[i].nombre = this.newNivel.nombre;
          }
        }
      }
      this.newNivel = null;
    }
  }

  /**
   * METODO PARA PROCESAR EL EVENTO CLICK DE UN BOTON NIVEL
   * AUTOR: FREDI ROMAN
   * @param event 
   * @param i 
   */
  public onNivelClick(event: any, index: number) {
    if (event) {
      event.preventDefault();
    }
    if (!this.nivelesList[index].selected && !this.nivelesList[index].editable) {
      this.nivelClickedIndex.emit(index);
      for (let i = 0; i < this.nivelesList.length; i++) {
        if (i == index) {
          this.nivelesList[i].selected = true;
        } else {
          this.nivelesList[i].selected = false;
        }
      }
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    for (let property in changes) {
      if (property == 'nivelesList') {
        this.addCancel = true;
        if (changes[property].currentValue) {
          if (changes[property].currentValue.length == 0) {
            this.addModifyNivel();
          }
        }
      }
    }
  }
}
