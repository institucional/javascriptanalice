/**
 * INTERFACE PARA DEFINIR EL TIPO DE DATO PARA UN OBJETO BOTON
 * AUTOR: FREDI ROMAN
 */
export interface Button {
    icon: string;
    label: string;
    btnClass: string;
    onClickAction: number;
}