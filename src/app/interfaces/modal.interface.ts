/**
 * INTERFACE PARA DEFINIR EL TIPO DE DATO PARA UN OBJETO MODAL
 * AUTOR: FREDI ROMAN
 */
import { Button } from "./button.interface";

export interface Modal {
    baseComponentId: string,
    icon: string;
    title: string;
    componentType: number;
    body: any;
    bodyData?: any;
    bodyClass?: string;
    btnList?: Button[];
}