import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


// ***************************
// Themplate Libs

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ChartsModule as Ng2ChartsModule } from 'ng2-charts/ng2-charts';
import { TextMaskModule } from 'angular2-text-mask';
import { PasswordStrengthBarModule } from 'ng2-password-strength-bar';
import { ToastrModule } from 'ngx-toastr';
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import { ConfirmationDialogService } from './components/confirmation-dialog/confirmation-dialog.service';

// Import toast module
import { ToastModule } from 'ng2-toastr/ng2-toastr';

// ***************************
// App

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppService } from './app.service';
import { LayoutModule } from './layout/layout.module';

// ***************************
// Libs
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthService } from './services/auth/auth.service';
import { AuthGuard } from './services/auth/auth.guard';
import { AuthInterceptor } from './services/interceptors/auth.service';
import { FunctionService } from './services/functions/function.service';
import { NgxSpinnerModule } from 'ngx-spinner';

// ***************************
// Pages

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ValidateComponent } from './validate/validate.component';
import { SettingsComponent } from './settings/settings.component';
import { ContactComponent } from './contact/contact.component';
import { HelpComponent } from './help/help.component';
import { SpinnerDialogComponent } from './components/spinner-dialog/spinner-dialog.component';
import { SpinnerDialogService } from './components/spinner-dialog/spinner-dialog.service';
import { ModalComponent } from './components/modal/modal.component';
import { DynamicLayerComponent } from './components/dynamic-layer/dynamic-layer.component';
import { ModalService } from './services/modal-service/modal.service';
import { DetallePresupuestoComponent } from './+admin/detalle-presupuesto/detalle-presupuesto.component';
import { DynaComponentService } from './services/component-services/dyna-component.service';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { EmpresaListComponent } from './+admin/empresa-list/empresa-list.component';
import { CalificaviajeComponent } from './components/calificaviaje/calificaviaje.component';
import { CalificaviajeService } from './components/calificaviaje/calificaviaje.service';
import { PreautorizacionComponent } from './components/Preautorizacion/preautorizacion.component';
import { PreautorizacionService } from './components/Preautorizacion/preautorizacion.service';
import { ResponsePreautorizacionComponent } from './components/ResponsePreautorizacion/responsepreautorizacion.component';
import { ResponsePreautorizacionService } from './components/ResponsePreautorizacion/responsepreautorizacion.service';
import { RestClientService } from './services/component-services/rest-client.service';
import { FrAccordionModule } from './fr-accordion/fr-accordion.module';
import { PerfilComponent } from './components/perfil/perfil.component';
import { PerfilService } from './components/perfil/perfil.service';
import { NgxMaskModule } from 'ngx-mask';
// ***************************
//

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    ValidateComponent,
    SettingsComponent,
    ContactComponent,
    HelpComponent,
    ConfirmationDialogComponent,
    SpinnerDialogComponent,
    ModalComponent,
    DynamicLayerComponent,
    DetallePresupuestoComponent,
    EmpresaListComponent,
    CalificaviajeComponent,
    PreautorizacionComponent,
    ResponsePreautorizacionComponent,
    PerfilComponent
  ],

  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    Ng2ChartsModule,
    TextMaskModule,
    PasswordStrengthBarModule,
    FrAccordionModule,
    ToastrModule.forRoot(),
    NgbModule.forRoot(),
    ToastModule.forRoot(),
    // App
    AppRoutingModule,
    LayoutModule,
    HttpClientModule,
    NgxSpinnerModule,
    NgxDatatableModule,
    NgxMaskModule.forRoot()
  ],

  providers: [
    Title,
    AppService,
    AuthService,
    FunctionService,
    AuthGuard,
    ConfirmationDialogService,
    CalificaviajeService,
    PreautorizacionService,
    ResponsePreautorizacionService,
    PerfilService,
    SpinnerDialogService,
    ModalService,
    DynaComponentService,
    RestClientService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],

  bootstrap: [
    AppComponent
  ],
  entryComponents: [
    ConfirmationDialogComponent,
    CalificaviajeComponent,
    PreautorizacionComponent,
    ResponsePreautorizacionComponent,
    PerfilComponent,
    SpinnerDialogComponent,
    ModalComponent,
    DetallePresupuestoComponent,
    EmpresaListComponent
  ],
})
export class AppModule { }
