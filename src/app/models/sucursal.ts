import { Empresa } from "./empresa";

export class Sucursal {
    constructor(
        public id?: string,
        public codAlterno?: string,
        public codSucursal?: number,
        public nomSucursal?: string,
        public codCiudad?: number,
        public direccion?: string,
        public telefono?: string,
        public fax?: string,
        public correo?: string,
        public codSector?: number,
        public codZona?: number,
        public nomCalle1?: string,
        public nomCalle2?: string,
        public numeroCalle?: string,
        public empresa?: Empresa,
        public active?: boolean,
        public status?: number,
        public ipMaquina?: string,
    ) {
        this.empresa = this.empresa || new Empresa();
    }
}