export class Empresa {
    constructor(
        public id?: number,
        public ruc?: string,
        public calificacion?: number,
        public vip?: number,
        public nombreEmpresa?: string,
        public nombre?: string,
        public nombreComercial?: string,
        public representanteLegal?: string,
        public tipoEmpresa?: number,
        public ciudad?: number,
        public direccion?: string,
        public telefono?: string,
        public fax?: string,
        public correo?: string,
        public sector?: number,
        public motivoDeshabilitacion?: string,
        public obsDeshabilitacion?: string
    ) { }
}