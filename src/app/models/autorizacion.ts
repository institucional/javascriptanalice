import { Empleado } from "./empleado";

export class Autorizacion {

    constructor(
        public autorizacion?: string,
        public empleado?: Empleado,
        public autorizador?: Empleado,
        public autorizadorBack?: Empleado,
        public fechaAutorizacion?: string,
        public horaAutorizacion?: string,
        public observacion?: string,
    ) {
        this.empleado = new Empleado();
        this.autorizador = new Empleado();
        this.autorizadorBack = new Empleado();
    }
}