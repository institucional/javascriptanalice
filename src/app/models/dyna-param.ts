export class DynaParam {
    constructor(
        public id?: string,
        public etiqueta?: string,
        public idTipoDato?: number,
        public tipoDato?: string,
        public idCatalogo?: number,
        public catalogo?: string,
        public obligatorio?: boolean,
        public activo?: boolean,
        public valor?: any,
    ) { }
}