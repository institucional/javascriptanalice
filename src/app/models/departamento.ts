import { Nivel } from "./nivel";

export class Departamento {
    constructor(
        public idDepartamento?: string,
        public codigoDepa?: string,
        public nombreDepa?: string,
        public codigoAlterno?: string,
        public departamentoPadre?: Departamento,
        public nivel?: Nivel,
        public active?: boolean,
        public estado?: number,
        public ipMaquina?: string,
        public status?: number
    ) {
        this.nivel = this.nivel || new Nivel();
    }
}