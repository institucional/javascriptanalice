import { Convenio } from "./convenio";
import { Departamento } from "./departamento";
import { Sucursal } from "./sucursal";

export class Presupuesto {
    constructor(
        public id?: string,
        public anio?: string,
        public mes?: string,
        public mesDesc?: string,
        public valor?: number,
        public convenio?: Convenio,
        public sucursal?: Sucursal,
        public departamento?: Departamento,
        public active?: boolean,
        public status?: number,
        public ipMaquina?: string,
        public poliTaxiLaborable?: any,
        public poliTaxiFeriaFds?: any
    ) {
        this.convenio = this.convenio || new Convenio();
        this.departamento = this.departamento || new Departamento();
        this.sucursal = this.sucursal || new Sucursal();
    }
}