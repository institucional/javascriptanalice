import { Departamento } from "./departamento";

export class Cargo {
    constructor(
        public id?: string,
        public codCargo?: string,
        public nombre?: string,
        public departamento?: Departamento,
        public active?: boolean,
        public estado?: number,
        public ipMaquina?: string,
        public status?: number
    ) {
        this.departamento = this.departamento || new Departamento();
    }
}