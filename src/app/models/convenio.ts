import { Empresa } from "./empresa";

export class Convenio {
    constructor(
        public id?: number,
        public codConvenio?: string,
        public montoLimite?: number,
        public numeroVoucherLimite?: number,
        public controlHoras?: number,
        public numeroHorasAprobar?: number,
        public fechaSuscripcion?: string,
        public fechaVencimiento?: string,
        public plazo?: number,
        public empresa?: Empresa,
        public tipoControl?: string,
        public tipoControlDesc?: string,
        public poliTaxiLaborable?: any,
        public poliTaxiFeriaFds?: any,
    ) {
        this.empresa = this.empresa || new Empresa()
    }
}