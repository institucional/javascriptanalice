import { User } from "./user";
import { Empresa } from "./empresa";

export class UsuarioEmpresa {
    constructor(
        public id?: string,
        public usuario?: User,
        public empresa?: Empresa,
        public status?: number,
        public ipMaquina?: string,
    ) {
        this.usuario = this.usuario || new User();
        this.empresa = this.empresa || new Empresa();
    }
}