import { User } from "./user";

export class Asociado {
    constructor(
        public idAsociado: String,
        public usuario: User,
        public usuarioAsociado: User,
        public estado?: number
    ) {
        usuario = new User('', '', '', '');
        usuarioAsociado = new User('', '', '', '');
    }
}