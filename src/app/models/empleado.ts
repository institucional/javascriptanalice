import { Cargo } from "./cargo";
import { User } from "./user";
import { Sucursal } from "./sucursal";

export class Empleado {
    constructor(
        public id?: number,
        public codEmpleado?: string,
        public codAlterno?: string,
        public tipoIdentif?: number,
        public identificacion?: string,
        public primerNombre?: string,
        public segundoNombre?: string,
        public primerApellido?: string,
        public segundoApellido?: string,
        public nomCompleto?: string,
        public telefonoCelular?: string,
        public telefonoConvencional?: string,
        public correo?: string,
        public cargo?: Cargo,
        public codOperadora?: number,
        public sucursal?: Sucursal,
        public usuario?: User,
        public fechaDesde?: any,
        public fechaHasta?: any,
        public estado?: number,
        public status?: number,
        public codusu?: number,
        public fecult?: string,
        public ipMaquina?: string,
        public dynaAttrib?: any
    ) {
        this.cargo = this.cargo || new Cargo();
        this.sucursal = this.sucursal || new Sucursal();
        this.usuario = this.usuario || new User();
        this.status = this.status || 1;
        this.codusu = this.codusu || 1;
        this.primerNombre = this.primerNombre || '';
        this.segundoNombre = this.segundoNombre || '';
        this.primerApellido = this.primerApellido || '';
        this.segundoApellido = this.segundoApellido || '';
    }
}