import { Convenio } from "./convenio";
import { Empleado } from "./empleado";
import { Sucursal } from "./sucursal";
import { Presupuesto } from "./presupuesto";
import { Empresa } from "./empresa";

export class ResponsableContacto {
    constructor(
        public id?: string,
        public clase?: number,
        public desClase?: string,
        public codTipoResponsable?: number,
        public tipoResponsable?: string,
        public empleado?: Empleado,
        public sucursal?: Sucursal,
        public convenio?: Convenio,
        public presupuesto?: Presupuesto,
        public active?: boolean,
        public ipMaquina?: string,
        public status?: number,
        public recibeNotif?: boolean,
        public empresaConvenio?: Empresa,
    ) {
        this.empleado = this.empleado || new Empleado();
        this.sucursal = this.sucursal || new Sucursal();
        this.convenio = this.convenio || new Convenio();
        this.presupuesto = this.presupuesto || new Presupuesto();
    }
}