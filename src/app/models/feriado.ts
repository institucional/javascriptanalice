import { Sucursal } from "./sucursal";

export class Feriado {
    constructor(
        public idFeriado?: string,
        public fechaFeriado?: any,
        public sucursal?: Sucursal,
        public active?: boolean,
        public status?: number,
        public ipMaquina?: string
    ) {
        this.sucursal = this.sucursal || new Sucursal();
    }
}