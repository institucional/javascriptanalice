import { Empresa } from "./empresa";

export class Nivel {
    constructor(
        public id?: string,
        public numero?: number,
        public nombre?: string,
        public empresa?: Empresa,
        public active?: boolean,
        public estado?: number,
        public ipMaquina?: string,
        public status?: number,
        public selected?: boolean,
        public editable?: boolean,
        public depaCount?: number
    ) {
        this.empresa = this.empresa || new Empresa();
        this.editable = this.editable || false;
        this.depaCount = this.depaCount || 0;
    }
}