export interface AccordionInterface {
    id: string,
    nombre: string,
    subItems?: AccordionInterface[],
    cssClass?: string
}