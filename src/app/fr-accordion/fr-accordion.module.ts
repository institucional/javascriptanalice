import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FrAccordionComponent } from './components/fr-accordion/fr-accordion.component';
import { FrAccordionItemComponent } from './components/fr-accordion-item/fr-accordion-item.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    FrAccordionComponent
  ],
  declarations: [FrAccordionComponent, FrAccordionItemComponent]
})
export class FrAccordionModule { }
