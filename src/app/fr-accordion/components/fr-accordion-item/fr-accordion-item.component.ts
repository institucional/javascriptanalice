import { Component, OnInit, Input, EventEmitter, Output, OnChanges, SimpleChanges } from '@angular/core';
import { AccordionInterface } from '../../interfaces/accordion-interface';

const FOCUS_CLASS = 'focused';
const OPEN_ICON_CLASS = 'fr-open-item';

@Component({
  selector: 'fr-accordion-item',
  templateUrl: './fr-accordion-item.component.html',
  styleUrls: ['./fr-accordion-item.component.scss']
})
export class FrAccordionItemComponent implements OnInit, OnChanges {
  @Input() public accordionItem: AccordionInterface;
  @Input() public show: boolean;
  @Input() public removeClassTo: string;
  @Input() selectedItemId: string;
  @Output() onFocusItem: EventEmitter<string>;

  public itemIconClass: string;

  constructor() {
    this.onFocusItem = new EventEmitter<string>();
  }

  ngOnInit() {
    if (this.show) {
      this.accordionItem.cssClass = FOCUS_CLASS;
      this.itemIconClass = OPEN_ICON_CLASS;
    }
  }

  /**
   * METODO PARA SELECCIONAR UN ITEM Y DAR EL ESTILO DE ACTIVO
   * AUTOR: FREDI ROMAN
   */
  public onFocus() {
    this.accordionItem.cssClass = FOCUS_CLASS;
    if (!this.itemIconClass) {
      this.itemIconClass = OPEN_ICON_CLASS;
    } else {
      this.itemIconClass = '';
    }
    this.onFocusItem.emit(this.accordionItem.id);
  }

  /**
   * METODO CAPTURAR EL ID DE UN ITEM DE LA LISTA HIJA DEL ACORDION
   * AUTOR: FREDI ROMAN
   * @param event 
   */
  public getFocusedChildItem(event: string) {
    this.onFocusItem.emit(event);
  }

  ngOnChanges(changes: SimpleChanges) {
    for (let property in changes) {
      if (changes[property].currentValue) {
        if (property == 'removeClassTo') {
          for (let child of this.accordionItem.subItems) {
            if (child.id != this.removeClassTo) {
              child.cssClass = '';
            }
          }
        } else if (property == 'selectedItemId') {
          if (changes[property].currentValue == this.accordionItem.id) {
            this.onFocus();
          }
        }
      }
    }
  }
}

