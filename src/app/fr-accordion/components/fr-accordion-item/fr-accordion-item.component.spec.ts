import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrAccordionItemComponent } from './fr-accordion-item.component';

describe('FrAccordionItemComponent', () => {
  let component: FrAccordionItemComponent;
  let fixture: ComponentFixture<FrAccordionItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrAccordionItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrAccordionItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
