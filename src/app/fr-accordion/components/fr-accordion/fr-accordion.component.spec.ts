import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrAccordionComponent } from './fr-accordion.component';

describe('FrAccordionComponent', () => {
  let component: FrAccordionComponent;
  let fixture: ComponentFixture<FrAccordionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrAccordionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrAccordionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
