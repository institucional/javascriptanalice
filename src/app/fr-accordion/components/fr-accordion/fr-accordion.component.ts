import { Component, OnInit, EventEmitter, Output, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AccordionInterface } from '../../interfaces/accordion-interface';

@Component({
  selector: 'fr-accordion',
  templateUrl: './fr-accordion.component.html',
  styleUrls: ['./fr-accordion.component.scss']
})
export class FrAccordionComponent implements OnInit, OnChanges {
  @Input() accordionList: AccordionInterface[];
  @Input() isChild: boolean;
  @Input() removeClassTo: string;
  @Input() selectedItemId: string;
  @Input() defaultSelect: boolean;
  @Output() onFocusItem: EventEmitter<string>;

  constructor() {
    this.onFocusItem = new EventEmitter<string>();
  }

  ngOnInit() { }

  /**
   * METODO PARA DETECTAR EL ID DEL ITEM SELECCIONADO Y ENVIARLO HACIA EL COMPONENTE PADRE DE ESTE COMPONENTE
   * AUTOR: FREDI ROMAN
   * @param event
   */
  public getFocusedItemId(event: string) {
    for (let accordion of this.accordionList) {
      if (accordion.id != event) {
        accordion.cssClass = '';
      }
    }
    this.removeClassTo = null;
    setTimeout(() => {
      this.removeClassTo = event;
    });
    this.onFocusItem.emit(event);
  }

  ngOnChanges(changes: SimpleChanges) {
    for (let property in changes) {
      if (changes[property].currentValue) {
        if (property == 'accordionList') {
          if (!this.isChild && this.defaultSelect != false) {
            this.onFocusItem.emit(this.accordionList[0].id);
          }
        }
      }
    }
  }
}

