import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {TAGS} from '../../TextLabels';
import { GV } from '../../GeneralVars';
import { WS } from '../../WebServices';
import {FunctionService} from '../../services/functions/function.service';
import {Page} from '../../components/pagination/page';
import {ConfirmationDialogService} from '../../components/confirmation-dialog/confirmation-dialog.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-payment-methods',
  templateUrl: './payment-methods.component.html',
  styleUrls: [
    '../../../../node_modules/@swimlane/ngx-datatable/release/index.css',
    '../../../vendor/libs/ngx-datatable/ngx-datatable.scss',
    '../../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css',
    '../../../vendor/libs/ngx-toastr/ngx-toastr.scss'
  ],
  encapsulation: ViewEncapsulation.None
})
export class PaymentMethodsComponent implements OnInit {

  constructor(
    private functionService: FunctionService,
    private confirmationDialogService: ConfirmationDialogService,
    private formBuilder: FormBuilder,
    public _domSanitizer: DomSanitizer
  ) { }

  @ViewChild(DatatableComponent) table: DatatableComponent;
  saveEdit: Boolean = true;
  submitedFormasPago: Boolean = null;
  formCard: FormGroup;
  page = new Page();
  temp = null;
  dataSesion = null;
  responseList = null;
  ipAddress = null;
  labels = null;
  gv = null;
  rows = null;
  loadingIndicator = true;
  selected = [];
  formasPago = [];
  tiposTarjetas = [];
  estados = [];
  check = false;
  viewPaymentz = true;
  nombreTajeta = '';
  mesAnio = '';
  cvc = '';
  formaPago = {
    'id': '',
    'formapago': '',
    'numtar': '',
    'token': '',
    'nombre': '',
    'anioexpira': '',
    'mesexpira': '',
    'tipotar': '',
    'principal': '',
    'estado': '',
    'fecult': '',
    'codusu': '',
    'status': '',
    'ipMaquina': '',
    'user': null
  };

  changeMethod() {
    this.saveEdit = true;
    this.check = false;
    this.nombreTajeta = '';
    this.mesAnio = '';
    this.cvc = '';
    this.formaPago.tipotar = this.responseList[2].content[0].desElemento;
    this.formaPago.numtar = '';
    document.getElementById('nomtar').removeAttribute('disabled');
    document.getElementById('numtar').removeAttribute('disabled');
    document.getElementById('yeartar').removeAttribute('disabled');
    document.getElementById('cvctar').removeAttribute('disabled');
    if (this.formaPago.formapago === this.gv.formasPago.tarjeta) {
      this.viewPaymentz = false;
    } else {
      this.viewPaymentz = true;
    }
  }

  confirmMethod() {
    if (this.formaPago.estado.toString() === GV.state.inactive) {
      this.openConfirmationDialog();
    } else {
      this.save();
    }
  }

  save() {
    let url = WS.formasPago.save;
    url = url.replace('${idUser}', this.dataSesion['user_id']);
    url = url.replace('${email}', this.dataSesion['email']);
    this.formaPago.nombre = this.dataSesion['user_name'];
    this.formaPago.principal = this.check ? '1' : '0';
    this.formaPago.ipMaquina = this.ipAddress;
    if (this.formaPago.formapago.toString() === GV.formasPago.tarjeta) {
      if (this.formCard.valid || !this.saveEdit) {
        url = url.replace('${cvc}', this.cvc);
        this.submitedFormasPago = false;
        this.formaPago.nombre = this.nombreTajeta;
        this.formaPago.mesexpira = this.mesAnio.substr(0, 2);
        this.formaPago.anioexpira = this.mesAnio.substr(2, 4);
        if (this.saveEdit) {
          this.functionService.httpPost(url, this.formaPago).then(() => {
            this.clear();
          });
        } else {
          let urlEdit = WS.formasPago.update;
          urlEdit = urlEdit.replace('${formaPagoId}', this.selected[0].id);
          urlEdit = urlEdit.replace('${idUser}', this.dataSesion['user_id']);
          this.formaPago.mesexpira = '0';
          this.formaPago.anioexpira = '0';
          this.formaPago.numtar = '0';
          this.functionService.httpPut(urlEdit, this.formaPago).then(() => {
            this.clear();
          });
        }
      } else {
        this.submitedFormasPago = true;
      }
    } else {
      url = url.replace('${cvc}', '0');
      this.formaPago.tipotar = null;
      if (this.saveEdit) {
        this.functionService.httpPost(url, this.formaPago).then(() => {
          this.clear();
        });
      } else {
        let urlEdit = WS.formasPago.update;
        urlEdit = urlEdit.replace('${formaPagoId}', this.selected[0].id);
        urlEdit = urlEdit.replace('${idUser}', this.dataSesion['user_id']);
        this.formaPago.mesexpira = '0';
        this.formaPago.anioexpira = '0';
        this.formaPago.numtar = '0';
        this.functionService.httpPut(urlEdit, this.formaPago).then(() => {
          this.clear();
        });
      }
    }
  }

  openConfirmationDialog() {
    const text = this.labels.messages.inactivate + ' ' + this.labels.employees.employee + '.  ' + this.labels.messages.inactivateConfirm;
    this.confirmationDialogService.confirm(this.labels.general.confirm.toLocaleUpperCase(), text)
      .then((confirmed) => {
        if (confirmed) {
          this.save();
        }
      }).catch(() =>
      console.log('Dismissed.)'
      ));
  }

  clear() {
    this.formaPago = {
      'id': '',
      'formapago': '',
      'numtar': '',
      'token': '',
      'nombre': '',
      'anioexpira': '',
      'mesexpira': '',
      'tipotar': '',
      'principal': '',
      'estado': '',
      'fecult': '',
      'codusu': '',
      'status': '',
      'ipMaquina': '',
      'user': null
    };
    this.saveEdit = true;
    this.check = false;
    this.viewPaymentz = true;
    this.submitedFormasPago = false;
    this.formCard.controls.nom.setValue('');
    this.formCard.controls.num.setValue('');
    this.formCard.controls.mon.setValue('');
    this.formCard.controls.cvc.setValue('');
    this.formaPago.formapago = this.responseList[0][0].id;
    this.formaPago.estado = this.responseList[1].content[0].id;
    this.formaPago.tipotar = this.responseList[2].content[0].desElemento;
    this.setDataPagination(0, 5);
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function(d) {
      return d.nomCompleto.toLowerCase().indexOf(val) !== -1 || d.empresa.nombreEmpresa.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows = temp;
    this.table.offset = 0;
  }

  deleteFormaPago() {
    let url = WS.formasPago.delete;
    url = url.replace('${formaPagoId}', this.selected[0].id);
    url = url.replace('${userId}', this.dataSesion['user_id']);
    url = url.replace('${token}', this.selected[0].token);
    this.functionService.httpDelete(url).then(() => {
      this.clear();
    });
  }

  onSelect({selected}) {
    this.saveEdit = false;
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    this.formaPago.formapago = this.selected[0].formapago;
    document.getElementById('nomtar').setAttribute('disabled', 'true');
    document.getElementById('numtar').setAttribute('disabled', 'true');
    document.getElementById('yeartar').setAttribute('disabled', 'true');
    document.getElementById('cvctar').setAttribute('disabled', 'true');
    this.check = this.selected[0].principal.toString() === '1' ? true : false;
    this.formaPago.estado = this.selected[0].estado;
    if (this.selected[0].formapago.toString() === GV.formasPago.tarjeta.toString()) {
      this.viewPaymentz = false;
      this.nombreTajeta = this.selected[0].nombre;
      this.formaPago.numtar = this.selected[0].numtar.toString() + '0000000000';
      this.formaPago.tipotar = this.selected[0].tipotar;
      this.cvc = '---';
      this.mesAnio = this.selected[0].mesexpira + ' / ' + this.selected[0].anioexpira;
    } else {
      this.viewPaymentz = true;
      this.nombreTajeta = '';
      this.formaPago.numtar = '';
      this.formaPago.tipotar = '';
      this.cvc = '';
    }
  }

  loadData() {
    this.page.pageNumber = 0;
    this.setDataPagination(0, this.page.size);
  }

  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    this.setDataPagination(pageInfo.offset, pageInfo.pageSize);
  }

  setDataPagination(pageNumber, size) {
    this.page.pageNumber = pageNumber;
    this.page.size = size;
    let url = WS.formasPago.fetch;
    url = url.replace('${idUser}', this.dataSesion['user_id']);
    url = url.replace('${pageNumber}', pageNumber);
    url = url.replace('${size}', size);
    this.functionService.fetch(url).then((res) => {
      this.temp = res['content'];
      this.rows = res['content'];
      this.page.totalElements = res['totalElements'];
      this.loadingIndicator = false;
    });
  }

  ngOnInit() {
    this.labels = TAGS;
    this.gv = GV;
    this.check = false;
    this.viewPaymentz = true;
    this.formCard = this.formBuilder.group({
      nom: [null, [Validators.required]],
      num: [null, [Validators.required, Validators.minLength(14)]],
      mon: [null, [Validators.required, Validators.minLength(4)]],
      cvc: [null, [Validators.required, Validators.minLength(3)]]
    });
    let wsUsuarioData = WS.formasPago.listaformas;
    this.dataSesion = localStorage.getItem('userToken');
    this.dataSesion = this.functionService.decode(this.dataSesion);
    wsUsuarioData = wsUsuarioData.replace('${celular}', this.dataSesion['cell_phone']);
    const array = [
      wsUsuarioData, WS.general.status, WS.formasPago.tiposTarjetas
    ];
    this.functionService.httpObservableGet(array).then((responseList) => {
      this.formasPago = responseList[0];
      this.estados = responseList[1].content;
      this.tiposTarjetas = responseList[2].content;
      this.responseList = responseList;
      this.functionService.getIpAddress().then((res) => {
        this.ipAddress = res;
      });
      this.clear();
    });
  }
  get fc() { return this.formCard.controls; }
}
