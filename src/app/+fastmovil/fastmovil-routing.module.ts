import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { AuthGuard } from '../services/auth/auth.guard';

// *******************************************************************************
//

import {BillingComponent} from './billing/billing.component';
import {OrderFastmovilComponent} from './order-fastmovil/order-fastmovil.component';
import {PaymentMethodsComponent} from './payment-methods/payment-methods.component';
import {TravelsComponent} from './travels/travels.component';
import {UsersComponent} from './users/users.component';

// *******************************************************************************
//

@NgModule({
  imports: [RouterModule.forChild([
    { path: 'billing', component: BillingComponent, canActivate: [AuthGuard] },
    { path: 'orderfastmovil', component: OrderFastmovilComponent, canActivate: [AuthGuard] },
    { path: 'paymentmethods', component: PaymentMethodsComponent, canActivate: [AuthGuard] },
    { path: 'travels', component: TravelsComponent, canActivate: [AuthGuard] },
    { path: 'usersfast', component: UsersComponent, canActivate: [AuthGuard] }
  ])],
  exports: [RouterModule],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
})
export class FastmovilRoutingModule { }
