import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { TAGS } from '../../TextLabels';
import { WS } from '../../WebServices';
import { GV } from '../../GeneralVars';
import { FunctionService } from '../../services/functions/function.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Page } from '../../components/pagination/page';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { User } from '../../models/user';
import { Asociado } from '../../models/asociado';

const ASOC_LIST_PAGE_PATTERN = '?page=0&size=5&sort=usuarioAsociado.lastName';
const ADD = 1;
const MODIFY = 2;
const REMOVE = 3;

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: [
    '../../../../node_modules/@swimlane/ngx-datatable/release/index.css',
    '../../../vendor/libs/ngx-datatable/ngx-datatable.scss',
    '../../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css',
    '../../../vendor/libs/ngx-toastr/ngx-toastr.scss',
  ],
  encapsulation: ViewEncapsulation.None
})
export class UsersComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;
  public associatedUser: User;
  public selectedAssocUser: User;
  public rows: any[];
  public page: Page;

  constructor(
    private functionService: FunctionService,
    public _domSanitizer: DomSanitizer
  ) {
    this.page = new Page();
  }

  responseList = null;
  labels = null;
  gv = null;
  ipAddress = null;
  loadingIndicator = false;
  selected = [];
  estados = [];
  servicioTrans = {
    'origen': ''
  };
  arrayOrigen1 = {};
  usuarios = {
    'estado': ''
  };
  timeout = null;

  clear() {
    this.usuarios.estado = this.responseList[0].content[0].id;
  }

  updateFilter(event) {
    console.log(event);
  }

  keyUpOrSt1() {
    if (this.servicioTrans.origen.toString() !== '' && this.servicioTrans.origen.toString() !== ' ') {
      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        let url = 'api/v1/usuariosfilter/' + this.servicioTrans.origen.toString();
        if (this.servicioTrans.origen.toString() !== '') {
          this.functionService.getService(url).then((responseList) => {
            this.arrayOrigen1 = responseList;
            console.log(this.arrayOrigen1[0]['email']);
          });
        }
      },500,"JavaScript");
    } else {
      this.servicioTrans.origen = '';
    }
  }

  ngOnInit() {
    this.resetForm();

    this.labels = TAGS;
    this.gv = GV;
    let userId = this.functionService.decode(localStorage.getItem('userToken'))['user_id'];
    const array = [
      WS.general.status, WS.asociados.get.replace('${userId}', userId) + ASOC_LIST_PAGE_PATTERN
    ];
    this.functionService.httpObservableGet(array).then((responseList) => {
      this.estados = responseList[0].content;
      this.setAssociatedUsers(responseList[1])
      this.responseList = responseList;
      this.functionService.getIpAddress().then((res) => {
        this.ipAddress = res;
      });
      this.clear();
    });
  }

  /**
   * METODO PARA ESTABLECER A UN USUARIO ASOCIADO COMO INACTIVO
   */
  public activateDeactivateAssocUser(deactivate: boolean = true) {
    if (this.selectedAssocUser) {
      let userId = this.functionService.decode(localStorage.getItem('userToken'))['user_id'];
      let url = WS.asociados.activate_deactivate.replace("${userId}", userId).replace("${assocUserId}", this.selectedAssocUser.id);

      if(deactivate) {
        this.functionService.httpDelete(url)
        .then((respData: any) => {
          console.log("respData", respData);
          this.rows = this.addModifDelAsocUserToTable(this.rows.slice(), respData, MODIFY);
        }).catch(err => console.log("[USERS COMPONENT] - ERROR", err));
      }
      else {
        this.functionService.httpPut(url, null)
        .then((respData: any) => {
          console.log("respData", respData);
          this.rows = this.addModifDelAsocUserToTable(this.rows.slice(), respData, MODIFY);
        }).catch(err => console.log("[USERS COMPONENT] - ERROR", err));
      }
    }
    else {
      alert("seleccione un usuario de la tabla primero");
    }
  }

  /**
   * METODO PARA VACIAR LOS CAMPOS DEL FORMULARIO, MEDIANTE DATA BINDING:
   * AUTOR: FREDI ROMAN
   */
    public resetForm() {
    this.associatedUser = new User('', '', '', '', '');
  }

  /**
   * METODO PARA PROCESAR EL REGISTRO DE UN NUEVO ASOCIADO
   * AUTOR: FREDI ROMAN
   */
  public addAssocUser() {
    let userId = this.functionService.decode(localStorage.getItem('userToken'))['user_id'];
    let url = WS.asociados.post.replace("${userId}", userId);
    let asociado = new Asociado('', null, this.associatedUser);

    this.functionService.httpPost(url, asociado)
      .then((respData: any) => {
        this.rows = this.addModifDelAsocUserToTable(this.rows.slice(), respData);
        this.resetForm();
      }).catch(err => console.log("[USERS COMPONENT] - ERROR", err));
  }

  /**
   * METODO PARA CAPTAR EL OBJETO USUARIO, FILTRADO DESDE EL AUTOCOMPLETE
   * AUTOR: FREDI ROMAN
   * @param event
   */
  public getFilteredData(event: User) {
    this.associatedUser = event;
  }

  /************************************************************************************
  *************************************************************************************
  * METODOS PARA DEFINIR LA APARIENCIA DE LA TABLA DE USUARIOS ASOCIADOS
  * AUTOR: FREDI ROMAN
  *************************************************************************************
  ************************************************************************************/

  /**
   * METODO PARA CAPTURAR EL OBJETO DE LA FILA SELECCIONADA
   * AUTOR: JUAL MOLINA
   * @param event
   */
  public onSelect(event: any) {
    this.selectedAssocUser = new User(event.selected[0].id, event.selected[0].email, event.selected[0].firstName, event.selected[0].lastName, event.selected[0].phone);
  }

  /**
   * METODO PARA DEFINIR LA PAGINACION DE LA TABLA
   * AUTOR: FREDI ROMAN
   */
  private defineTablePagination() {

  }

  /**
   * METODO PARA OBTENER LA LISTA DE USUARIOS ASOCIADOS A LA CUENTA DEL USUARIO LOGEADO
   * AUTOR: FREDI ROMAN
   */
  private setAssociatedUsers(asocUsersData) {
    let auxRows: any = this.rows ? this.rows.slice() : [];

    for (let asoc of asocUsersData.content) {
      this.addModifDelAsocUserToTable(auxRows, asoc);
    }
    this.rows = auxRows;
  }

  /**
   * METODO PARA AGREGAR UN NUEVO USUARIO A LA TABLA DE ASOCIADOS:
   */
  private addModifDelAsocUserToTable(asscoUserList: any[], assocUser: any, option: number = ADD) {
    if (asscoUserList.length > 0) {
      let index;
      switch (option) {
        case ADD:
          asscoUserList.push(assocUser.usuarioAsociado);
          asscoUserList[asscoUserList.length - 1]["estado"] = assocUser.estado;
          break;
        case MODIFY:
          index = asscoUserList.findIndex(assoU => assoU.id == assocUser.usuarioAsociado.id);
          asscoUserList.splice(index, 1, assocUser.usuarioAsociado);
          asscoUserList[index]["estado"] = assocUser.estado;
          break;
        case REMOVE:
          index = asscoUserList.findIndex(asocU => asocU.id == assocUser.usuarioAsociado.id);
          asscoUserList.splice(index, 1);
          break;
      }
    }
    else {
      if (option == ADD) {
        asscoUserList.splice(0, 0, assocUser.usuarioAsociado);
        asscoUserList[0]["estado"] = assocUser.estado;
      }

    }
    return asscoUserList;
  }

  /************************************************************************************
  ************************************************************************************/
}
