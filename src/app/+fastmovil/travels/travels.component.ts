import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {TAGS} from '../../TextLabels';
import {WS} from '../../WebServices';
import { GV } from '../../GeneralVars';
import { FunctionService } from '../../services/functions/function.service';
import { DomSanitizer } from '@angular/platform-browser';
import {AppService} from '../../app.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-travels',
  templateUrl: './travels.component.html',
  styleUrls: [
    '../../../../node_modules/@swimlane/ngx-datatable/release/index.css',
    '../../../vendor/libs/ngx-datatable/ngx-datatable.scss',
    '../../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css',
    '../../../vendor/libs/ngx-toastr/ngx-toastr.scss',
    'travels.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TravelsComponent implements OnInit {

  constructor(
    private functionService: FunctionService,
    private appService: AppService,
    public toastrService: ToastrService,
    public _domSanitizer: DomSanitizer
  ) { }

  options_notitications = {
    tapToDismiss: true,
    closeButton: false,
    progressBar: true,
    positionClass: 'toast-top-right',
    rtl: this.appService.isRTL
  };

  banViajesAnteriores = false;
  banReservasRealizadas = false;
  responseList = null;
  labels = null;
  rowsActiva = null;
  tempActiva = null;

  rowsReserva = null;
  tempReserva = null;

  rowsAnterior = null;
  tempAnterior = null;

  loadingIndicatorActiva = true;

  loadingIndicatorReserva = true;

  loadingIndicatorAnterior = true;

  selected = [];
  banSave = false;
  ipAddress = null;
  gv = null;
  viewCancelBtn = false;
  estados = [];
  inconvenientes = [];
  subinconvenientes = [];
  dataSesion = null;
  fechaActivo = null;
  fechaReserva = null;
  fechaAnterior = null;
  viajes = {
    'estado': ''
  };
  servicioTrans = {
    'autorizacion': '',
    'id': 0,
    'nombreUsuario': '',
    'nombreAsociado': '',
    'fechaHoraServicio': '',
    'origen': '',
    'destino': '',
    'estadoservicio': 0,
    'tarifa': 0,
    'formapago': 0,
    'tipotar': '',
    'numtar': 0,
    'nvoucher': 0,
    'telefono': '',
    'unidad': '',
    'ciudad': 0,
  };
  unidad = {
    'ciudad': '',
    'codigounidad': '',
    'placa': '',
    'marca': '',
    'modelo': '',
    'color': '',
    'nombreChofer': '',
    'celularChofer': ''
  };

  // changeCalification(val) {
  //   this.calificaviaje.calificacion = val;
  //   console.log(this.calificaviaje.calificacion);
  // }

  onChangeDateActiva(event) {
    if (event) {
      const fec = event['year'] + '-' + (event['month'] < 10 ? ('0' + event['month']) : event['month']) + '-'
        + (event['day'] < 10 ? ('0' + event['day']) : event['day']);
      const temp = this.tempActiva.filter(function(d) {
        return d.fechaHoraServicio.indexOf(fec) !== -1 || !fec;
      });
      this.rowsActiva = temp;
    } else {
      this.rowsActiva = this.tempActiva;
    }
  }

  onChangeDateReserva(event) {
    if (event) {
      const fec = event['year'] + '-' + (event['month'] < 10 ? ('0' + event['month']) : event['month']) + '-'
        + (event['day'] < 10 ? ('0' + event['day']) : event['day']);
      const temp = this.tempReserva.filter(function(d) {
        return d.fechaHoraServicio.indexOf(fec) !== -1 || !fec;
      });
      this.rowsReserva = temp;
    } else {
      this.rowsReserva = this.tempReserva;
    }
  }

  onChangeDateAnterior(event) {
    if (event) {
      const fec = event['year'] + '-' + (event['month'] < 10 ? ('0' + event['month']) : event['month']) + '-' +
        (event['day'] < 10 ? ('0' + event['day']) : event['day']);
      const temp = this.tempAnterior.filter(function(d) {
        return d.fechaHoraServicio.indexOf(fec) !== -1 || !fec;
      });
      this.rowsAnterior = temp;
    } else {
      this.rowsAnterior = this.tempAnterior;
    }
  }

  updateFilterActiva(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.tempActiva.filter(function(d) {
      return d.nombreUsuario.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rowsActiva = temp;
  }

  updateFilterReserva(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.tempReserva.filter(function(d) {
      return d.nombreUsuario.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rowsReserva = temp;
  }

  updateFilterAnterior(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.tempAnterior.filter(function(d) {
      return d.nombreUsuario.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rowsAnterior = temp;
  }

  cancelTravel() {
    const updtContext = {
      'tarifa': '',
      'nvoucher': '',
      'motivocancelacion': '',
      'estadoservicio': GV.estadosCarreras.cancelada
    };
    let url = WS.travels.canceltravel;
    url = url.replace('${autorizacion}', this.servicioTrans.autorizacion);
    this.functionService.httpPut(url, updtContext).then((response) => {
      console.log(response);
      if (response['estadoservicio'].toString() === GV.estadosCarreras.activa.toString()) {
        this.toastrService['error']('Tiene una carrera ACTIVA no puede cancelar la misma.',
          this.appService.pageTitle, this.options_notitications);
      } else {
        this.clear();
        this.fetchReservas();
      }
    });
  }

  onSelect({ selected }) {
    this.banSave = true;
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    this.servicioTrans = this.selected[0];
    let url = WS.travels.unidad;
    url = url.replace('${codigo}', this.servicioTrans.unidad);
    url = url.replace('${ciudad}', this.servicioTrans.ciudad.toString());
    if (this.servicioTrans.estadoservicio.toString() === GV.estadosCarreras.pendiente &&
    this.banReservasRealizadas) {
      this.banViajesAnteriores = true;
    } else {
      this.banViajesAnteriores = false;
    }
    if (this.servicioTrans.unidad.toString() !== '' && this.servicioTrans.ciudad.toString() !== '0') {
      this.functionService.httpGet(url).then((response) => {
        if (response && response.toString().toUpperCase() !== 'UNDEFINED') {
          this.unidad.color = response['color'];
          this.unidad.nombreChofer = response['nombreChofer'];
          this.unidad.placa = response['placa'];
          this.unidad.modelo = response['modelo'];
          this.unidad.marca = response['marca'];
          this.unidad.codigounidad = response['pkunidad']['codigounidad'];
        }
      });
    } else {
      this.unidad = {
        'ciudad': '',
        'codigounidad': '',
        'placa': '',
        'marca': '',
        'modelo': '',
        'color': '',
        'nombreChofer': '',
        'celularChofer': ''
      };
    }
  }

  fetchActivas() {
    let url = WS.travels.fetchActivas;
    url = url.replace('${idUser}', this.dataSesion['user_id']);
    this.functionService.fetch(url).then((res) => {
      this.tempActiva = res['content'];
      this.rowsActiva = res['content'];
      this.loadingIndicatorActiva = false;
    });
  }

  fetchReservas() {
    let url = WS.travels.fetchReservas;
    url = url.replace('${idUser}', this.dataSesion['user_id']);
    this.functionService.fetch(url).then((res) => {
      this.tempReserva = res['content'];
      this.rowsReserva = res['content'];
      this.loadingIndicatorReserva = false;
    });
  }

  fetchAnteriores() {
    let url = WS.travels.fetchAnteriores;
    url = url.replace('${idUser}', this.dataSesion['user_id']);
    this.functionService.fetch(url).then((res) => {
      this.tempAnterior = res['content'];
      this.rowsAnterior = res['content'];
      this.loadingIndicatorAnterior = false;
    });
  }

  clear() {
    this.banSave = false;
    this.servicioTrans = {
      'autorizacion': '',
      'id': 0,
      'nombreUsuario': '',
      'nombreAsociado': '',
      'fechaHoraServicio': '',
      'origen': '',
      'destino': '',
      'estadoservicio': 0,
      'tarifa': 0,
      'formapago': 0,
      'tipotar': '',
      'numtar': 0,
      'nvoucher': 0,
      'telefono': '',
      'unidad': '',
      'ciudad': 0,
    };
    this.unidad = {
      'ciudad': '',
      'codigounidad': '',
      'placa': '',
      'marca': '',
      'modelo': '',
      'color': '',
      'nombreChofer': '',
      'celularChofer': ''
  };
    this.viajes.estado = this.responseList[0].content[0].id;
    this.banViajesAnteriores = false;
    this.getSubInconvenientes();
  }

  viajesAnteriores() {
    this.clear();
    this.banViajesAnteriores = true;
    this.banReservasRealizadas = false;
    console.log('VIajes Anteriores');
    this.fetchAnteriores();
  }

  viajesActivos() {
    this.clear();
    this.banViajesAnteriores = false;
    this.banReservasRealizadas = false;
    console.log('VIajes Activos');
    this.fetchActivas();
  }

  reservasRealizadas() {
    this.clear();
    this.banViajesAnteriores = false;
    this.banReservasRealizadas = true;
    console.log('Reservar Realizadas');
    this.fetchReservas();
  }

  getSubInconvenientes() {
    let url = WS.inconvenietes.subinconveniente;
    url = url.replace('${codigo}', this.inconvenientes[0].codSubcatalogo);
    this.functionService.httpGetWithout(url).then((response) => {
      this.subinconvenientes = response['content'];
    });
  }

  ngOnInit() {
    this.banSave = false;
    this.dataSesion = localStorage.getItem('userToken');
    this.dataSesion = this.functionService.decode(this.dataSesion);
    this.banViajesAnteriores = false;
    this.banReservasRealizadas = false;
    this.labels = TAGS;
    this.gv = GV;
    this.viewCancelBtn = false;
    const array = [
      WS.general.status, WS.inconvenietes.inconveniences
    ];
    this.functionService.httpObservableGet(array).then((responseList) => {
      this.estados = responseList[0].content;
      this.inconvenientes = responseList[1].content;
      this.responseList = responseList;
      this.functionService.getIpAddress().then((res) => {
        this.ipAddress = res;
      });
      this.functionService.getIpAddress().then((res) => {
        this.ipAddress = res;
      });
      this.clear();
      this.fetchActivas();
    });
  }

}
