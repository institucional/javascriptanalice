import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';

// *******************************************************************************
//

import { FastmovilRoutingModule } from './fastmovil-routing.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxMaskModule } from 'ngx-mask';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TextMaskModule } from 'angular2-text-mask';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';

// *******************************************************************************
// Page components

import { AuthInterceptor } from '../services/interceptors/auth.service';
import {BillingComponent} from './billing/billing.component';
import {OrderFastmovilComponent} from './order-fastmovil/order-fastmovil.component';
import {PaymentMethodsComponent} from './payment-methods/payment-methods.component';
import {TravelsComponent} from './travels/travels.component';
import {UsersComponent} from './users/users.component';
import { SearchUserComponent } from './search-user/search-user.component';

@NgModule({
  imports: [
    CommonModule,
    FastmovilRoutingModule,
    NgxDatatableModule,
    NgxMaskModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    TextMaskModule,
    HttpClientModule,
    NgxMapboxGLModule.withConfig({
      accessToken: 'pk.eyJ1IjoiYXF1aW50YW5hciIsImEiOiJjam5ldm56MjgwcWwzM3FueWV5aGt2ZXhvIn0.EF7ZnmtFwoWcrBx0SsPJZw'
    })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  declarations: [
    BillingComponent,
    OrderFastmovilComponent,
    PaymentMethodsComponent,
    TravelsComponent,
    UsersComponent,
    SearchUserComponent
  ]
})
export class FastmovilModule { }
