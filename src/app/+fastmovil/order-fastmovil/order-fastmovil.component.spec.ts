import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderFastmovilComponent } from './order-fastmovil.component';

describe('OrderFastmovilComponent', () => {
  let component: OrderFastmovilComponent;
  let fixture: ComponentFixture<OrderFastmovilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderFastmovilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderFastmovilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
