import {Component, OnInit, ViewEncapsulation, HostListener, ViewChild} from '@angular/core';
import {environment} from '../../../environments/environment';
import {FunctionService} from '../../services/functions/function.service';
import {WS} from '../../WebServices';
import {GV} from '../../GeneralVars';
import {HttpClient} from '@angular/common/http';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import L from 'leaflet';
import {TAGS} from '../../TextLabels';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {SpinnerDialogService} from '../../components/spinner-dialog/spinner-dialog.service';
import {ToastrService} from 'ngx-toastr';
import {AppService} from '../../app.service';
import {ConfirmationDialogService} from '../../components/confirmation-dialog/confirmation-dialog.service';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import {AuthService} from '../../services/auth/auth.service';
import {DomSanitizer} from '@angular/platform-browser';
import {Page} from '../../components/pagination/page';
import {CalificaviajeService} from '../../components/calificaviaje/calificaviaje.service';
import { Router, Event, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';

@Component({
  selector: 'app-order-fastmovil',
  templateUrl: './order-fastmovil.component.html',
  styleUrls: [
    '../../../../node_modules/@swimlane/ngx-datatable/release/index.css',
    '../../../vendor/libs/ngx-datatable/ngx-datatable.scss',
    '../../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css',
    '../../../vendor/libs/ngx-toastr/ngx-toastr.scss',
    './order-fastmovil.css'
  ],
  encapsulation: ViewEncapsulation.None
})
export class OrderFastmovilComponent implements OnInit {

  constructor(
    private confirmationDialogService: ConfirmationDialogService,
    private calificaviajeservice: CalificaviajeService,
    private appService: AppService,
    private functionService: FunctionService,
    private http: HttpClient,
    private modalService: NgbModal,
    private spinner: SpinnerDialogService,
    public toastrService: ToastrService,
    private authService: AuthService,
    public _domSanitizer: DomSanitizer,
    private router: Router
  ) {
    router.events.subscribe( (event: Event) => {

      if (event instanceof NavigationStart) {
        if (this.router.url.toString().indexOf('orderfastmovil') !== -1 ) {
          console.log('true');
          if (this.websocket) {
            this.websocket.close();
          }
          // this.stompClient.end();
        } else {
          console.log('false');
        }
      }
    });
  }

  @ViewChild(DatatableComponent) table: DatatableComponent;
  page = new Page();

  fechaInicial = { year: 2017, month: 8, day: 8 };
  fechaServicioReserva = { year: 2017, month: 8, day: 8 };

  options_notitications = {
    tapToDismiss: true,
    closeButton: false,
    progressBar: true,
    positionClass: 'toast-top-right',
    rtl: this.appService.isRTL
  };

  citySend = '';

  private serverUrl = environment.socketHost;
  private stompClient;
  mensaje = '';
  banInit = true;
  origenText = '';
  websocket = null;
  valorUnit = 0;
  imagePath = 'assets/img/avatars/user-default.jpg';

  contCarreraActiva = 0;
  distancia = '';
  tiempo = '';
  banCancelTravel = false;
  banSolicitarPin = true;
  banviewemployee = false;
  banBtnInf1 = false;
  banArea = false;
  banDivision = false;
  banSubdiv = false;
  banUnidad = false;
  banSendMsg = true;
  velocidad = 0;
  responseList = null;
  ipAddress = null;
  modalRef = null;
  modalRefFormas = null;
  map = null;
  layer = null;
  layerCar = null;
  layerUbication = null;
  markerCar = null;
  markerOrigen = null;
  markerCarDestino = null;
  gv = null;
  labels = null;
  reservaCheck: Boolean = false;
  estados = [];
  formasPago = [];
  asociados = [];
  rows = [];
  loadingIndicator = true;
  selected = [];
  arrayOrigen1 = {};
  arrayDestino1 = {};
  nombreCompleto = '';
  timeout = null;
  time = 0;
  tiempoEspera = 0;
  contGetUnity = 0;
  dataSesion = null;
  countInit = 0;
  valGeneral = 0;
  unidadAsignada = false;
  banBtnInf2 = false;
  timeReserva = {hour: 12, minute: 0};
  destino = {
    'calle': ''
  };
  origenActPen = '';
  carreraPedida = false;
  servicioTransOtro = {
    'formaPago': '',
    'idUsrAsociado': '',
    'origen': '',
    'destino': '',
    'reserva': '',
    'observacion': ''
  };
  servicioTrans = {
    'idServicio': '',
    'idUsuario': '',
    'idUsrAsociado': '',
    'idconvenio': '',
    'idempresa': '',
    'idEmpleado': '',
    'idPin': '',
    'telefono': '',
    'telefonocelular': '',
    'fechaServicio': null,
    'horaServicio': '',
    'unidad': '',
    'nombreUnidad': '',
    'nombreCliente': '',
    'identificacionCliente': '',
    'formaPago': '',
    'tarifa': '',
    'tarifahora': null,
    'tarifapeaje': null,
    'autorizacion': '',
    'nvoucher': '',
    'origen': '',
    'destino': '',
    'operador': '',
    'nlote': '',
    'latitud': 0,
    'longitud': 0,
    'latitudDest': 0,
    'longitudDest': 0,
    'reserva': '',
    'ciudad': '',
    'motivocancelacion': '',
    'estadoservicio': '',
    'observacion': '',
    'msgbeepe': null,
    'valido':  null,
    'codpuntea':  null,
    'opefac':  null,
    'tiempoespera':  null,
    'tdestino':  null,
    'operador2':  null,
    'costochofer':  null,
    'kmodometro':  null,
    'horaaceptado':  null,
    'horaocupado':  null,
    'horafin':  null,
    'horallegada':  null,
    'estado': '',
    'codusu': '',
    'fecult': '',
    'status': '',
    'ipMaquina': ''
  };
  unidad = {
    'ciudad': '',
    'codigounidad': '',
    'placa': '',
    'marca': '',
    'modelo': '',
    'color': '',
    'nombreChofer': '',
    'celularChofer': ''
  };
  usuario = {
    'codusu': 1,
    'fecult': '',
    'status': null,
    'estado': null,
    'ipMaquina': '',
    'id': 0,
    'email': '',
    'firstName': '',
    'lastName': '',
    'phone': '',
    'level': null,
    'verificationCode': null
  };
  empleado = {
    'empresa': '',
    'codEmpleado': '',
    'nomCompleto': '',
    'nomsucursal': '',
    'nomarea': '',
    'nomdivision': '',
    'nomsubdivision': '',
    'nomunidad': ''
  };
  calificaviaje = {
    'id': '',
    'idServicio': '',
    'calificacion': '0',
    'inconveniente': '',
    'subinconveniente': '',
    'observacion': '',
    'estado': '',
    'fecult': '',
    'codusu': '',
    'status': '',
    'ipMaquina': ''
  };

  openNav() {
    document.getElementById('mySidenav').style.width = '350px';
  }

  closeNav() {
    document.getElementById('mySidenav').style.width = '0';
  }

  deleteFavorito(event, cell, rowIndex) {
    let url = WS.favoritos.delete;
    url = url.replace('${favoritoId}', cell['id']);
    this.functionService.httpDelete(url).then(() => {
      this.openModal(null, false);
      this.clear();
    });
  }

  solicitarPin() {
    console.log('Solicitar PIN');
    let url = WS.solicitarTaxi.getPin;
    url = url.replace('${autorizacion}', this.servicioTrans.autorizacion);
    this.functionService.httpGet(url).then(() => {
      this.toastrService['info']('Solicitud de PIN enviada.', this.appService.pageTitle, this.options_notitications);
    });
  }

  checkElement(event, cell, rowIndex) {
    if (event.target.checked) {
      if (this.layer) {
        this.map.removeLayer(this.layer);
      }
      if (this.layerCar) {
        this.map.removeLayer(this.layerCar);
      }
      if (this.markerCar) {
        this.map.removeLayer(this.markerCar);
      }
      if (this.markerOrigen) {
        this.map.removeLayer(this.markerOrigen);
      }
      if (this.markerCarDestino) {
        this.map.removeLayer(this.markerCarDestino);
      }
      this.servicioTrans.origen = cell['origen'];
      this.servicioTrans.destino = cell['destino'];
      this.centerLocation();
      if (this.servicioTrans.origen != "" && this.servicioTrans.destino != "") {
        this.blur();
      } else {
        this.blurMap();
      }
      this.closeModal();
    }
  }

  agregarFavorito(param) {
    if (this.servicioTrans.origen === '') {
      if (param == '1') {
        this.toastrService['error']('Debe agregar el origen.',
          this.appService.pageTitle, this.options_notitications);
        return;
      } else {
        return;
      }
    }
    let url = WS.favoritos.save;
    url = url.replace('${idusuario}', this.dataSesion['user_id']);
    url = url.replace('${param}', param);
    const favoritoContext = {
      origen: this.servicioTrans.origen,
      destino: this.servicioTrans.destino,
      latorigen: this.servicioTrans.latitud,
      longorigen: this.servicioTrans.longitud,
      latdestino: this.servicioTrans.latitudDest,
      longdestino: this.servicioTrans.longitudDest,
      codusu: this.dataSesion['user_id'],
      status: '1',
      estado: GV.state.active,
      ipMaquina: this.ipAddress
    };
    if (param === '1') {
      this.spinner.openSpinner();
    }
    this.http.post(url, favoritoContext).subscribe(
      res => {
        if (param === '1') {
          this.spinner.closeSpinner();
        }
        if (param === '1') {
          this.toastrService['success']('Favorito agregado de manera correcta.',
            this.appService.pageTitle, this.options_notitications);
        }
      },
      err => {
        if (param === '1') {
          this.spinner.closeSpinner();
        }
        console.log(err);
      }
    );
  }

  openCalificaViaje() {
    const text = 'Por favor califique la calidad del servicio. GRACIAS.';
    this.calificaviajeservice.confirm('CALIFICACIÓN', text)
      .then((result) => {
        console.log(result);
        if (result['boolean'] && result['calificacion'] !== '0') {
          let url = WS.travels.saveCalification;
          url = url.replace('${autorizacion}', this.servicioTrans.autorizacion);
          this.calificaviaje.estado = GV.state.active;
          this.calificaviaje.ipMaquina = this.ipAddress;
          this.calificaviaje.status = '1';
          this.calificaviaje.codusu = this.dataSesion['user_id'];
          this.calificaviaje.fecult = this.functionService.getToday();
          this.calificaviaje.idServicio = '';
          this.calificaviaje.calificacion = result['calificacion'];
          this.calificaviaje.inconveniente = '127';
          this.calificaviaje.subinconveniente = '146';
          this.functionService.httpPost(url, this.calificaviaje).then(() => {
            this.clear();
          });
        } else {
          this.clear();
        }
      }).catch(() => {
        console.log('Dismissed.');
        this.clear();
      });
  }

  initializeWebSocketConnection() {
    this.websocket = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(this.websocket);
    let that = this;
    this.stompClient.connect({}, (frame) => {
      console.log(frame);
      that.stompClient.subscribe('/location/' + this.dataSesion['user_id'], (message) => {
        console.log(message);
        this.getMessageApi(message);
      });
      that.stompClient.subscribe('/getunit/' + this.dataSesion['user_id'], (message) => {
        console.log(message);
        this.time = 0;
        this.toastrService['info']('Solicitando Unidad. Autorizacion: ' + message.body, this.appService.pageTitle, this.options_notitications);
        this.servicioTrans.autorizacion = message.body;
        this.getUnity(message.body, this.valorUnit);
      });
      console.log(frame);
    }, (message) => {
      // this.toastrService['error']('Servidor desconectado. Inicie nuevamente.', this.appService.pageTitle, this.options_notitications);
      // this.authService.logout();
      console.log('Servidor Desconectado.');
    });
  }

  getMessageApi(message) {
    this.banSendMsg = false;
    if (message.body) {
      var obj = JSON.parse(message.body);
      const userId = obj.uid.toString();
      const uid = this.dataSesion['user_id'];
      if (userId.toString() === uid.toLocaleString()) {
        this.carreraPedida = true;
        console.log('Ingreso');

        if (obj.unidad) {
          this.unidadAsignada = true;
          this.unidad.celularChofer = '0' + obj.unidad.celularChofer;
          this.unidad.codigounidad = obj.unidad.pkunidad.codigounidad;
          this.unidad.nombreChofer = obj.unidad.nombreChofer;
          this.unidad.placa = obj.unidad.placa;
          this.unidad.marca = obj.unidad.marca;
          this.unidad.modelo = obj.unidad.modelo;
          this.unidad.color = obj.unidad.color;
          this.velocidad = obj.ruta.velocidad;
          if (this.servicioTrans.ciudad == GV.ciudadesFastline.guayaquil) {
            this.imagePath = GV.urlsfotos.guayaquil + obj.unidad.identificacion + '.jpg';
          } else {
            this.imagePath = GV.urlsfotos.quito + obj.unidad.identificacion + '.jpg';
          }
        }
        console.log(obj);
        this.servicioTrans.autorizacion = obj.atorizacion;
        if (obj.estadoViaje.toString() === GV.estadosCarreras.pendiente.toString()) {
          this.addCar(obj.ruta.latitud, obj.ruta.longitud, obj.latitud, obj.longitud, '#F9B100', true);
          this.banCancelTravel = false;
          this.servicioTrans.origen = this.origenActPen;
          if (obj.formapago.toString() === GV.formasPago.voucherElectronico ||
            obj.formapago.toString() === GV.formasPago.tarjeta) {
            this.banSolicitarPin = true;
          } else {
            this.banSolicitarPin = false;
          }
        } else if (obj.estadoViaje.toString() === GV.estadosCarreras.activa.toString()) {
          this.servicioTrans.origen = this.origenActPen;
          if (this.contCarreraActiva === 0) {
            this.countInit = 0;
            this.toastrService['info']('Carrera INICIADA.', this.appService.pageTitle, this.options_notitications);
          }
          this.contCarreraActiva++;
          this.banCancelTravel = true;
          if (this.layer) {
            this.map.removeLayer(this.layer);
          }
          if (obj.latitudDest.toString() !== '' && obj.latitudDest !== null && obj.latitudDest.toString() !== '0' &&
            obj.longitudDest.toString() !== '' && obj.longitudDest !== null && obj.longitudDest.toString() !== '0') {
            this.addCar(obj.ruta.latitud, obj.ruta.longitud, obj.latitudDest, obj.longitudDest, '#2D2E7E', false);
          } else {
            this.addCar(obj.ruta.latitud, obj.ruta.longitud, obj.ruta.latitud, obj.ruta.longitud, '#2D2E7E', false);
          }
          this.banSolicitarPin = false;
        } else if (obj.estadoViaje.toString() === GV.estadosCarreras.cancelada.toString()) {
          this.banCancelTravel = true;
          this.unidadAsignada = false;
          this.banBtnInf2 = false;
          this.carreraPedida = false;
          this.countInit = 0;
          this.banSolicitarPin = false;
          this.toastrService['info']('Carrera CANCELADA.', this.appService.pageTitle, this.options_notitications);
          this.origenText = 'CANCELADA';
          this.clear();
        } else {
          this.banCancelTravel = true;
          this.unidadAsignada = false;
          this.banBtnInf2 = false;
          this.carreraPedida = false;
          this.banSolicitarPin = false;
          this.countInit = 0;
          this.toastrService['success']('Carrera FINALIZADA.', this.appService.pageTitle, this.options_notitications);
          this.origenText = 'FINALIZADA';
          this.clear();
          // this.openCalificaViaje();
        }
      }
    }
  }

  openConfirmationDialog() {
    const options = this.options_notitications;
    options['timeOut'] = this.tiempoEspera.toString();
    const text = 'Puede esperar mas tiempo hasta encontrar un conductor o si desea puede cancelar el viaje.';
    this.confirmationDialogService.confirm(this.labels.general.confirm.toLocaleUpperCase(), text)
      .then((confirmed) => {
        if (confirmed) {
          this.time = 0;
          this.toastrService['info']('Solicitando Unidad. Autorizacion: ' + this.servicioTrans.autorizacion, this.appService.pageTitle, options);
          this.getUnity(this.servicioTrans.autorizacion, this.valGeneral);
        } else {
          this.cancelTravel();
          this.toastrService['info']('Viaje Cancelado. Autorizacion: ' + this.servicioTrans.autorizacion, this.appService.pageTitle, options);
          this.time = 0;
        }
      }).catch(() => {
      this.time = 0;
      this.cancelTravel();
      this.toastrService['info']('Viaje Cancelado. Autorizacion: ' + this.servicioTrans.autorizacion, this.appService.pageTitle, options);
    });
  }

  keyUpOrSt1Otr() {
    this.servicioTransOtro.origen = this.servicioTransOtro.origen.toUpperCase();
    if (this.servicioTransOtro.origen.toString() !== '' && this.servicioTransOtro.origen.toString() !== ' ') {
      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        let url = WS.geo.findStreetsCom;
        if (this.servicioTransOtro.origen.toString() !== '') {
          url = url.replace('${street}', this.servicioTransOtro.origen);
          url = url.replace('${ciudad}', this.citySend);
          this.functionService.getService(url).then((responseList) => {
            this.arrayOrigen1 = responseList;
          });
        }
      }, 500, 'JavaScript');
    } else {
      this.servicioTransOtro.origen = '';
    }
  }

  keyUpOrSt3Otr() {
    this.servicioTransOtro.destino = this.servicioTransOtro.destino.toUpperCase();
    if (this.servicioTransOtro.destino.toString() !== '' && this.servicioTransOtro.destino.toString() !== ' ') {
      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        let url = WS.geo.findStreetsCom;
        if (this.servicioTransOtro.destino.toString() !== '') {
          url = url.replace('${street}', this.servicioTransOtro.destino);
          url = url.replace('${ciudad}', this.citySend);
          this.functionService.getService(url).then((responseList) => {
            this.arrayDestino1 = responseList;
          });
        }
      }, 500, 'JavaScript');
    } else {
      this.servicioTransOtro.destino = '';
    }
  }

  keyUpOrSt1() {
    this.servicioTrans.origen = this.servicioTrans.origen.toUpperCase();
    if (this.servicioTrans.origen.toString() !== '' && this.servicioTrans.origen.toString() !== ' ') {
      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        let url = WS.geo.findStreetsCom;
        if (this.servicioTrans.origen.toString() !== '') {
          url = url.replace('${street}', this.servicioTrans.origen);
          url = url.replace('${ciudad}', this.citySend);
          this.functionService.getService(url).then((responseList) => {
            this.arrayOrigen1 = responseList;
          });
        }
      }, 500, 'JavaScript');
    } else {
      this.servicioTrans.origen = '';
    }
  }

  keyUpOrSt3() {
    this.servicioTrans.destino = this.servicioTrans.destino.toUpperCase();
    if (this.servicioTrans.destino.toString() !== '' && this.servicioTrans.destino.toString() !== ' ') {
      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        let url = WS.geo.findStreetsCom;
        if (this.servicioTrans.destino.toString() !== '') {
          url = url.replace('${street}', this.servicioTrans.destino);
          url = url.replace('${ciudad}', this.citySend);
          this.functionService.getService(url).then((responseList) => {
            this.arrayDestino1 = responseList;
          });
        }
      }, 500, 'JavaScript');
    } else {
      this.servicioTrans.destino = '';
    }
  }

  getValueForma(id) {
    console.log(id);
    this.servicioTrans.formaPago = id;
  }

  openModalFormas(content) {
    this.modalRefFormas = this.modalService.open(content);
  }

  openModal(content, param) {
    this.page.pageNumber = 0;
    this.page.size = 5;
    let url = WS.favoritos.get;
    url = url.replace('${pageNumber}', this.page.pageNumber.toString());
    url = url.replace('${size}', this.page.size.toString());
    url = url.replace('${idUsuario}', this.dataSesion['user_id']);
    this.functionService.httpGetWithout(url).then((response) => {
      this.rows = response['content'];
      this.page.totalElements = response['totalElements'];
      this.loadingIndicator = false;
      if (this.rows && param) {
        this.modalRef = this.modalService.open(content);
      }
    });
  }

  onSelect({ selected }) {
    // if (this.layer) {
    //   this.map.removeLayer(this.layer);
    // }
    // if (this.layerCar) {
    //   this.map.removeLayer(this.layerCar);
    // }
    // if (this.markerCar) {
    //   this.map.removeLayer(this.markerCar);
    // }
    // if (this.markerOrigen) {
    //   this.map.removeLayer(this.markerOrigen);
    // }
    // if (this.markerCarDestino) {
    //   this.map.removeLayer(this.markerCarDestino);
    // }
    // this.selected.splice(0, this.selected.length);
    // this.selected.push(...selected);
    // this.servicioTrans.origen = this.selected[0]['origen'];
    // this.servicioTrans.destino = this.selected[0]['destino'];
    // // this.centerLocation();
    // if (this.servicioTrans.origen != "" && this.servicioTrans.destino != "") {
    //   this.blur();
    // } else {
    //   this.blurMap();
    // }
    // this.closeModal();
  }

  closeModal() {
    this.modalRef.close();
  }

  closeModalFormas() {
    this.modalRefFormas.close();
  }

  setCruceCoordenadas(lat, long) {
    let url = WS.geo.coordsToStreets;
    url = url.replace('${lat}', lat);
    url = url.replace('${long}', long);
    this.functionService.getService(url).then((responseList) => {
      if (responseList[0]) {
        this.servicioTrans.origen = responseList[0]['calle1'] + ' Y ' + responseList[0]['calle2'];
        // this.spinner.openSpinner();
        this.getRoute().then(() => {
          // this.spinner.closeSpinner();
        });
      } else {
        this.servicioTrans.origen = '';
        this.toastrService['warning'](TAGS.messages.wrongAddress, this.appService.pageTitle, this.options_notitications);
      }
    });
  }

  blur() {
    this.spinner.openSpinner();
    this.getRoute().then(() => {
      this.spinner.closeSpinner();
    });
  }

  setLocation() {
    let url1 = WS.geo.crossStreet;
    if (this.servicioTrans.origen.toUpperCase().split(' Y ')[0] == '') {
      url1 = url1.replace('${street1}', '-');
    } else {
      url1 = url1.replace('${street1}', this.servicioTrans.origen.toUpperCase().split(' Y ')[0]);
    }
    if (this.servicioTrans.origen.toUpperCase().split(' Y ')[1] == '') {
      url1 = url1.replace('${street2}', '-');
    } else {
      url1 = url1.replace('${street2}', this.servicioTrans.origen.toUpperCase().split(' Y ')[1]);
    }
    url1 = url1.replace('${ciudad}', this.citySend);
    this.functionService.getService(url1).then((org) => {
      if (org) {
        if (org[0]) {
          this.servicioTrans.latitud = org[0]['latitud'];
          this.servicioTrans.longitud = org[0]['longitud'];
          this.getCity(this.servicioTrans.latitud, this.servicioTrans.longitud);
          this.map.setView([this.servicioTrans.latitud, this.servicioTrans.longitud], 17);
        } else {
          this.toastrService['warning'](TAGS.messages.wrongAddress, this.appService.pageTitle, this.options_notitications);
        }
      } else {
        this.toastrService['warning'](TAGS.messages.wrongAddress, this.appService.pageTitle, this.options_notitications);
      }
    });
  }

  blurMap() {
    let url1 = WS.geo.crossStreet;
    if (this.servicioTrans.origen.toUpperCase().split(' Y ')[0] == '') {
      url1 = url1.replace('${street1}', '-');
    } else {
      url1 = url1.replace('${street1}', this.servicioTrans.origen.toUpperCase().split(' Y ')[0]);
    }
    if (this.servicioTrans.origen.toUpperCase().split(' Y ')[1] == '') {
      url1 = url1.replace('${street2}', '-');
    } else {
      url1 = url1.replace('${street2}', this.servicioTrans.origen.toUpperCase().split(' Y ')[1]);
    }
    url1 = url1.replace('${ciudad}', this.citySend);
    this.functionService.getService(url1).then((org) => {
      if (org) {
        if (org[0]) {
          this.servicioTrans.latitud = org[0]['latitud'];
          this.servicioTrans.longitud = org[0]['longitud'];
          this.getCity(this.servicioTrans.latitud, this.servicioTrans.longitud);
          this.map.setView([this.servicioTrans.latitud, this.servicioTrans.longitud], 17);
          this.setCruceCoordenadas(this.servicioTrans.latitud, this.servicioTrans.longitud);
        } else {
          this.toastrService['warning'](TAGS.messages.wrongAddress, this.appService.pageTitle, this.options_notitications);
        }
      } else {
        this.toastrService['warning'](TAGS.messages.wrongAddress, this.appService.pageTitle, this.options_notitications);
      }
    });
  }

  getRoute() {
    const promise = new Promise((resolve, reject) => {
      if (this.servicioTrans.origen.toUpperCase().split(' Y ')[0] !== '' &&
        this.servicioTrans.origen.toUpperCase().split(' Y ')[1] !== '' &&
        this.servicioTrans.destino.toUpperCase().split(' Y ')[0] !== '' &&
        this.servicioTrans.destino.toUpperCase().split(' Y ')[1] !== '') {
        let url1 = WS.geo.crossStreet;
        url1 = url1.replace('${street1}', this.servicioTrans.origen.toUpperCase().split(' Y ')[0]);
        url1 = url1.replace('${street2}', this.servicioTrans.origen.toUpperCase().split(' Y ')[1]);
        url1 = url1.replace('${ciudad}', this.citySend);
        let url2 = WS.geo.crossStreet;
        url2 = url2.replace('${street1}', this.servicioTrans.destino.toUpperCase().split(' Y ')[0]);
        url2 = url2.replace('${street2}', this.servicioTrans.destino.toUpperCase().split(' Y ')[1]);
        url2 = url2.replace('${ciudad}', this.citySend);
        if (!url1.toString().includes('undefined') && !url2.toString().includes('undefined')) {
          let arrayOrigen = {};
          let arrayDestino = {};
          this.functionService.getService(url1).then((org) => {
            arrayOrigen = org[0];
            this.functionService.getService(url2).then((dest) => {
              arrayDestino = dest[0];
              if (arrayOrigen && arrayDestino) {
                this.servicioTrans.latitud = arrayOrigen['latitud'];
                this.servicioTrans.longitud = arrayOrigen['longitud'];
                this.servicioTrans.latitudDest = arrayDestino['latitud'];
                this.servicioTrans.longitudDest = arrayDestino['longitud'];
                this.getCity(this.servicioTrans.latitud, this.servicioTrans.longitud);
                if (!this.carreraPedida) {
                  try {
                    if (this.map) {
                      const directionsRequest = 'https://api.mapbox.com/directions/v5/mapbox/driving/'
                        + arrayOrigen['longitud'] + ',' + arrayOrigen['latitud'] + ';'
                        + arrayDestino['longitud'] + ',' + arrayDestino['latitud'] +
                        '?geometries=geojson&access_token=pk.eyJ1IjoiYXF1aW50YW5hciIsImEiOi' +
                        'Jjam5ldm56MjgwcWwzM3FueWV5aGt2ZXhvIn0.EF7ZnmtFwoWcrBx0SsPJZw';
                      this.http.get(directionsRequest).subscribe((response) => {
                        const pathStyle = {
                          'color': '#2D2E7E',
                          'weight': 10,
                          'opacity': 0.7
                        };
                        if (this.layer) {
                          this.map.removeLayer(this.layer);
                        }
                        this.layer = L.geoJson(
                          response['routes'][0].geometry, {
                            style: pathStyle
                          }
                        );
                        const ditancia = (response['routes'][0].distance / 1000).toFixed(2);
                        const tiempo = (response['routes'][0].duration / 60).toFixed(2);
                        this.layer.addTo(this.map);
                        this.addMarket(0, arrayOrigen['latitud'], arrayOrigen['longitud'], '', '', '');
                        this.addMarket(1, arrayDestino['latitud'], arrayDestino['longitud'], ditancia, tiempo, 5.50);
                        var group = new L.featureGroup([this.markerOrigen, this.markerCarDestino]);
                        this.map.fitBounds(group.getBounds());
                        resolve();
                      });
                      // this.spinner.closeSpinner();
                    }
                  } catch (e) {
                    resolve();
                    this.toastrService['warning'](TAGS.messages.wrongAddress, this.appService.pageTitle, this.options_notitications);
                    // this.spinner.closeSpinner();
                  }
                } else {
                  resolve();
                }
              } else {
                resolve();
                this.toastrService['warning'](TAGS.messages.wrongAddress, this.appService.pageTitle, this.options_notitications);
                // this.spinner.closeSpinner();
              }
            }).catch((err) => {
              resolve();
              this.toastrService['warning'](TAGS.messages.wrongAddress, this.appService.pageTitle, this.options_notitications);
              // this.spinner.closeSpinner();
            });
          }).catch((err) => {
            resolve();
            this.toastrService['warning'](TAGS.messages.wrongAddress, this.appService.pageTitle, this.options_notitications);
            // this.spinner.closeSpinner();
          });
        } else {
          resolve();
          this.toastrService['warning'](TAGS.messages.wrongAddress, this.appService.pageTitle, this.options_notitications);
        }
      } else {
        this.setLocation();
        resolve();
        // this.toastrService['info'](TAGS.messages.destinyAddress, this.appService.pageTitle, this.options_notitications);
        // this.spinner.closeSpinner();
      }
    });
    return promise;
  }

  addCar(latitud, longitud, latitudOr, longitudOr, color, pendiente) {
    try {
      if (this.map) {
        this.distancia = this.functionService.getKilometros(latitudOr, longitudOr, latitud, longitud);
        this.tiempo = (((parseFloat(this.distancia) / parseFloat(this.velocidad.toString())) * 60).toFixed(2)).toString();
        const customPopup = '<div id="container"> ' +
          '<form>' +
          '<label for="username" style="padding-bottom: 5px; color: #2D2E7E; font-size: 13px; font-weight: bold;">FASTLINE</label>' +
          '<br>' +
          '<label>Distancia: ' + this.distancia + ' Km</label>' +
          '<br>' +
          '<label>Tiempo: ' + this.tiempo + ' Min</label>' +
          '</form>' +
          '</div>';
        const customOptions = {
          'maxWidth': '500',
          'className': 'custom'
        };
        const popup = L.popup(customOptions).setContent(customPopup);
        const iconMarker = L.icon({
          iconUrl: 'assets/img/taxi.png',
          iconSize: [48, 48],
          iconAnchor: [25, 25]
        });
        const center = [latitud, longitud];
        if (this.markerCar) {
          this.map.removeLayer(this.markerCar);
        }
        this.markerCar = L.marker(center, {icon: iconMarker}).bindPopup(popup);
        this.markerCar.addTo(this.map);
        this.markerCar.openPopup();
        let options = this.options_notitications;
        options['timeOut'] = GV.solicitarTaxi.tiempoSolicitud.toString();
        if ((parseFloat(this.distancia)) <= 0.1 && pendiente) {
          this.toastrService['success']('POR FAVOR SALGA, LA UNIDAD LE ESPERA, MUCHAS GRACIAS.', this.appService.pageTitle, options);
        }
        if (this.countInit == 0) {
          const directionsRequest = 'https://api.mapbox.com/directions/v5/mapbox/driving/'
            + longitud + ',' + latitud + ';'
            + longitudOr + ',' + latitudOr +
            '?geometries=geojson&access_token=pk.eyJ1IjoiYXF1aW50YW5hciIsImEiOi' +
            'Jjam5ldm56MjgwcWwzM3FueWV5aGt2ZXhvIn0.EF7ZnmtFwoWcrBx0SsPJZw';
          this.http.get(directionsRequest).subscribe((response) => {
            const pathStyle = {
              'color': color,
              'weight': 10,
              'opacity': 0.7
            };
            if (this.layerCar) {
              this.map.removeLayer(this.layerCar);
            }
            this.layerCar = L.geoJson(
              response['routes'][0].geometry, {
                style: pathStyle
              }
            );
            this.layerCar.addTo(this.map);
            this.distancia = (response['routes'][0].distance / 1000).toFixed(2);
            this.tiempo = (response['routes'][0].duration / 60).toFixed(2);

            if (this.markerOrigen) {
              this.map.removeLayer(this.markerOrigen);
            }

            const centerOrg = [latitudOr, longitudOr];
            const iconMarkerOrg = L.icon({
              iconUrl: 'assets/img/user.png',
              iconSize: [48, 48],
              iconAnchor: [25, 50]
            });
            this.markerOrigen = L.marker(centerOrg, {icon: iconMarkerOrg});
            this.markerOrigen.addTo(this.map);
            var group = new L.featureGroup([this.markerCar, this.markerOrigen]);
            this.map.fitBounds(group.getBounds());
          });
        }
        this.countInit++;
      }
    } catch (e) {
      this.toastrService['warning'](TAGS.messages.wrongAddress, this.appService.pageTitle, this.options_notitications);
      // this.spinner.closeSpinner();
    }
  }

  addMarket(ban, latitud, longitud, dis, tie, tar) {
    const center = [latitud, longitud];
    if (ban === 0) {
      if (this.markerOrigen) {
        this.map.removeLayer(this.markerOrigen);
      }
      const iconMarker = L.icon({
        iconUrl: 'assets/img/user.png',
        iconSize: [48, 48],
        iconAnchor: [25, 50]
      });
      this.markerOrigen = L.marker(center, {icon: iconMarker});
      this.markerOrigen.addTo(this.map);
    } else {
      if (this.markerCarDestino) {
        this.map.removeLayer(this.markerCarDestino);
      }
      const customPopup = '<div id="container"> ' +
        '<form>' +
        '<label for="username" style="padding-bottom: 5px; color: #2D2E7E; font-size: 13px; font-weight: bold;">FASTLINE</label>' +
        '<br>' +
        '<label>Distancia: ' + dis + ' Km</label>' +
        '<br>' +
        '<label>Tiempo: ' + tie + ' Min</label>' +
        '</form>' +
        '</div>';
      const customOptions = {
        'maxWidth': '500',
        'className': 'custom'
      };
      const iconMarker = L.icon({
        iconUrl: 'assets/img/destiny.png',
        iconSize: [48, 48],
        iconAnchor: [25, 50]
      });
      const popup = L.popup(customOptions).setContent(customPopup);
      this.markerCarDestino = L.marker(center, {icon: iconMarker}).bindPopup(popup);
      this.markerCarDestino.addTo(this.map);
      this.markerCarDestino.openPopup();
    }
  }

  centerLocation() {
    if (this.banInit === false && this.origenText !== this.servicioTrans.origen) {
      if (navigator.geolocation) {
        try {
          navigator.geolocation.getCurrentPosition(position => {
            this.servicioTrans.longitud = position.coords.longitude;
            this.servicioTrans.latitud = position.coords.latitude;
            this.getCity(this.servicioTrans.latitud, this.servicioTrans.longitud);
            if (this.map) {
              this.map.setView([this.servicioTrans.latitud, this.servicioTrans.longitud], 17);
              this.setCruceCoordenadas(this.servicioTrans.latitud, this.servicioTrans.longitud);
            }
          }, failure => {
            if (failure.message.startsWith('Only secure origins are allowed')) {
              this.servicioTrans.longitud = -0.2243608462926889;
              this.servicioTrans.latitud = -78.51207733154298;
              this.map.setView([this.servicioTrans.latitud, this.servicioTrans.longitud], 17);
              this.setCruceCoordenadas(this.servicioTrans.latitud, this.servicioTrans.longitud);
            }
          });
        } catch (e) {
          console.log('Error mapa');
        }
      }
      this.origenText = this.servicioTrans.origen;
    }
  }

  clear() {
    this.servicioTrans = {
      'idServicio': '',
      'idUsuario': '',
      'idUsrAsociado': '',
      'idconvenio': '',
      'idempresa': '',
      'idEmpleado': '',
      'idPin': '',
      'telefono': '',
      'telefonocelular': '',
      'fechaServicio': null,
      'horaServicio': '',
      'unidad': '',
      'nombreUnidad': '',
      'nombreCliente': '',
      'identificacionCliente': '',
      'formaPago': '',
      'tarifa': '',
      'tarifahora': null,
      'tarifapeaje': null,
      'autorizacion': '',
      'nvoucher': '',
      'origen': '',
      'destino': '',
      'operador': '',
      'nlote': '',
      'latitud': 0,
      'longitud': 0,
      'latitudDest': 0,
      'longitudDest': 0,
      'reserva': '',
      'ciudad': '',
      'motivocancelacion': '',
      'estadoservicio': '',
      'observacion': '',
      'msgbeepe': null,
      'valido':  null,
      'codpuntea':  null,
      'opefac':  null,
      'tiempoespera':  null,
      'tdestino':  null,
      'operador2':  null,
      'costochofer':  null,
      'kmodometro':  null,
      'horaaceptado':  null,
      'horaocupado':  null,
      'horafin':  null,
      'horallegada':  null,
      'estado': '',
      'codusu': '',
      'fecult': '',
      'status': '',
      'ipMaquina': ''
    };
    if (this.layer) {
      this.map.removeLayer(this.layer);
    }
    if (this.layerCar) {
      this.map.removeLayer(this.layerCar);
    }
    if (this.markerCar) {
      this.map.removeLayer(this.markerCar);
    }
    if (this.markerOrigen) {
      this.map.removeLayer(this.markerOrigen);
    }
    if (this.markerCarDestino) {
      this.map.removeLayer(this.markerCarDestino);
    }
    this.carreraPedida = false;
    this.unidadAsignada = false;
    this.banBtnInf2 = false;
    this.banCancelTravel = true;

    let cont = 0;
    let ban = true;
    while (ban && cont < this.formasPago.length) {
      if (this.formasPago[cont]['principal'].toString() === '1') {
        this.servicioTrans.formaPago = this.formasPago[cont]['id'];
        this.servicioTransOtro.formaPago = this.formasPago[cont]['id'];
        ban = false;
      }
      cont++;
    }
    if (ban === true) {
      if (this.formasPago[0]) {
        this.servicioTrans.formaPago = this.formasPago[0]['id'];
        this.servicioTransOtro.formaPago = this.formasPago[0]['id'];
      } else {
        this.toastrService['error']('Debe agregar una forma de pago para continuar con el servicio.',
          this.appService.pageTitle, this.options_notitications);
        return;
      }
    }
    this.servicioTrans.estado = this.responseList[0].content[0].id;
    if (this.responseList[4].content[0]) {
      this.servicioTrans.idUsrAsociado = this.responseList[4].content[0].usuarioAsociado.id;
      this.servicioTransOtro.idUsrAsociado = this.responseList[4].content[0].usuarioAsociado.id;
    }
    this.servicioTrans.idUsuario = this.dataSesion['user_id'];
    this.servicioTrans.telefono = this.dataSesion['cell_phone'];
    this.servicioTrans.nombreCliente = this.dataSesion['user_name'];
    this.centerLocation();
    this.banInit = false;
    this.banSolicitarPin = false;
  }

  solicitarTaxi(val) {

    this.closeModalFormas();

    this.valorUnit = val;

    this.closeNav();

    let urlFpId = WS.formasPago.getid;
    urlFpId = urlFpId.replace('${id}', this.servicioTrans.formaPago);

    this.functionService.httpGetWithout(urlFpId).then((responseFp) => {
      let urlValidate = WS.solicitarTaxi.validate;
      urlValidate = urlValidate.replace('${celular}', this.dataSesion['cell_phone']);
      urlValidate = urlValidate.replace('${formapago}', responseFp['formapago'].toString());
      this.spinner.openSpinner();
      this.http.get(urlValidate).subscribe(
        res => {
          this.spinner.closeSpinner();
          if (res['bandera']) {
            this.valGeneral = val;
            this.countInit = 0;
            this.servicioTrans.estado = GV.state.active;
            this.servicioTrans.codusu = this.dataSesion['user_id'];
            this.servicioTrans.fecult = this.functionService.getToday();
            this.servicioTrans.status = '1';
            this.servicioTrans.ipMaquina = this.ipAddress;
            this.servicioTrans.estadoservicio = GV.estadosCarreras.pendiente;
            if (this.servicioTrans.formaPago.toString() === '') {
              this.toastrService['error']('Debe agregar una forma de pago para continuar con el servicio.',
                this.appService.pageTitle, this.options_notitications);
              return;
            }
            if (!this.reservaCheck) {
              this.servicioTrans.reserva = '0';
              this.servicioTrans.fechaServicio = this.functionService.getTodayNow();
              this.servicioTrans.horaServicio = this.functionService.getHour();
              this.servicioTransOtro.reserva = '0';
            } else {
              const now = new Date();
              const date1 = now.getFullYear() + '' + (now.getMonth() < 10 ? '0' + (now.getMonth() + 1) : (now.getMonth() + 1))
                + '' + (now.getDate() < 10 ? '0' + now.getDay() : now.getDate());
              const date2 = this.fechaServicioReserva.year + '' +
                (this.fechaServicioReserva.month < 10 ? '0' + this.fechaServicioReserva.month : this.fechaServicioReserva.month)
                + '' + (this.fechaServicioReserva.day < 10 ? '0' + this.fechaServicioReserva.day : this.fechaServicioReserva.day);
              if (date1 == date2) {
                const hora1 = now.getHours().toString() + '' + (now.getMinutes() < 10 ? '0' + now.getMinutes().toString() : now.getMinutes().toString());
                const hora2 = this.timeReserva.hour.toString() + (this.timeReserva.minute < 10 ? '0' + this.timeReserva.minute.toString() : this.timeReserva.minute.toString());
                if (hora2 <= hora1) {
                  this.toastrService['error']('La hora de reserva debe ser mayor a la hora actual del servicio.', this.appService.pageTitle, this.options_notitications);
                  return;
                }
              }
              this.servicioTrans.reserva = '1';
              this.servicioTransOtro.reserva = '';
              const fecha = this.fechaServicioReserva.year + '-' + this.fechaServicioReserva.month + '-' + this.fechaServicioReserva.day;
              this.servicioTrans.fechaServicio = new Date(fecha);
              this.servicioTrans.horaServicio = this.timeReserva.hour + ':' + this.timeReserva.minute;
            }
            if (val.toString() === '1') {
              this.servicioTrans.idUsrAsociado = this.servicioTrans.idUsuario;
            } else {
              this.servicioTrans.idUsrAsociado = this.servicioTransOtro.idUsrAsociado;
              this.servicioTrans.formaPago = this.servicioTransOtro.formaPago;
              this.servicioTrans.origen = this.servicioTransOtro.origen;
              this.servicioTrans.destino = this.servicioTransOtro.destino;
              this.servicioTrans.reserva = this.servicioTransOtro.reserva == '1' ? '1' : '0';
              this.servicioTrans.observacion = this.servicioTransOtro.observacion;
            }

            if (this.servicioTrans.origen == '') {
              this.toastrService['error']('Debe agregar el Origen de la Carrera.', this.appService.pageTitle, this.options_notitications);
              return;
            }
            if (responseFp['formapago'].toString() === GV.formasPago.tarjeta.toString() ||
              responseFp['formapago'].toString() === GV.formasPago.voucherElectronico.toString()) {
              this.banSolicitarPin = true;
            } else {
              this.banSolicitarPin = false;
            }
            if (responseFp['formapago'].toString() === GV.formasPago.tarjeta.toString() && this.servicioTrans.destino == '') {
              this.toastrService['error']('Debe agregar el Destino de la Carrera.', this.appService.pageTitle, this.options_notitications);
              this.banSolicitarPin = false;
              return;
            }

            let urlAddsw = '';
            let voucher = '';
            const fpcod = responseFp['formapago']
            switch (fpcod.toString()) {
              case GV.formasPago.efectivo.toString():
                voucher = '0';
                break;
              case GV.formasPago.tarjeta.toString():
                voucher = '2';
                break;
              case GV.formasPago.voucherManual.toString():
                voucher = '1';
                break;
              case GV.formasPago.voucherElectronico.toString():
                voucher = '3';
                break;
            }
            if (responseFp['formapago'].toString() === GV.formasPago.voucherElectronico.toString()) {
              if (this.empleado == null) {
                this.toastrService['error']('No se encuentra el empleado registrado con el celular. ' +
                  'Por favor ponerse en contacto con su empresa. PUEDE SOLICITAR SU SERVICIO CON OTRO MEDIO DE PAGO.',
                  this.appService.pageTitle, this.options_notitications);
                this.banSolicitarPin = false;
                return;
              }
            }
            if (responseFp['formapago'].toString() === GV.formasPago.tarjeta.toString()) {
              urlAddsw = WS.solicitarTaxi.addswtar;
              urlAddsw = urlAddsw.replace('${uid}', this.dataSesion['user_id']);
              urlAddsw = urlAddsw.replace('${voucher}', voucher);
              urlAddsw = urlAddsw.replace('${idRechazado}', res['id']);
            } else {
              urlAddsw = WS.solicitarTaxi.addsw;
              urlAddsw = urlAddsw.replace('${voucher}', voucher);
              urlAddsw = urlAddsw.replace('${idRechazado}', res['id']);
            }

            let urllist = WS.preautorizados.getList;
            urllist = urllist.replace('${idAutorizador}', this.dataSesion['user_id']);
            urllist = urllist.replace('${estadoservicio}', GV.estadopreautorizados.autorizar.toString());
            urllist = urllist.replace('${param}', 'false');
            if (responseFp['formapago'].toString() === GV.formasPago.voucherElectronico.toString()) {
              this.functionService.httpGetWithout(urllist).then((resPre: any[]) => {
                if (resPre.length == 0) {
                  let urlPreautorizar = WS.solicitarTaxi.preautorizar;
                  urlPreautorizar = urlPreautorizar.replace('${origenserv}', GV.origenservicio.web.toString())
                  this.functionService.httpPostWithout(urlPreautorizar, this.servicioTrans).then((respreautorizar) => {
                    if (respreautorizar['bandera']) {
                      this.functionService.httpPost(urlAddsw, this.servicioTrans).then((response) => {
                        if (response['autorizacion'] && response.toString().toUpperCase() != 'UNDEFINED') {
                          if (response['autorizacion'].toString() !== '') {
                            console.log(response);
                            const options = this.options_notitications;
                            options['timeOut'] = this.tiempoEspera.toString();
                            this.toastrService['info']('Solicitando Unidad. Autorización: ' + response['autorizacion'],
                              this.appService.pageTitle, options);
                            this.servicioTrans.autorizacion = response['autorizacion'];
                            if (!this.reservaCheck) {
                              this.getUnity(response['autorizacion'], val);
                              this.agregarFavorito('0');
                            }
                          } else {
                            this.toastrService['error']('Error al solicitar el viaje.', this.appService.pageTitle, this.options_notitications);
                          }
                        } else {
                          this.toastrService['error']('Error al solicitar el viaje.', this.appService.pageTitle, this.options_notitications);
                        }
                      });
                    } else {
                      this.toastrService['info'](respreautorizar['descripcion'], this.appService.pageTitle, this.options_notitications);
                    }
                  });
                } else {
                  this.toastrService['error']('Tiene servicios pendientes de autorización.', this.appService.pageTitle, this.options_notitications);
                }
              });
            } else {
              this.functionService.httpPost(urlAddsw, this.servicioTrans).then((response) => {

                if (response['autorizacion'] && response.toString().toUpperCase() != 'UNDEFINED') {
                  if (response['autorizacion'].toString() !== '') {
                    console.log(response);
                    const options = this.options_notitications;
                    options['timeOut'] = this.tiempoEspera.toString();
                    this.toastrService['info']('Solicitando Unidad. Autorización: ' + response['autorizacion'],
                      this.appService.pageTitle, options);
                    this.servicioTrans.autorizacion = response['autorizacion'];
                    if (!this.reservaCheck) {
                      this.getUnity(response['autorizacion'], val);
                      this.agregarFavorito('0');
                    }
                  } else {
                    this.toastrService['error']('Error al solicitar el viaje.', this.appService.pageTitle, this.options_notitications);
                  }
                } else {
                  this.toastrService['error']('Error al solicitar el viaje.', this.appService.pageTitle, this.options_notitications);
                }
              });
            }

          } else {
            this.toastrService['error'](res['descripcion'], this.appService.pageTitle, this.options_notitications);
          }
        },
        err => {
          this.spinner.closeSpinner();
          this.toastrService['error']('Ocurrio un error al momento de realizar las validaciones para el servicio. ' +
            'INTENTE NUEVAMENTE GRACIAS.', this.appService.pageTitle, this.options_notitications);
        }
      );
    });
      }

  openReservas() {
    this.toastrService['info']('Verificando sus reservas.', this.appService.pageTitle, this.options_notitications);
  }

  getUnity(autorizacion, val) {
    if (this.contGetUnity === 0) {
      this.spinner.openSpinner();
    }
    this.contGetUnity ++;
    if (this.time >= this.tiempoEspera) {
      this.openConfirmationDialog();
      this.spinner.closeSpinner();
      this.contGetUnity = 0;
      return;
    }
    let unityUrl = WS.solicitarTaxi.getUnity.replace('${autorizacion}', autorizacion);
    unityUrl = unityUrl.replace('${ciudad}', this.servicioTrans.ciudad);
    this.functionService.httpGetWithout(unityUrl).then((response) => {
      if (response[0]['unidad'] && response.toString().toUpperCase() != 'UNDEFINED') {
        if (response[0]['unidad'].toString() != '' && response[0]['unidad'].toString() != ' '
          && response[0]['unidad'].toString() != 'null' && response[0]['unidad'].toString().toUpperCase() != 'UNDEFINED') {
          if (response[0]['unidad'].toString().toUpperCase() === 'T11') {
            this.cancelTravel();
            this.toastrService['error']('No se tiene unidad. Viaje cancelado. Autorización', this.appService.pageTitle,
              this.options_notitications);
            this.spinner.closeSpinner();
            this.contGetUnity = 0;
          } else {
            this.toastrService['success']('Unidad Asignada.', this.appService.pageTitle, this.options_notitications);
            let urlRuta = WS.solicitarTaxi.getRuta;
            let user_id = '';
            if (val.toString() === '1') {
              user_id = this.dataSesion['user_id'];
            } else {
              user_id = this.servicioTrans.idUsrAsociado;
            }
            urlRuta = urlRuta.replace('${uid}', user_id);
            urlRuta = urlRuta.replace('${autorizacion}', autorizacion);
            this.functionService.httpGetWithout(urlRuta).then((response) => {
              console.log(response);
              this.spinner.closeSpinner();
              this.contGetUnity = 0;
            });
          }
        } else {
          setTimeout(() => {
            this.time = this.time + GV.solicitarTaxi.tiempoSolicitud;
            // this.spinner.closeSpinner();
            this.getUnity(autorizacion, val);
          }, GV.solicitarTaxi.tiempoSolicitud);
        }
      } else {
        setTimeout(() => {
          this.time = this.time + GV.solicitarTaxi.tiempoSolicitud;
          // this.spinner.closeSpinner();
          this.getUnity(autorizacion, val);
        }, GV.solicitarTaxi.tiempoSolicitud);
      }
    });
  }

  cancelTravel() {
    const updtContext = {
      'tarifa': '',
      'nvoucher': '',
      'motivocancelacion': '',
      'estadoservicio': GV.estadosCarreras.cancelada
    };
    let url = WS.travels.canceltravel;
    url = url.replace('${autorizacion}', this.servicioTrans.autorizacion);
    this.functionService.httpPut(url, updtContext).then((response) => {
      console.log(response);
      if (response['estadoservicio'].toString() === GV.estadosCarreras.activa.toString()) {
        this.toastrService['error']('Tiene una carrera ACTIVA no puede cancelar la misma.', this.appService.pageTitle, this.options_notitications);
        this.banCancelTravel = false;
      } else {
        this.origenText = 'CANCELADA';
        this.clear();
      }
    });
  }

  getCity(latitud, longitud) {
    let url = WS.solicitarTaxi.getCity;
    url = url.replace('${latitud}', latitud);
    url = url.replace('${longitud}', longitud);
    this.functionService.httpGetWithout(url).then((response) => {
      this.servicioTrans.ciudad = response.toString();
      this.citySend = response.toString();
    });
  }

  viewInitMap(latitud, longitud) {
    this.servicioTrans.longitud = longitud;
    this.servicioTrans.latitud = latitud;
    this.getCity(this.servicioTrans.latitud, this.servicioTrans.longitud);
    this.setCruceCoordenadas(this.servicioTrans.latitud, this.servicioTrans.longitud);
    this.map = L.map('mapid').setView([this.servicioTrans.latitud, this.servicioTrans.longitud], 17);
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
      '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © ' +
      '<a href="https://www.mapbox.com/">Mapbox</a>',
      maxZoom: 18,
      id: 'mapbox.streets',
      accessToken: environment.mapbox.accessToken
    }).addTo(this.map);
    this.map.on('dragend', (ev) => {
      if (!this.carreraPedida) {
        this.servicioTrans.latitud = this.map.getCenter().lat;
        this.servicioTrans.longitud = this.map.getCenter().lng;
        this.getCity(this.servicioTrans.latitud, this.servicioTrans.longitud);
        this.setCruceCoordenadas(this.map.getCenter().lat, this.map.getCenter().lng);
      }
    });
  }

  ngOnInit() {
    this.closeNav();
    this.contGetUnity = 0;
    const now = new Date();
    this.fechaInicial = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    this.fechaServicioReserva = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    this.timeReserva.hour = now.getHours();
    this.timeReserva.minute = now.getMinutes();
    this.banSolicitarPin = false;
    this.banInit = true;
    this.dataSesion = localStorage.getItem('userToken');
    this.dataSesion = this.functionService.decode(this.dataSesion);
    this.labels = TAGS;
    this.gv = GV;
    let wsFormasPago = WS.formasPago.getActive;
    let wsUsuarioData = WS.usuario.data;
    let wsAsociados = WS.asociados.get;
    let wsDataEmpleado = WS.employees.getData;
    wsUsuarioData = wsUsuarioData.replace('${id}', this.dataSesion['user_id']);
    wsFormasPago = wsFormasPago.replace('${idUser}', this.dataSesion['user_id']);
    wsAsociados = wsAsociados.replace('${userId}', this.dataSesion['user_id']);
    wsDataEmpleado = wsDataEmpleado.replace('${celular}', this.dataSesion['cell_phone']);
    const array = [
      WS.general.status, wsUsuarioData, wsFormasPago, WS.solicitarTaxi.tiempoEspera, wsAsociados, wsDataEmpleado
    ];
    this.functionService.httpObservableGet(array).then((responseList) => {
      this.estados = responseList[0].content;
      this.usuario = responseList[1];
      this.formasPago = responseList[2].content;
      this.tiempoEspera = responseList[3].content[0]['valorNumerico'];
      if (responseList[4]) {
        this.asociados = responseList[4].content;
      }
      this.empleado = responseList[5];
      if (this.empleado) {
        if (this.empleado.empresa) {
          this.banviewemployee = true;
          this.banBtnInf1 = false;
          if (this.empleado['sucursal']['nomSucursal'] !== null && this.empleado['sucursal']['nomSucursal'] !== '') {
            this.empleado.nomsucursal = this.empleado['sucursal']['nomSucursal'].toUpperCase();
            this.banArea = true;
          }
          if (this.empleado['departamento']['nombreDepa'] !== null && this.empleado['departamento']['nombreDepa'] !== '') {
            this.empleado.nomarea = this.empleado['departamento']['nombreDepa'].toUpperCase();
            this.banDivision = true;
          }
        } else {
          this.banviewemployee = false;
          this.banBtnInf1 = false;
        }
      } else {
        this.banviewemployee = false;
        this.banBtnInf1 = false;
      }
      this.nombreCompleto = this.usuario.firstName + ' ' + this.usuario.lastName;
      this.responseList = responseList;
      this.functionService.getIpAddress().then((res) => {
        this.ipAddress = res;
      });
      this.clear();
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(position => {
          this.viewInitMap(position.coords.latitude, position.coords.longitude);
        }, failure => {
          if (failure.message.startsWith('Only secure origins are allowed')) {
            this.viewInitMap(-0.2243608462926889, -78.51207733154298);
          }
        });
      } else {
        this.viewInitMap(-0.2243608462926889, -78.51207733154298);
      }
      this.banSendMsg = true;
      this.carreraPedida = false;
      this.unidadAsignada = false;
      this.banCancelTravel = true;
      this.contCarreraActiva = 0;
      this.initializeWebSocketConnection();
      let ultViaUrl = WS.travels.ultimoViaje;
      ultViaUrl = ultViaUrl.replace('${idUser}', this.dataSesion['user_id']);
      this.functionService.httpGetWithout(ultViaUrl).then((response) => {
        console.log(response);
        if (response) {
          if (response['estadoservicio'].toString() === GV.estadosCarreras.activa.toString() ||
            response['estadoservicio'].toString() === GV.estadosCarreras.pendiente.toString()
          ) {
            this.servicioTrans = {
              'idServicio': response['idServicio'],
              'idUsuario': response['idUsuario'],
              'idUsrAsociado': response['idUsrAsociado'],
              'idconvenio': response['idconvenio'],
              'idempresa': response['idempresa'],
              'idEmpleado': response['idEmpleado'],
              'idPin': response['idPin'],
              'telefono': response['telefono'],
              'telefonocelular': response['telefonocelular'],
              'fechaServicio': response['fechaServicio'],
              'horaServicio': response['horaServicio'],
              'unidad': response['unidad'],
              'nombreUnidad': response['nombreUnidad'],
              'nombreCliente': response['nombreCliente'],
              'identificacionCliente': response['identificacionCliente'],
              'formaPago': response['formaPago'].toString(),
              'tarifa': response['tarifa'],
              'tarifahora': response['tarifahora'],
              'tarifapeaje': response['tarifapeaje'],
              'autorizacion': response['autorizacion'],
              'nvoucher': response['nvoucher'],
              'origen': response['origen'],
              'destino': response['destino'],
              'operador': response['operador'],
              'nlote': response['nlote'],
              'latitud': response['latitud'],
              'longitud': response['longitud'],
              'latitudDest': response['latitudDest'],
              'longitudDest': response['longitudDest'],
              'reserva': response['reserva'],
              'ciudad': response['ciudad'],
              'motivocancelacion': response['motivocancelacion'],
              'estadoservicio': response['estadoservicio'],
              'observacion': response['observacion'],
              'msgbeepe': response['msgbeepe'],
              'valido': response['valido'],
              'codpuntea': response['codpuntea'],
              'opefac': response['opefac'],
              'tiempoespera': response['tiempoespera'],
              'tdestino': response['tdestino'],
              'operador2': response['operador2'],
              'costochofer': response['costochofer'],
              'kmodometro': response['kmodometro'],
              'horaaceptado': response['horaaceptado'],
              'horaocupado': response['horaocupado'],
              'horafin': response['horafin'],
              'horallegada': response['horallegada'],
              'estado': response['estado'],
              'codusu': response['codusu'],
              'fecult': response['fecult'],
              'status': response['status'],
              'ipMaquina': response['ipMaquina']
            };
          }
          this.origenActPen = this.servicioTrans.origen;
          let options = this.options_notitications;
          let timeOut = (GV.solicitarTaxi.tiempoSolicitud + GV.solicitarTaxi.tiempoSolicitud);
          options['timeOut'] = timeOut;
          if (response['estadoservicio'].toString() === GV.estadosCarreras.pendiente.toString()) {
            this.toastrService['info']('Tiene una carrera PENDIENTE. Buscando datos de la carrera.', this.appService.pageTitle, options);
            this.banCancelTravel = false;
            this.carreraPedida = true;
            setTimeout(() => {
              if (this.countInit === 0) {
                let val = '1';
                if (this.servicioTrans.idUsrAsociado.toString() === this.dataSesion['user_id'].toString()) {
                  val = '1';
                } else {
                  val = '0';
                }
                console.log('Iniciar solicitudes de carrera en el server');
                this.getUnity(this.servicioTrans.autorizacion, val);
              }
            }, timeOut);
          } else if (response['estadoservicio'].toString() === GV.estadosCarreras.activa.toString()) {
            this.toastrService['info']('Tiene una carrera Activa.  Buscando datos de la carrera.', this.appService.pageTitle, options);
            this.banCancelTravel = true;
            this.carreraPedida = true;
            setTimeout(() => {
              if (this.countInit === 0) {
                console.log('Iniciar solicitudes de carrera en el server');
                let val = '1';
                if (this.servicioTrans.idUsrAsociado.toString() === this.dataSesion['user_id'].toString()) {
                  val = '1';
                } else {
                  val = '0';
                }
                this.getUnity(this.servicioTrans.autorizacion, val);
              }
            }, timeOut);
          } else {
            this.banSendMsg = true;
            this.carreraPedida = false;
            this.unidadAsignada = false;
            this.banCancelTravel = true;
            this.contCarreraActiva = 0;
          }
        }
      });
    });
    // this.spinner.openSpinner();
  }
}

