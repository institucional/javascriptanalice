/**
 * AUTOR: FREDI ROMAN
 */
import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { WS } from '../../WebServices';
import { FunctionService } from '../../services/functions/function.service';
import { User } from '../../models/user';
import { TAGS } from '../../TextLabels';

const AUTO_COMPLETE_CLASS = 'show-autocomplete';

@Component({
  selector: 'search-user',
  templateUrl: './search-user.component.html',
  styleUrls: ['./search-user.component.scss']
})
export class SearchUserComponent implements OnInit {
  @ViewChild("autoComplete") autoCompleteContRef: ElementRef;
  private autoCompleteCont: HTMLElement;

  @Output() onDataFilter: EventEmitter<User>;

  private timeout: any;
  private timeoutBlur: any;

  public filterPattern: string;
  public filteredUsers: User[];
  public labels: any;

  constructor(
    private _functionService: FunctionService,
  ) {
    this.onDataFilter = new EventEmitter<User>();
    this.labels = TAGS;
  }

  ngOnInit() {
    this.autoCompleteCont = <HTMLElement>this.autoCompleteContRef.nativeElement;
  }

  /**
   * METODO PARA SELECCIONAR UNA OPCION DEL AUTOCOMPLETE PARA ENVIARLO HACIA EL COMPONENTE PADRE
   * @param event
   * AUTOR: FREDI ROMAN
   * @param index
   */
  public onOptionClick(event: any, index: number) {
    event.preventDefault();
    this.onDataFilter.emit(this.filteredUsers[index]);
    this.showHideOptions();
  }

  /**
   * METODO PARA MOSTRAR U OCULTAR LA LISTA DE OPCIONES:
   * AUTOR: FREDI ROMAN
   * @param hide
   */
  public showHideOptions(hide: boolean = true) {
    if (this.timeoutBlur) {
      clearTimeout(this.timeoutBlur);
    }
    this.timeoutBlur = setTimeout(() => {
      let autoComplete = this.autoCompleteCont.querySelector('.fr-auto-complete');
      let autoCompleteClass = autoComplete.classList;
      if (hide) {
        autoCompleteClass.remove(AUTO_COMPLETE_CLASS);
      }
      else {
        autoCompleteClass.add(AUTO_COMPLETE_CLASS);
      }
    }, 300);
  }

  /**
   * METODO PARA MANIPULAR EL EVENTO DE KEYUP DEL FILTRO DE BUSQUEDA DE USUARIOS:
   * AUTOR: FREDI ROMAN
   */
  public onFilteringUser() {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }

    this.timeout = setTimeout(() => {
      this.loadFilteredUsers();
    }, 500);
  }

  /**
   * METODO PARA CARGAR UNA LISTA DE USUARIOS DADO UN FILTRO
   * AUTOR: FREDI ROMAN
   */
  private loadFilteredUsers() {
    let userId = this._functionService.decode(localStorage.getItem('userToken'))['user_id'];
    let url = WS.asociados.getUsersByFilter.replace('${userId}', userId).replace('${filter}', this.filterPattern);
    this._functionService.getService(url).then((respData: any) => {
      this.filteredUsers = [];
      for (let user of respData) {
        this.filteredUsers.push(new User(user.id, user.email, user.firstName, user.lastName, user.phone));
      }
      this.showHideOptions(false);
    }).catch(err => console.log("[SEARCH USER COMPONENT] - ERROR: ", err))
  }
}
