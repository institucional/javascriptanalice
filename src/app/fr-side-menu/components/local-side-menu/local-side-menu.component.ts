import { Component, OnInit, ViewChild, ElementRef, Input, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';

const OPEN_CLASS = 'fr-open-menu';
const DISPLAY_CLASS = 'fr-show';
const OVER_CLASS = 'fr-over-hidden';

@Component({
  selector: 'local-side-menu',
  templateUrl: './local-side-menu.component.html',
  styleUrls: ['./local-side-menu.component.scss']
})
export class LocalSideMenuComponent implements OnInit {
  @ViewChild("menuContainer") menuContainerRef: ElementRef;
  private menuContainer: HTMLElement;

  constructor(
    private _router: Router,
  ) { }

  ngOnInit() {
    this.menuContainer = this.menuContainerRef.nativeElement;
  }

  /**
   * METODO PARA ABRIR O CERRAR EL MENU AL DAR CLICK EN EL BOTON DE APERTURA O EN LA PANTALLA DE CIERRE DEL MENU
   * AUTOR: FREDI ROMAN
   * 
   * @param event 
   * @param open 
   */
  public openHideMenu(event: any = null, open: boolean = true) {
    if (event) {
      event.preventDefault();
    }
    let menu = this.menuContainer.querySelector('.fr-side-menu');
    let backMenu = this.menuContainer.querySelector('.fr-cristal-back');
    let body = document.querySelector('body');
    if (open) {
      body.classList.add(OVER_CLASS);
      menu.classList.add(OPEN_CLASS);
      backMenu.classList.add(DISPLAY_CLASS);
      backMenu.classList.add(OPEN_CLASS);
    } else {
      body.classList.remove(OVER_CLASS);
      menu.classList.remove(OPEN_CLASS);
      backMenu.classList.remove(OPEN_CLASS);
      setTimeout(() => {
        backMenu.classList.remove(DISPLAY_CLASS);
      }, 500);
    }
  }

  public onClick() {
    this._router.navigate(['admin/departamentos']);
  }

}
