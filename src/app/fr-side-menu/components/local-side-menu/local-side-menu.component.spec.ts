import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalSideMenuComponent } from './local-side-menu.component';

describe('LocalSideMenuComponent', () => {
  let component: LocalSideMenuComponent;
  let fixture: ComponentFixture<LocalSideMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocalSideMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalSideMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
