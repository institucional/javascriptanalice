/**
 * AUTOR: FREDI ROMAN
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocalSideMenuComponent } from './components/local-side-menu/local-side-menu.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    LocalSideMenuComponent
  ],
  exports: [
    LocalSideMenuComponent
  ]
})
export class FrSideMenuModule { }
