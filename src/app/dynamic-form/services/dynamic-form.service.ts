import { Injectable } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { DynamicInputForm } from '../models/dynamic-input-form';

@Injectable()
export class DynamicFormService {

  constructor() { }

  /**
   * METODO PARA CREAR UN NUEVO FORM GROUP A PARTIR DE UN ARRAY DE TIPO INPUT-GROUP
   * @param inputGroup 
   */
  public createFormGroup(inputFormList: DynamicInputForm<any>[]) {
    let group: any = {};

    inputFormList.forEach(inputForm => {
      group[inputForm.inputData.key] = inputForm.inputData.required ?
        new FormControl(inputForm.inputData.value || '', Validators.required) :
        new FormControl(inputForm.inputData.value || '');
    });
    return new FormGroup(group);
  }
}
