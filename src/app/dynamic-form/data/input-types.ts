export const INPUT_TYPES = {
    entero: 170,
    decimal: 171,
    dinero: 172,
    porcentaje: 173,
    alfanumerico: 181,
    texto_corto: 174,
    texto_largo: 182,
    fecha: 175,
    hora: 176,
    booleano: 177,
    subcatalogo: 178
}