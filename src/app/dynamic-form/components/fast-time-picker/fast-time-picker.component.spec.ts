import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FastTimePickerComponent } from './fast-time-picker.component';

describe('FastTimePickerComponent', () => {
  let component: FastTimePickerComponent;
  let fixture: ComponentFixture<FastTimePickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FastTimePickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FastTimePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
