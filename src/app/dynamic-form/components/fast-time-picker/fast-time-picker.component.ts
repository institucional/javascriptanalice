/**
 * AUTOR: FREDI ROMAN
 */
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TimeInterface } from '../../interfaces/time.interface';

@Component({
  selector: 'fast-time-picker',
  templateUrl: './fast-time-picker.component.html',
  styleUrls: ['./fast-time-picker.component.scss']
})
export class FastTimePickerComponent implements OnInit {
  @Input() timeObj: TimeInterface;
  @Output() onTimeChange: EventEmitter<TimeInterface>;

  constructor() {
    this.onTimeChange = new EventEmitter<TimeInterface>();
  }

  ngOnInit() {
    this.validateH();
    this.validateM();
  }

  /**
   * METODO PARA VALIDAR QUE LAS HORAS SEAN CORRECTAS
   * AUTOR: FREDI ROMAN
   */
  public validateH() {
    if (!isNaN(parseInt(this.timeObj.hours)) && parseInt(this.timeObj.hours) >= 0) {
      if (parseInt(this.timeObj.hours) < 10) {
        setTimeout(() => {
          this.timeObj.hours = '0' + parseInt(this.timeObj.hours);
        });
      } else if (parseInt(this.timeObj.hours) > 23) {
        setTimeout(() => {
          this.timeObj.hours = 23 + "";
        });
      } else {
        setTimeout(() => {
          this.timeObj.hours = parseInt(this.timeObj.hours) + '';
        });
      }
    } else {
      setTimeout(() => {
        this.timeObj.hours = '00';
      });
    }
    setTimeout(() => {
      this.onTimeChange.emit(this.timeObj);
    }, 500);
  }

  /**
   * METODO PARA VALIDAR QUE LOS MINUTOS SEAN CORRECTAS
   * AUTOR: FREDI ROMAN
   */
  public validateM() {
    if (!isNaN(parseInt(this.timeObj.minutes)) && parseInt(this.timeObj.minutes) >= 0) {
      if (parseInt(this.timeObj.minutes) < 10) {
        setTimeout(() => {
          this.timeObj.minutes = '0' + parseInt(this.timeObj.minutes);
        });
      } else if (parseInt(this.timeObj.minutes) > 59) {
        setTimeout(() => {
          this.timeObj.minutes = '59';
        });
      }
      else {
        setTimeout(() => {
          this.timeObj.minutes = parseInt(this.timeObj.minutes) + '';
        });
      }
    } else {
      setTimeout(() => {
        this.timeObj.minutes = '00';
      });
    }
    setTimeout(() => {
      this.onTimeChange.emit(this.timeObj);
    }, 500);
  }
}
