/**
 * AUTOR: FREDI ROMAN
 */
import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { SwitchInputInterface } from '../../interfaces/switch-input.interface';
import { SWITCH_SIZE } from '../../data/switch-size';

@Component({
  selector: 'switch-input',
  templateUrl: './switch-input.component.html',
  styleUrls: ['./switch-input.component.scss']
})
export class SwitchInputComponent implements OnInit, OnChanges {
  @Input() switchInput: SwitchInputInterface;
  @Output() onChange: EventEmitter<SwitchInputInterface>;

  public switchSize: any;

  constructor() {
    this.onChange = new EventEmitter<SwitchInputInterface>();
    this.switchSize = SWITCH_SIZE;
  }

  ngOnInit() {
  }

  /**
   * METODO PARA DETECTAR EL CAMBIO DE ESTADO DEL SWITCH INPUT:
   * AUTOR: FREDI ROMAN
   * @param event 
   */
  switchChange(event: any) {
    if (this.switchInput.immutable == true) {
      if (!this.switchInput.checked == true) {
        this.switchInput.checked = !this.switchInput.checked;
        this.onChange.emit(this.switchInput);
      } else {
        event.preventDefault();
      }
    } else {
      this.switchInput.checked = !this.switchInput.checked;
      this.onChange.emit(this.switchInput);
    }
  }

  /**
   * METODO PARA DETECTAR LOS CAMBIOS DE UNA PROPIEDAD INYECTADA DESDE EL COMPONENTE PADRE DE ESTE COMPONENTE:
   * AUTOR: FREDI ROMAN
   * @param changes LOS CAMBIOS GENERADOS
   */
  ngOnChanges(changes: SimpleChanges) {
    for (let property in changes) {
      switch (property) {
        case 'switchInput':
          if (changes[property].currentValue) {
            this.switchInput = changes[property].currentValue;
          }
          break;
      }
    }
  }

}
