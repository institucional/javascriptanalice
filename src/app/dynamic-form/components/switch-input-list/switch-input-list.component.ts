/**
 * AUTOR: FREDI ROMAN
 */
import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { SwitchInputInterface } from '../../interfaces/switch-input.interface';
import { SW_LIST_TYPE } from '../../data/switch-list-type';

@Component({
  selector: 'switch-input-list',
  templateUrl: './switch-input-list.component.html',
  styleUrls: ['./switch-input-list.component.scss']
})
export class SwitchInputListComponent implements OnInit {
  @Input() title: string;
  @Input() switchIns: SwitchInputInterface[];
  @Input() listType: number;
  @Output() selectedInput: EventEmitter<any>;

  public listTypes: any;

  constructor() {
    this.selectedInput = new EventEmitter<any>();
    this.listTypes = SW_LIST_TYPE;
  }

  ngOnInit() {
    if (!this.listType) {
      this.listType = this.listTypes.radio;
    }
  }

  /**
   * METODO PARA PROPAGAR LOS CAMBIOS DE UN INPUT EN UNA LISTA SWITCH-INPUT-RADIO
   * AUTOR: FREDI ROMAN
   */
  private inputRadioChanges(swInput: SwitchInputInterface) {
    for (let i = 0; i < this.switchIns.length; i++) {
      if (swInput.id != this.switchIns[i].id) {
        this.switchIns[i].checked = false;
      }
    }
  }

  /**
   * METODO PARA DETECTAR LOS CAMBIOS DEL SWITCH INPUT COMO COMPONENTE HIJO
   * AUTOR: FREDI ROMAN
   * @param event VALOR BOOLEANO DEL EVENT EMITTER DEL COMPONENTE HIJO
   */
  public getSwitchChanges(event: SwitchInputInterface) {
    switch (this.listType) {
      case this.listTypes.radio:
        this.inputRadioChanges(event);
        break;
    }
    this.selectedInput.emit(event.id);
  }
}
