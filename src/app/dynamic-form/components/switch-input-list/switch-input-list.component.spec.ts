import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwitchInputListComponent } from './switch-input-list.component';

describe('SwitchInputListComponent', () => {
  let component: SwitchInputListComponent;
  let fixture: ComponentFixture<SwitchInputListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SwitchInputListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwitchInputListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
