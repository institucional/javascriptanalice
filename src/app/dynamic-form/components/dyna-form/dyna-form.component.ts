/**
 * AUTOR: FREDI ROMAN
 */
import { Component, OnInit, OnChanges, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DynamicFormService } from '../../services/dynamic-form.service';
import { DynamicInputForm } from '../../models/dynamic-input-form';
import { ToastrService } from 'ngx-toastr';
import { AppService } from '../../../app.service';

@Component({
  selector: 'fr-dyna-form',
  templateUrl: './dyna-form.component.html',
  styleUrls: ['./dyna-form.component.scss']
})
export class DynaFormComponent implements OnInit, OnChanges {
  @Input() dynaInputList: DynamicInputForm<any>[];
  @Input() letReset: boolean;
  @Input() letSubmit: boolean;
  @Input() pushedInputValue: { key: string; value: string };
  @Output() changedInput: EventEmitter<{ key: string; value: string }>;
  @Output() submitedData: EventEmitter<JSON>;

  private options_notitications: any;

  private payLoad;
  public form: FormGroup;
  public showInputError: boolean;

  constructor(
    private _dynamicFormService: DynamicFormService,
    private _appService: AppService,
    private _toastrService: ToastrService,
  ) {
    this.changedInput = new EventEmitter<{ key: string; value: string }>();
    this.submitedData = new EventEmitter<JSON>();
    this.options_notitications = {
      tapToDismiss: true,
      closeButton: false,
      progressBar: true,
      positionClass: 'toast-top-right',
      rtl: this._appService.isRTL
    };
  }

  ngOnInit() { }

  /**
   * METODO PARA CREAR UN NUEVO FORMULARIO CON LOS DATOS PRE DEFINIDOS
   * AUTOR: FREDI ROMAN
   */
  private createForm() {
    this.showInputError = false;
    this.form = this._dynamicFormService.createFormGroup(this.dynaInputList);
  }

  /**
   * METODO PARA DETECTAR EL CAMBIO DE ALGUN INPUT DEL FORM VIEW Y ENVIARLO AL COMPONENTE PADRE
   * AUTOR: FREDI ROMAN
   * @param event VALOR DEL EVENT EMITTER
   */
  public getChangedInput(event: { key: string; value: string }) {
    this.changedInput.emit(event);
  }

  /**
   * METODO PARA EXTRAER LOS DATOS DEL FORMULARIO EN EL EVENTO DE SUBMIT
   * AUTOR: FREDI ROMAN
   */
  public onSubmit() {
    this.payLoad = JSON.stringify(this.form.value);
    this.payLoad = JSON.parse(this.payLoad);
    for (let property in this.payLoad) {
      if (this.payLoad[property].year) {
        this.payLoad[property] = this.payLoad[property]['year'] + '-' + (this.payLoad[property]['month'] < 10 ? '0' + this.payLoad[property]['month'] : this.payLoad[property]['month']) + '-' + (this.payLoad[property]['day'] < 10 ? '0' + this.payLoad[property]['day'] : this.payLoad[property]['day']);
      }
    }
    if (!this.form.valid) {
      this.showInputError = true;
      this._toastrService['error']("Existen atributos del empleado que son obligatorios", "Proceso Fallido", this.options_notitications);
    } else {
      this.submitedData.emit(this.payLoad);
    }
  }

  /**
   * METODO PARA DETECTAR LOS CAMBIOS DE UNA PROPIEDAD INYECTADA DESDE EL COMPONENTE PADRE DE ESTE COMPONENTE:
   * AUTOR: FREDI ROMAN
   * @param changes LOS CAMBIOS GENERADOS
   */
  ngOnChanges(changes: SimpleChanges) {
    for (const property in changes) {
      switch (property) {
        case 'dynaInputList':
          if (changes[property].currentValue) {
            this.createForm();
          }
          break;
        case 'letSubmit':
          if (changes[property].currentValue == true) {
            this.onSubmit();
          }
          break;
        case 'letReset':
          if (changes[property].currentValue == true) {
            this.createForm();
          }
          break;
      }
    }
  }
}
