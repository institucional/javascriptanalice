/**
 * AUTOR: FREDI ROMAN
 */
import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FunctionService } from '../../../services/functions/function.service';
import { INPUT_TYPES } from '../../data/input-types';
import { DynamicInputForm } from '../../models/dynamic-input-form';
import { SelectInput } from '../../models/select-input';
import { SwitchInputInterface } from '../../interfaces/switch-input.interface';
import { TimeInterface } from '../../interfaces/time.interface';
import { TimeInput } from '../../models/time-input';

@Component({
  selector: 'fr-dyna-input',
  templateUrl: './dynamic-form-input.component.html',
  styleUrls: ['./dynamic-form-input.component.scss']
})
export class DynamicFormInputComponent implements OnInit, OnChanges {
  @Input() formInput: DynamicInputForm<any>;
  @Input() parentForm: FormGroup;
  @Input() pushedInputValue: { key: string; value: string };
  @Input() showError: boolean;
  @Output() changedInput: EventEmitter<{ key: string; value: string }>;

  public inputTypes: any;

  constructor(
    private _functionService: FunctionService
  ) {
    this.inputTypes = INPUT_TYPES;
    this.changedInput = new EventEmitter<{ key: string; value: string }>();
  }

  ngOnInit() {
    if (this.formInput.inputData.controlType == INPUT_TYPES.subcatalogo) {
      if ((<SelectInput>this.formInput).url) {
        this._functionService.httpGetWithout((<SelectInput>this.formInput).url).then((respData: any) => {
          let catData: { label: string, value: any }[] = [];
          for (let data of respData.content) {
            catData.push({ label: data.nomSubCatalogo, value: data.id });
          }
          (<SelectInput>this.formInput).selectInputData.dataList = catData;
          this.parentForm.controls[this.formInput.inputData.key].setValue((<SelectInput>this.formInput).inputData.value ? (<SelectInput>this.formInput).inputData.value : catData[0] ? catData[0].value : null);
        });
      }
    }
  }

  /**
   * METODO PARA CAPTURAR EL VALOR DEL INPUT-BOOLEAN
   * AUTOR: FREDI ROMAN
   * @param event ID QUE VIENE POR EL OUTPUT EVENT EMITTER DEL COMPONENTE HIJO
   */
  public onSwitchInputChange(event: SwitchInputInterface) {
    this.parentForm.controls[this.formInput.inputData.key].setValue(event.checked);
  }

  /**
   * METODO PARA CAPTURAR EL VALOR DEL INPUT-TIME
   * AUTOR: FREDI ROMAN
   * @param event 
   */
  public getTimeChanges(event: TimeInterface) {
    this.parentForm.controls[this.formInput.inputData.key].setValue(event.hours + ":" + event.minutes + ":" + event.seconds);
  }

  /**
   * METODO PARA DETECTAR EL CAMBIO DE VALOR EN UN INPUT Y ENVIARLO AL COMPONENTE PADRE
   * AUTOR: FREDI ROMAN
   * @param event 
   */
  public onInputChange() {
    if (this.formInput.inputData.propagateChanges) {
      this.changedInput.emit({ key: this.formInput.inputData.key, value: this.parentForm.controls[this.formInput.inputData.key].value });
    }
  }

  /**
   * METODO PARA DEFINIR SI EL INPUT ES INVALIDO O NO:
   * AUTOR: FREDI ROMAN
   */
  get isInvalid() {
    let valid = this.parentForm.controls[this.formInput.inputData.key].valid;
    let touched = this.showError ? this.showError : this.parentForm.controls[this.formInput.inputData.key].touched;
    return this.formInput.inputData.required && touched && !valid;
  }

  /**
   * METODO PARA DETECTAR LOS CAMBIOS DE UNA PROPIEDAD INYECTADA DESDE EL COMPONENTE PADRE DE ESTE COMPONENTE:
   * AUTOR: FREDI ROMAN
   * @param changes LOS CAMBIOS GENERADOS
   */
  ngOnChanges(changes: SimpleChanges) {
    for (const property in changes) {
      changes[property].currentValue
      if (changes[property].currentValue) {
        if (property == 'pushedInputValue') {
          this.pushedInputValue = changes[property].currentValue;
          if (this.pushedInputValue.key == this.formInput.inputData.key) {
            this.parentForm.controls[this.formInput.inputData.key].setValue(this.pushedInputValue.value);
          }
        } else if (property == 'formInput') {
          switch (this.formInput.inputData.controlType) {
            case INPUT_TYPES.fecha:
              if (this.formInput.inputData.value) {
                this.formInput.inputData.value = this.formInput.inputData.value.split("-");
                this.formInput.inputData.value = { year: parseInt(this.formInput.inputData.value[0]), month: parseInt(this.formInput.inputData.value[1]), day: parseInt(this.formInput.inputData.value[2]) };
                this.parentForm.controls[this.formInput.inputData.key].setValue(this.formInput.inputData.value);
              }
              break;
            case INPUT_TYPES.hora:
              this.parentForm.controls[this.formInput.inputData.key].setValue((<TimeInput>this.formInput).timeInputData.hours + ":" + (<TimeInput>this.formInput).timeInputData.minutes + ":" + (<TimeInput>this.formInput).timeInputData.seconds);
              break;
            case INPUT_TYPES.booleano:
              this.parentForm.controls[this.formInput.inputData.key].setValue(this.formInput.inputData.value);
              break;
          }
        }
      }
    }
  }

}
