import { DynamicInputForm } from "./dynamic-input-form";
import { DateIn } from "../interfaces/date-input.interface";
import { GenericInput } from "../interfaces/generic-input.interface";
import { INPUT_TYPES } from "../data/input-types";

export class DateInput extends DynamicInputForm<string> {
    constructor(
        public inputData: GenericInput<string>,
        public dateInputData: DateIn,
    ) {
        super(inputData);
        inputData.controlType = INPUT_TYPES.fecha;
    }
}