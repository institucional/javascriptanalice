import { DynamicInputForm } from "./dynamic-input-form";
import { GenericInput } from "../interfaces/generic-input.interface";
import { TimeInterface } from "../interfaces/time.interface";
import { INPUT_TYPES } from "../data/input-types";

export class TimeInput extends DynamicInputForm<string> {
    constructor(
        public inputData: GenericInput<string>,
        public timeInputData: TimeInterface,
    ) {
        super(inputData);
        inputData.controlType = INPUT_TYPES.hora;
    }
}