import { DynamicInputForm } from "./dynamic-input-form";
import { GenericInput } from "../interfaces/generic-input.interface";
import { TextBoxInput } from "../interfaces/textbox-input.interface";
import { INPUT_TYPES } from "../data/input-types";

export class TextInput extends DynamicInputForm<string>{

    constructor(
        public inputData: GenericInput<string>,
        public textInputData: TextBoxInput
    ) {
        super(inputData);
        this.textInputData.readonly = !!this.textInputData.readonly;
        this.textInputData.pattern = this.textInputData.pattern || '';
        this.textInputData.type = this.textInputData.type || 'text';
        this.inputData.controlType = INPUT_TYPES.texto_corto;
    }
}