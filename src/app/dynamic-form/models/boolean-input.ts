import { DynamicInputForm } from "./dynamic-input-form";
import { GenericInput } from "../interfaces/generic-input.interface";
import { SwitchInputInterface } from "../interfaces/switch-input.interface";
import { INPUT_TYPES } from "../data/input-types";

export class BooleanInput extends DynamicInputForm<boolean> {
    constructor(
        public inputData: GenericInput<boolean>,
        public booleanInputData: SwitchInputInterface,
    ) {
        super(inputData);
        inputData.controlType = INPUT_TYPES.booleano;
    }
}