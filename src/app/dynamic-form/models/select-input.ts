import { DynamicInputForm } from "./dynamic-input-form";
import { GenericInput } from "../interfaces/generic-input.interface";
import { SelectInterface } from "../interfaces/select.interface";
import { INPUT_TYPES } from "../data/input-types";

export class SelectInput extends DynamicInputForm<string> {
    constructor(
        public inputData: GenericInput<string>,
        public selectInputData: SelectInterface,
        public url?: string
    ) {
        super(inputData);
        inputData.controlType = INPUT_TYPES.subcatalogo;
    }
}