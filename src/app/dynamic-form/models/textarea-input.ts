import { DynamicInputForm } from "./dynamic-input-form";
import { Textarea } from "../interfaces/textarea-input.interface";
import { GenericInput } from "../interfaces/generic-input.interface";
import { INPUT_TYPES } from "../data/input-types";

export class TextareaInput extends DynamicInputForm<string>{
    constructor(
        public inputData: GenericInput<string>,
        public textareaData: Textarea,
    ) {
        super(inputData);
        this.textareaData.cols = this.textareaData.cols || '30';
        this.textareaData.rows = this.textareaData.rows || '3';
        this.textareaData.pattern = this.textareaData.pattern || '';
        this.inputData.controlType = INPUT_TYPES.texto_largo;
    }
}