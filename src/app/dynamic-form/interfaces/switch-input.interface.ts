export interface SwitchInputInterface {
    id: string;
    label: string;
    checked: boolean;
    customClass: string;
    size: number;
    immutable?: boolean;
    disabled?: boolean;
}