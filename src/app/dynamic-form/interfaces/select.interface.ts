export interface SelectInterface {
    dataList: { label: string, value: any }[];
    value: any;
    selectedItem?: { label: string, value: any };
}