export interface TimeInterface {
    id: string;
    label: string;
    hours: string;
    minutes: string;
    seconds?: string;
    hideLabel?: boolean;
}