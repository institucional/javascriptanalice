import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicFormService } from './services/dynamic-form.service';
import { DynamicFormInputComponent } from './components/dynamic-form-input/dynamic-form-input.component';
import { DynaFormComponent } from './components/dyna-form/dyna-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FastTimePickerComponent } from './components/fast-time-picker/fast-time-picker.component';
import { SwitchInputComponent } from './components/switch-input/switch-input.component';
import { SwitchInputListComponent } from './components/switch-input-list/switch-input-list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ],
  exports: [
    DynaFormComponent,
    FastTimePickerComponent,
    SwitchInputComponent,
    SwitchInputListComponent
  ],
  declarations: [
    DynamicFormInputComponent,
    DynaFormComponent,
    FastTimePickerComponent,
    SwitchInputComponent,
    SwitchInputListComponent
  ],
  providers: [DynamicFormService]
})
export class DynamicFormModule { }
