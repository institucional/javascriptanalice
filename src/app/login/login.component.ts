import { Router } from '@angular/router';
import {Component, ViewEncapsulation} from '@angular/core';
import { AppService } from '../app.service';
import { AuthService } from '../services/auth/auth.service';
import * as textMaskAddons from 'text-mask-addons/dist/textMaskAddons';
import { TAGS } from '../TextLabels';
import {FunctionService} from '../services/functions/function.service';
import {SpinnerDialogService} from '../components/spinner-dialog/spinner-dialog.service';
import { ModalService } from '../services/modal-service/modal.service';
import terminosCondiciones from '../data/terminos-condiciones';
import { BODY_TYPES } from '../data/modal-body-type';
import {GV} from '../GeneralVars';
import {ToastrService} from 'ngx-toastr';
import {WS} from '../WebServices';
import {HttpClient} from '@angular/common/http';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {PerfilService} from '../components/perfil/perfil.service';

const BASE_NAME = 'LoginComponent';

@Component({
  templateUrl: './login.component.html',
  styleUrls: [
    '../../vendor/styles/pages/authentication.scss',
    '../../vendor/libs/ngx-toastr/ngx-toastr.scss'
  ],
  encapsulation: ViewEncapsulation.None
})

export class LoginComponent {
  labels = null;
  ipAddress = null;
  tiposIdentificacion = [];
  intTIpoID = 0;
  options_notitications = {
    tapToDismiss: true,
    closeButton: false,
    progressBar: true,
    positionClass: 'toast-top-right',
    rtl: this.appService.isRTL
  };
  emailMaskOptions = {
    mask: textMaskAddons.emailMask
  };
  mask = ['(', /[0-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  loginCredentials = {
    email: '',
    password: '',
    rememberMe: false
  };
  registerCredentials = {
    name: '',
    lastName: '',
    email: '',
    password: '',
    cell: '',
    aceptTerms: false,
    tipoIdentificacion: '',
    identificacion: ''
  };

  modalRef = null;

  constructor(
    private appService: AppService,
    private authService: AuthService,
    private functionService: FunctionService,
    private router: Router,
    private spinner: SpinnerDialogService,
    public toastrService: ToastrService,
    private _modalService: ModalService,
    private modalService: NgbModal,
    private http: HttpClient,
    private perfilService: PerfilService
  ) {
    this.labels = TAGS;
    this.appService.pageTitle = this.labels.general.welcome;
    functionService.getIpAddress().then((res) => {
      this.ipAddress = res;
    });
  }

  modalOlvido = {
    title: 'TITULO MODAL',
    button: '',
    label: '',
    bandera: false,
    mensaje: '',
    param: 0
  };

  celularParam = '';
  correoParam = '';

  public alerts: Array<IAlert> = [];
  public closeAlert(alert: IAlert) {
    const index: number = this.alerts.indexOf(alert);
    this.alerts.splice(index, 1);
  }

  openPerfil() {
    this.perfilService.confirm('VALIDACIONES')
      .then((result) => {
        console.log(result);
        localStorage.removeItem('userToken');
        localStorage.removeItem('dataSesion');
        this.loginCredentials.email = '';
        this.loginCredentials.password = '';
      }).catch(() => {
      console.log('Dismissed.');
      localStorage.removeItem('userToken');
      localStorage.removeItem('dataSesion');
      this.loginCredentials.email = '';
      this.loginCredentials.password = '';
    });
  }

  openModal(content, param) {
    this.modalOlvido.param = param;
    if (param == 0) {
      this.modalOlvido.title = 'RECUPERAR CORREO ELECTRONICO';
      this.modalOlvido.button = 'RECUPERAR CORREO';
      this.modalOlvido.label = 'Ingrese su Celular';
      this.modalOlvido.bandera = false;
      this.modalOlvido.mensaje = 'Por seguridad su correo electrónico se enviará a su número de celular registrado dentro del sistema.';
    } else {
      this.modalOlvido.title = 'RECUPERAR CONTRASEÑA';
      this.modalOlvido.button = 'RECUPERAR CONTRASEÑA';
      this.modalOlvido.label = 'Ingrese su Correo';
      this.modalOlvido.bandera = true;
      this.modalOlvido.mensaje = 'Para recuperar su contraseña realice los pasos enviados a su correo electrónico registrado dentro del sistema.';
    }
    this.modalRef = this.modalService.open(content);
  }

  closeModal() {
    this.modalRef.close();
  }

  getUserPass() {
    let url = '';
    if (this.modalOlvido.param == 0) {
      if (this.celularParam == '') {
        this.toastrService['error'](this.labels.login.errorCell, this.appService.pageTitle, this.options_notitications);
        return;
      }
      this.celularParam = this.celularParam.replace('(', '');
      this.celularParam = this.celularParam.replace(')', '');
      this.celularParam = this.celularParam.replace('-', '');
      this.celularParam = this.celularParam.replace(' ', '');
      url = WS.login.getCorreo;
      url = url.replace('${celular}', this.celularParam);
      this.functionService.httpGet(url).then((response) => {
        if (response['status'] == 0) {
          this.toastrService['success'](response['message'], this.appService.pageTitle, this.options_notitications);
          this.modalRef.close();
        } else {
          this.toastrService['error'](response['message'], this.appService.pageTitle, this.options_notitications);
        }
      });
    } else {
      if (this.correoParam == '') {
        this.toastrService['error'](this.labels.login.errorMail, this.appService.pageTitle, this.options_notitications);
        return;
      }
      url = WS.login.getPassword;
      url = url.replace('${correo}', this.correoParam);
      this.spinner.openSpinner();
      this.http.get(url).subscribe(
        res => {
          this.spinner.closeSpinner();
          if (res['status'] == 0) {
            this.toastrService['success'](res['message'], this.appService.pageTitle, this.options_notitications);
            this.modalRef.close();
          } else {
            this.toastrService['error'](res['message'], this.appService.pageTitle, this.options_notitications);
          }
        },
        err => {
          this.spinner.closeSpinner();
          this.toastrService['error'](TAGS.login.mailNotRegister, this.appService.pageTitle, this.options_notitications);
        }
      );
    }
  }

  /**
   * METODO PARA ABRIR UN NUEVO MODAL PARA MOSTRAR LOS TERMINOS Y CONDICIONES:
   * @param event
   */
  public openTermsModal(event: any) {
    event.preventDefault();
    this._modalService.openModal({
      baseComponentId: BASE_NAME,
      icon: '',
      title: TAGS.terminos.title,
      componentType: BODY_TYPES.html,
      body: null,
      bodyData: terminosCondiciones.terminos,
      bodyClass: 'fr-terms-font-size',
      btnList: [
        {
          icon: '',
          label: TAGS.terminos.leftModalBtn,
          btnClass: 'btn-primary',
          onClickAction: 1
        },
        {
          icon: '',
          label: TAGS.terminos.rightModalBtn,
          btnClass: 'btn-secondary',
          onClickAction: 2
        }]
    });
  }

  onChangeDate(event) {
    this.registerCredentials.tipoIdentificacion = event.target.value;
    for (let i = 0; i < this.tiposIdentificacion.length; i++) {
      if (event.target.value.toString() === this.tiposIdentificacion[i].id.toString()) {
        switch (this.tiposIdentificacion[i].nomSubCatalogo.toUpperCase()) {
          case GV.tiposIdentificacion.cedula:
            this.intTIpoID = 0;
            break;
          case GV.tiposIdentificacion.ruc:
            this.intTIpoID = 1;
            break;
          default:
            this.intTIpoID = 2;
        }
      }
    }
  }

  clear() {
    this.registerCredentials.name = '';
    this.registerCredentials.lastName = '';
    this.registerCredentials.email = '';
    this.registerCredentials.password = '';
    this.registerCredentials.cell = '';
    this.registerCredentials.tipoIdentificacion = '';
    this.registerCredentials.identificacion = '';
    this.registerCredentials.tipoIdentificacion = this.tiposIdentificacion[0].id;
  }

  loginUser() {
    if (this.loginCredentials.email.length < 5) {
      this.alerts.push({
        id: 1,
        type: 'dark-danger',
        message: this.labels.messages.insertMail
      });
    } else if (this.loginCredentials.password.length < 5) {
      this.alerts.push({
        id: 1,
        type: 'dark-danger',
        message: this.labels.messages.insertPassword
      });
    } else {
      this.spinner.openSpinner();
      this.authService.login(
        this.loginCredentials.email,
        this.loginCredentials.password,
        this.loginCredentials.rememberMe
        ).subscribe(response => {
            this.spinner.closeSpinner();
            const data = response.json();
            try {
              const decode = this.functionService.decode(data['access_token']);
              if (decode) {
                // localStorage.setItem('dataSesion', JSON.stringify(decode));
                localStorage.setItem('userToken', data['access_token']);
                this.getCarteraVencida();
                this.getValidations();
              }
            } catch (e) {
              this.router.navigate(['/login']);
              this.alerts.push({
                id: 1,
                type: 'dark-danger',
                message: this.labels.messages.initError
              });
            }
          }, error => {
            if (error['status'].toString() === '504') {
              this.spinner.closeSpinner();
              this.alerts.push({
                id: 1,
                type: 'dark-danger',
                message: this.labels.messages['504']
              });
            } else {
              this.spinner.closeSpinner();
              this.alerts.push({
                id: 1,
                type: 'dark-danger',
                message: this.labels.messages.invalidCredentials
              });
            }
          });
    }
  }

  registerUser() {
    if (this.registerCredentials.name.length < 5) {
      this.alerts.push({
        id: 1,
        type: 'dark-danger',
        message: this.labels.messages.nameRequired + this.labels.messages.minimoReq5
      });
    } else if (this.registerCredentials.lastName.length < 5) {
      this.alerts.push({
        id: 1,
        type: 'dark-danger',
        message: this.labels.messages.surnameRequired + this.labels.messages.minimoReq5
      });
    } else if (this.registerCredentials.email.length < 5) {
      this.alerts.push({
        id: 1,
        type: 'dark-danger',
        message: this.labels.messages.emailRequired + this.labels.messages.minimoReq5
      });
    } else if (this.registerCredentials.password.length < 7) {
      this.alerts.push({
        id: 1,
        type: 'dark-danger',
        message: this.labels.messages.passwordRequired + this.labels.messages.minimoReq7
      });
    } else if (this.registerCredentials.cell.length < 10) {
      this.alerts.push({
        id: 1,
        type: 'dark-danger',
        message: this.labels.messages.cellRequired + this.labels.messages.minimoReq10
      });
    } else if (this.registerCredentials.identificacion.length < 10) {
      this.alerts.push({
        id: 1,
        type: 'dark-danger',
        message: 'Identificación requerida, ' + this.labels.messages.minimoReq10
      });
    } else {
      switch (this.intTIpoID) {
        case 0:
          if (!this.functionService.validateIdentificationCard(this.registerCredentials.identificacion, 0)) {
            this.toastrService['error'](this.labels.messages.invalidCardId, this.appService.pageTitle, this.options_notitications);
            return;
          }
          break;
        case 1:
          if (!this.functionService.validateIdentificationCard(this.registerCredentials.identificacion, 1)) {
            this.toastrService['error'](this.labels.messages.invalidRuc, this.appService.pageTitle, this.options_notitications);
            return;
          }
          break;
      }
      this.spinner.openSpinner();
      this.authService.register(
        this.registerCredentials.name,
        this.registerCredentials.lastName,
        this.registerCredentials.email,
        this.registerCredentials.password,
        this.registerCredentials.cell,
        this.ipAddress.toString(),
        this.functionService.getToday().toString(),
        this.registerCredentials.tipoIdentificacion,
        this.registerCredentials.identificacion
      ).subscribe(response => {
        this.spinner.closeSpinner();
        if (response.status === 200) {
          this.alerts.push({
            id: 1,
            type: 'dark-success',
            message: this.labels.login.verification
          });
        } else {
          this.alerts.push({
            id: 1,
            type: 'dark-error',
            message: this.labels.login.verificationErr
          });
        }
        this.clear();
      }, error => {
        if (error.status === 400) {
          this.alerts.push({
            id: 1,
            type: 'dark-danger',
            message: this.labels.messages.existUsr
          });
        } else {
          const data = error.json();
          this.alerts.push({
            id: 1,
            type: 'dark-danger',
            message: this.labels.messages.initError
          });
        }
        this.spinner.closeSpinner();
        this.clear();
      });
    }
  }

  getValidations() {
    let dataSesion = localStorage.getItem('userToken');
    dataSesion = this.functionService.decode(dataSesion);
    if (dataSesion['valid_cell'] != 1 || dataSesion['valid_email'] != 1 || dataSesion['acept_terms'] != 1 ||
      (dataSesion['identification'] == null || dataSesion['identification'] == '')) {
      this.openPerfil();
    } else {
      this.router.navigate(['/fastmovil/orderfastmovil']);
      // this.router.navigate(['/admin/company']);
    }
  }

  getCarteraVencida() {
    let dataSesion = localStorage.getItem('userToken');
    dataSesion = this.functionService.decode(dataSesion);
    let urlCartera = WS.login.carteraVencida;
    urlCartera = urlCartera.replace('${celular}', dataSesion['cell_phone']);
    this.http.get(urlCartera).subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.log(err);
      }
    );
  }

  ngOnInit() {
    if (localStorage.getItem('userToken')) {
      this.getCarteraVencida();
      this.getValidations();
    } else {
      const array = [WS.employees.identTypesLogin];
      this.functionService.httpObservableGet(array).then((responseList) => {
        this.tiposIdentificacion = responseList[0].content;
        this.clear();
      });
    }
  }
}

export interface IAlert {
  id: number;
  type: string;
  message: string;
}
export interface IResponse {
  status: boolean;
  message: string;
}

