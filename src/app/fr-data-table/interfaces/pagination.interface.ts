export interface Pagination {
    pageNumber?: number;
    active?: boolean
    disabled?: boolean
    firstPageBtn?: boolean;
    lastPageBtn?: boolean;
    nextBtn?: boolean;
    prevBtn?: boolean;
}