export interface ColumnInterface {
    label: string,
    columnWidth: number
}