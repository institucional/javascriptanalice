import { ColumnInterface } from "./column.interface";
import { FooterInterface } from "./footer.interface";

export interface TableDataInterface {
    title: string;
    tableHeaders?: ColumnInterface[];
    tableRows?: string[][];
    tableFooter?: FooterInterface[];
    pageSize?: number
}