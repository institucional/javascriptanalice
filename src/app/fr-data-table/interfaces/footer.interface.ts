export interface FooterInterface {
    label: string;
    colspan?: string;
}