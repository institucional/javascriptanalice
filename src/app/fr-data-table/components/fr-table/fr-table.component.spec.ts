import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrTableComponent } from './fr-table.component';

describe('FrTableComponent', () => {
  let component: FrTableComponent;
  let fixture: ComponentFixture<FrTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
