import { Component, OnInit, Input, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { ColumnInterface } from '../../interfaces/column.interface';
import { FooterInterface } from '../../interfaces/footer.interface';
import { Pagination } from '../../interfaces/pagination.interface';
import { Page } from '../../../components/pagination/page';

const PAGE_LIMIT = 5;

@Component({
  selector: 'fr-table',
  templateUrl: './fr-table.component.html',
  styleUrls: ['./fr-table.component.scss']
})
export class FrTableComponent implements OnInit, OnChanges {
  @Input() tableHeaders: ColumnInterface[];
  @Input() tableRows: string[][];
  @Input() tableFooter: FooterInterface[];
  @Input() pageSize: number;
  @Output() onRowClick: EventEmitter<{ row: string[], index: number }>;
  public filterTableRows: string[][];

  public page: Page;
  public paginationArray: Pagination[];
  public loadingIndicator: boolean;

  constructor() {
    this.page = new Page();
    this.loadingIndicator = true;
    this.onRowClick = new EventEmitter<{ row: string[], index: number }>();
  }

  ngOnInit() {
  }

  /**
   * METODO PARA DETECTAR EL CLICK EN UNA FILA DE LA TABLA
   * AUTOR: FREDI ROMAN
   */
  public clickedRow(tableRow: string[]) {
    let index = this.tableRows.findIndex(tr => tr == tableRow);
    this.onRowClick.emit({ index: index, row: tableRow });
  }

  /**
   * METODO PARA HABILITAR O DESHABILITAR LOS BOTONES DE NAVEGACION DE LA PAGINACION
   * AUTOR: FREDI ROMAN
   */
  private handlePaginationControls(currentPageIndex: number) {
    if (currentPageIndex >= 3) {
      this.paginationArray[0].disabled = false;
      this.paginationArray[1].disabled = false;
    } else {
      this.paginationArray[0].disabled = true;
      this.paginationArray[1].disabled = true;
    }

    if (currentPageIndex <= this.paginationArray.length - 4) {
      this.paginationArray[this.paginationArray.length - 1].disabled = false;
      this.paginationArray[this.paginationArray.length - 2].disabled = false;
    } else {
      this.paginationArray[this.paginationArray.length - 1].disabled = true;
      this.paginationArray[this.paginationArray.length - 2].disabled = true;
    }
  }

  /**
   * METODO PARA NAVEGAR EN LA PAGINACION PARA IR A LA PRIMERA O A LA ULTIMA PAGINA
   * AUTOR: FREDI ROMAN
   */
  private goFirstLast(last: boolean = false) {
    let maxPages, pagCount = 1;
    if (last) {
      maxPages = Math.ceil(this.tableRows.length / this.page.size);
      if (maxPages > PAGE_LIMIT) {
        pagCount = maxPages - PAGE_LIMIT + 1;
      }
    }

    for (let page of this.paginationArray) {
      if (page.pageNumber) {
        page.pageNumber = pagCount;
        pagCount++;
      }
    }
  }

  /**
   * METODO PARA REDEFINIR LA PAGINACION, DEPENDIENDO DE SI EXISTEN MAS DE 5 PAGINAS
   * AUTOR: FREDI ROMAN
   */
  private reDefinePagination(currentPageIndex: number) {
    if (currentPageIndex == this.paginationArray.length - 3) {
      if (this.tableRows.length - this.paginationArray[currentPageIndex].pageNumber * this.page.size > 0) {
        this.paginationArray.splice(this.paginationArray.length - 2, 0, { pageNumber: this.paginationArray[currentPageIndex].pageNumber + 1, active: false });
        this.paginationArray.splice(2, 1);
        if (this.tableRows.length - this.paginationArray[currentPageIndex].pageNumber * this.page.size > 0) {
          this.paginationArray.splice(this.paginationArray.length - 2, 0, { pageNumber: this.paginationArray[currentPageIndex].pageNumber + 1, active: false });
          this.paginationArray.splice(2, 1);
        }
        this.paginationArray[0].disabled = false;
        this.paginationArray[1].disabled = false;
        this.paginationArray[this.paginationArray.length - 1].disabled = false;
        this.paginationArray[this.paginationArray.length - 2].disabled = false;
      } else {
        this.handlePaginationControls(currentPageIndex);
      }
    } else if (currentPageIndex == 2) {
      if (this.paginationArray[2].pageNumber > 1) {
        this.paginationArray.splice(2, 0, { pageNumber: this.paginationArray[2].pageNumber - 1, active: false });
        this.paginationArray.splice(this.paginationArray.length - 3, 1);
        if (this.paginationArray[2].pageNumber > 1) {
          this.paginationArray.splice(2, 0, { pageNumber: this.paginationArray[2].pageNumber - 1, active: false });
          this.paginationArray.splice(this.paginationArray.length - 3, 1);
        }
        this.paginationArray[0].disabled = false;
        this.paginationArray[1].disabled = false;
        this.paginationArray[this.paginationArray.length - 1].disabled = false;
        this.paginationArray[this.paginationArray.length - 2].disabled = false;
      } else {
        this.handlePaginationControls(currentPageIndex);
      }
    } else {
      this.handlePaginationControls(currentPageIndex);
    }
  }

  /**
   * METODO PARA CAMBIAR DE PAGINA EN LA TABLA
   * AUTOR: FREDI ROMAN
   * @param event 
   */
  public setPage(event: any, index: number) {
    event.preventDefault();
    if (!this.paginationArray[index].disabled) {
      if (!this.paginationArray[index].pageNumber) {
        if (this.paginationArray[index].firstPageBtn) {
          index = 2;
          this.goFirstLast();
        } else if (this.paginationArray[index].lastPageBtn) {
          index = this.paginationArray.length - 3;
          this.goFirstLast(true);
        } else if (this.paginationArray[index].nextBtn) {
          index = this.paginationArray.findIndex(pag => pag.pageNumber == this.page.pageNumber + 1);
        } else if (this.paginationArray[index].prevBtn) {
          index = this.paginationArray.findIndex(pag => pag.pageNumber == this.page.pageNumber - 1);
        }
      }

      if (this.page.pageNumber != this.paginationArray[index].pageNumber) {
        this.loadingIndicator = true;

        for (let i = 0; i < this.paginationArray.length; i++) {
          if (i == index) {
            this.paginationArray[i].active = true;
          } else {
            this.paginationArray[i].active = false;
          }
        }
        this.page.pageNumber = this.paginationArray[index].pageNumber;
        this.reDefinePagination(index);
        setTimeout(() => {
          this.loadingIndicator = false;
          this.setTableOffset();
        }, 300);
      }
    }
  }

  /**
   * METODO PARA ESTABLECER EL OFFSET PARA LA PAGINACION DE LA TABLA
   * AUTOR: FREDI ROMAN
   */
  private setTableOffset() {
    let indexInit: number;
    if (this.page.pageNumber - 1 == 0) {
      this.filterTableRows = this.tableRows.slice(0, this.page.size);
    } else {
      indexInit = (this.page.pageNumber - 1) * this.page.size;
      this.filterTableRows = this.tableRows.slice(indexInit, indexInit + this.page.size);
    }
  }

  /**
   * METODO PARA DEFINIR LA LISTA DE LOS BOTONES DE PAGINACION
   * AUTOR: FREDI ROMAN
   */
  private definePagination() {
    let counter = 1;
    let pageNumber = 1;
    this.paginationArray = [
      { firstPageBtn: true, disabled: true },
      { prevBtn: true, disabled: true }
    ];

    for (let i = 0; i < this.tableRows.length; i++) {
      if (counter == this.page.size) {
        counter = 0;
        this.paginationArray.push({ pageNumber: pageNumber, active: pageNumber == 1 ? true : false });
        pageNumber++;
      } else if (this.tableRows.length - (pageNumber - 1) * this.page.size > 0 && this.tableRows.length - (pageNumber - 1) * this.page.size <= this.page.size) {
        this.paginationArray.push({ pageNumber: pageNumber, active: pageNumber == 1 ? true : false });
        i = this.tableRows.length;
      }
      if ((pageNumber - 1) == PAGE_LIMIT) {
        i = this.tableRows.length;
      }
      counter++
    }
    this.paginationArray.push({ nextBtn: true, disabled: this.paginationArray.length - 2 == 1 ? true : false });
    this.paginationArray.push({ lastPageBtn: true, disabled: this.paginationArray.length - 3 == 1 ? true : false });
  }

  /**
   * METODO PARA DETECTAR LOS CAMBIOS EN LOS PARAMETROS INPUT
   * AUTOR: FREDI ROMAN
   */
  ngOnChanges(changes: SimpleChanges) {
    for (let property in changes) {
      if (changes[property].currentValue) {
        if (property == 'tableRows') {
          this.tableRows = changes[property].currentValue;
          this.page.size = this.pageSize;
          this.filterTableRows = this.tableRows.slice(0, this.pageSize);
          this.page.totalElements = this.tableRows.length;
          this.loadingIndicator = true;
          this.page.pageNumber = 1;

          if (this.page.totalElements > 0) {
            setTimeout(() => {
              this.loadingIndicator = false;
              this.definePagination();
            }, 300);
          } else {
            this.loadingIndicator = false;
            this.paginationArray = [];
          }
        } else if (property == 'pageSize') {
          this.page.size = this.pageSize;
        }
      }
    }
  }
}
