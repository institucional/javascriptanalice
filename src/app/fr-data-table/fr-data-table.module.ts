import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FrTableComponent } from './components/fr-table/fr-table.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [FrTableComponent],
  exports: [FrTableComponent]
})
export class FrDataTableModule { }
