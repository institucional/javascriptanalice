import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AppService } from '../app.service';
import * as textMaskAddons from 'text-mask-addons/dist/textMaskAddons';

@Component({
  selector: 'app-validate',
  templateUrl: './validate.component.html',
  styleUrls: [
    '../../vendor/styles/pages/authentication.scss'
  ]
})
export class ValidateComponent {
  constructor(
    private appService: AppService,
    private activatedRoute: ActivatedRoute,
    private http: Http) {
    this.appService.pageTitle = 'Validación de Usuario - Fastadmin';
    this.activatedRoute.queryParams.subscribe(params => {
            this.user.key = params['key'];
            this.user.username = params['username'];
            this.user.name = params['name'];
    });
  }

  public alerts: Array<IAlert> = [];

  public closeAlert(alert: IAlert) {
    const index: number = this.alerts.indexOf(alert);
    this.alerts.splice(index, 1);
  }

  phoneMaskOptions = {
    mask: [/[0]/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]
  };

  codeMaskOptions = {
    mask: [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/]
  };

  user = {
    name: 'Ususario',
    username: '',
    sms: false,
    phone: '',
    key: '',
    code: ''
  };

  sendSms() {
    let url = '/api/user/sms/validation';
    this.http.post(url, {
      username:this.user.username,
      phone:this.user.phone,
      key:this.user.key
    }).subscribe(response => {
      let data = response.json();
      this.alerts.push({
        id: 1,
        type: 'primary',
        message: data.message
      });
      this.user.sms = true;
    }, error => {
        let data = error.json();
        this.alerts.push({
        id: 1,
        type: 'dark-danger',
        message: data.message
      });
    });
  }

  activateUser() {
    let url = '/api/user/activate';
    this.http.post(url, {
      username:this.user.username,
      phone:this.user.phone,
      key:this.user.key,
      code:this.user.code
    }).subscribe(response => {
      let data = response.json();
      this.alerts.push({
        id: 1,
        type: 'dark-success',
        message: data.message
      });
      this.user.sms = true;
    }, error => {
        let data = error.json();
        this.alerts.push({
        id: 1,
        type: 'dark-danger',
        message: data.message
      });
    });
  }
}

export interface IAlert {
  id: number;
  type: string;
  message: string;
}