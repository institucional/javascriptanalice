export const environment = {
  production: true,
  socketHost: 'https://admin.fastline.tech:8443/socket',
  mapbox: {
    accessToken: 'pk.eyJ1IjoiYXF1aW50YW5hciIsImEiOiJjam5ldm56MjgwcWwzM3FueWV5aGt2ZXhvIn0.EF7ZnmtFwoWcrBx0SsPJZw'
  }
};
