// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  socketHost: 'http://localhost:8080/socket',
  mapbox: {
    accessToken: 'pk.eyJ1IjoiYXF1aW50YW5hciIsImEiOiJjam5ldm56MjgwcWwzM3FueWV5aGt2ZXhvIn0.EF7ZnmtFwoWcrBx0SsPJZw'
  }
};
